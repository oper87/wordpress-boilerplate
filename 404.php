<?php
/**
 * The template for displaying 404 page
 */

//header( "HTTP/1.1 301 Moved Permanently" );
//header( "Location: " . get_bloginfo( 'url' ) );
//exit();

if ( ! THEME_AJAX ) {
	get_header();
} else {
	\App\Functions::ajax_header_response();
}

?>

    <main id="main" class="site-main main-404">
        <div class="container">
            <h1><?php _e( 'Page not found', 'ruby_studio' ); ?></h1>
            <a href="<?php echo get_home_url(); ?>" class="button"><?php _e( 'Go to homepage', 'ruby_studio' ); ?></a>
        </div>
    </main>

<?php
if ( ! THEME_AJAX ) {
	get_footer();
}