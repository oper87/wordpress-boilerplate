(function ($) {

    /**
     *  initialize_field
     *
     *  This function will initialize the $field.
     *
     *  @date    30/11/17
     *  @since    5.6.5
     *
     *  @param    n/a
     *  @return    n/a
     */
    var dateFormat = "yy/mm/dd";

    function initialize_field($field, props) {
        // defaults
        props = acf.parseArgs(props, {
            allowNull: false,
            placeholder: '',
            multiple: false,
            field: $field.get(0),
            ajax: false,
            ajaxAction: 'acf/fields/product_object/query',
            ajaxData: function (data) {
                return data;
            },
            ajaxResults: function (json) {
                return json;
            },
        });

        var $select = $field.find('select');

        selectField.init($select, props);
    }

    var selectField = {

        init: function ($select, props) {
            var self = this;

            this.$select = $select;
            this._select = $select.get(0);
            this.props = Object.assign(props, this._select.dataset);

            var options = {
                width: '100%',
                allowClear: this.props.allowNull,
                placeholder: this.props.placeholder,
                multiple: this.props.multiple,
                data: [],
                escapeMarkup: function (string) {
                    return string;
                },
                templateResult: function (product) {
                    return self.templateProduct(product);
                },
                templateSelection: function (product) {
                    return self.templateProduct(product);
                }
            };

            // multiple
            if (options.multiple) {
                // reorder options
                this.getValue().map(function( item ){
                    item.$el.detach().appendTo( $select );
                });
            }

            // Temporarily remove conflicting attribute.
            var attrAjax = $select.attr('data-ajax');
            if (attrAjax !== undefined) {
                $select.removeData('ajax');
                $select.removeAttr('data-ajax');
            }

            // ajax
            if (this.get('ajax')) {
                options.ajax = {
                    url: acf.get('ajaxurl'),
                    delay: 250,
                    dataType: 'json',
                    type: 'post',
                    cache: false,
                    data: $.proxy(this.getAjaxData, this),
                    processResults: $.proxy(this.processAjaxResults, this),
                };
            }

            // add select2
            $select.select2(options);

            // get container (Select2 v4 does not return this from constructor)
            var $container = $select.next('.select2-container');

            // multiple
            if( options.multiple ) {

                // vars
                var $ul = $container.find('ul');

                // sortable
                $ul.sortable({
                    stop: function( e ) {

                        // loop
                        $ul.find('.select2-selection__choice').each(function() {

                            // vars
                            var $option = $( $(this).data('data').element );

                            // detach and re-append to end
                            $option.detach().appendTo( $select );
                        });

                        // trigger change on input (JS error if trigger on select)
                        $select.trigger('change');
                    }
                });

                // on select, move to end
                $select.on('select2:select', $.proxy(function( e ){
                    self.$select.find('option[value="' + e.params.data.id + '"]').detach().appendTo( self.$select );
                }));
            }

            // add class
            $container.addClass('-acf wc-product-object-acf');

            // Add back temporarily removed attr.
            if (attrAjax !== undefined) {
                $select.attr('data-ajax', attrAjax);
            }

        },

        get: function (name) {
            return this.props[name];
        },

        getValue: function(){

            // vars
            var val = [];
            var $options = this.$select.find('option:selected');

            // bail early if no selected
            if( !$options.exists() ) {
                return val;
            }

            // sort by attribute
            $options = $options.sort(function(a, b) {
                return +a.getAttribute('data-i') - +b.getAttribute('data-i');
            });

            // loop
            $options.each(function(){
                var $el = $(this);
                val.push({
                    $el:	$el,
                    id:		$el.attr('value'),
                    text:	$el.text(),
                });
            });

            // return
            return val;

        },

        getAjaxData: function (params) {

            // vars
            var ajaxData = {
                action: this.get('ajaxAction'),
                s: params.term || '',
                paged: params.page || 1
            };

            // field helper
            var field = this.get('field');
            if (field) {
                ajaxData.field_key = field.dataset.key;
            }

            // callback
            var callback = this.get('ajaxData');
            if (callback) {
                ajaxData = callback.apply(this, [ajaxData, params]);
            }

            // return
            return acf.prepareForAjax(ajaxData);
        },

        getAjaxResults: function (json, params) {

            // defaults
            json = acf.parseArgs(json, {
                results: false,
                more: false,
            });

            // callback
            var callback = this.get('ajaxResults');
            if (callback) {
                json = callback.apply(this, [json, params]);
            }

            // filter
            json = acf.applyFilters('select2_ajax_results', json, params, this);

            // return
            return json;
        },

        processAjaxResults: function (json, params) {

            // vars
            var json = this.getAjaxResults(json, params);

            // change more to pagination
            if (json.more) {
                json.pagination = {more: true};
            }

            // merge together groups
            setTimeout($.proxy(this.mergeOptions, this), 1);

            // return
            return json;
        },

        templateProduct: function (product) {
            if (!product.id) {
                return product.text;
            }

            var productData = product.text.split('|');

            var textName = '<span>' + productData[0] + '</span>';

            if (productData[1]) {
                textName = '<img width="25" height="40" src="' + productData[1] + '">' + textName;
            }

            return '<span class="wc-product-item">' + textName + '</span>';
        },

        escapeHtml: function (unsafe) {
            return unsafe
                .replace(/&/g, "&amp;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#039;");
        }
    }

    if (typeof acf.add_action !== 'undefined') {

        /*
        *  ready & append (ACF5)
        *
        *  These two events are called when a field element is ready for initizliation.
        *  - ready: on page load similar to $(document).ready()
        *  - append: on new DOM elements appended via repeater field or other AJAX calls
        *
        *  @param	n/a
        *  @return	n/a
        */

        acf.add_action('ready_field/type=wc_product_object', initialize_field);
        acf.add_action('append_field/type=wc_product_object', initialize_field);

    } else {

        /*
        *  acf/setup_fields (ACF4)
        *
        *  These single event is called when a field element is ready for initizliation.
        *
        *  @param	event		an event object. This can be ignored
        *  @param	element		An element which contains the new HTML
        *  @return	n/a
        */

        $(document).on('acf/setup_fields', function (e, postbox) {

            // find all relevant fields
            $(postbox).find('.acf-field.acf-field-wc-product-object').each(function () {
                // initialize
                initialize_field($(this));
            });

        });

    }

})(jQuery);