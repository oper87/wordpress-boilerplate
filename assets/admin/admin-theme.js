(function ($) {
    'use strict';

    if (window.ClipboardJS) {
        var clipboard = new ClipboardJS('.button-clipboard', {
            text: function (trigger) {
                var rowId = '',
                    shortcode = trigger.dataset.shortcode;

                if (trigger.closest('.acf-row')) {
                    rowId = trigger.closest('.acf-row').querySelector('.acf-row-number').innerText;
                    rowId = parseInt(rowId);

                    shortcode = shortcode.replace('X', rowId);
                }

                return shortcode;
            }
        });

        clipboard.on('success', function (e) {
            var $el = $(e.trigger);

            $el.addClass('copied');

            setTimeout(function () {
                $el.removeClass('copied');
            }, 1000);

            e.clearSelection();
        });
    }

    var users = {
        init: function () {

        }
    };

    users.init();
}(jQuery));