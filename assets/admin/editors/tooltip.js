(function (tinymce) {
    tinymce.ui.Factory.add('AppTooltipPreview', tinymce.ui.Control.extend({
        text: 'fasd',
        renderHtml: function () {
            return (
                '<div id="' + this._id + '" class="app-tooltip-preview">' +
                '<span>' + this.text + '</stan>' +
                '</div>'
            );
        },
        setText: function (text) {
            var index, lastIndex;

            if (this.text !== text) {
                this.text = text;

                text = this.text;
                //text = text.replace(/\<br\>/g, "\n");
                //text = _.unescape(this.text);

                tinymce.$(this.getEl().firstChild).html(text);
            }
        }
    }));

    tinymce.ui.Factory.add('AppTooltipInput', tinymce.ui.Control.extend({
        renderHtml: function () {
            return (
                '<div id="' + this._id + '" class="app-tooltip-input">' +
                '<textarea placeholder="' + tinymce.translate('Paste tooltip content') + '"></textarea>' +
                '<input type="text" style="display:none" value="" />' +
                '</div>'
            );
        },
        setText: function (text) {
            text = _.unescape(text)
            text = text.replace(/\<br\>/g, "\n");

            this.getEl().firstChild.value = text;
        },
        getText: function () {
            var text = tinymce.trim(this.getEl().firstChild.value);

            return text;
        },
        getTooltipText: function () {
            var text = this.getEl().firstChild.nextSibling.value;

            if (!tinymce.trim(text)) {
                return '';
            }

            return text.replace(/[\r\n\t ]+/g, ' ');
        },
        reset: function () {
            var urlInput = this.getEl().firstChild;

            urlInput.value = '';
            urlInput.nextSibling.value = '';
        }
    }));

    tinymce.PluginManager.add('app_tooltip_button', function (editor) {
        var toolbar,
            editToolbar,
            previewInstance,
            inputInstance,
            tooltipNode,
            doingUndoRedo,
            doingUndoRedoTimer,
            $ = window.jQuery;

        /**
         * TinyMCE events
         *
         */
        editor.on('preinit', function () {
            if (editor.wp && editor.wp._createToolbar) {
                toolbar = editor.wp._createToolbar([
                    'app_tooltip_preview',
                    'app_tooltip_edit',
                    'app_tooltip_remove'
                ], true);

                var editButtons = [
                    'app_tooltip_input',
                    'app_tooltip_apply'
                ];

                editToolbar = editor.wp._createToolbar(editButtons, true);

                editToolbar.on( 'show', function() {

                } );

                editToolbar.on( 'hide', function() {
                    if ( ! editToolbar.scrolling ) {
                        editor.execCommand( 'app_tooltip_cancel' );
                    }
                } );
            }
        });

        editor.on( 'wptoolbar', function( event ) {
            var tooltipNode = editor.dom.getParent( event.element, 'span[data-tooltip]' ),
                $tooltipNode, text, edit;

            editToolbar.tempHide = false;

            if ( tooltipNode ) {
                $tooltipNode = editor.$( tooltipNode );
                text = $tooltipNode.attr( 'data-tooltip' );
                edit = $tooltipNode.attr( 'data-tooltip-edit' );

                if ( text === '_app_tooltip_placeholder' || edit ) {
                    if ( text !== '_app_tooltip_placeholder' && ! inputInstance.getText() ) {
                        inputInstance.setText( text );
                    }

                    event.element = tooltipNode;
                    event.toolbar = editToolbar;
                } else if ( text ) {
                    previewInstance.setText( text );
                    event.element = tooltipNode;
                    event.toolbar = toolbar;
                }
            } else if ( editToolbar.visible() ) {
                editor.execCommand( 'app_tooltip_cancel' );
            }
        } );

        /**
         * TinyMCE buttons
         */
        editor.addButton('app_tooltip_button', {
            tooltip: 'Insert/edit tooltip',
            icon: 'tooltip dashicons-warning',
            cmd: 'App_Tooltip',
            stateSelector: 'span[data-tooltip]'
        });

        editor.addButton( 'app_tooltip_input', {
            type: 'AppTooltipInput',
            onPostRender: function() {
                var element = this.getEl(),
                    input = element.firstChild,
                    $input, cache, last;

                inputInstance = this;

                tinymce.$( input ).on( 'keydown', function( event ) {
                    console.log(event)
                    console.log(event.keyCode)
                    if ( event.keyCode === 13 && false === event.shiftKey ) {
                        editor.execCommand( 'app_tooltip_apply' );
                        event.preventDefault();
                    }
                } );
            }
        } );

        editor.addButton( 'app_tooltip_apply', {
            tooltip: 'Apply',
            icon: 'dashicon dashicons-editor-break',
            cmd: 'app_tooltip_apply',
            classes: 'widget btn primary'
        } );

        editor.addButton( 'app_tooltip_preview', {
            type: 'AppTooltipPreview',
            onPostRender: function() {
                previewInstance = this;
            }
        } );

        editor.addButton( 'app_tooltip_edit', {
            tooltip: 'Edit|button', // '|button' is not displayed, only used for context.
            icon: 'dashicon dashicons-edit',
            cmd: 'App_Tooltip'
        } );

        editor.addButton( 'app_tooltip_remove', {
            tooltip: 'Remove tooltip',
            icon: 'dashicon dashicons-no-alt',
            cmd: 'app_tooltip_remove'
        } );

        /**
         * TinyMCE commands
         */
        editor.addCommand( 'App_Tooltip', function () {
            var selectedText = editor.selection.getContent();

            tooltipNode = getSelectedTooltip();
            toolbar.tempHide = true;
            editToolbar.tempHide = false;

            if ( ! tooltipNode ) {
                removePlaceholders();
                editor.selection.setContent('<span data-tooltip="_app_tooltip_placeholder">'+selectedText+'</span>');

                tooltipNode = editor.$( 'span[data-tooltip="_app_tooltip_placeholder"]' )[0];
                editor.selection.select(tooltipNode);

                editor.nodeChanged();
            }

            editor.dom.setAttribs( tooltipNode, { 'data-tooltip-edit': true } );
        });

        editor.addCommand( 'app_tooltip_cancel', function() {
            inputInstance.reset();

            if ( ! editToolbar.tempHide ) {
                removePlaceholders();
            }
        } );

        editor.addCommand( 'app_tooltip_apply', function() {
            if ( editToolbar.scrolling ) {
                return;
            }

            var text, tooltipText;

            if ( tooltipNode ) {
                text = inputInstance.getText();
                tooltipText = inputInstance.getTooltipText();
                editor.focus();

                var parser = document.createElement( 'span' );
                parser.dataset.tooltip = text;

                if ( ! text ) {
                    editor.dom.remove( tooltipNode, true );
                    return;
                }

                text = _.escape(text)
                text = text.replace(/\r\n|\r|\n/g, "<br>");

                editor.dom.setAttribs( tooltipNode, { 'data-tooltip': text, 'data-tooltip-edit': null } );

                if ( ! tinymce.trim( tooltipNode.innerHTML ) ) {
                    editor.$( tooltipNode ).text( tooltipText || text );
                }
            }

            inputInstance.reset();
            editor.nodeChanged();
        } );

        editor.addCommand( 'app_tooltip_remove', function() {
            tooltipNode = getSelectedTooltip();

            editor.dom.remove(tooltipNode, true);

            //editor.execCommand( 'unlink' );
            editToolbar.tempHide = false;
            editor.execCommand( 'app_tooltip_cancel' );
        } );

        /**
         * Remove any remaining placeholders on saving.
         */
        editor.on( 'savecontent', function( event ) {
            event.content = removePlaceholderStrings( event.content, true );
        });

        /**
         * Prevent adding undo levels on inserting link placeholder.
         */
        editor.on( 'BeforeAddUndo', function( event ) {
            if ( event.lastLevel && event.lastLevel.content && event.level.content &&
                event.lastLevel.content === removePlaceholderStrings( event.level.content ) ) {

                event.preventDefault();
            }
        });

        /**
         * When doing undo and redo with keyboard shortcuts (Ctrl|Cmd+Z, Ctrl|Cmd+Shift+Z, Ctrl|Cmd+Y),
         * set a flag to not focus the inline dialog. The editor has to remain focused so the users can do consecutive undo/redo.
         */
        editor.on( 'keydown', function( event ) {
            if ( event.keyCode === 27 ) { // Esc
                editor.execCommand( 'app_tooltip_cancel' );
            }

            if ( event.altKey || ( tinymce.Env.mac && ( ! event.metaKey || event.ctrlKey ) ) ||
                ( ! tinymce.Env.mac && ! event.ctrlKey ) ) {

                return;
            }

            if ( event.keyCode === 89 || event.keyCode === 90 ) { // Y or Z
                doingUndoRedo = true;

                window.clearTimeout( doingUndoRedoTimer );
                doingUndoRedoTimer = window.setTimeout( function() {
                    doingUndoRedo = false;
                }, 500 );
            }
        } );

        /**
         * Find selected tooltip
         * @returns {*}
         */
        function getSelectedTooltip() {
            var text, html,
                node = editor.selection.getStart(),
                tooltip = editor.dom.getParent(node, 'span[data-tooltip]');

            if (!tooltip) {
                html = editor.selection.getContent({format: 'html'});

                if (html && html.indexOf('</span>') !== -1) {
                    text = html.match(/data-tooltip="([^">]+)"/);

                    if (text && text[1]) {
                        tooltip = editor.$('span[data-tooltip="' + text[1] + '"]', node)[0];
                    }

                    if (tooltip) {
                        editor.selection.select(tooltip);
                    }
                }
            }

            return tooltip;
        }

        /**
         * Remove placeholder for getting selected element
         */
        function removePlaceholders() {
            editor.$('span[data-tooltip]').each(function (i, element) {
                var $element = editor.$(element);

                if ($element.attr('data-tooltip') === '_app_tooltip_placeholder') {
                    editor.dom.remove(element, true);
                } else if ($element.attr('data-tooltip-edit')) {
                    $element.attr('data-tooltip-edit', null);
                }
            });
        }

        function removePlaceholderStrings( content, dataAttr ) {
            return content.replace( /(<span [^>]+>)([\s\S]*?)<\/span>/g, function( all, tag, text ) {
                if ( tag.indexOf( ' data-tooltip="_app_tooltip_placeholder"' ) > -1 ) {
                    return text;
                }

                if ( dataAttr ) {
                    tag = tag.replace( / data-tooltip-edit="true"/g, '' );
                }

                return tag + text + '</span>';
            });
        }
    });

})(window.tinymce);