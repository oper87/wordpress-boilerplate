(function ($) {
    'use strict';

    var mailchimpSettings = {

        init: function () {
            var self = this;

            self.$mailchimp_settings = $('#mailchimp_settings');
            self.$mailchimp_actions = $('#newsletter-campaign-action');

            self.bindEvents();
        },

        onLoad: function () {
            $(window).off('beforeunload');
        },

        bindEvents: function () {
            var self = this;

            self.$mailchimp_settings.on('click', '.mailchimp_save_settings', self.saveSettingsAction);
            self.$mailchimp_settings.on('click', '.mailchimp_save_account', self.saveAccountAction);
            self.$mailchimp_settings.on('click', '.mailchimp_fetch_account', self.fetchAccountAction);

            self.$mailchimp_actions.on('click', '.newsletter_show_test_email', self.showEmailField);
            self.$mailchimp_actions.on('click', '.newsletter_send_campaign', self.sendEmailCampaign);
            self.$mailchimp_actions.on('click', '.newsletter_send_test_campaign', self.sendEmailCampaign);

            self.$mailchimp_actions.on('click', '.final_send_campaign', self.finallySendEmailCampaign);
            self.$mailchimp_actions.on('click', '.final_delete_campaign', self.finallyDeleteEmailCampaign);
        },

        saveSettingsAction: function (e) {
            var self = mailchimpSettings,
                $wrap = self.$mailchimp_settings.find('.mailchimp-settings'),
                $status = self.$mailchimp_settings.find('.connected-status'),
                $apiKey = self.$mailchimp_settings.find('#mailchimp_api_key'),
                apiKey = self.$mailchimp_settings.find('#mailchimp_api_key').val();

            e.preventDefault();

            $apiKey.parent().find('.acf-notice').remove();

            /*if (apiKey === '') {
                $apiKey.before('<div class="acf-notice -error acf-error-message"><p>API Key value is required</p></div>');
                return;
            }*/

            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: {
                    action: 'theme_ajax_newsletter_admin',
                    ajax_action: 'mailchimp_save_settings',
                    api_key: apiKey
                },
                beforeSend: function () {
                    $wrap.find('p.submit-action').append('<span>Saving...</span>');
                }
            }).done(function (response) {
                $status.next('.acf-notice').remove();

                if (response.success === false) {
                    $status.removeClass('positive');
                    $status.addClass('negative');
                    $status.text('NOT CONNECTED');

                    if (response.data) {
                        $status.after('<div class="acf-notice -error acf-error-message connected-error-message"><p>' + response.data + '</p></div>');
                    }

                    $('.mailchimp-account-settings').html('');
                } else {
                    $status.removeClass('negative');
                    $status.addClass('positive');
                    $status.text(response.data.connection.health_status + ' - CONNECTED');
                    $('.mailchimp-account-settings').html(response.data.account_template);
                }

                $wrap.find('p.submit-action span').remove();
            });
        },

        saveAccountAction: function (e) {
            var self = mailchimpSettings,
                $wrap = self.$mailchimp_settings.find('.mailchimp-account-settings'),
                $list = self.$mailchimp_settings.find('#mailchimp_list'),
                $wc_list = self.$mailchimp_settings.find('#woocommerce_list'),
                $wc_subscribe_event = self.$mailchimp_settings.find('#woocommerce_subscribe_event'),
                $template = self.$mailchimp_settings.find('#mailchimp_template'),
                $double_optin = self.$mailchimp_settings.find('.double_optin:checked');

            e.preventDefault();

            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: {
                    action: 'theme_ajax_newsletter_admin',
                    ajax_action: 'mailchimp_save_account',
                    list_id: $list.val(),
                    wc_list_id: $wc_list.length ? $wc_list.val() : '',
                    wc_subscribe_event: $wc_subscribe_event.length ? $wc_subscribe_event.val() : '',
                    template_id: $template.val(),
                    double_optin: $double_optin.val()
                },
                beforeSend: function () {
                    $wrap.find('p.submit-action').append('<span>Saving...</span>');
                }
            }).done(function (response) {
                $wrap.find('p.submit-action span').html('<span>' + response.data.message + '</span>');
                $wrap.find('tr.mailchimp-merge-fields').html(response.data.merge_fields);
                $wrap.find('tr.mailchimp-wc-merge-fields').html(response.data.wc_merge_fields);
                $wrap.find('tr.mailchimp-segments').html(response.data.segments);
                $wrap.find('tr.mailchimp-groups').html(response.data.groups);

                setTimeout(function () {
                    $wrap.find('p.submit-action span').remove();
                }, 1500);
            });
        },

        fetchAccountAction: function (e) {
            var self = mailchimpSettings,
                $wrap = self.$mailchimp_settings.find('.mailchimp-account-settings'),
                $list = self.$mailchimp_settings.find('#mailchimp_list'),
                $wc_list = self.$mailchimp_settings.find('#woocommerce_list'),
                $template = self.$mailchimp_settings.find('#mailchimp_template');

            e.preventDefault();

            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: {
                    action: 'theme_ajax_newsletter_admin',
                    ajax_action: 'mailchimp_fetch_account',
                    list_id: $list.val(),
                    template_id: $template.val(),
                    wc_list_id: $wc_list.length ? $wc_list.val() : '',
                },
                beforeSend: function () {
                    $wrap.find('p.submit-action').append('<span>Fetching...</span>');
                }
            }).done(function (response) {

                if (response.success === false) {
                    $wrap.find('p.submit-action span').html('<span>' + response + '</span>');

                    setTimeout(function () {
                        $wrap.find('p.submit-action span').remove();
                    }, 1500);
                } else {
                    $('.mailchimp-account-settings').html(response.data.account_template);

                    $wrap = $('.mailchimp-account-settings');
                    $wrap.find('p.submit-action').append('<span>' + response.data.message + '</span>');

                    setTimeout(function () {
                        $wrap.find('p.submit-action span').remove();
                    }, 1500);
                }
            });
        },

        showEmailField: function (e) {
            var _emailField = document.querySelector('.newsletter-test-email-field');

            if (!_emailField.classList.contains('active')) {
                _emailField.classList.add('active')
            } else {
                _emailField.classList.remove('active')
            }
        },

        sendEmailCampaign: function (e) {
            var $this = $(this),
                _form = this.closest('form'),
                $response = $('.newsletter-campaign-response'),
                _emailField = document.querySelector('.newsletter-test-email-field'),
                $emailFromName = $('[data-name="from"] [data-name="name"] input'),
                $emailFromEmail = $('[data-name="from"] [data-name="email_address"] input'),
                $emailTestInput = $('#newsletter-test-email'),
                $emailSubject = $('[data-name="email_subject"] input'),
                $emailTitle = $('[data-name="email_title"] input'),
                $emailContent = $('[data-name="email_content"] textarea'),
                $emailNewsPosts = $('[data-name="email_news_posts"] select option'),
                newsPosts = [], testEmail = '', isTestEmail = 0, errors = 0;

            $emailFromName.parent().find('.acf-notice').remove();
            $emailFromEmail.parent().find('.acf-notice').remove();
            $emailTestInput.parent().find('.acf-notice').remove();
            $emailSubject.parent().find('.acf-notice').remove();
            $emailTitle.parent().find('.acf-notice').remove();
            $emailContent.parent().find('.acf-notice').remove();

            if (this.classList.contains('newsletter_send_test_campaign') && _emailField.classList.contains('active')) {
                if ($emailTestInput.val() === '') {
                    $emailTestInput.before('<div class="acf-notice -error acf-error-message"><p>Email is required</p></div>');
                    errors++;
                } else {
                    testEmail = $emailTestInput.val();
                    isTestEmail = 1;
                }
            }

            /*if ($emailFromName.val() === '') {
                $emailFromName.before('<div class="acf-notice -error acf-error-message"><p>Subject is required</p></div>');
                errors++;
            }

            if ($emailFromEmail.val() === '') {
                $emailFromEmail.before('<div class="acf-notice -error acf-error-message"><p>Subject is required</p></div>');
                errors++;
            }

            if ($emailSubject.val() === '') {
                $emailSubject.before('<div class="acf-notice -error acf-error-message"><p>Subject is required</p></div>');
                errors++;
            }

            if ($emailTitle.val() === '') {
                $emailTitle.before('<div class="acf-notice -error acf-error-message"><p>Title is required</p></div>');
                errors++;
            }

            if ($emailContent.val() === '') {
                $emailContent.before('<div class="acf-notice -error acf-error-message"><p>Content is required</p></div>');
                errors++;
            }

            if ($emailNewsPosts.length === 0) {
                $emailNewsPosts.before('<div class="acf-notice -error acf-error-message"><p>News posts are required</p></div>');
                errors++;
            } else {
                $emailNewsPosts.each(function () {
                    newsPosts.push(this.value)
                });
            }*/

            if (errors > 0) {
                return;
            }

            $response.html('<div class="acf-notice">Sending...</div>')

            var formData = new FormData(_form);

            formData.append('action', 'theme_ajax_admin');
            formData.append('ajax_action', 'newsletter_send_campaign');
            formData.append('test_email', testEmail);
            formData.append('send_as_test', isTestEmail);

            $.ajax({
                url: ajaxurl,
                processData: false,
                contentType: false,
                type: 'POST',
                data: formData,
            }).done(function (response) {
                if (response.success === true) {
                    $response.html('<div class="acf-notice -success acf-success-message">' + response.data + '</div>');
                } else {
                    $response.html('<div class="acf-notice -error acf-error-message">' + response.data + '</div>');
                }
            });

        },

        finallySendEmailCampaign: function (e) {
            var $response = $('.newsletter-campaign-response');

            $response.html('<div class="acf-notice">Sending...</div>')

            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: {
                    action: 'theme_ajax_newsletter_admin',
                    ajax_action: 'newsletter_finally_send_campaign',
                    campaign_id: this.dataset.campaign
                }
            }).done(function (response) {
                if (response.success === true) {
                    $response.html('<div class="acf-notice -success acf-success-message">' + response.data + '</div>');
                } else {
                    $response.html('<div class="acf-notice -error acf-error-message">' + response.data + '</div>');
                }
            });
        },

        finallyDeleteEmailCampaign: function (e) {
            var $response = $('.newsletter-campaign-response');

            $response.html('<div class="acf-notice">Deleting...</div>')

            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: {
                    action: 'theme_ajax_admin',
                    ajax_action: 'newsletter_finally_delete_campaign',
                    campaign_id: this.dataset.campaign
                }
            }).done(function (response) {
                if (response.success === true) {
                    $response.html('<div class="acf-notice -success acf-success-message">' + response.data + '</div>');
                } else {
                    $response.html('<div class="acf-notice -error acf-error-message">' + response.data + '</div>');
                }
            });
        }

    }

    mailchimpSettings.init();

    $(window).on('load', function () {
        mailchimpSettings.onLoad();

        setTimeout(function () {
            mailchimpSettings.onLoad();
        }, 1000);
    });

}(jQuery));