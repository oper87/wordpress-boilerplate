<?php
/**
 * Custom styles
 */

if ( ! defined( 'ABSPATH' ) ) {
	return false;
}

use \App\Helper;

if( !empty($primary_color) ){

	Helper::render_style( [
		'.small-header:after',
		'header',
		'.fp-current-programs',
		'.blue-book',
		'.blue-book .small-header:after',
		], [
		'background-color' => $primary_color,
		], true );

}

if( !empty($secondary_color) ){

    Helper::render_style( [
    	'.fp-programs-container a',
    	'.fp-current-programs-wrapper .small-header:after',
    	], [
		'background-color' => $secondary_color,
		], true );

}