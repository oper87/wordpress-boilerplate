;(function ($, window, document, undefined) {

	$.appThemeMainFilter = $.appThemeMainFilter || {};

	var mainFilter = function (settings) {
		this.eventHandlers = {};

		this.settings = $.extend({
			pageContainer: '',
			postsContainer: '',
			postsContainerInner: '',
			formName: '',
			loadMorePagination: '',
			loadMoreButton: '',
			refreshFragments: {}
		}, settings);

		this._pageContainer = document.querySelector(this.settings.pageContainer);

	}
	mainFilter.prototype.init = function () {
		if (!this._pageContainer) {
			return;
		}

		this._form = document.forms[this.settings.formName];
		this.form = 'form[name="' + this.settings.formName + '"]';
		this.termsItem = '.filter-terms-item';
		this.termCheckbox = 'input.term[type="checkbox"]';
		this.termValue = 'input.taxonomy-terms-value';
		this.resetTermButton = '[data-action="unselect-term"]';
		this.termSelect = 'select.terms-autocomplete';

		this.isAjax = false;
		this.historyKey = this.settings.pageContainer;
		this.autoSubmit = this._form.dataset.auto_submit === '1';
		this.searchTimer = false;
		this.termSearch = false;//'.posts-filter__terms-search-field input[type="text"]';
		this.termSearchBtn = '.posts-filter__terms-search-field button';
		this.isLoadMorePagination = this.settings.loadMorePagination ?? false;
		this.filterApplyButton = '.btn-apply-filter';

		// Infinity pagination
		this.infinityPagination = this.settings.infinityPagination ?? false;
		this.isScrolling = false;
		this.scrollY = 0;
		this.offsetShift = 250;
		this.ajaxRequestTimeout = false;

		history.replaceState({name: this.historyKey}, null, null);

		this.eventHandlers.formSubmitHandler = this.formSubmitHandler.bind(this);
		this.eventHandlers.termCheckboxChangeHandler = this.termCheckboxChangeHandler.bind(this);
		this.eventHandlers.formResetHandler = this.formResetHandler.bind(this);
		this.eventHandlers.formApplyHandler = this.formApplyHandler.bind(this);
		this.eventHandlers.termResetButtonHandler = this.termResetButtonHandler.bind(this);
		this.eventHandlers.historyPopstateHandler = this.historyPopstateHandler.bind(this);
		this.eventHandlers.infinityPaginationScrollHandler = this.infinityPaginationScrollHandler.bind(this);
		this.eventHandlers.termSearchInputHandler = this.termSearchInputHandler.bind(this);
		this.eventHandlers.termSearchButtonHandler = this.termSearchButtonHandler.bind(this);
		this.eventHandlers.termSelectHandler = this.termSelectHandler.bind(this);

		$(document.body).on('submit', this.form, this.eventHandlers.formSubmitHandler);
		$(document.body).on('click', this.filterApplyButton, this.eventHandlers.formApplyHandler);
		$(this.form).on('change', this.termCheckbox, this.eventHandlers.termCheckboxChangeHandler);
		$(this.form).on('change input', this.termSearch, this.eventHandlers.termSearchInputHandler);
		$(this.form).on('change', this.termSelect, this.eventHandlers.termSelectHandler);
		$(this.form).on('click', this.termSearchBtn, this.eventHandlers.termSearchButtonHandler);
		$(document.body).on('click', '.btn-reset-filter', this.eventHandlers.formResetHandler);
		$(document.body).on('click', this.resetTermButton, this.eventHandlers.termResetButtonHandler);
		window.addEventListener('popstate', this.eventHandlers.historyPopstateHandler);

		if (this.isLoadMorePagination) {
			this._loadMorePagination = document.querySelector(this.settings.loadMorePagination);

			this.eventHandlers.loadMorePaginationClickHandler = this.loadMorePaginationClickHandler.bind(this);
			$(document.body).on('click', this.settings.loadMoreButton, this.eventHandlers.loadMorePaginationClickHandler);
		}

		if (this.infinityPagination) {
			this.initializeInfinityPagination();
		}

		this.extendInit();
		//this.initSelect2();
	};

	mainFilter.prototype.extendInit = function () {
	};

	mainFilter.prototype.formSubmitHandler = function (e, trigger) {
		e.preventDefault();

		/*if (this.autoSubmit === false && typeof (trigger) !== 'undefined') {
			return;
		}*/

		this.sendRequest($(e.currentTarget).serialize(), true);
	};

	mainFilter.prototype.formResetHandler = function (e) {
		e.preventDefault();

		this.sendRequest('', true);
	};

	mainFilter.prototype.formApplyHandler = function (e) {
		e.preventDefault();

		if (e.currentTarget.hasAttribute('data-dismiss')) {
			let _button = document.querySelector('[data-target="' + this.settings.sidebarContainer + '"]')
			theme.sidebarModal.hideModal(this.settings.sidebarContainer, _button);
			return;
		}

		this.sendRequest($(this._form).serialize(), true);

		$(document.body).one('app_filter_posts_loaded.' + this.settings.formName, () => {
			let top = $(this.settings.postsContainer).offset().top;

			$([document.documentElement, document.body]).animate({
				scrollTop: top - (document.body.classList.contains('logged-in') ? 107 : 62)
			}, 1000);
		});
	};

	mainFilter.prototype.termResetButtonHandler = function (e) {
		var termValue = e.currentTarget.dataset.value,
			urlParams = new URLSearchParams(window.location.search),
			buildActionUrl = [], values, newValues = [];

		e.preventDefault();

		for (const key of urlParams.keys()) {
			values = urlParams.get(key).split(',');
			newValues = [];

			for (const value of values) {
				if (termValue !== value) {
					newValues.push(value)
				}
			}

			if (newValues.length > 0) {
				buildActionUrl.push(key + '=' + newValues.join(','));
			}
		}

		this.sendRequest(buildActionUrl.join('&'), true);
	};

	mainFilter.prototype.termCheckboxChangeHandler = function (e) {
		var _input = e.currentTarget,
			_taxonomy = _input.closest(this.termsItem),
			_termValue = _taxonomy.querySelector(this.termValue),
			values = [];

		if (_taxonomy.dataset.filterChooseType === 'single') {
			if (_input.checked) {
				$(_input).closest('li').siblings().find('input:checked').prop('checked', false)

				if (_input.value !== '') {
					values.push(_input.value);
				}
			}
		} else {
			values = this.prepareValues(_taxonomy);
		}

		_termValue.disabled = values.length === 0;
		_termValue.value = values.join(',');

		if (this._form.querySelector(this.filterApplyButton)) {
			this._form.querySelector(this.filterApplyButton).disabled = false;
		}

		if (this.autoSubmit) {
			$(_taxonomy.closest(this.form)).trigger('submit', 'triggerSubmit');
		}
	};

	mainFilter.prototype.termSelectHandler = function (e) {
		var _form = this.form,
			_input = e.currentTarget,
			_termValue = _input.closest(this.termsItem).querySelector(this.termValue);

		this.prepareSelectValues(_input, _termValue);

		if (this._form.querySelector(this.filterApplyButton)) {
			this._form.querySelector(this.filterApplyButton).disabled = false;
		}

		this.sendRequest($(_input.closest(_form)).serialize(), true, !this.autoSubmit);
	};

	mainFilter.prototype.termSearchInputHandler = function (e) {
		var _form = this.form,
			_input = e.currentTarget,
			_termValue = _input.closest(this.termsItem).querySelector(this.termValue);

		clearTimeout(this.searchTimer);

		if (e.type === 'input' && _input.value.length < 3) {
			return;
		}

		//_input.value = _input.value.replace(/[^a-zA-Z\s]+/g, '');

		this.searchTimer = setTimeout(() => {
			this.prepareSearchValues(_input, _termValue);

			if (this._form.querySelector(this.filterApplyButton)) {
				this._form.querySelector(this.filterApplyButton).disabled = false;
			}

			if (_input.closest(_form)) {
				this.sendRequest($(_input.closest(_form)).serialize(), true, !this.autoSubmit);
			}
		}, e.type === 'input' ? 800 : 0);
	};

	mainFilter.prototype.termSearchButtonHandler = function (e) {
		var _form = this.form,
			_input = e.currentTarget.previousElementSibling,
			_termValue = _input.closest(this.termsItem).querySelector(this.termValue);

		this.prepareSearchValues(_input, _termValue);

		//if (this.autoSubmit) {
		//$(_input.closest(_form)).trigger('submit', 'triggerSubmit');
		//}
	};

	mainFilter.prototype.prepareValues = function (_taxonomy) {
		var values = [];

		_taxonomy.querySelectorAll(this.termCheckbox + ':checked').forEach(function (_el) {
			values.push(_el.value);
		});

		return values;
	};

	mainFilter.prototype.prepareSearchValues = function (_input, _termValue) {
		let value = '';

		if (_input.value.length === 0) {
			return;
		}

		_termValue.disabled = _input.value.length === 0;

		if (_input.dataset.multiple) {
			var values = [];

			if (_termValue.value.length === 0) {
				values[0] = _input.value.trim();
			} else {
				values = _termValue.value.split(',');
				values.push(_input.value.trim());

				values = values.filter((value, index, array) => array.indexOf(value) === index);
			}

			value = values.join(',')
		} else {
			value = _input.value.trim();
		}

		_termValue.value = value;
	};

	mainFilter.prototype.prepareSelectValues = function (_select, _termValue) {
		let value = '', options = [];

		if (_select.options.length === 0) {
			return;
		}

		options = Array.from(_select.options).filter((_option) => {
			return _option.selected
		})

		if (options.length === 0) {
			return;
		}

		_termValue.disabled = options.length === 0;

		if (_select.dataset.multiple) {
			let values = [];

			if (_termValue.value.length === 0) {
				options.forEach((_option) => {
					values.push(_option.value);
				});
			} else {
				values = _termValue.value.split(',');

				options.forEach((_option) => {
					values.push(_option.value);
				});
			}

			value = values.join(',')
		} else {
			value = _select.value.trim();
		}

		_termValue.value = value;
	};

	mainFilter.prototype.sendRequest = function (data, pushState, refreshFilter) {
		var self = this,
			historyUrl = this._form.action;

		if (this.isAjax) {
			return;
		}

		this.isAjax = true;

		if ($.fn.block) {
			$(this._form).block({
				message: null, overlayCSS: {
					background: '#fff', opacity: 0.6
				}
			});
		}

		$(document.body).trigger('app_filter_before_send_request.' + self.settings.formName);

		$.get(updateQueryStringParameter(this._form.action, '_ajax', 1), data).done(function (response) {
			var $response = $('<div>' + response + '</div>'),
				data_filters = $response.find('#wp_data_filters').text(),
				title = $response.find('#wp_title').text();

			try {
				data_filters = JSON.parse(data_filters);
			} catch (e) {
				data_filters = {};//JSON.parse(this._form.dataset.filters);
			}

			if ($.fn.block) {
				$(self._form).unblock();
			}

			self.setProducts($response, refreshFilter);

			for (const [key, value] of Object.entries(self.settings.refreshFragments)) {
				$(value).html($response.find(value).html());
			}

			self.setDataFilter(data_filters, $response);

			if (pushState === true) {
				historyUrl = updateQueryStringParameters(historyUrl, parseQueryStringParameter(data));

				history.pushState({name: self.historyKey}, title, removeURLParameter(historyUrl, 'paged'));
				document.title = title;
			}

			$(document.body).trigger('app_filter_send_request_done.' + self.settings.formName, [$response, data_filters]);
			$(document.body).trigger('app_filter_posts_loaded.' + self.settings.formName, [$response, data_filters]);

			self.isAjax = false;

			setTimeout(() => {
				if (self.infinityPagination) {
					self.initializeInfinityPagination();
				}
				self.initSelect2();
			}, 100);
		});
	};

	mainFilter.prototype.historyPopstateHandler = function (e) {
		if (e.state !== null && typeof (e.state) === 'object' && e.state.name === this.historyKey) {
			this.historySendRequest(e.state);
		} else if (e.state !== null && typeof (e.state) === 'object' && e.state.name === this.historyKey + '_loadMore') {
			var $pageContainer = $(e.state.pageContainerHtml);

			document.title = e.state.documentTitle;

			$(this.settings.pageContainer).find(this.settings.postsContainer).html($pageContainer.find(this.settings.postsContainer).html());
			$(this.settings.pageContainer).find(this.form).html($pageContainer.find(this.form).html());

			for (const [key, selector] of Object.entries(this.settings.refreshFragments)) {
				$(this.settings.pageContainer).find(selector).html($pageContainer.find(selector).html());
			}

			this.loadMorePaginationHistoryLoaded(e.state);

			var _loadMorePagination = document.querySelector(this.settings.loadMorePagination);

			setTimeout(() => {
				//window.scrollTo(0, e.state.windowScrollY);
				_loadMorePagination.classList.remove('loading');
				if (this.infinityPagination) {
					this.initializeInfinityPagination();
				}
			}, 100);

		}
	};

	mainFilter.prototype.historySendRequest = function (state) {
		this.sendRequest(document.location.search.replace('?', ''), false);
	}

	mainFilter.prototype.setDataFilter = function (data_filters, $response) {
		var self = this;

		var filterItems = document.querySelectorAll('[data-filter-item]'),
			responseFilterItems = $response.get(0).querySelectorAll('[data-filter-item]');

		filterItems.forEach(function (_item, key) {
			var _responseItem = responseFilterItems[key];

			if (_responseItem) {
				_item.innerHTML = _responseItem.innerHTML;
			}
		});

		for (const [filterKey, filterItem] of Object.entries(data_filters)) {
			var _filterItems = document.querySelectorAll('[data-filter-item="' + filterKey + '"]');

			_filterItems.forEach(function (_item) {
				_item.querySelector(self.termValue).value = filterItem['selected_value'];

				if (filterItem['selected_array'].length > 0) {
					_item.closest('.posts-filter__item').classList.add('selected');
				} else {
					_item.closest('.posts-filter__item').classList.remove('selected');
				}

				if (filterItem.choose_type === 'multiple' && filterItem.type === 'taxonomy') {
					if (filterItem['terms'].length === 0) {
						_item.closest('.posts-filter__item').classList.add('display-none');
					} else {
						_item.closest('.posts-filter__item').classList.remove('display-none');
					}
				} else if (filterItem.choose_type === 'slider') {
					//console.log(filterItem)
				}
			});
		}
	};

	mainFilter.prototype.setProducts = function ($response, refreshFilter) {
		if (!refreshFilter) {
			$(this.settings.postsContainer).html($response.find(this.settings.postsContainer).html());
		} else {
			$(this.settings.postsContainer).html('');
		}
	};

	mainFilter.prototype.loadMorePaginationClickHandler = function (e) {
		var self = this,
			_loadMorePagination = document.querySelector(this.settings.loadMorePagination),
			_nextLink = _loadMorePagination.querySelector('.load-more-button'),
			ajaxUrl = '';

		e.preventDefault();

		if (!_nextLink) {
			_loadMorePagination.classList.add('loaded');

			if (this.infinityPagination) {
				window.removeEventListener('scroll', this.eventHandlers.infinityPaginationScrollHandler);
			}

			return;
		}

		if (this.isAjax) {
			return;
		}

		this.isAjax = true;

		ajaxUrl = (_nextLink.dataset.ajaxUrl) ? _nextLink.dataset.ajaxUrl : _nextLink.getAttribute('href');
		ajaxUrl = updateQueryStringParameter(ajaxUrl, '_ajax', 1);
		ajaxUrl = updateQueryStringParameter(ajaxUrl, '_load_more', 1);

		_loadMorePagination.classList.add('loading');

		setTimeout(() => {
			$.ajax({
				method: "GET",
				url: ajaxUrl,
				complete: function () {
					_loadMorePagination.classList.remove('loading');
				}
			}).done(function (response) {
				var $response = $('<div>' + response + '</div>');

				self.loadMorePaginationAjaxResponse($response, _loadMorePagination, _nextLink);

				$(document.body).trigger('app_filter_load_more_posts_loaded.' + self.settings.formName, [$response, _loadMorePagination, _nextLink]);
				$(document.body).trigger('app_filter_posts_loaded.' + self.settings.formName, [$response]);

				self.isAjax = false;
				self.ajaxRequestTimeout = false;
			});
		}, self.ajaxRequestTimeout);
	};

	mainFilter.prototype.loadMorePaginationAjaxResponse = function ($response, _loadMorePagination, _nextLink) {
		var documentTitle = $response.find('#wp_title').text(),
			historyState = history.state, historyData = {}, historyUrl;

		historyData = {
			name: this.historyKey + '_loadMore',
			documentTitle: document.title,
			windowScrollY: window.scrollY,
			pageContainerHtml: $(this.settings.pageContainer).prop('outerHTML'),
		}

		if (historyState === null || (typeof (historyState) === 'object' && historyState.name !== historyData.name)) {
			history.replaceState(historyData, document.title, window.location.href);
		}

		_loadMorePagination.innerHTML = $response.find(this.settings.loadMorePagination).html();

		this.loadMorePaginationPostsLoaded($response);

		document.title = documentTitle;
		historyData.documentTitle = documentTitle;
		historyData.windowScrollY = window.scrollY;
		historyData.pageContainerHtml = $(this.settings.pageContainer).prop('outerHTML');
		historyUrl = (_nextLink.dataset.historyUrl) ? _nextLink.dataset.historyUrl : _nextLink.getAttribute('href')

		history.pushState(historyData, documentTitle, removeURLParameter(_nextLink.getAttribute('href'), 'paged'));
	};

	mainFilter.prototype.loadMorePaginationHistoryLoaded = function (historyState) {

	};

	mainFilter.prototype.loadMorePaginationPostsLoaded = function ($response) {

	};

	mainFilter.prototype.initializeInfinityPagination = function () {
		var _loadMoreContainer = document.querySelector(this.settings.loadMorePagination);

		if (_loadMoreContainer) {
			if (_loadMoreContainer.querySelector('input[type="hidden"]')) {
				window.addEventListener('scroll', this.eventHandlers.infinityPaginationScrollHandler, {passive: true});
			} else {
				_loadMoreContainer.classList.add('loaded');
				window.removeEventListener('scroll', this.eventHandlers.infinityPaginationScrollHandler);
			}
		}
	};

	mainFilter.prototype.infinityPaginationScrollHandler = function (e) {
		if (!this.isScrolling) {
			window.requestAnimationFrame(() => {
				this.infinityPaginationTracker(e);
				this.isScrolling = false;
			});
			this.isScrolling = true;
		}
	};

	mainFilter.prototype.infinityPaginationTracker = function (e) {
		if (!document.querySelector(this.settings.loadMorePagination)) {
			return;
		}

		var scrollTop = document.documentElement.scrollTop,
			windowHeight = window.innerHeight,
			loadMoreProps = document.querySelector(this.settings.loadMorePagination).getBoundingClientRect(),
			loadMoreOffsetTop = scrollTop + loadMoreProps.top,
			scrollDirection = (scrollTop - this.scrollY);

		if (scrollTop > (loadMoreOffsetTop - windowHeight - this.offsetShift) && scrollTop < (loadMoreOffsetTop + loadMoreProps.height - this.offsetShift)) {
			this.ajaxRequestTimeout = 500;
			this.loadMorePaginationClickHandler(e);
		}

		this.scrollY = scrollTop;
	};

	mainFilter.prototype.initSelect2 = function () {
		var autocompleteInputs = this._form.querySelectorAll('.terms-autocomplete');

		if (!window.jQuery.fn.select2) {
			return;
		}

		autocompleteInputs.forEach(function (_autocompleteInput) {
			$(_autocompleteInput).select2({
				data: JSON.parse(_autocompleteInput.dataset.terms),
				placeholder: _autocompleteInput.dataset.placeholder,
				multiple: _autocompleteInput.dataset.multiple
			});
		});
	}


	$.appThemeMainFilter = mainFilter;

	var filter = {}, filterArgs = {};

	if (document.querySelector('.shows-container')) {
		filterArgs = {
			pageContainer: '.shows-container',
			postsContainer: '.shows-posts-wrapper',
			formName: 'wc-shows-filter-form',
			postsContainerInner: 'div.shows-posts__item',
			loadMorePagination: '.load-more-pagination',
			loadMoreButton: '.load-more-button',
			infinityPagination: true,
			refreshFragments: {
				scheduleViews: '.shows-filter__schedule-views',
				genreDropdown: '#shows-filter-genre-dropdown'
			}
		};

		filter = new $.appThemeMainFilter(filterArgs);

		filter.extendInit = function () {
			this.pageLayout = $(this.settings.postsContainer).find('.shows-posts-layout-time').length ? 'time' : 'venue';

			this.eventHandlers.switchPageViewHandler = this.switchPageViewHandler.bind(this);

			$(this._form).on('click', '.shows-filter__schedule-views a', this.eventHandlers.switchPageViewHandler);

			$(document.body).on('app_filter_before_send_request.' + this.settings.formName, function (e) {
				$('#shows-filter-genre-dropdown').trigger('click');
			});
		};

	} else if (document.querySelector('.news-container')) {
		filterArgs = {
			pageContainer: '.news-container',
			postsContainer: '.news-posts-wrapper',
			formName: 'wc-shows-filter-form',
			postsContainerInner: 'div.article-post',
			loadMorePagination: '.load-more-pagination',
			loadMoreButton: '.load-more-button',
			infinityPagination: true,
			refreshFragments: {
				genreDropdown: '#shows-filter-genre-dropdown'
			}
		};

		filter = new $.appThemeMainFilter(filterArgs);

		filter.extendInit = function () {
			$(document.body).on('app_filter_before_send_request.' + this.settings.formName, function (e) {
				$('#shows-filter-genre-dropdown').trigger('click');
			});
		};
	}

	filter.loadMorePaginationPostsLoaded = function ($response) {
		var self = this, $responsePostLoops = $response.find(this.settings.postsContainerInner);

		$.each($responsePostLoops, function (key, _postLoop) {
			$(self.settings.postsContainerInner).last().after(_postLoop.outerHTML);
		});
	};

	filter.historySendRequest = function (state) {
		if (document.location.href.indexOf('venue') !== -1 && this.pageLayout === 'time') {
			this.switchPageView(document.location.href, false);
		} else if (document.location.href.indexOf('venue') === -1 && this.pageLayout === 'venue') {
			this.switchPageView(document.location.href, false);
		} else {
			this.sendRequest(document.location.search.replace('?', ''), false);
		}
	};

	filter.switchPageViewHandler = function (e) {
		var requestUrl = e.currentTarget.href;

		e.preventDefault();
		this.switchPageView(requestUrl, true);
	};

	filter.switchPageView = function (requestUrl, pushState) {
		var self = this;

		$.get(updateQueryStringParameter(requestUrl, '_ajax', 1)).done(function (response) {
			var $response = $('<div>' + response + '</div>');

			$(self.settings.postsContainer)
				.html($response.find(self.settings.postsContainer).html());

			$('.shows-filter__schedule-views')
				.html($response.find('.shows-filter__schedule-views').html());

			self._form.action = $response.find(self.form).get(0).action;

			if (requestUrl.indexOf('venue') !== -1) {
				self.pageLayout = 'venue';
			} else {
				self.pageLayout = 'time';
			}

			$(document.body).trigger('app_filter_posts_loaded.' + self.settings.formName, [$response]);

			if (pushState) {
				history.pushState({name: self.historyKey}, null, requestUrl);
			}
		});

	};

	filter.init();


})(jQuery, window, document);