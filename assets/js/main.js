(function ($) {
	'use strict';

	const theme_params = window.theme_params || {};
	const cacheEnabled = theme_params.cache_enabled === '1';

	var main = {
		init: function () {

			this.eventHandlers = {};

            this.burgerMenuBtn = false;
			//this.burgerMenuBtn = '.menu-btn';
			//this.mobileMenu = '.header__nav';

            this.bindEvents();
            this.initTopBar();
            this.setCssVariables();
            this.addTargetBlankExternalLinks();
        },

		bindEvents: function () {
			var self = this;

			this.eventHandlers.setCssVariables = self.setCssVariables.bind(this);
			this.eventHandlers.burgerBtnHandler = self.burgerBtnHandler.bind(this);

            if (this.burgerMenuBtn) {
                $(document.body).on('click', this.burgerMenuBtn, this.eventHandlers.burgerBtnHandler);
            }
        },

		setCssVariables: function () {
			let vh = window.innerHeight * 0.01;
			let hh = document.getElementById('header').offsetHeight;

			document.documentElement.style.setProperty('--vh', `${vh}px`);
			document.documentElement.style.setProperty('--hh', `${hh}px`);
		},

		initTopBar: function () {
			var self = this;

			self.topbar = '.header-top-bar';

			if (Cookies.get('disableTopBar') !== '1') {
				document.documentElement.classList.add('top-bar-active');

				$(self.topbar).on('click', '.btn-close', function () {
					Cookies.set('disableTopBar', '1', {expires: 30})
					document.documentElement.classList.remove('top-bar-active');
				});
			}
		},

		addTargetBlankExternalLinks: function () {
			var a = new RegExp('/' + window.location.host + '/');

			$(document.body).on('click', 'a', function () {
				if (this.href && !a.test(this.href)) {
					this.target = '_blank';
				}
			});
		},

		burgerBtnHandler: function (e) {
			this.mobileMenuToggleHandler(!e.currentTarget.classList.contains('btn-open'));
		},

		mobileMenuToggleHandler: function (state) {
			var self = this;

			if (state) {
				document.querySelector(this.burgerMenuBtn).classList.add('btn-open');
				document.querySelector(this.mobileMenu).classList.add('show');

				setTimeout(function () {
					document.querySelector(self.mobileMenu).classList.add('fade-in');
				}, 100);
			} else {
				document.querySelector(this.burgerMenuBtn).classList.remove('btn-open');

				document.querySelector(this.mobileMenu).classList.remove('fade-in');

				setTimeout(function () {
					document.querySelector(self.mobileMenu).classList.remove('show');
				}, 600);
			}
		}
	};

	var filter = {

		init: function (wrapper) {
			var self = this;

			self.$wrapper = $(wrapper);

			if (self.$wrapper.length === 0) {
				return;
			}

			self.historyKey = wrapper;
			self.actionUrl = self.$wrapper.find('form[action]').attr('action');

			self.contentPosts = self.$wrapper.data('content-posts');
			self.contentFilter = self.$wrapper.data('content-filter');

			self.termsCheckBox = 'input[type="checkbox"].term';
			self.taxonomyTermsValue = '.taxonomy-terms-value';
			self.searchValue = '.filter-search input.search-value';
			self.searchButton = '.filter-search button.btn-search';

			history.replaceState(self.historyKey, null, null);
			self.bindEvents();

			self.searchPopup.init('#sidebar-search-content');
		},

		bindEvents: function () {
			var self = this;

			self.$wrapper.on('submit', '.filter-body form', function () {
				self.filterAction($(this).serialize(), true);

				return false;
			});

			self.$wrapper.on('change', self.termsCheckBox, function () {
				self.onChangeCheckBox($(this));
			});

			self.$wrapper.on('click', '.btn-reset-filter', function (e) {
				e.preventDefault();
				self.filterAction('', true);
			});

			self.$wrapper.on('click', '.btn-show-all', function (e) {
				e.preventDefault();

				var $wrapper = $(this).closest('.taxonomy-filter');

				if (!this.classList.contains('less')) {
					this.classList.add('less');
					$wrapper.find('.term-label.hidden').addClass('shown');
					this.innerText = this.dataset.showLess;
				} else {
					this.classList.remove('less');
					$wrapper.find('.term-label.hidden').removeClass('shown');
					this.innerText = this.dataset.showAll;
				}
			});

			$(self.contentPosts).on('change', '.filter-selected-terms input.term', function (e) {
				e.preventDefault();

				var termValue = this.value,
					urlParams = new URLSearchParams(window.location.search),
					buildActionUrl = [], values, newValues = [];

				for (const key of urlParams.keys()) {
					values = urlParams.get(key).split(',');
					newValues = [];

					for (const value of values) {
						if (termValue !== value) {
							newValues.push(value)
						}
					}

					if (newValues.length > 0) {
						buildActionUrl.push(key + '=' + newValues.join(','));
					}
				}

				self.filterAction(buildActionUrl.join('&'), true);
			});

			window.addEventListener('popstate', self.filterHistoryHandler);

			$.fn.scrollBar.init('.filter-selected-terms');
		},

		onChangeCheckBox: function ($this) {
			var self = this,
				$form = $this.closest('form'),
				$taxonomy = $this.closest('.filter-terms'),
				$taxonomyTermsValue = $taxonomy.find(self.taxonomyTermsValue),
				values = this.prepareValues($taxonomy);

			if (values.length === 0) {
				$taxonomyTermsValue.prop('disabled', true);
			} else {
				$taxonomyTermsValue.prop('disabled', false);
			}

			self.propSearch();

			$taxonomyTermsValue.val(values.join(','));
			$form.trigger('submit');
		},

		filterAction: function (actionData, pushState) {
			var self = this,
				historyUrl = self.actionUrl,
				visibility = [];

			$(self.contentFilter).find('.filter-terms').each(function () {
				if ($(this).find('.btn-show-all').hasClass('less')) {
					visibility.push($(this).attr('id'));
				}
			});

			$.get(self.actionUrl + '?_ajax=1', actionData).done(function (response) {
				var $response = $('<div>' + response + '</div>'),
					title = $response.find('#wp_title').text();

				$(self.contentPosts).html($(response).find(self.contentPosts).html());
				$(self.contentFilter).html($(response).find(self.contentFilter).html());
				$('.total-posts').html($(response).find('.total-posts').html());

				visibility.forEach(function (taxonomy) {
					var $taxonomyFilter =$('#'+taxonomy),
						$showButton = $taxonomyFilter.find('.btn-show-all');

					$taxonomyFilter.find('.taxonomy-filter .term-label.hidden').addClass('shown');

					if($showButton.length) {
						$showButton.get(0).classList.add('less');
						$showButton.get(0).innerText = $showButton.get(0).dataset.showLess;
					}
				});

				grid.resizeGrid($(self.contentPosts).find('.grid'));

				if (actionData !== '' && actionData !== 'paged=1') {
					historyUrl = '?' + actionData;
				}

				console.log(historyUrl);

				if (pushState === true) {
					history.pushState(self.historyKey, title, removeURLParameter(historyUrl, 'paged'));
					document.title = title;
				}

				self.propSearch(false);
			});
		},

		filterHistoryHandler: function (e) {
			var self = filter;

			if (e.state === self.historyKey) {
				self.filterAction(document.location.search.replace('?', ''), false);
			} else {
				e.preventDefault();
				return false;
			}
		},

		prepareValues: function ($taxonomy) {
			var self = this;

			let values = [];

			$taxonomy.find(self.termsCheckBox + ':checked').filter(function () {
				values.push(this.value);
			});

			return values;
		},

		getQueryParameters: function (str) {
			var self = this;

			return (str || document.location.search).replace(/(^\?)/, '').split("&").map(function (n) {
				return n = n.split("="), this[n[0]] = n[1], this
			}.bind({}))[0];
		},

		propSearch: function (state) {
			var self = this;

			if (document.querySelector(self.searchValue) === null) {
				return;
			}

			if (state !== undefined) {
				document.querySelector(self.searchValue).disabled = state;
				return;
			}

			if (document.querySelector(self.searchValue).value === '') {
				document.querySelector(self.searchValue).disabled = true;
			} else {
				document.querySelector(self.searchValue).disabled = false;
			}
		},

		searchPopup: {
			init: function (searchInput) {
				var self = this;

				if (document.querySelector(searchInput) === null) {
					return;
				}

				self.keyPressTimer = 0;
				self.searchInput = searchInput;
				self.resultCache = [];
				self.ajaxAction = document.querySelector(searchInput).dataset.ajaxAction;

				self._filterBody = document.querySelector('.filter-body');
				self._searchResultProducts = document.querySelector('.search-body');

				self.bindEvents();
			},

			bindEvents: function () {
				var self = this;

				document.querySelector(self.searchInput).closest('form')
					.addEventListener('submit', function (e) {
						e.preventDefault();
					});

				$(document.body).on('input', self.searchInput, function (e) {
					e.preventDefault();

					var keyword = this.value;

					clearTimeout(self.keyPressTimer);

					self.keyPressTimer = setTimeout(function () {
						self.sendRequest(keyword);
					}, 400);
				});
			},

			sendRequest: function (keyword) {
				var self = this;

				if (keyword.length < 3) {
					self.showResult(false);
					return;
				}

				if (self.resultCache[keyword]) {
					self.showResult(self.resultCache[keyword]);
					return;
				}

				$.ajax({
					url: theme_params.ajax_url,
					method: "POST",
					dataType: 'json',
					data: {
						action: 'theme_ajax',
						ajax_action: self.ajaxAction,
						keyword: keyword
					},
					beforeSend: function () {
						self._searchResultProducts.classList.add('loading');
					},
					complete: function () {
						self._searchResultProducts.classList.remove('loading');
					},
					success: function (response) {
						self.resultCache[keyword] = response.data;
						self.showResult(response.data);
					},
				});
			},

			showResult: function (data) {
				var self = this;

				self._searchResultProducts.innerHTML = '';

				if (false === data) {
					self._filterBody.style.display = 'block';
					self._searchResultProducts.style.display = 'none';
					return;
				}

				self._filterBody.style.display = 'none';
				self._searchResultProducts.style.display = 'block';

				var regExp = new RegExp(data.keyword, "gi");

				if (data.found_products !== 0) {
					data.products.forEach(function (product) {
						product.title = product.title.replace(regExp, function (match) {
							return `<mark>${match}</mark>`;
						});

						const markup = `
						<div class="search-post">
							<a href="${product.url}">
								<div class="post-title">
									${product.title}
								</div>
							</a>
						</div>`;

						self._searchResultProducts.insertAdjacentHTML('afterbegin', markup);
					});
				} else {
					const markup = `<p class="nothing-found">${data.message}</p>`;

					self._searchResultProducts.insertAdjacentHTML('afterbegin', markup);
				}
			}
		}

	};

	var search = {
		init: function () {
			var self = this;

			self.searchInput = '#site-search-content';

			if (document.querySelector(self.searchInput) === null) {
				return;
			}

			self.keyPressTimer = 0;
			self.resultCache = [];

			self._trendingBody = document.querySelector('#search-sidebar .trending-body');
			self._searchResult = document.querySelector('#search-sidebar .search-body');
			self._showMoreResults = document.querySelector('#search-sidebar .show-more-results');

			self.bindEvents();
		},

		bindEvents: function () {
			var self = this;

			$(document.body).on('input', self.searchInput, function (e) {
				e.preventDefault();

				var keyword = this.value;

				clearTimeout(self.keyPressTimer);

				self.keyPressTimer = setTimeout(function () {
					self.sendRequest(keyword);
				}, 400);
			});

			$(document.body).on('click', '.load-more-search .load-more-button', function (e) {
				e.preventDefault();

				self.loadMore(this, this.dataset.paged, this.dataset.post_type, this.dataset.search_term);
			});
		},

		sendRequest: function (keyword) {
			var self = this;

			if (keyword.length < 3) {
				self.showResult(false);
				return;
			}

			if (self.resultCache[keyword]) {
				self.showResult(self.resultCache[keyword]);
				return;
			}

			$.ajax({
				url: theme_params.ajax_url,
				method: "POST",
				dataType: 'json',
				data: {
					action: 'theme_ajax',
					ajax_action: 'site_search',
					keyword: keyword
				},
				beforeSend: function () {
					self._searchResult.classList.add('loading');
				},
				complete: function () {
					self._searchResult.classList.remove('loading');
				},
				success: function (response) {
					self.resultCache[keyword] = response.data;
					self.showResult(response.data);
				},
			});
		},

		showResult: function (data) {
			var self = this,
				resultHtml = '';

			self._searchResult.innerHTML = '';

			if (false === data) {
				self._trendingBody.style.display = 'block';
				self._searchResult.style.display = 'none';
				self._showMoreResults.style.visibility = 'hidden';
				return;
			}

			self._trendingBody.style.display = 'none';
			self._searchResult.style.display = 'block';
			self._showMoreResults.style.visibility = 'hidden';

			if (data.count_posts !== 0) {
				resultHtml += self.getPostsLayout(data.central_banks, theme_params.labels.central_banks_title, data.keyword);
				resultHtml += self.getPostsLayout(data.posts, theme_params.labels.news_title, data.keyword);
				resultHtml += self.getPostsLayout(data.research, theme_params.labels.research_title, data.keyword);

				self._searchResult.insertAdjacentHTML('afterbegin', resultHtml);
				self._showMoreResults.style.visibility = 'visible';
			} else {
				const markup = `<p class="nothing-found">${data.message}</p>`;

				self._searchResult.insertAdjacentHTML('afterbegin', markup);
			}
		},

		getPostsLayout: function (posts, title, keyword) {
			var markup = '',
				regExp = new RegExp(keyword, "gi");

			if (posts.length > 0) {

				markup += `<div class="search-post">`;
				markup += `<span class="title-search-label">${title}</span>`;
				markup += `<ul>`;

				posts.forEach(function (post) {
					post.title = post.title.replace(regExp, function (match) {
						return `<mark>${match}</mark>`;
					});

					markup += `
						<li>
							<a href="${post.url}">
								<div class="post-title">
									${post.title}
								</div>
							</a>
						</li>`;
				});

				markup += `</ul></div>`;
			}

			return markup;
		},

		loadMore: function (_button, paged, post_type, keyword) {

			$.ajax({
				url: theme_params.ajax_url,
				method: "POST",
				dataType: 'json',
				data: {
					action: 'theme_ajax',
					ajax_action: 'search_load_more',
					keyword: keyword,
					paged: paged,
					post_type: post_type
				},
				beforeSend: function () {
					//self._searchResult.classList.add('loading');
				},
				complete: function () {
					//self._searchResult.classList.remove('loading');
				},
				success: function (response) {
					//console.log(response.data);

					if (response.data.post_count > 0) {
						var loopPosts = _button.closest('.search-result').querySelector('.loop-posts');

						loopPosts.insertAdjacentHTML('beforeend', response.data.posts_html);

						grid.resizeGrid($(loopPosts));

						if (response.data.next_paged !== '') {
							_button.dataset.paged = response.data.next_paged;
						} else {
							_button.parentNode.remove();
						}
					} else {
						_button.parentNode.remove();
					}
				},
			});

		}

	};

	var loadMore = {

		init: function () {
			var self = this;

			self.posts = '.loop-posts';

			self.pagination = '.load-more-pagination';

			$(document.body).on('click', '.load-more-pagination .load-more-button', function (e) {
				e.preventDefault();
				self.loadPosts(this);
			});

		},

		loadPosts: function (_button) {
			var self = loadMore,
				loadMoreUrl = _button.getAttribute('href'),
				$posts = $(self.posts),
				$pagination = $(self.pagination);

			if (loadMoreUrl === '#') {
				return;
			}

			if ($pagination.hasClass('loading')) {
				return;
			}

			$.ajax({
				method: "GET",
				url: updateQueryStringParameter(loadMoreUrl, '_ajax', 1),
				beforeSend: function () {
					$pagination.addClass('loading');
				},
				complete: function () {
					$pagination.removeClass('loading');
				}
			}).done(function (response) {
				var $html = $('<div>' + response + '</div>'),
					$parsedPosts = $html.find(self.posts).children();

				$pagination.html($html.find(self.pagination).html());

				$.each($parsedPosts, function (key, _post) {
					$posts.append(_post);

					//grid.resizeGridItem($(_post));

					/*gsap.set(_post, {opacity: 0, scale: 0});
                    gsap.to(_post, {duration: 0.4, opacity: 1, scale: 1});*/
				});

				history.replaceState(false, $html.find('#wp_title').text(), loadMoreUrl);
				document.title = $html.find('#wp_title').text()

				self._paginationBtn = document.querySelector(self.pagination);
				$(document.body).trigger('inspiration_load_more_posts_loaded', self._paginationBtn);
			});
		},

	};

	var shareButton = {
		init: function () {
			var self = this;

			self.windowArgs = 'toolbar=no,scrollbars=yes,resizable=yes,width=700,height=700'

			$('.entry-share .share-buttons').on('click', 'a.btn-share', self.shareButtonHandler);
			$('.entry-container').on('click', 'a.btn-share-pinterest', self.sharePinterestButtonHandler);

			self.addPinterestBtn();
		},

		shareButtonHandler: function (e) {
			var self = shareButton, url,
				_this = this,
				_share = _this.parentNode;

			e.preventDefault();

			switch (_this.dataset.type) {
				case 'facebook':
					url = 'https://www.facebook.com/sharer/?u=' + encodeURIComponent(_share.dataset.permalink);
					window.open(url, '_blank', self.windowArgs);
					break;
				case 'pinterest':
					url = 'https://pinterest.com/pin/create/bookmarklet/?is_video=false&media=' + encodeURIComponent(_share.dataset.thumbnail) + '&url=' + encodeURIComponent(_share.dataset.permalink) + '&description=' + encodeURIComponent(_share.dataset.title);
					window.open(url, '_blank', self.windowArgs);
					break;
				case 'twitter':
					url = 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(_share.dataset.title) + '&url=' + encodeURIComponent(_share.dataset.permalink);
					window.open(url, '_blank', self.windowArgs);
					break;
				case 'linkedin':
					var imgUrl = $('.woocommerce-product-gallery__image img').attr('src');
					url = 'https://www.linkedin.com/shareArticle/?mini=true&title=' + encodeURIComponent(_share.dataset.title) + '&url=' + encodeURIComponent(_share.dataset.permalink);
					window.open(url, '_blank', self.windowArgs);
					break;
				case 'email':
					url = 'mailto:?subject=' + encodeURIComponent(_share.dataset.title) + '&body=' + encodeURIComponent('Your message...\n\n\n Read more: ' + _share.dataset.permalink);
					window.open(url, '_blank');
					break;
				case 'print':
					window.print();
					break;
				case 'copy':
					self.copyToBuffer(_this, _share);
					break;
			}
		},

		sharePinterestButtonHandler: function (e) {
			var self = shareButton;

			e.preventDefault();
			window.open(this.href, '_blank', self.windowArgs);
		},

		addPinterestBtn: function () {
			if (!document.body.classList.contains('single-post')) {
				return;
			}

			var _post = document.querySelector('article#post-item'),
				images = document.querySelectorAll('.entry-content img');

			images.forEach(function (image, key) {
				var url, button;

				//console.log(image.getBoundingClientRect())

				if (image.className.indexOf('wp') === -1) {
					return;
				}

				if (image.dataset.pinterest === 'false') {
					return;
				}

				url = 'https://pinterest.com/pin/create/bookmarklet/?is_video=false';
				url += '&url=' + _post.dataset.permalink;
				url += '&media=' + image.src;
				url += '&description=' + _post.dataset.title;

				button = '<a href="' + url + '" class="btn-share btn-share-pinterest" data-type="pinterest"><i class="fab fa-pinterest"></i></a>';

				image.insertAdjacentHTML('afterend', button);
			});
		},

		copyToBuffer: function (_this, _share) {
			var input = document.createElement('input');

			input.type = 'text';
			input.class = 'hidden';
			input.value = _share.dataset.permalink;

			//document.body.appendChild(input);

			/* Select the text field */
			input.focus();
			input.select();
			input.setSelectionRange(0, 99999); /* For mobile devices */

			/* Copy the text inside the text field */
			navigator.clipboard.writeText(input.value);

			_this.classList.add('copied');

			input.remove();

			setTimeout(function () {
				_this.classList.remove('copied');
			}, 500)
		}
	};

	var mailchimpEmbed = {

		init: function () {
			var self = this;

			self.mailchimp = 'form#mc-embedded-subscribe-form';
			self.fnames = [];
			self.ftypes = [];

			self.fnames[1] = "FNAME";
			self.ftypes[1] = "text";
			self.fnames[0] = "EMAIL";
			self.ftypes[0] = "email";

			$(document.body).on('submit', self.mailchimp, self.submitHandler);

		},

		submitHandler: function (e) {
			var self = mailchimpEmbed,
				$form = $(this),
				errors = 0;

			e.preventDefault();

			$('#mce-success-response').hide();
			$('#mce-error-response').hide();

			$form.find('.mc-field-group.invalid').removeClass('invalid');
			$form.find('.mce_inline_error').remove();

			self.fnames.forEach(function (name) {
				var $input = $form.find('[name="' + name + '"]');

				if (self.validateInput($input)) {
					$input.closest('.mc-field-group').addClass('invalid');
					errors++;
				}
			});

			if (errors > 0) {
				return;
			}

			$.ajax({
				url: self.getAjaxSubmitUrl($form),
				type: 'GET',
				dataType: 'json',
				contentType: "application/json; charset=utf-8",
				data: $form.serializeArray(),
				success: function (response) {
					self.onSuccessCallback(response, $form)
				}
			});
		},

		getAjaxSubmitUrl: function ($form) {
			var url = $form.attr("action");
			url = url.replace("/post?u=", "/post-json?u=");
			url += "&c=?";
			return url;
		},

		onSuccessCallback: function (resp, $form) {
			var self = mailchimpEmbed;

			// On successful form submission, display a success message and reset the form
			if (resp.result === "success") {
				$('#mce-' + resp.result + '-response').show();
				$('#mce-' + resp.result + '-response').html(resp.msg);
				$('#mc-embedded-subscribe-form').each(function () {
					this.reset();
				});

				// If the form has errors, display them, inline if possible, or appended to #mce-error-response
			} else {
				if (resp.msg === "captcha") {
					var url = $("form#mc-embedded-subscribe-form").attr("action");
					var parameters = $.param(resp.params);
					url = url.split("?")[0];
					url += "?";
					url += parameters;
					window.open(url);
				}

				// Example errors - Note: You only get one back at a time even if you submit several that are bad.
				// Error structure - number indicates the index of the merge field that was invalid, then details
				// Object {result: "error", msg: "6 - Please enter the date"}
				// Object {result: "error", msg: "4 - Please enter a value"}
				// Object {result: "error", msg: "9 - Please enter a complete address"}

				// Try to parse the error into a field index and a message.
				// On failure, just put the dump thing into in the msg variable.
				var index = -1;
				var msg;
				try {
					var parts = resp.msg.split(' - ', 2);
					if (parts[1] == undefined) {
						msg = resp.msg;
					} else {
						const i = parseInt(parts[0]);
						if (i.toString() == parts[0]) {
							index = parts[0];
							msg = parts[1];
						} else {
							index = -1;
							msg = resp.msg;
						}
					}
				} catch (e) {
					index = -1;
					msg = resp.msg;
				}

				try {
					// If index is -1 if means we don't have data on specifically which field was invalid.
					// Just lump the error message into the generic response div.
					if (index === -1) {
						$('#mce-' + resp.result + '-response').show();
						$('#mce-' + resp.result + '-response').html(msg);

					} else {
						var fieldName = $("input[name*='" + self.fnames[index] + "']").attr('name'); // Make sure this exists (they haven't deleted the fnames array lookup)
						var data = {};
						data[fieldName] = msg;

						$.each(data, function (name, message) {
							$form.find('[name="' + name + '"]').closest('.mc-field-group').addClass('invalid');
							$form.find('[name="' + name + '"]').closest('.mc-field-group').append('<div class="mce_inline_error">' + message + '</div>');
						});
					}
				} catch (e) {
					$('#mce-' + resp.result + '-response').show();
					$('#mce-' + resp.result + '-response').html(msg);
				}
			}
		},

		validateInput: function ($input) {
			var self = this;

			if ($input.attr('aria-required') !== 'true') {
				return false;
			}

			if ($input.val() === '') {
				return true;
			}

			if ($input.attr('type') === 'email' && !self.validateEmail($input.val())) {
				return true;
			}

			return false;
		},

		validateEmail: function (email) {
			const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}

	};

	$(document).ready(function () {
		main.init();
	});

	$(document.body).CustomResize({
		resize: function () {
			main.setCssVariables();
		},
		timeout: 200
	});

}(jQuery));