/**
 * detect mobile devices function
 * @returns {boolean}
 */
function is_mobile() {
	return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
}

window.theme = window.theme || {};

/**
 * Bind on resize window
 */
(function ($) {
	$.fn.CustomResize = function (settings) {
		var timer = 0;

		settings = $.extend({
			resize: function () {
			},
			timeout: false
		}, settings);

		function is_mobile() {
			return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
		}

		function resize() {
			if (settings.timeout) {
				clearTimeout(timer);

				timer = setTimeout(function () {
					settings.resize();
				}, settings.timeout);
			} else {
				settings.resize();
			}
		}

		if (is_mobile()) {
			if (typeof (screen.orientation) !== 'undefined') {
				screen.orientation.addEventListener('change', resize, {passive: true});
			} else {
				window.addEventListener('orientationchange', resize, {passive: true});
			}
		} else {
			window.addEventListener('resize', resize, {passive: true});
		}

	};

	var modalHistory = {
		modalHistory: [],
		add: function (id, type) {
			this.modalHistory.push({id: id, type: type});
		},
		get: function (id) {
			return this.modalHistory.findIndex(function (val) {
				return val.id === id;
			})
		},
		delete: function (id) {
			this.modalHistory.splice(this.get(id), 1);
		},
		getLast: function () {
			return this.modalHistory[this.modalHistory.length - 1];
		}
	};

	var modal = {

		init: function () {
			$(document.body).on('click', '[data-toggle="modal"]', function (e) {
				e.preventDefault();
				modal.showModal(this.getAttribute('data-target'), this);
			});
		},

		showModal: function (targetId, _button) {
			if (targetId === null) {
				return false;
			}

			var $target = $(targetId);

			if ($target.length === 0) {
				return false;
			}

			document.body.classList.add('modal-open');
			$target.addClass('show');

			$target.trigger($.Event('show_modal', {
				relatedTarget: _button
			}));

			$(document.body).append('<div class="modal-backdrop fade"></div>');
			var $overlay = $('.modal-backdrop');

			modalHistory.add(targetId, 'modal');

			setTimeout(function () {
				$target.addClass('slide');
				$overlay.addClass('show');

				$target.one(transitionEvent, function () {
					$target.trigger($.Event('shown_modal', {
						relatedTarget: _button
					}));
				});
			}, 100);

			$target.find('[data-dismiss="modal"]').one('click', function (e) {
				e.preventDefault();
				modal.hideModal(targetId, _button);
			});

			$target.on('click', function (e) {
				if ($(e.target).closest('.modal-content').length === 0) {
					modal.hideModal(targetId, _button);
				}
			});

			$(window).on('keyup.modal', function (e) {
				var findModal = modalHistory.getLast();

				if (e.keyCode === 27 && findModal.type === 'modal') {
					$(window).off('keyup.modal');
					modal.hideModal(findModal.id, _button);
				}
			});
		},

		hideModal: function (targetId, _button) {
			if (targetId === null) {
				return false;
			}

			var $target = $(targetId),
				$overlay = $('.modal-backdrop').last();

			if ($target.length === 0) {
				return false;
			}

			$target.removeClass('slide');
			$target.trigger($.Event('hide_modal', {
				relatedTarget: _button
			}));
			$overlay.removeClass('show');

			$target.off('click');
			$target.find('[data-dismiss="modal"]').off('click');
			$(window).off('keyup.modal');
			modalHistory.delete(targetId);

			setTimeout(function () {
				$target.find('.modal-dialog').one(transitionEvent, function () {
					document.body.classList.remove('modal-open');
					$target.removeClass('show');
					$target.trigger($.Event('hidden_modal', {
						relatedTarget: _button
					}));
					$overlay.remove();
				});
			}, 100);
		}

	};

	var sidebarModal = {

		init: function () {
			$(document.body).on('click', '[data-toggle="sidebar-modal"]', function (e) {
				e.preventDefault();

				if (this.classList.contains('disabled')) {
					return;
				}

				sidebarModal.showModal(this.getAttribute('data-target'), this);
			});
		},

		showModal: function (targetId, _button) {
			if (targetId === null) {
				return false;
			}

			var $target = $(targetId);

			if ($target.length === 0) {
				return false;
			}

			if ($target.hasClass('show')) {
				sidebarModal.hideModal(targetId, _button);
				return false;
			}

			document.body.classList.add('sidebar-modal-open');
			_button.classList.add('sidebar-opened');
			$target.addClass('show');

			$target.trigger($.Event('show_sidebar_modal', {
				relatedTarget: _button
			}));

			$(document.body).append('<div class="modal-backdrop sidebar-backdrop fade"></div>');
			var $overlay = $('.modal-backdrop');

			modalHistory.add(targetId, 'sidebar-modal');

			setTimeout(function () {
				$target.addClass('slide');
				$overlay.addClass('show');

				$target.one(transitionEvent, function () {
					$target.trigger($.Event('shown_sidebar_modal', {
						relatedTarget: _button
					}));
				});
			}, 100);

			$target.find('[data-dismiss="sidebar-modal"]').one('click', function (e) {
				e.preventDefault();
				sidebarModal.hideModal(targetId, _button);
			});

			$target.on('click', function (e) {
				if ($(e.target).closest('.sidebar-modal-content').length === 0) {
					sidebarModal.hideModal(targetId, _button);
				}
			});

			$(window).on('keyup.sidebarModal', function (e) {
				var findModal = modalHistory.getLast();

				if (e.keyCode === 27 && findModal.type === 'sidebar-modal') {
					$(window).off('keyup.sidebarModal');
					sidebarModal.hideModal(findModal.id, _button);
				}
			});
		},

		hideModal: function (targetId, _button) {
			if (targetId === null) {
				return false;
			}

			var $target = $(targetId),
				$overlay = $('.modal-backdrop').last();

			if ($target.length === 0) {
				return false;
			}

			$target.removeClass('slide');
			$target.trigger('hide_sidebar_modal', {
				relatedTarget: _button
			});
			$overlay.removeClass('show');
			_button.classList.remove('sidebar-opened');

			$target.off('click');
			$target.find('[data-dismiss="sidebar-modal"]').off('click');
			$(window).off('keyup.sidebarModal');
			modalHistory.delete(targetId);

			setTimeout(function () {
				setTimeout(function () {
					//$target.one(transitionEvent, function () {
					document.body.classList.remove('sidebar-modal-open');
					$target.removeClass('show');
					$target.trigger('hidden_sidebar_modal', {
						relatedTarget: _button
					});
					$overlay.remove();
				}, 500);
			}, 100);
		}
	};

	var dropdownToggle = {
		init: function () {

			this.eventHandlers = {};
			this.dropdownToggle = '.dropdown-toggle';

			this.eventHandlers.dropdownToggle = this.dropdownToggleHandler.bind(this);
			this.eventHandlers.dropdownCloseHandler = this.dropdownCloseHandler.bind(this);

			$(document.body).on('click', this.dropdownToggle, this.eventHandlers.dropdownToggle);
			document.addEventListener('click', this.eventHandlers.dropdownCloseHandler);
		},

		dropdownToggleHandler: function (e) {
			var _this = e.currentTarget,
				_menu;

			e.preventDefault();

			_menu = document.querySelector('[aria-labelledby="' + _this.id + '"]');

			if (!_menu) {
				return;
			}

			if (document.querySelector('.dropdown-menu.show')) {
				var _openedMenu = document.querySelector('.dropdown-menu.show'),
					_openedButton = document.getElementById(_openedMenu.getAttribute('aria-labelledby'));

				if (_this.id !== _openedMenu.getAttribute('aria-labelledby')) {
					this.dropdownToggleMenu(_openedButton, _openedMenu, false);
				}
			}

			if (!_this.classList.contains('show')) {
				this.dropdownToggleMenu(_this, _menu, true);
			} else {
				this.dropdownToggleMenu(_this, _menu, false);
			}
		},

		dropdownCloseHandler: function (e) {
			if (document.querySelector('.dropdown-menu.show') && !e.target.closest('.dropdown')) {
				var _openedMenu = document.querySelector('.dropdown-menu.show'),
					_openedButton = document.getElementById(_openedMenu.getAttribute('aria-labelledby'));

				this.dropdownToggleMenu(_openedButton, _openedMenu, false);
			}
		},

		dropdownToggleMenu: function (_button, _menu, state) {
			if (!_button || !_menu) {
				return;
			}

			if (state === true) {
				var menuHeight = $(_menu).outerHeight(true), i,
					inside = _menu.children,
					menuStyles = window.getComputedStyle(_menu),
					_parentButton = _button.parentNode.parentNode,
					openedCount = document.querySelectorAll('.dropdown.show').length;

				menuHeight += parseInt(menuStyles.paddingTop);
				menuHeight += parseInt(menuStyles.paddingBottom);

				for (i = 0; i < inside.length; i++) {
					menuHeight += $(_menu.children[i]).outerHeight(true);
				}

				_menu.style.setProperty('--height', `${menuHeight}px`);
				_menu.style.setProperty('--button-height', `${$(_button).outerHeight(true)}px`);

				_parentButton.classList.add('dropdown-shown');
				_parentButton.style.setProperty('--z-index', openedCount + 1);

				_button.parentNode.classList.add('show');
				_button.parentNode.classList.add('showing');
				_button.classList.add('show');
				_menu.classList.add('show');

				if (_button.id === 'scorecard-compare-dropdown') {
					document.body.classList.add('compare-dropdown-open');
				}

				setTimeout(function () {
					_button.parentNode.classList.remove('showing');
					_parentButton.style.setProperty('--z-index', 1);
				}, 300);
			} else {
				_button.classList.remove('show');
				_menu.classList.remove('show');

				if (_button.id === 'scorecard-compare-dropdown') {
					document.body.classList.remove('compare-dropdown-open');
				}

				if (_menu.querySelector('input[type="text"]')) {
					_menu.querySelector('input[type="text"]').value = '';
					$(_menu.querySelector('input[type="text"]')).trigger('change');
				}

				setTimeout(function () {
					_button.parentNode.classList.remove('show');
					_button.parentNode.parentNode.classList.remove('dropdown-shown');
					_button.parentNode.parentNode.style.removeProperty('--z-index');
				}, 300);
			}
		}

	}

	modal.init();
	sidebarModal.init();
	dropdownToggle.init();

	theme.modal = modal;
	theme.sidebarModal = sidebarModal;
	theme.dropdownToggle = dropdownToggle;
	theme.ajax = {
		url: theme_params.ajax_url || {},

		post: function (data) {
			return theme.ajax.send(data);
		},

		send: function (data, options) {
			var promise, deferred;

			options = options || {};
			options.data = $.extend({
				action: 'theme_ajax'
			}, data);

			options = $.extend(options, {
				type: 'POST',
				url: theme.ajax.url,
				//dataType: 'json',
				context: this
			});

			deferred = $.Deferred(function (deferred) {
				// Transfer success/error callbacks.
				if (options.success) {
					deferred.done(options.success);
				}

				if (options.error) {
					deferred.fail(options.error);
				}

				delete options.success;
				delete options.error;

				// Use with PHP's wp_send_json_success() and wp_send_json_error().
				deferred.jqXHR = $.ajax(options).done(function (response) {
					if (typeof (response) === 'object' && typeof (response.success) !== 'undefined') {
						deferred[response.success ? 'resolveWith' : 'rejectWith'](this, [response.data]);
					} else {
						deferred.rejectWith(this, [response]);
					}
				}).fail(function () {
					deferred.rejectWith(this, arguments);
				});
			});

			promise = deferred.promise();
			promise.abort = function () {
				deferred.jqXHR.abort();
				return this;
			};

			return promise;
		}
	};

	$.fn.scrollBar = function () {

		this._scrollWrap = this.get(0);
		this._scrollInner = this._scrollWrap.querySelector('.scrollable');
		this._scrollContent = this._scrollWrap.querySelector('.scrollable-content');
		this.scrollIndicatorBar = this._scrollWrap.querySelector('.sc-scroll-indicator-bar');
		this._scrollNavPrev = this._scrollWrap.querySelector('.sc-scroll-nav-prev');
		this._scrollNavNext = this._scrollWrap.querySelector('.sc-scroll-nav-next');

		this.scrollBarWidth = 1;
		this.scrollBarOffset = 0;
		this.mouseDraggeble = true;
		this.mouseShiftX = 0;
		this.eventHandlers = {};

		this.init = function () {
			this.calculate();

			this.eventHandlers.scrollHandler = this.scrollHandler.bind(this);
			this.eventHandlers.calculate = this.calculate.bind(this);
			this.eventHandlers.mouseDownHandler = this.mouseDownHandler.bind(this);
			this.eventHandlers.mouseUpHandler = this.mouseUpHandler.bind(this);
			this.eventHandlers.mouseMoveHandler = this.mouseMoveHandler.bind(this);
			this.eventHandlers.onDragStartHandler = this.onDragStartHandler.bind(this);
			this.eventHandlers.onNavClickHandler = this.onNavClickHandler.bind(this);

			this._scrollInner.addEventListener('scroll', this.eventHandlers.scrollHandler, {passive: true});
			window.addEventListener('resize', this.eventHandlers.calculate, {passive: true});

			this._scrollNavPrev.addEventListener('click', this.eventHandlers.onNavClickHandler);
			this._scrollNavNext.addEventListener('click', this.eventHandlers.onNavClickHandler);

			if (this.mouseDraggeble) {
				this.scrollIndicatorBar.addEventListener('mousedown', this.eventHandlers.mouseDownHandler);
				this.scrollIndicatorBar.addEventListener('ondragstart', this.eventHandlers.onDragStartHandler);
			}

			return this;
		}

		this.calculate = function () {
			this._scrollWrap.parentElement.style
				.setProperty('--width', this._scrollWrap.parentElement.clientWidth + "px");

			this.scrollBarWidth = this._scrollInner.offsetWidth / this._scrollContent.offsetWidth;

			if (this.scrollBarWidth > 1) {
				this.scrollBarWidth = 1;
			}

			if ((this._scrollContent.offsetWidth - this._scrollInner.offsetWidth) <= 0) {
				this._scrollWrap.classList.remove('initialized');
				this._scrollWrap.classList.add('disabled');
			} else {
				this._scrollWrap.classList.add('initialized');
				this._scrollWrap.classList.remove('disabled');
			}

			this.scrollClasses();
			this.setStyle();
		}

		this.setStyle = function () {
			this.scrollBarOffset = (this._scrollInner.scrollLeft / this._scrollInner.offsetWidth) * 100;
			this.scrollIndicatorBar.style.transform = `scaleX(${this.scrollBarWidth}) translateX(${this.scrollBarOffset}%)`;
		}

		this.scrollClasses = function () {

			if (this._scrollInner.scrollLeft <= 0) {
				this._scrollNavPrev.classList.add('sc-scroll-nav-disabled');
			} else {
				this._scrollNavPrev.classList.remove('sc-scroll-nav-disabled');
			}

			if (this._scrollContent.offsetWidth <= Math.round(this._scrollInner.scrollLeft + this._scrollInner.offsetWidth)) {
				this._scrollNavNext.classList.add('sc-scroll-nav-disabled');
			} else {
				this._scrollNavNext.classList.remove('sc-scroll-nav-disabled');
			}

			if (this._scrollInner.scrollLeft <= 10) {
				this._scrollWrap.classList.add('scroll-start');
			} else {
				this._scrollWrap.classList.remove('scroll-start');
			}

			if (this._scrollContent.offsetWidth - 10 <= Math.round(this._scrollInner.scrollLeft + this._scrollInner.offsetWidth)) {
				this._scrollWrap.classList.add('scroll-end');
			} else {
				this._scrollWrap.classList.remove('scroll-end');
			}
		}

		this.scrollHandler = function () {
			this.scrollClasses();
			this.setStyle();
		}

		this.onNavClickHandler = function (e) {
			var navItem = e.currentTarget,
				step = this._scrollWrap.clientWidth / 5;

			e.preventDefault();

			if (navItem.classList.contains('sc-scroll-nav-disabled')) {
				return;
			}

			if (navItem.classList.contains('sc-scroll-nav-prev')) {
				this._scrollInner.scrollTo({
					top: 0,
					left: this._scrollInner.scrollLeft - step,
					behavior: 'smooth'
				});
			} else if (navItem.classList.contains('sc-scroll-nav-next')) {
				this._scrollInner.scrollTo({
					top: 0,
					left: this._scrollInner.scrollLeft + step,
					behavior: 'smooth'
				});
			}
		}

		this.mouseDownHandler = function (e) {
			//view-source:https://en.js.cx/task/slider/solution/
			e.preventDefault(); // prevent selection start (browser action)

			this.mouseShiftX = e.clientX - this._scrollInner.scrollLeft;

			this.scrollIndicatorBar.classList.add('sc-scroll-indicator-dragging');

			document.addEventListener('mousemove', this.eventHandlers.mouseMoveHandler);
			document.addEventListener('mouseup', this.eventHandlers.mouseUpHandler);
		}

		this.mouseMoveHandler = function (e) {
			var mouseShiftXLeft = e.clientX - this.mouseShiftX - this._scrollInner.scrollLeft;

			this._scrollInner.scrollLeft = this._scrollInner.scrollLeft + mouseShiftXLeft;
		}

		this.mouseUpHandler = function (e) {
			this.scrollIndicatorBar.classList.remove('sc-scroll-indicator-dragging');

			document.removeEventListener('mouseup', this.eventHandlers.mouseUpHandler);
			document.removeEventListener('mousemove', this.eventHandlers.mouseMoveHandler);
		}

		this.onDragStartHandler = function (e) {
			return false;
		}

		this.destroy = function () {
			this._scrollInner.removeEventListener('scroll', this.eventHandlers.scrollHandler);
			window.removeEventListener('resize', this.eventHandlers.calculate);
			this._scrollWrap.classList.remove('initialized');
			this._scrollWrap.classList.add('disabled');
		}

		this.reInit = function () {
			this._scrollWrap.classList.add('initialized');
			this._scrollWrap.classList.remove('disabled');

			this.calculate();
			this._scrollInner.addEventListener('scroll', this.eventHandlers.scrollHandler);
			window.addEventListener('resize', this.eventHandlers.calculate);
		}

		return this.init();
	};

})(jQuery);

/**
 * Functions from David Walsh: http://davidwalsh.name/css-animation-callback
 */
function whichTransitionEvent() {
	var t,
		el = document.createElement("fakeelement");

	var transitions = {
		"transition": "transitionend",
		"OTransition": "oTransitionEnd",
		"MozTransition": "transitionend",
		"WebkitTransition": "webkitTransitionEnd"
	}

	for (t in transitions) {
		if (el.style[t] !== undefined) {
			return transitions[t];
		}
	}
}

function whichAnimationEvent(type) {
	var t,
		el = document.createElement("fakeelement");

	var animations = {
		"animation": "animationend",
		"OAnimation": "oAnimationEnd",
		"MozAnimation": "animationend",
		"WebkitAnimation": "webkitAnimationEnd"
	};

	for (t in animations) {
		if (el.style[t] !== undefined) {
			return animations[t];
		}
	}
}

var transitionEvent = whichTransitionEvent();
var animationEvent = whichAnimationEvent();

function setCssStyles(el, styles) {
	var keys = {}, i, prop;

	if (typeof el[0] !== 'undefined') {
		el = el.get(0);
	}

	keys = Object.keys(styles);

	for (i = 0; i < keys.length; i++) {
		prop = keys[i];

		if (prop === 'transition' || prop === 'transform') {
			el.style['-webkit-' + prop] = styles[prop];
			el.style['-ms-' + prop] = styles[prop];
			el.style['-moz-' + prop] = styles[prop];
			el.style['-o-' + prop] = styles[prop];
		}

		el.style[prop] = styles[prop];
	}
}

function getQueryStringParameter(uri, key) {
	if (uri.indexOf('?') === -1) {
		return false;
	}

	var params = uri.split("?")[1].split("&");
	for (var i = 0; i < params.length; i = i + 1) {
		var singleParam = params[i].split("=");
		if (singleParam[0] === key) {
			return singleParam[1];
		}
	}
}

function updateQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	} else {
		return uri + separator + key + "=" + value;
	}
}

function updateQueryStringParameters(uri, parameters) {
	if (typeof (parameters) !== 'object') {
		return uri;
	}

	for (const [key, value] of Object.entries(parameters)) {
		uri = updateQueryStringParameter(uri, key, value)
	}

	return uri;
}

function removeURLParameter(url, key) {
	//prefer to use l.search if you have a location/link object
	var urlparts = url.split('?');
	if (urlparts.length >= 2) {

		var prefix = encodeURIComponent(key) + '=';
		var pars = urlparts[1].split(/[&;]/g);

		//reverse iteration as may be destructive
		for (var i = pars.length; i-- > 0;) {
			//idiom for string.startsWith
			if (pars[i].lastIndexOf(prefix, 0) !== -1) {
				pars.splice(i, 1);
			}
		}

		url = urlparts[0] + '?' + pars.join('&');
		return url;
	} else {
		return url;
	}
}

function parseQueryStringParameter(queryString) {
	// we'll store the parameters here
	var obj = {};

	if (queryString.length === 0) {
		return '';
	}

	var arr = queryString.split('&');

	for (var i = 0; i < arr.length; i++) {
		// separate the keys and the values
		var a = arr[i].split('=');

		// set parameter name and value (use 'true' if empty)
		var paramName = a[0];
		var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

		// (optional) keep case consistent
		paramName = paramName.toLowerCase();
		if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

		// if the paramName ends with square brackets, e.g. colors[] or colors[2]
		if (paramName.match(/\[(\d+)?\]$/)) {

			// create key if it doesn't exist
			var key = paramName.replace(/\[(\d+)?\]/, '');
			if (!obj[key]) obj[key] = [];

			// if it's an indexed array e.g. colors[2]
			if (paramName.match(/\[\d+\]$/)) {
				// get the index value and add the entry at the appropriate position
				var index = /\[(\d+)\]/.exec(paramName)[1];
				obj[key][index] = paramValue;
			} else {
				// otherwise add the value to the end of the array
				obj[key].push(paramValue);
			}
		} else {
			// we're dealing with a string
			if (!obj[paramName]) {
				// if it doesn't exist, create property
				obj[paramName] = paramValue;
			} else if (obj[paramName] && typeof obj[paramName] === 'string') {
				// if property does exist and it's a string, convert it to an array
				obj[paramName] = [obj[paramName]];
				obj[paramName].push(paramValue);
			} else {
				// otherwise add the property
				obj[paramName].push(paramValue);
			}
		}
	}

	return obj;
}

function isElementInViewport(element) {
	const rect = element.getBoundingClientRect();

	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		rect.right <= (window.innerWidth || document.documentElement.clientWidth)
	);
}

function isElementPartInViewport(element, shiftTop = 0) {
    const rect = element.getBoundingClientRect(),
        windowHeight = (window.innerHeight || document.documentElement.clientHeight),
        percentVisible = 0;

    if (!shiftTop) {
        shiftTop = 0;
    }

    return !(
        Math.floor(100 - ((((rect.top - shiftTop) >= 0 ? 0 : (rect.top - shiftTop)) / +-rect.height) * 100)) < percentVisible ||
        Math.floor(100 - (((rect.bottom - shiftTop) - windowHeight) / rect.height) * 100) < percentVisible
    );

    /*return (
        rect.top >= -windowHeight &&
        rect.left >= -element.offsetWidth &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) + element.offsetHeight &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) + element.offsetWidth
    );*/
}

/**
 * js-cookie v3.0.5 | MIT
 */
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):(e="undefined"!=typeof globalThis?globalThis:e||self,function(){var n=e.Cookies,o=e.Cookies=t();o.noConflict=function(){return e.Cookies=n,o}}())}(this,(function(){"use strict";function e(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var o in n)e[o]=n[o]}return e}var t=function t(n,o){function r(t,r,i){if("undefined"!=typeof document){"number"==typeof(i=e({},o,i)).expires&&(i.expires=new Date(Date.now()+864e5*i.expires)),i.expires&&(i.expires=i.expires.toUTCString()),t=encodeURIComponent(t).replace(/%(2[346B]|5E|60|7C)/g,decodeURIComponent).replace(/[()]/g,escape);var c="";for(var u in i)i[u]&&(c+="; "+u,!0!==i[u]&&(c+="="+i[u].split(";")[0]));return document.cookie=t+"="+n.write(r,t)+c}}return Object.create({set:r,get:function(e){if("undefined"!=typeof document&&(!arguments.length||e)){for(var t=document.cookie?document.cookie.split("; "):[],o={},r=0;r<t.length;r++){var i=t[r].split("="),c=i.slice(1).join("=");try{var u=decodeURIComponent(i[0]);if(o[u]=n.read(c,u),e===u)break}catch(e){}}return e?o[e]:o}},remove:function(t,n){r(t,"",e({},n,{expires:-1}))},withAttributes:function(n){return t(this.converter,e({},this.attributes,n))},withConverter:function(n){return t(e({},this.converter,n),this.attributes)}},{attributes:{value:Object.freeze(o)},converter:{value:Object.freeze(n)}})}({read:function(e){return'"'===e[0]&&(e=e.slice(1,-1)),e.replace(/(%[\dA-F]{2})+/gi,decodeURIComponent)},write:function(e){return encodeURIComponent(e).replace(/%(2[346BF]|3[AC-F]|40|5[BDE]|60|7[BCD])/g,decodeURIComponent)}},{path:"/"});return t}));
/**!
 * imagesLoaded PACKAGED v5.0.0
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
!function(t,e){"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}("undefined"!=typeof window?window:this,(function(){function t(){}let e=t.prototype;return e.on=function(t,e){if(!t||!e)return this;let i=this._events=this._events||{},s=i[t]=i[t]||[];return s.includes(e)||s.push(e),this},e.once=function(t,e){if(!t||!e)return this;this.on(t,e);let i=this._onceEvents=this._onceEvents||{};return(i[t]=i[t]||{})[e]=!0,this},e.off=function(t,e){let i=this._events&&this._events[t];if(!i||!i.length)return this;let s=i.indexOf(e);return-1!=s&&i.splice(s,1),this},e.emitEvent=function(t,e){let i=this._events&&this._events[t];if(!i||!i.length)return this;i=i.slice(0),e=e||[];let s=this._onceEvents&&this._onceEvents[t];for(let n of i){s&&s[n]&&(this.off(t,n),delete s[n]),n.apply(this,e)}return this},e.allOff=function(){return delete this._events,delete this._onceEvents,this},t})),
/**!
 * imagesLoaded v5.0.0
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
function(t,e){"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter")):t.imagesLoaded=e(t,t.EvEmitter)}("undefined"!=typeof window?window:this,(function(t,e){let i=t.jQuery,s=t.console;function n(t,e,o){if(!(this instanceof n))return new n(t,e,o);let r=t;var h;("string"==typeof t&&(r=document.querySelectorAll(t)),r)?(this.elements=(h=r,Array.isArray(h)?h:"object"==typeof h&&"number"==typeof h.length?[...h]:[h]),this.options={},"function"==typeof e?o=e:Object.assign(this.options,e),o&&this.on("always",o),this.getImages(),i&&(this.jqDeferred=new i.Deferred),setTimeout(this.check.bind(this))):s.error(`Bad element for imagesLoaded ${r||t}`)}n.prototype=Object.create(e.prototype),n.prototype.getImages=function(){this.images=[],this.elements.forEach(this.addElementImages,this)};const o=[1,9,11];n.prototype.addElementImages=function(t){"IMG"===t.nodeName&&this.addImage(t),!0===this.options.background&&this.addElementBackgroundImages(t);let{nodeType:e}=t;if(!e||!o.includes(e))return;let i=t.querySelectorAll("img");for(let t of i)this.addImage(t);if("string"==typeof this.options.background){let e=t.querySelectorAll(this.options.background);for(let t of e)this.addElementBackgroundImages(t)}};const r=/url\((['"])?(.*?)\1\)/gi;function h(t){this.img=t}function d(t,e){this.url=t,this.element=e,this.img=new Image}return n.prototype.addElementBackgroundImages=function(t){let e=getComputedStyle(t);if(!e)return;let i=r.exec(e.backgroundImage);for(;null!==i;){let s=i&&i[2];s&&this.addBackground(s,t),i=r.exec(e.backgroundImage)}},n.prototype.addImage=function(t){let e=new h(t);this.images.push(e)},n.prototype.addBackground=function(t,e){let i=new d(t,e);this.images.push(i)},n.prototype.check=function(){if(this.progressedCount=0,this.hasAnyBroken=!1,!this.images.length)return void this.complete();let t=(t,e,i)=>{setTimeout((()=>{this.progress(t,e,i)}))};this.images.forEach((function(e){e.once("progress",t),e.check()}))},n.prototype.progress=function(t,e,i){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!t.isLoaded,this.emitEvent("progress",[this,t,e]),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,t),this.progressedCount===this.images.length&&this.complete(),this.options.debug&&s&&s.log(`progress: ${i}`,t,e)},n.prototype.complete=function(){let t=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emitEvent(t,[this]),this.emitEvent("always",[this]),this.jqDeferred){let t=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[t](this)}},h.prototype=Object.create(e.prototype),h.prototype.check=function(){this.getIsImageComplete()?this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,this.img.crossOrigin&&(this.proxyImage.crossOrigin=this.img.crossOrigin),this.proxyImage.addEventListener("load",this),this.proxyImage.addEventListener("error",this),this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.proxyImage.src=this.img.currentSrc||this.img.src)},h.prototype.getIsImageComplete=function(){return this.img.complete&&this.img.naturalWidth},h.prototype.confirm=function(t,e){this.isLoaded=t;let{parentNode:i}=this.img,s="PICTURE"===i.nodeName?i:this.img;this.emitEvent("progress",[this,s,e])},h.prototype.handleEvent=function(t){let e="on"+t.type;this[e]&&this[e](t)},h.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},h.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},h.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this),this.proxyImage.removeEventListener("error",this),this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},d.prototype=Object.create(h.prototype),d.prototype.check=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.img.src=this.url,this.getIsImageComplete()&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},d.prototype.unbindEvents=function(){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},d.prototype.confirm=function(t,e){this.isLoaded=t,this.emitEvent("progress",[this,this.element,e])},n.makeJQueryPlugin=function(e){(e=e||t.jQuery)&&(i=e,i.fn.imagesLoaded=function(t,e){return new n(this,t,e).jqDeferred.promise(i(this))})},n.makeJQueryPlugin(),n}));
/**
 * Resize sensor for Sticky Sidebar
 */
!function(e,t){"function"==typeof define&&define.amd?define(t):"object"==typeof exports?module.exports=t():e.ResizeSensor=t()}("undefined"!=typeof window?window:this,function(){if("undefined"==typeof window)return null;var t="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")(),z=t.requestAnimationFrame||t.mozRequestAnimationFrame||t.webkitRequestAnimationFrame||function(e){return t.setTimeout(e,20)},o=t.cancelAnimationFrame||t.mozCancelAnimationFrame||t.webkitCancelAnimationFrame||function(e){t.clearTimeout(e)};function r(e,t){var n=Object.prototype.toString.call(e),n="[object Array]"===n||"[object NodeList]"===n||"[object HTMLCollection]"===n||"[object Object]"===n||"undefined"!=typeof jQuery&&e instanceof jQuery||"undefined"!=typeof Elements&&e instanceof Elements,i=0,o=e.length;if(n)for(;i<o;i++)t(e[i]);else t(e)}function v(e){if(!e.getBoundingClientRect)return{width:e.offsetWidth,height:e.offsetHeight};e=e.getBoundingClientRect();return{width:Math.round(e.width),height:Math.round(e.height)}}function w(t,n){Object.keys(n).forEach(function(e){t.style[e]=n[e]})}var n,s=function(t,n){var p=0;function m(){var n,i,o=[];this.add=function(e){o.push(e)},this.call=function(e){for(n=0,i=o.length;n<i;n++)o[n].call(this,e)},this.remove=function(e){var t=[];for(n=0,i=o.length;n<i;n++)o[n]!==e&&t.push(o[n]);o=t},this.length=function(){return o.length}}function i(n,e){var i,o,r,t,s,d,c,a,f,h,l,u;n&&(n.resizedAttached?n.resizedAttached.add(e):(n.resizedAttached=new m,n.resizedAttached.add(e),n.resizeSensor=document.createElement("div"),n.resizeSensor.dir="ltr",n.resizeSensor.className="resize-sensor",u={pointerEvents:"none",position:"absolute",left:"0px",top:"0px",right:"0px",bottom:"0px",overflow:"hidden",zIndex:"-1",visibility:"hidden",maxWidth:"100%"},e={position:"absolute",left:"0px",top:"0px",transition:"0s"},w(n.resizeSensor,u),(i=document.createElement("div")).className="resize-sensor-expand",w(i,u),w(o=document.createElement("div"),e),i.appendChild(o),(r=document.createElement("div")).className="resize-sensor-shrink",w(r,u),w(u=document.createElement("div"),e),w(u,{width:"200%",height:"200%"}),r.appendChild(u),n.resizeSensor.appendChild(i),n.resizeSensor.appendChild(r),n.appendChild(n.resizeSensor),"absolute"!==(u=(e=window.getComputedStyle(n))?e.getPropertyValue("position"):null)&&"relative"!==u&&"fixed"!==u&&"sticky"!==u&&(n.style.position="relative"),t=!1,s=0,d=v(n),f=!(a=c=0),p=0,h=function(){if(f){if(0===n.offsetWidth&&0===n.offsetHeight)return void(p=p||z(function(){p=0,h()}));f=!1}var e,t;e=n.offsetWidth,t=n.offsetHeight,o.style.width=e+10+"px",o.style.height=t+10+"px",i.scrollLeft=e+10,i.scrollTop=t+10,r.scrollLeft=e+10,r.scrollTop=t+10},n.resizeSensor.resetSensor=h,l=function(){s=0,t&&(c=d.width,a=d.height,n.resizedAttached&&n.resizedAttached.call(d))},(e=function(e,t,n){e.attachEvent?e.attachEvent("on"+t,n):e.addEventListener(t,n)})(i,"scroll",u=function(){d=v(n),(t=d.width!==c||d.height!==a)&&!s&&(s=z(l)),h()}),e(r,"scroll",u),p=z(function(){p=0,h()})))}r(t,function(e){i(e,n)}),this.detach=function(e){p&&(o(p),p=0),s.detach(t,e)},this.reset=function(){t.resizeSensor.resetSensor&&t.resizeSensor.resetSensor()}};return s.reset=function(t){r(t,function(e){t.resizeSensor.resetSensor&&e.resizeSensor.resetSensor()})},s.detach=function(e,t){r(e,function(e){e&&(e.resizedAttached&&"function"==typeof t&&(e.resizedAttached.remove(t),e.resizedAttached.length())||e.resizeSensor&&(e.contains(e.resizeSensor)&&e.removeChild(e.resizeSensor),delete e.resizeSensor,delete e.resizedAttached))})},"undefined"!=typeof MutationObserver&&(n=new MutationObserver(function(e){for(var t in e)if(e.hasOwnProperty(t))for(var n=e[t].addedNodes,i=0;i<n.length;i++)n[i].resizeSensor&&s.reset(n[i])}),document.addEventListener("DOMContentLoaded",function(e){n.observe(document.body,{childList:!0,subtree:!0})})),s});
/**
 * sticky-sidebar - A JavaScript plugin for making smart and high performance.
 * @version v3.3.1
 * @link https://github.com/abouolia/sticky-sidebar
 * @author Ahmed Bouhuolia
 * @license The MIT License (MIT)
 **/
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):t.StickySidebar=e()}(this,function(){"use strict";"undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self&&self;function t(t){return t&&t.__esModule&&Object.prototype.hasOwnProperty.call(t,"default")?t.default:t}function e(t,e){return t(e={exports:{}},e.exports),e.exports}var i=e(function(t,e){(function(t){Object.defineProperty(t,"__esModule",{value:!0});var l,n,e=function(){function n(t,e){for(var i=0;i<e.length;i++){var n=e[i];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(t,n.key,n)}}return function(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}}(),i=(l=".stickySidebar",n={topSpacing:0,bottomSpacing:0,containerSelector:!1,innerWrapperSelector:".inner-wrapper-sticky",stickyClass:"is-affixed",resizeSensor:!0,minWidth:!1},function(){function c(t){var e=this,i=1<arguments.length&&void 0!==arguments[1]?arguments[1]:{};if(function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,c),this.options=c.extend(n,i),this.sidebar="string"==typeof t?document.querySelector(t):t,void 0===this.sidebar)throw new Error("There is no specific sidebar element.");this.sidebarInner=!1,this.container=this.sidebar.parentElement,this.affixedType="STATIC",this.direction="down",this.support={transform:!1,transform3d:!1},this._initialized=!1,this._reStyle=!1,this._breakpoint=!1,this.dimensions={translateY:0,maxTranslateY:0,topSpacing:0,lastTopSpacing:0,bottomSpacing:0,lastBottomSpacing:0,sidebarHeight:0,sidebarWidth:0,containerTop:0,containerHeight:0,viewportHeight:0,viewportTop:0,lastViewportTop:0},["handleEvent"].forEach(function(t){e[t]=e[t].bind(e)}),this.initialize()}return e(c,[{key:"initialize",value:function(){var i=this;if(this._setSupportFeatures(),this.options.innerWrapperSelector&&(this.sidebarInner=this.sidebar.querySelector(this.options.innerWrapperSelector),null===this.sidebarInner&&(this.sidebarInner=!1)),!this.sidebarInner){var t=document.createElement("div");for(t.setAttribute("class","inner-wrapper-sticky"),this.sidebar.appendChild(t);this.sidebar.firstChild!=t;)t.appendChild(this.sidebar.firstChild);this.sidebarInner=this.sidebar.querySelector(".inner-wrapper-sticky")}if(this.options.containerSelector){var e=document.querySelectorAll(this.options.containerSelector);if((e=Array.prototype.slice.call(e)).forEach(function(t,e){t.contains(i.sidebar)&&(i.container=t)}),!e.length)throw new Error("The container does not contains on the sidebar.")}"function"!=typeof this.options.topSpacing&&(this.options.topSpacing=parseInt(this.options.topSpacing)||0),"function"!=typeof this.options.bottomSpacing&&(this.options.bottomSpacing=parseInt(this.options.bottomSpacing)||0),this._widthBreakpoint(),this.calcDimensions(),this.stickyPosition(),this.bindEvents(),this._initialized=!0}},{key:"bindEvents",value:function(){window.addEventListener("resize",this,{passive:!0,capture:!1}),window.addEventListener("scroll",this,{passive:!0,capture:!1}),this.sidebar.addEventListener("update"+l,this),this.options.resizeSensor&&"undefined"!=typeof ResizeSensor&&(new ResizeSensor(this.sidebarInner,this.handleEvent),new ResizeSensor(this.container,this.handleEvent))}},{key:"handleEvent",value:function(t){this.updateSticky(t)}},{key:"calcDimensions",value:function(){if(!this._breakpoint){var t=this.dimensions;t.containerTop=c.offsetRelative(this.container).top,t.containerHeight=this.container.clientHeight,t.containerBottom=t.containerTop+t.containerHeight,t.sidebarHeight=this.sidebarInner.offsetHeight,t.sidebarWidth=this.sidebarInner.offsetWidth,t.viewportHeight=window.innerHeight,t.maxTranslateY=t.containerHeight-t.sidebarHeight,this._calcDimensionsWithScroll()}}},{key:"_calcDimensionsWithScroll",value:function(){var t=this.dimensions;t.sidebarLeft=c.offsetRelative(this.sidebar).left,t.viewportTop=document.documentElement.scrollTop||document.body.scrollTop,t.viewportBottom=t.viewportTop+t.viewportHeight,t.viewportLeft=document.documentElement.scrollLeft||document.body.scrollLeft,t.topSpacing=this.options.topSpacing,t.bottomSpacing=this.options.bottomSpacing,"function"==typeof t.topSpacing&&(t.topSpacing=parseInt(t.topSpacing(this.sidebar))||0),"function"==typeof t.bottomSpacing&&(t.bottomSpacing=parseInt(t.bottomSpacing(this.sidebar))||0),"VIEWPORT-TOP"===this.affixedType?t.topSpacing<t.lastTopSpacing&&(t.translateY+=t.lastTopSpacing-t.topSpacing,this._reStyle=!0):"VIEWPORT-BOTTOM"===this.affixedType&&t.bottomSpacing<t.lastBottomSpacing&&(t.translateY+=t.lastBottomSpacing-t.bottomSpacing,this._reStyle=!0),t.lastTopSpacing=t.topSpacing,t.lastBottomSpacing=t.bottomSpacing}},{key:"isSidebarFitsViewport",value:function(){var t=this.dimensions,e="down"===this.scrollDirection?t.lastBottomSpacing:t.lastTopSpacing;return this.dimensions.sidebarHeight+e<this.dimensions.viewportHeight}},{key:"observeScrollDir",value:function(){var t=this.dimensions;if(t.lastViewportTop!==t.viewportTop){var e="down"===this.direction?Math.min:Math.max;t.viewportTop===e(t.viewportTop,t.lastViewportTop)&&(this.direction="down"===this.direction?"up":"down")}}},{key:"getAffixType",value:function(){this._calcDimensionsWithScroll();var t=this.dimensions,e=t.viewportTop+t.topSpacing,i=this.affixedType;return e<=t.containerTop||t.containerHeight<=t.sidebarHeight?(t.translateY=0,i="STATIC"):i="up"===this.direction?this._getAffixTypeScrollingUp():this._getAffixTypeScrollingDown(),t.translateY=Math.max(0,t.translateY),t.translateY=Math.min(t.containerHeight,t.translateY),t.translateY=Math.round(t.translateY),t.lastViewportTop=t.viewportTop,i}},{key:"_getAffixTypeScrollingDown",value:function(){var t=this.dimensions,e=t.sidebarHeight+t.containerTop,i=t.viewportTop+t.topSpacing,n=t.viewportBottom-t.bottomSpacing,o=this.affixedType;return this.isSidebarFitsViewport()?t.sidebarHeight+i>=t.containerBottom?(t.translateY=t.containerBottom-e,o="CONTAINER-BOTTOM"):i>=t.containerTop&&(t.translateY=i-t.containerTop,o="VIEWPORT-TOP"):t.containerBottom<=n?(t.translateY=t.containerBottom-e,o="CONTAINER-BOTTOM"):e+t.translateY<=n?(t.translateY=n-e,o="VIEWPORT-BOTTOM"):t.containerTop+t.translateY<=i&&0!==t.translateY&&t.maxTranslateY!==t.translateY&&(o="VIEWPORT-UNBOTTOM"),o}},{key:"_getAffixTypeScrollingUp",value:function(){var t=this.dimensions,e=t.sidebarHeight+t.containerTop,i=t.viewportTop+t.topSpacing,n=t.viewportBottom-t.bottomSpacing,o=this.affixedType;return i<=t.translateY+t.containerTop?(t.translateY=i-t.containerTop,o="VIEWPORT-TOP"):t.containerBottom<=n?(t.translateY=t.containerBottom-e,o="CONTAINER-BOTTOM"):this.isSidebarFitsViewport()||t.containerTop<=i&&0!==t.translateY&&t.maxTranslateY!==t.translateY&&(o="VIEWPORT-UNBOTTOM"),o}},{key:"_getStyle",value:function(t){if(void 0!==t){var e={inner:{},outer:{}},i=this.dimensions;switch(t){case"VIEWPORT-TOP":e.inner={position:"fixed",top:i.topSpacing,left:i.sidebarLeft-i.viewportLeft,width:i.sidebarWidth};break;case"VIEWPORT-BOTTOM":e.inner={position:"fixed",top:"auto",left:i.sidebarLeft,bottom:i.bottomSpacing,width:i.sidebarWidth};break;case"CONTAINER-BOTTOM":case"VIEWPORT-UNBOTTOM":var n=this._getTranslate(0,i.translateY+"px");e.inner=n?{transform:n}:{position:"absolute",top:i.translateY,width:i.sidebarWidth}}switch(t){case"VIEWPORT-TOP":case"VIEWPORT-BOTTOM":case"VIEWPORT-UNBOTTOM":case"CONTAINER-BOTTOM":e.outer={height:i.sidebarHeight,position:"relative"}}return e.outer=c.extend({height:"",position:""},e.outer),e.inner=c.extend({position:"relative",top:"",left:"",bottom:"",width:"",transform:""},e.inner),e}}},{key:"stickyPosition",value:function(t){if(!this._breakpoint){t=this._reStyle||t||!1,this.options.topSpacing,this.options.bottomSpacing;var e=this.getAffixType(),i=this._getStyle(e);if((this.affixedType!=e||t)&&e){var n="affix."+e.toLowerCase().replace("viewport-","")+l;for(var o in c.eventTrigger(this.sidebar,n),"STATIC"===e?c.removeClass(this.sidebar,this.options.stickyClass):c.addClass(this.sidebar,this.options.stickyClass),i.outer){var s="number"==typeof i.outer[o]?"px":"";this.sidebar.style[o]=i.outer[o]+s}for(var r in i.inner){var a="number"==typeof i.inner[r]?"px":"";this.sidebarInner.style[r]=i.inner[r]+a}var p="affixed."+e.toLowerCase().replace("viewport-","")+l;c.eventTrigger(this.sidebar,p)}else this._initialized&&(this.sidebarInner.style.left=i.inner.left);this.affixedType=e}}},{key:"_widthBreakpoint",value:function(){window.innerWidth<=this.options.minWidth?(this._breakpoint=!0,this.affixedType="STATIC",this.sidebar.removeAttribute("style"),c.removeClass(this.sidebar,this.options.stickyClass),this.sidebarInner.removeAttribute("style")):this._breakpoint=!1}},{key:"updateSticky",value:function(){var t,e=this,i=0<arguments.length&&void 0!==arguments[0]?arguments[0]:{};this._running||(this._running=!0,t=i.type,requestAnimationFrame(function(){switch(t){case"scroll":e._calcDimensionsWithScroll(),e.observeScrollDir(),e.stickyPosition();break;case"resize":default:e._widthBreakpoint(),e.calcDimensions(),e.stickyPosition(!0)}e._running=!1}))}},{key:"_setSupportFeatures",value:function(){var t=this.support;t.transform=c.supportTransform(),t.transform3d=c.supportTransform(!0)}},{key:"_getTranslate",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:0,e=1<arguments.length&&void 0!==arguments[1]?arguments[1]:0,i=2<arguments.length&&void 0!==arguments[2]?arguments[2]:0;return this.support.transform3d?"translate3d("+t+", "+e+", "+i+")":!!this.support.translate&&"translate("+t+", "+e+")"}},{key:"destroy",value:function(){window.removeEventListener("resize",this,{capture:!1}),window.removeEventListener("scroll",this,{capture:!1}),this.sidebar.classList.remove(this.options.stickyClass),this.sidebar.style.minHeight="",this.sidebar.removeEventListener("update"+l,this);var t={inner:{},outer:{}};for(var e in t.inner={position:"",top:"",left:"",bottom:"",width:"",transform:""},t.outer={height:"",position:""},t.outer)this.sidebar.style[e]=t.outer[e];for(var i in t.inner)this.sidebarInner.style[i]=t.inner[i];this.options.resizeSensor&&"undefined"!=typeof ResizeSensor&&(ResizeSensor.detach(this.sidebarInner,this.handleEvent),ResizeSensor.detach(this.container,this.handleEvent))}}],[{key:"supportTransform",value:function(t){var i=!1,e=t?"perspective":"transform",n=e.charAt(0).toUpperCase()+e.slice(1),o=document.createElement("support").style;return(e+" "+["Webkit","Moz","O","ms"].join(n+" ")+n).split(" ").forEach(function(t,e){if(void 0!==o[t])return i=t,!1}),i}},{key:"eventTrigger",value:function(t,e,i){try{var n=new CustomEvent(e,{detail:i})}catch(t){(n=document.createEvent("CustomEvent")).initCustomEvent(e,!0,!0,i)}t.dispatchEvent(n)}},{key:"extend",value:function(t,e){var i={};for(var n in t)void 0!==e[n]?i[n]=e[n]:i[n]=t[n];return i}},{key:"offsetRelative",value:function(t){var e={left:0,top:0};do{var i=t.offsetTop,n=t.offsetLeft;isNaN(i)||(e.top+=i),isNaN(n)||(e.left+=n),t="BODY"===t.tagName?t.parentElement:t.offsetParent}while(t);return e}},{key:"addClass",value:function(t,e){c.hasClass(t,e)||(t.classList?t.classList.add(e):t.className+=" "+e)}},{key:"removeClass",value:function(t,e){c.hasClass(t,e)&&(t.classList?t.classList.remove(e):t.className=t.className.replace(new RegExp("(^|\\b)"+e.split(" ").join("|")+"(\\b|$)","gi")," "))}},{key:"hasClass",value:function(t,e){return t.classList?t.classList.contains(e):new RegExp("(^| )"+e+"( |$)","gi").test(t.className)}},{key:"defaults",get:function(){return n}}]),c}());t.default=i,window.StickySidebar=i})(e)});return t(i),t(e(function(t,e){(function(t){var e,s=(e=t)&&e.__esModule?e:{default:e};!function(){if("undefined"!=typeof window){var n=window.jQuery||window.Zepto,o="stickySidebar";if(n.fn){n.fn.stickySidebar=function(i){return this.each(function(){var t=n(this),e=n(this).data(o);if(e||(e=new s.default(this,"object"==typeof i&&i),t.data(o,e)),"string"==typeof i){if(void 0===e[i]&&-1===["destroy","updateSticky"].indexOf(i))throw new Error('No method named "'+i+'"');e[i]()}})},n.fn.stickySidebar.Constructor=s.default;var t=n.fn.stickySidebar;n.fn.stickySidebar.noConflict=function(){return n.fn.stickySidebar=t,this}}}}()})(i)}))});