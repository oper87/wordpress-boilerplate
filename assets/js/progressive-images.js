(function ($) {
    'use strict';

    window.lazyLoadOptions = {
        elements_selector: '.progressive-media',
        unobserve_entered: true, // <- Avoid executing the function multiple times
        threshold: 100,
        callback_enter: function (element) {
            let lazyMedia = element, imgLarge,
                lazyImage = lazyMedia.querySelector('img');

            imgLarge = new Image();
            imgLarge.src = lazyImage.dataset.src;
            imgLarge.className = lazyImage.className;

            if (lazyImage.dataset.srcsize) {
                imgLarge.srcsize = lazyImage.dataset.srcsize;
            }

            if (lazyImage.dataset.srcset) {
                imgLarge.srcset = lazyImage.dataset.srcset;
            }

            imgLarge.onload = function () {
                imgLarge.classList.add('loaded');
                lazyMedia.classList.remove('entered');
                lazyMedia.classList.add('media-loaded');
                lazyMedia.dataset.llStatus = 'loaded';
            };
            lazyMedia.appendChild(imgLarge);
        }
    };

    window.addEventListener("LazyLoad::Initialized", function (event) {
        window.lazyLoadInstance = event.detail.instance;
    }, false);

    $(document.body).on('theme_fragments_replaced theme_instagram_post_loaded theme_filter_posts_loaded', function () {
        if(window.lazyLoadInstance) {
            window.lazyLoadInstance.update();
        }
    });

}(jQuery));