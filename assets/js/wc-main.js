(function ($) {
    'use strict';

    if ($.fn.blockUI) {
        $.blockUI.defaults.overlayCSS.cursor = 'default';
    }

    var settings = {
        ajaxAction: updateQueryStringParameter(theme_params.ajax_url, 'action', 'theme_ajax'),
        tabletBrakePoint: 1025,
        mobileBrakePoint: 640,
        blockUiOpt: {
            message: null, overlayCSS: {
                background: '#fff', opacity: 0.6
            }
        }
    };

    var wcShop = {

        init: function () {

            this.eventHandlers = {};
            this.isScrolling = false;
            this.scrollY = 0;
            this.offsetShift = 250;
            this.proceeding = false;
            this.productsContainer = '.woocommerce-wrapper ul.products';
            this.loadMoreContainer = '.load-more-pagination.infinity-load-more';

            this.eventHandlers.infinityPaginationScrollHandler = this.infinityPaginationScrollHandler.bind(this);

            this.bindEvents();
        },

        bindEvents: function () {
            var _loadMoreContainer = document.querySelector(this.loadMoreContainer);

            if (_loadMoreContainer) {
                if (_loadMoreContainer.querySelector('input[type="hidden"]')) {
                    window.addEventListener('scroll', this.eventHandlers.infinityPaginationScrollHandler, {passive: true});
                } else {
                    _loadMoreContainer.classList.add('loaded');
                }
            }
        },

        infinityPaginationScrollHandler: function () {
            var self = this;

            if (!self.isScrolling) {
                window.requestAnimationFrame(function () {
                    self.infinityPaginationTracker();

                    self.isScrolling = false;
                });
                self.isScrolling = true;
            }
        },

        infinityPaginationTracker: function () {
            var self = this, scrollTop = document.documentElement.scrollTop, clientHeight = document.documentElement.clientHeight,
                loadMoreProps = document.querySelector(this.loadMoreContainer).getBoundingClientRect(),
                loadMoreOffsetTop = scrollTop + loadMoreProps.top, scrollDirection = (scrollTop - self.scrollY);

            if (scrollTop > (loadMoreOffsetTop - clientHeight - this.offsetShift) && scrollTop < (loadMoreOffsetTop + loadMoreProps.height - this.offsetShift)) {
                this.infinityPaginationLoader();
            }

            self.scrollY = scrollTop;
        },

        infinityPaginationLoader: function () {
            var self = this, _productsContainer = document.querySelector(this.productsContainer),
                _loadMoreContainer = document.querySelector(this.loadMoreContainer),
                _nextUrl = _loadMoreContainer.querySelector('input[type="hidden"]'),
                ajaxUrl = '';

            if (!_nextUrl) {
                _loadMoreContainer.classList.add('loaded');
                window.removeEventListener('scroll', this.eventHandlers.infinityPaginationScrollHandler);
                return;
            }

            if (self.proceeding) {
                return;
            }

            self.proceeding = true;
            _loadMoreContainer.classList.add('loading');

            ajaxUrl = updateQueryStringParameter(_nextUrl.value, '_ajax', 1);
            ajaxUrl = updateQueryStringParameter(ajaxUrl, '_load_more', 1)

            setTimeout(function () {
                $.ajax({
                    method: "GET", url: ajaxUrl, complete: function () {
                        self.proceeding = false;
                        _loadMoreContainer.classList.remove('loading');
                    }
                }).done(function (response) {
                    var $html = $('<div>' + response + '</div>'),
                        $parsedProducts = $(response).find(self.productsContainer).children();

                    $(_loadMoreContainer).html($html.find(self.loadMoreContainer).html());

                    $.each($parsedProducts, function (key, _product) {
                        $(_productsContainer).append(_product);
                    });

                    history.replaceState(false, $html.find('#wp_title').text(), _nextUrl.value);
                    document.title = $html.find('#wp_title').text()

                    self._paginationBtn = document.querySelector(self.pagination);
                    $(document.body).trigger('ajax_load_more_posts_loaded', self._paginationBtn);
                });
            }, 500);
        }

    };

    var wcProduct = {

        init: function () {

            this.grid = false;
            this.galleryContainer = '.woocommerce-product-gallery';
            this.resizeTimer = 0;

            this.eventHandlers = {};
            this.eventHandlers.resizeHandler = this.resizeHandler.bind(this);

            this.bindEvents();
            this.isotopeGallery();

        },

        bindEvents: function () {
            window.addEventListener('resize', this.eventHandlers.resizeHandler, {passive: true});
        },

        resizeHandler: function () {
            var self = this;

            clearTimeout(this.resizeTimer);

            this.resizeTimer = setTimeout(function () {
                if (self.grid && window.innerWidth < settings.mobileBrakePoint) {
                    self.grid.isotope('destroy');
                    self.grid = false;
                } else if (false === self.grid && window.innerWidth >= settings.mobileBrakePoint) {
                    self.isotopeGallery();
                }

            }, 500);
        },

        isotopeGallery: function () {
            var self = this,
                _gallery = document.querySelector(this.galleryContainer);

            if (window.innerWidth < settings.mobileBrakePoint) {
                return;
            }

            if (!_gallery && typeof (Isotope) === 'undefined') {
                return;
            }

            if (_gallery.querySelectorAll('.woocommerce-product-gallery__image').length === 1) {
                return;
            }

            this.grid = $(_gallery).isotope({
                itemSelector: '.woocommerce-product-gallery__image',
                percentPosition: true,
                masonry: {
                    columnWidth: '.grid-sizer',
                    horizontalOrder: true
                }
            });

            $(_gallery).imagesLoaded().progress(function () {
                self.grid.isotope('layout');
                $(document.body).trigger('product-gallery-image-loaded');
            });
        }

    };

    var wcCheckout = {
        init: function () {

            this.eventHandlers = {};

            this.resizeTimer = 0;
            this.view = 'desktop';
            this.orderReview = '#order_review';
            this.orderReviewWrapper = '.order-review-wrapper';
            this.orderReviewPlaceholder = '#order-review-placeholder';

            this.eventHandlers.resizeHandler = this.resizeHandler.bind(this);
            this.eventHandlers.creditDiscountInputChangeHandler = this.creditDiscountInputChangeHandler.bind(this);

            this.bindEvents();
            //this.toggleCheckoutMobileView();

            window.addEventListener('resize', this.eventHandlers.resizeHandler, {passive: true});
            $(this.orderReview).on('change', 'input[name="credit_discount_opt_in"]', this.eventHandlers.creditDiscountInputChangeHandler);
        },

        bindEvents: function () {
        },

        resizeHandler: function () {
            clearTimeout(this.resizeTimer);

            this.resizeTimer = setTimeout(function () {
                $(document.body).trigger('checkout_resize_event_action');
            }, 500);

            this.toggleCheckoutMobileView();
        },

        toggleCheckoutMobileView: function () {
            if (this.view === 'desktop' && window.innerWidth < settings.mobileBrakePoint) {
                this.view = 'mobile';

                $(this.orderReviewPlaceholder).html($(this.orderReviewWrapper).html());
                $(this.orderReviewWrapper).html('');


            } else if (this.view === 'mobile' && window.innerWidth >= settings.mobileBrakePoint) {
                this.view = 'desktop';

                $(this.orderReviewWrapper).html($(this.orderReviewPlaceholder).html());
                $(this.orderReviewPlaceholder).html('');
            }
        },

        creditDiscountInputChangeHandler: function () {
            $(document.body).trigger('update_checkout');
        }

    };

    var wcApplyCoupon = {
        init: function () {

            this.couponContainer = '.cart-coupon-form';
            this.couponCodeInput = 'input#coupon_code';
            this.isCart = document.querySelector('.woocommerce-cart-form') !== null ?? true;
            this.isCheckout = document.querySelector('.woocommerce-checkout') !== null ?? true;

            this.eventHandlers = {};
            this.eventHandlers.applyCouponButtonClickHandler = this.applyCouponButtonClickHandler.bind(this);
            this.eventHandlers.applyCouponFromSubmitHandler = this.applyCouponFromSubmitHandler.bind(this);
            this.eventHandlers.applyCouponCheckoutFromSubmitHandler = this.applyCouponCheckoutFromSubmitHandler.bind(this);

            $(document.body).on('click', 'button#apply-coupon-btn', this.eventHandlers.applyCouponButtonClickHandler);
            $(document.body).on('submit', 'form.woocommerce-coupon-form', this.eventHandlers.applyCouponFromSubmitHandler);
            //$(document.body).bind('submit', 'form.woocommerce-checkout', this.eventHandlers.applyCouponCheckoutFromSubmitHandler);
            $('form.woocommerce-checkout').bind('checkout_place_order', this.eventHandlers.applyCouponCheckoutFromSubmitHandler);

            /*$('form.woocommerce-checkout').bind('checkout_place_order', function (e, param, param2) {
                if ($(couponCode).is(':focus')) {
                    sendCouponAjaxRequest($(couponCode).val());
                    return false;
                }

                return true;
            });*/
        },

        applyCouponButtonClickHandler: function (e) {
            e.preventDefault();
            e.stopPropagation();

            var _couponContainer = e.currentTarget.closest(this.couponContainer),
                _couponInput = _couponContainer.querySelector(this.couponCodeInput);

            console.log(_couponInput.value)
            console.log(this.isCart)
            console.log(this.isCheckout)


            this.applyCoupon(e.currentTarget.closest(this.couponContainer));

        },

        applyCouponFromSubmitHandler: function (e) {
            e.preventDefault();

            this.applyCoupon(e.currentTarget.closest(this.couponContainer));
        },

        applyCouponCheckoutFromSubmitHandler: function (e) {
            var $submit = $(document.activeElement),
                $clicked = $(':input[type=submit][clicked=true]');

            if ($clicked.is(':input[name="apply_coupon"]') || $submit.is('#coupon_code')) {
                e.preventDefault();
                this.applyCoupon(document.activeElement.closest(this.couponContainer));
                return false;
            }
        },

        applyCoupon: function (_couponContainer) {
            var self = this,
                _couponError = _couponContainer.querySelector('.coupon-error'),
                _couponCode = _couponContainer.querySelector(this.couponCodeInput);

            _couponError.innerHTML = '';

            var data = {
                security: this.security(),
                coupon_code: _couponCode.value
            };

            if (data.coupon_code === '') {
                _couponError.innerHTML = wc_params.woocommerce_errors.wc_coupon_please_enter;
                return;
            }

            // Trigger "adding_coupon" event
            $(document.body).trigger('adding_coupon', [$(_couponContainer), data.coupon_code]);

            this.block(true);

            $.ajax({
                type: 'POST',
                url: this.ajaxUrl(),
                data: data,
                dataType: 'html',
                success: function (response) {
                    var $html = $.parseHTML('<div>' + response + '</div>'),
                        $wc_error = $('.woocommerce-error', $html);

                    self.block(false);

                    if ($wc_error.length || $wc_error.hasClass('woocommerce-error')) {
                        _couponError.innerHTML = $wc_error.find('li:eq(0)').text().trim();
                        _couponCode.value = '';
                    } else {
                        self.refresh();
                        $(document.body).trigger('applied_coupon', [_couponCode.value]);
                    }
                }
            });
        },

        ajaxUrl: function () {
            if (this.isCart) {
                return wc_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'apply_coupon')
            } else {
                return wc_checkout_params.wc_ajax_url.toString().replace('%%endpoint%%', 'apply_coupon')
            }
        },

        security: function () {
            if (this.isCart) {
                return wc_cart_params.apply_coupon_nonce
            } else {
                return wc_checkout_params.apply_coupon_nonce
            }
        },

        block: function (state) {
            if (state) {
                if (this.isCart) {
                    $('.cart-collaterals').block(settings.blockUiOpt);
                } else {
                    $('#order_review').block(settings.blockUiOpt);
                }
            } else {
                if (this.isCart) {
                    $('.cart-collaterals').unblock();
                } else {
                    $('#order_review').unblock();
                }
            }
        },

        refresh: function () {
            if (this.isCart) {
                $(document).trigger('wc_update_cart');
            } else {
                $(document.body).trigger('update_checkout', {update_shipping_method: false});
            }
        }
    };

    var addToCart = {

        init: function () {

            this.cartSidebar = '#cart-panel-modal';
            this.$cartPanel = $(this.cartSidebar + ' .cart-panel-body');
            this.$simpleForm = $('.product-entry-wrapper form.cart');
            this.$variationsForm = $('.product-entry-wrapper form.variations_form.cart');
            this.formSubmitBtn = '.single_add_to_cart_button';
            this.quantityPlusMinusBtn = '.btn-qty-minus, .btn-qty-plus';
            this.miniCartBtn = '.shop-cart-btn';
            this.miniCartCloseBtn = '.cart-panel-close-btn';
            this.cartPanelAjax = false;

            this.eventHandlers = {};
            this.eventHandlers.addingToCartHandler = this.addingToCartHandler.bind(this);
            this.eventHandlers.addedToCartHandler = this.addedToCartHandler.bind(this);
            this.eventHandlers.bindHideSidebarModalHandle = this.bindHideSidebarModalHandle.bind(this);
            this.eventHandlers.quantityInputKeyHandler = this.quantityInputKeyHandler.bind(this);

            this.bindEvents();
        },

        bindEvents: function () {
            var self = this;

            $(document.body).on('click', self.formSubmitBtn, self.addToCartOnSingle);

            $(document.body).on('click', self.quantityPlusMinusBtn, self.quantityPlusMinus);
            $(document.body).on('keyup keydown', 'input.qty[type="number"]', this.eventHandlers.quantityInputKeyHandler);

            self.$variationsForm.on('reset_data, woocommerce_variation_has_changed', function (e) {
                self.$variationsForm.find(self.formSubmitBtn).removeClass('added');
                self.$variationsForm.find('a.added_to_cart').remove();
            });

            $(document.body).on('quantity_change', self.updateMiniCartPanel);
            $(document.body).on('input', '.mini-cart-list .mini_cart_item .qty', function (e) {
                self.updateMiniCartPanel(e, $(this));
            });

            $(document.body).on('adding_to_cart', this.eventHandlers.addingToCartHandler);
            $(document.body).on('added_to_cart wc_fragments_refreshed', this.eventHandlers.addedToCartHandler);
            $(self.cartSidebar).on('hide_sidebar_modal', this.eventHandlers.bindHideSidebarModalHandle);

        },

        addToCartOnSingle: function (e) {
            var self = addToCart,
                $thisButton = $(this),
                $thisForm = $(this).closest('form');

            if ($thisButton.is('.disabled')) {
                return true;
            }

            if ($thisForm.find('input[name="ajax_add_to_cart"]').length > 0) {

                var $product = $thisForm.find('[name="add-to-cart"]'),
                    $quantity = $thisForm.find('input[name="quantity"]'),
                    $variation = $thisForm.find('input[name="variation_id"]'),
                    data = {};

                if ($product.length === 0 || $product.val() === '0') {
                    return true;
                }

                e.preventDefault();

                // Allow 3rd parties to validate and quit early.
                if (false === $(document.body).triggerHandler('should_send_ajax_request.adding_to_cart', [$thisButton])) {
                    $(document.body).trigger('ajax_request_not_sent.adding_to_cart', [false, false, $thisButton]);
                    return true;
                }

                data['quantity'] = $quantity.val();

                if ($variation.length !== 0 && $variation.val() === '0') {
                    return false;
                }

                if ($variation.length !== 0 && $variation.val() !== '0') {
                    data['product_id'] = $variation.val();
                    data['variation_id'] = $variation.val();
                } else {
                    data['product_id'] = $product.val();
                }

                $thisButton.prop('disabled', true);
                $thisButton.removeClass('added');
                $thisButton.addClass('loading');

                // Trigger event.
                $(document.body).trigger('adding_to_cart', [$thisButton, data]);

                $.ajax({
                    type: 'POST',
                    url: wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'),
                    data: data,
                    dataType: 'json',
                    complete: function () {
                        $thisButton.prop('disabled', false);
                        $thisButton.removeClass('loading');
                    },
                    success: function (response) {
                        if (!response) {
                            return;
                        }

                        if (response.error && response.product_url) {
                            window.location = response.product_url;
                            return;
                        }

                        if (response.success && response.checkout_url) {
                            window.location = response.checkout_url;
                            return;
                        }

                        // Redirect to cart option
                        if (wc_add_to_cart_params.cart_redirect_after_add === 'yes') {
                            window.location = wc_add_to_cart_params.cart_url;
                            return;
                        }

                        $thisButton.addClass('added');

                        // Trigger event so themes can refresh other areas.
                        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisButton]);
                    },
                });
            }
        },

        quantityPlusMinus: function (e) {
            var self = addToCart;

            var $this = $(this), $qty = $this.closest('.quantity').find('.qty'), currentVal = parseFloat($qty.val()),
                max = parseFloat($qty.attr('max')), min = parseFloat($qty.attr('min')), step = $qty.attr('step'),
                qtyChanged = false;

            // Format values
            if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
            if (max === '' || max === 'NaN') max = '';
            if (min === '' || min === 'NaN') min = 0;
            if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;

            // Change the value
            if ($this.data('eq') === 'plus') {
                if (max && (max === currentVal || currentVal > max)) {
                    $qty.val(max);
                } else {
                    $qty.val(currentVal + parseFloat(step));
                    qtyChanged = true;
                }
            } else {
                if (min && (min === currentVal || currentVal < min)) {
                    $qty.val(min);
                } else if (currentVal > 0) {
                    $qty.val(currentVal - parseFloat(step));
                    qtyChanged = true;
                }
            }

            if (qtyChanged) {
                // Trigger quantity input "change" event
                $qty.trigger('change');
                $(document.body).trigger('quantity_change', $qty);
            }
        },

        quantityInputKeyHandler: function (e) {
            if (e.currentTarget.value !== '') {
                if (e.currentTarget.min !== '' && parseInt(e.currentTarget.value) < parseInt(e.currentTarget.min)) {
                    e.currentTarget.value = e.currentTarget.min;
                }
                if (e.currentTarget.max !== '' && parseInt(e.currentTarget.value) > parseInt(e.currentTarget.max)) {
                    e.currentTarget.value = e.currentTarget.max;
                }
            }
        },

        updateMiniCartPanel: function (e, _qty) {
            var self = addToCart;

            if (!document.body.classList.contains('sidebar-modal-open')) {
                return false;
            }

            var $qty = $(_qty);
            var $row = $qty.closest('.woocommerce-mini-cart-item')

            // Is an Ajax request already running?
            if (self.cartPanelAjax) {
                self.cartPanelAjax.abort(); // Abort current Ajax request
            }

            $row.addClass('loading');
            $row.block({
                message: null, overlayCSS: {
                    opacity: 0.6
                }
            });

            var data = {};
            data[$qty.attr('name')] = $qty.val();
            data['security'] = woocommerce_params.update_cart_item_nonce;

            // Make call to actual form post URL.
            self.cartPanelAjax = $.ajax({
                type: 'POST',
                url: wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'update_cart_item'),
                data: data,
                dataType: 'html',
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // Hide any visible thumbnail loaders (no need to hide on "success" since the cart panel is replaced)
                    $('.mini-cart-list .cart_list').children('.loading').removeClass('loading');
                },
                success: function (response) {
                    // Replace cart fragments
                    $(document.body).trigger('wc_fragment_refresh').trigger('updated_cart_totals');
                },
                complete: function () {
                    self.cartPanelAjax = null; // Reset Ajax state
                }
            });
        },

        addingToCartHandler: function (e) {
            this.addedToCart = true;
            clearTimeout(this.panelAutoClose);

            this.$cartPanel.addClass('refreshing');
            theme.sidebarModal.showModal(this.cartSidebar, e.currentTarget);
        },

        addedToCartHandler: function () {
            var self = this;

            if (self.$cartPanel.hasClass('refreshing')) {
                self.$cartPanel.addClass('fade');
                setTimeout(function () {
                    self.$cartPanel.removeClass('refreshing fade');
                }, 200);
            }

            if (self.addedToCart === true) {
                self.panelAutoClose = setTimeout(function () {
                    self.addedToCart = false;

                    if ($(self.cartSidebar + ' .sidebar-modal-dialog').is(':hover') === false) {
                        theme.sidebarModal.hideModal(self.cartSidebar);
                    }
                }, 5000);
            }

            if (document.body.classList.contains('checkout') && document.querySelector('div.shop-cart-item a.shop-cart-btn')) {
                document.querySelector('div.shop-cart-item a.shop-cart-btn').classList.add('disabled')
            }

        },

        bindHideSidebarModalHandle: function () {
            clearTimeout(this.panelAutoClose);
            this.addedToCart = false;
            this.panelAutoClose = false;
        },

    };

    var wcMyaccount = {
        init: function () {
            this.eventsHandler = {};
        }
    }

    $(document).ready(function () {
        wcShop.init()
        wcProduct.init()
        wcCheckout.init()
        addToCart.init()
        wcMyaccount.init()
        wcApplyCoupon.init()
    });

}(jQuery));