<?php
/**
 * The footer template
 */
?>
			<!-- footer -->
			<footer class="footer" role="contentinfo">

                <div class="footer__copyright">&copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?></div>

                <div class="footer__poweredby">
                    <a href="https://rubystudio.dk/" target="_blank" class="powered-by">Powered by Ruby Studio</a>
                </div>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>