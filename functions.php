<?php
/**
 * Define Theme constants
 */
define( 'APP_THEME_VERSION', '4.1.0' );
define( 'APP_THEME_DB_VERSION', '0' );
define( 'APP_SCRIPTS_VERSION', '3.2.1' );
define( 'THEME_AJAX', isset( $_GET['_ajax'] ) && $_GET['_ajax'] == '1' );
define( 'WC_AJAX_ADD_TO_CART', true );
define( 'WC_BREADCRUMBS_ENABLED', false );
define( 'WC_BREADCRUMBS', true );
define( 'WC_QUANTITY_INPUT_HIDDEN', false );

/**
 * Disable Nag Notices
 *
 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/admin_notices#Disable_Nag_Notices
 */
if ( ! defined( 'DISABLE_NAG_NOTICES' ) ) {
	define( 'DISABLE_NAG_NOTICES', true );
}

/**
 * Disable file editing
 */
if ( ! defined( 'DISALLOW_FILE_EDIT' ) ) {
	define( 'DISALLOW_FILE_EDIT', true );
}

/**
 * Disable all automatic updates for theme and plugins and WP core
 */
if ( ! defined( 'AUTOMATIC_UPDATER_DISABLED' ) ) {
	define( 'AUTOMATIC_UPDATER_DISABLED', true );
}

if ( function_exists( 'get_rocket_option' ) && get_rocket_option( 'lazyload' ) && ! ( defined( 'DONOTROCKETOPTIMIZE' ) && DONOTROCKETOPTIMIZE ) ) {
	define( 'ROCKET_LAZYLOAD', true );
} else {
	define( 'ROCKET_LAZYLOAD', false );
}

if ( 'development' == wp_get_environment_type() ) {
	//
} else {
	//
}

// Include autoloader.php
//require_once 'vendor/autoload.php';
require_once 'includes/helpers.php';
require_once 'includes/autoloader.php';

// WordPress Admin Theme Instance
new App\Admin\Functions();
new App\Admin\Ajax();

// WordPress Theme Instance
new App\Install();
new App\Settings();
new App\Scripts();
new App\Functions();
new App\Users();
new App\Ajax();
new App\Acf\Acf();

if ( defined( 'WP_CLI' ) && WP_CLI && class_exists( 'WP_CLI' ) ) {
	WP_CLI::add_command( 'app', App\WP_Console::class );
}

// WordPress Theme Extensions
//App\Extensions\Newsletter\Mailchimp::getInstance();

//App\Front_Styles::getInstance();
//App\Simple_CPT\Post::getInstance();

// WooCommerce Instance
//new App\WooCommerce\Core();
//new App\WooCommerce\Shop();
//App\WooCommerce\Cart::getInstance();
//App\WooCommerce\Checkout::getInstance();
//App\WooCommerce\Product::getInstance();