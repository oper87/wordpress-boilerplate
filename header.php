<?php
/**
 * The header template
 */
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php
/**
 * Head
 */
get_template_part( 'template-parts/global/head' );

?>
<body <?php body_class(); ?>>
    <div class="wrapper">
        <header id="header" class="header" role="banner">
            <div class="header__logo">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="Logo" class="logo-img">
                </a>
            </div>

            <nav class="header__nav" role="navigation">
	            <?php wp_nav_menu( [
		            'theme_location' => 'header',
		            'container'      => false,
		            'menu_id'        => 'header-menu',
		            'menu_class'     => 'header-menu',
		            'fallback_cb'    => false,
		            'depth'          => 1
	            ] ); ?>
            </nav>

            <div class="header__buttons">
                <span class="menu-btn"><span></span><span></span><span></span><span></span></span>
            </div>

            <div class="header__cart">
                <?php if ( function_exists( 'WC' ) ): ?>
                    <div class="nav-item shop-cart-item">
                        <a class="shop-cart-btn<?php echo is_checkout() ? ' disabled' : '' ?>" href="#" data-toggle="sidebar-modal"
                           data-target="#cart-panel-modal">
                            <i class="fas fa-shopping-cart"></i>
                            <?php echo \App\WooCommerce\Cart::get_cart_contents_count() ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </header>