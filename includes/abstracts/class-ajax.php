<?php
/**
 * Abstract Ajax Class
 */

namespace App\Abstracts;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class Ajax {

	protected $ajax_key;

	public function __construct( $ajax_key = false ) {
		$this->ajax_key = $ajax_key;

		if ( $this->ajax_key ) {
			$this->ajax_key = 'theme_ajax_' . $this->ajax_key;
		} else {
			$this->ajax_key = 'theme_ajax';
		}

		add_action( 'wp_ajax_' . $this->ajax_key, [ $this, 'ajax' ] );
		add_action( 'wp_ajax_nopriv_' . $this->ajax_key, [ $this, 'ajax' ] );

		add_action( 'init', [ $this, 'define_ajax' ], 0 );
		add_action( 'template_redirect', [ $this, 'do_wc_ajax' ], 0 );
	}

	/**
	 * Set WC AJAX constant and headers.
	 */
	public static function define_ajax() {
		// phpcs:disable
		if ( ! empty( $_GET['wp-theme-ajax'] ) ) {
			self::maybe_define_constant( 'DOING_AJAX', true );
			self::maybe_define_constant( 'WC_DOING_AJAX', true );
			if ( ! WP_DEBUG || ( WP_DEBUG && ! WP_DEBUG_DISPLAY ) ) {
				@ini_set( 'display_errors', 0 ); // Turn off display_errors during AJAX events to prevent malformed JSON.
			}
			$GLOBALS['wpdb']->hide_errors();
		}
		// phpcs:enable
	}

	/**
	 * Send headers for WC Ajax Requests.
	 *
	 * @since 2.5.0
	 */
	private static function ajax_headers() {
		if ( ! headers_sent() ) {
			send_origin_headers();
			send_nosniff_header();
			self::maybe_define_constant( 'DONOTCACHEPAGE', true );
			self::maybe_define_constant( 'DONOTCACHEOBJECT', true );
			self::maybe_define_constant( 'DONOTCACHEDB', true );
			nocache_headers();
			header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
			header( 'X-Robots-Tag: noindex' );
			status_header( 200 );
		} elseif ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			headers_sent( $file, $line );
			trigger_error( "wc_ajax_headers cannot set headers - headers already sent by {$file} on line {$line}", E_USER_NOTICE ); // @codingStandardsIgnoreLine
		}
	}

	/**
	 * Check for WC Ajax request and fire action.
	 */
	public function do_wc_ajax() {
		global $wp_query;

		if ( empty( $_GET['wp-theme-ajax'] ) ) {
			return;
		}

		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		if ( ! empty( $_GET['wp-theme-ajax'] ) ) {
			$wp_query->set( 'wp-theme-ajax', sanitize_text_field( wp_unslash( $_GET['wp-theme-ajax'] ) ) );
		}

		[ $class, $action ] = explode( '@', $wp_query->get( 'wp-theme-ajax' ) );

		if ( $action && get_class( $this ) == $class ) {
			self::ajax_headers();
			$action = sanitize_text_field( $action );

			if ( method_exists( $this, $action ) ) {
				call_user_func( [ $this, $action ] );
			} else {
				wp_send_json_error( 'ERROR: Ajax method does not exist' );
			}
			wp_die();
		}
		// phpcs:enable
	}

	public function get_ajax_key() {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX && ! empty( $_REQUEST['action'] ) ) {
			return str_replace( 'theme_ajax_', '', $_REQUEST['action'] );
		} else {
			return false;
		}
	}

	public function ajax() {
		if ( ! empty( $_REQUEST['ajax_action'] ) ) {
			$ajax_action = sanitize_text_field( wp_unslash( $_REQUEST['ajax_action'] ) );

			if ( method_exists( $this, $ajax_action ) ) {
				call_user_func( [ $this, $ajax_action ] );
			} else {
				wp_send_json_error( 'ERROR: Ajax method does not exist' );
			}
		} else {
			wp_send_json_error( 'ERROR: Ajax action empty' );
		}
	}


	/**
	 * Define a constant if it is not already defined.
	 *
	 * @param string $name Constant name.
	 * @param mixed $value Value.
	 *
	 * @since 3.0.0
	 */
	protected static function maybe_define_constant( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}
}