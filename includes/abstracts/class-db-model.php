<?php
/**
 * Abstract Database Model Class
 */

namespace App\Abstracts;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class DB_Model {

	/**
	 * @var \wpdb
	 */
	protected $db;

	/**
	 * @var string
	 */
	protected string $table_name;

	/**
	 * Constructor
	 */
	public function __construct( $table_name ) {
		$this->set_db();
		$this->table_name = $this->generate_table_name( $table_name );
	}

	/**
	 * @return \wpdb
	 */
	public function db(): \wpdb {
		return $this->db;
	}

	/**
	 * @return void
	 */
	public function set_db(): void {
		global $wpdb;

		$this->db = $wpdb;
	}

	/**
	 * @param $table_name
	 *
	 * @return string
	 */
	public function generate_table_name( $table_name ): string {
		return $this->db()->prefix . $table_name;
	}

	/**
	 * @return string
	 */
	public function table() {
		return $this->table_name;
	}

}