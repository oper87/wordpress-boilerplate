<?php
/**
 * Abstract Filter Class
 */

namespace App\Abstracts;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Helper;
use WP_Tax_Query;
use WP_Meta_Query;

abstract class Filter {

	/**
	 *
	 * @var bool
	 */
	private bool $active = false;

	/**
	 * @var bool
	 */
	public static bool $cache = true;

	public $array_filters = [];

	public $post_type;

	//public $filter_cache_data_key;

	protected $filter_params = [];

	public $filter_has_any_term = false;

	public $filter_has_any_selected_term = false;

	public $query_prefix = '';


	/**
	 * taxonomy condition
	 *
	 * @var string
	 */
	protected string $taxonomy_condition = 'AND';

	public function __construct( $post_type = false ) {
		$this->post_type = $post_type;

		//$filter_key = md5( maybe_serialize( $this->array_filters ) );

		//$this->filter_cache_data_key = 'app_filter_' . $this->post_type . '_' . $filter_key . '_data';

		/**
		 * Clear cache on update/delete posts
		 */
		add_action( 'save_post_' . $this->post_type, [ $this, 'clear_filter_terms_cache' ], 10, 1 );
		add_action( 'trashed_post', [ $this, 'clear_filter_terms_cache' ], 10, 1 );
		add_action( 'untrash_post', [ $this, 'clear_filter_terms_cache' ], 10, 1 );

		/**
		 * Clear cache on set terms
		 */
		add_action( 'set_object_terms', [ $this, 'clear_filter_tax_terms_cache' ], 10, 4 );
	}

	public function hook_init() {
		add_action( 'pre_get_posts', [ $this, 'pre_get_posts' ] );

		if ( false != $this->post_type ) {
			add_action( 'save_post_' . $this->post_type, [ $this, 'clear_filter_cache' ], 10, 1 );
			add_action( 'trashed_post', [ $this, 'clear_filter_cache' ], 10, 1 );
			add_action( 'untrash_post', [ $this, 'clear_filter_cache' ], 10, 1 );
		}
	}

	public function set_active( $state = false ) {
		$this->active = $state;
	}

	public function is_active() {
		return $this->active;
	}

	public function get_form_action( ...$args ) {
		global $wp;

		if ( '' === get_option( 'permalink_structure' ) ) {
			$url = remove_query_arg( [
				'page',
				'paged',
				'_ajax'
			], add_query_arg( $wp->query_string, '', home_url( $wp->request ) ) );
		} else {
			$url = preg_replace( '%\/page/[0-9]+%', '', home_url( trailingslashit( $wp->request ) ) );
		}

		if ( ! empty( $args ) ) {
			if ( is_array( $args[0] ) ) {
				$url = add_query_arg( $args, $url );
			} else {
				$url = add_query_arg( $args[0], $args[1], $url );
			}
		}

		return $url;
	}

	public function is_run_pre_get_posts( $query ) {
		// We only want to affect the main query.
		if ( $query->is_main_query() ) {
			return $this->is_active();
		}

		return false;
	}

	public function pre_get_posts( $query ) {

		if ( false == $this->is_run_pre_get_posts( $query ) ) {
			return;
		}

		$query->set( 'app_filter_posts', 1 );

		$tax_query    = [];
		$meta_query   = [];
		$custom_query = [];

		foreach ( $this->array_filters as $query_key => $data ) :
			if ( empty( $_GET[ $this->query_prefix . $query_key ] ) && $data['query_from'] != 'search' ) :
				continue;
			endif;

			$filter_terms = empty( $_GET[ $this->query_prefix . $query_key ] ) ? '' : $_GET[ $this->query_prefix . $query_key ];
			$filter_terms = str_replace( '/', '', $filter_terms );
			$filter_terms = explode( ',', $this->term_clean( wp_unslash( $filter_terms ) ) );
			$filter_terms = array_map( 'sanitize_title', $filter_terms );

			switch ( $data['query_from'] ) :
				case 'search':
					$search_term = ! empty( $_GET[ $query_key ] ) ? wp_unslash( $_GET[ $query_key ] ) : '';
					$search_term = $this->term_clean( $search_term );
					if ( isset( $data['multiple'] ) && $data['multiple'] ) {
						$search_term = str_replace( ',', '+', $search_term );
					}
					break;
				case 'post_meta':
					if ( $query_key == 'case_creators' && ! empty( $filter_terms ) ) :
						foreach ( $filter_terms as $term ) :
							$meta_query[] = [
								'key'     => $query_key,
								'compare' => 'LIKE',
								'value'   => '"' . $term . '"',
							];
						endforeach;
					elseif ( isset( $data['meta_query_compare'] ) && $data['meta_query_compare'] == 'LIKE' ) :

						$meta_query[ $data['meta_key'] ]['relation'] = $data['meta_query_relation'];

						foreach ( $filter_terms as $term ) :
							$meta_query[ $data['meta_key'] ][] = [
								'key'     => $data['meta_key'],
								'compare' => $data['meta_query_compare'],
								'value'   => '"' . $term . '"',
							];
						endforeach;
					else :
						$meta_query[] = [
							'key'     => $data['meta_key'],
							'compare' => 'IN',
							'value'   => $filter_terms,
						];
					endif;
					break;
				case 'taxonomy':
					$tax_query[] = [
						'taxonomy' => $data['taxonomy'],
						'field'    => 'slug',
						'terms'    => $filter_terms,
					];
					break;
				case 'custom_db_table':
					$custom_query[] = [
						'data_db_table'  => $data['data_db_table'],
						'data_field_id'  => $data['data_field_id'],
						'data_field_key' => $data['data_field_key'],
						'data_values'    => $filter_terms,
					];
					break;
			endswitch;
		endforeach;

		if ( ! empty( $search_term ) ) :
			$query->set( 's', $search_term );
		endif;

		if ( ! empty( $tax_query ) ) :
			$tax_query['relation'] = $this->taxonomy_condition;
			$pre_tax_query         = $query->get( 'tax_query' );

			if ( ! is_array( $pre_tax_query ) ) {
				$pre_tax_query = array(
					'relation' => 'AND',
				);
			}

			$pre_tax_query['filter_tax_query'] = $tax_query;

			$query->set( 'tax_query', $pre_tax_query );

		endif;

		if ( ! empty( $meta_query ) ) :
			if ( count( $meta_query ) > 1 ) :
				$meta_query['relation'] = 'OR';
			endif;

			if ( $query->get( 'meta_query' ) ) {
				$meta_query = array_merge( $query->get( 'meta_query' ), (array) $meta_query );
			} else {
				$meta_query = (array) $meta_query;
			}

			$query->set( 'meta_query', $meta_query );
		endif;

		if ( ! empty( $custom_query ) ) :
			$query->set( 'custom_query', $custom_query );
			add_filter( 'posts_clauses', [ $this, 'custom_filter_query_post_clauses' ], 10, 2 );
		endif;

	}

	public function set_filters() {
		global $wpdb;

		if ( empty( $this->filter_params ) ) {
			$this->filter_params = $this->array_filters;
		} else {
			$this->filter_params = array_merge( $this->array_filters, $this->filter_params );
		}

		$this->filter_has_any_term = false;

		foreach ( $this->array_filters as $query_key => $data ) :

			$this->filter_params[ $query_key ]['name']           = $data['name'];
			$this->filter_params[ $query_key ]['type']           = $data['query_from'];
			$this->filter_params[ $query_key ]['choose_type']    = ! empty( $data['choose_type'] ) ? $data['choose_type'] : '';
			$this->filter_params[ $query_key ]['query_key']      = $this->query_prefix . $query_key;
			$this->filter_params[ $query_key ]['selected_value'] = '';
			$this->filter_params[ $query_key ]['selected_array'] = [];
			$this->filter_params[ $query_key ]['terms']          = [];

			switch ( $data['data_from'] ) :
				case 'post_type':
					$this->filter_params[ $query_key ]['terms'] = array_map( function ( $term ) {
						return (object) [
							'id'   => $term->ID,
							'slug' => $term->ID,
							'name' => $term->post_title,
						];
					}, get_posts( [
						'post_type'      => $data['post_type'],
						'posts_per_page' => - 1,
						'order'          => 'asc',
						'orderby'        => 'title',
					] ) );
					break;
				case 'post_meta_options':
					$this->filter_params[ $query_key ]['terms'] = [];

					$post_meta_values = $wpdb->get_col(
						$wpdb->prepare(
							"SELECT meta_value 
										FROM {$wpdb->postmeta} pm
										LEFT JOIN {$wpdb->posts} p 
											ON (pm.post_id = p.ID)
										WHERE pm.meta_key = %s 
											AND p.post_status IN ('publish', 'private')
										GROUP BY pm.meta_value", $data['meta_key'] ) );

					$stored_values = [];

					if ( ! empty( $post_meta_values ) ) {
						foreach ( $post_meta_values as $post_meta_value ) {
							$post_meta_value = maybe_unserialize( $post_meta_value );

							if ( is_array( $post_meta_value ) ) {
								$stored_values = array_merge( $stored_values, $post_meta_value );
							} else {
								$stored_values[] = $post_meta_value;
							}
						}
					}

					$stored_values = array_unique( $stored_values );

					foreach ( $data['options'] as $term_key => $term_value ) {
						if ( in_array( $term_key, $stored_values ) ) {
							$this->filter_params[ $query_key ]['terms'][] = (object) [
								'id'   => $term_key,
								'slug' => $term_key,
								'name' => $term_value,
							];
						}
					}
					break;
				case 'post_meta':
					$this->filter_params[ $query_key ]['terms'] = array_map( function ( $term ) {
						return (object) [
							'id'   => $term->meta_id,
							'slug' => $term->meta_value,
							'name' => ucfirst( $term->meta_value ),
						];
					}, $wpdb->get_results( $wpdb->prepare( "SELECT meta_id, meta_key, meta_value FROM {$wpdb->postmeta} WHERE meta_key = %s GROUP BY meta_value", $data['meta_key'] ) ) );
					break;
				case 'taxonomy':
					$this->filter_params[ $query_key ]['terms']       = $this->get_terms( $data['taxonomy'], $query_key, $data );
					$this->filter_params[ $query_key ]['found_posts'] = 0;

					foreach ( $this->filter_params[ $query_key ]['terms'] as $term ) {
						$this->filter_params[ $query_key ]['found_posts'] += $term->count;
					}

					break;
				case 'custom_db_table':
					if ( is_callable( [ $this, 'get_custom_' . str_replace( '-', '_', $query_key ) . '_db_table_terms' ] ) ) {
						$this->filter_params[ $query_key ]['terms'] = call_user_func( [
							$this,
							'get_custom_' . str_replace( '-', '_', $query_key ) . '_db_table_terms'
						], $data );
					} else {
						$this->filter_params[ $query_key ]['terms'] = $this->get_custom_db_table_terms( $data );
					}
					break;
			endswitch;

			if ( $this->filter_has_any_term === false && ! empty( $this->filter_params[ $query_key ]['terms'] ) ) {
				$this->filter_has_any_term = true;
			} else {
				//$this->filter_has_any_term = false;
			}

		endforeach;

		foreach ( $this->array_filters as $query_key => $data ) :

			if ( ! empty( $_GET[ $this->query_prefix . $query_key ] ) ) {
				$filter_terms                                        = $this->term_clean( wp_unslash( $_GET[ $this->query_prefix . $query_key ] ) );
				$this->filter_params[ $query_key ]['selected_value'] = $filter_terms;
				$this->filter_params[ $query_key ]['selected_array'] = explode( ',', $filter_terms );
				$this->filter_has_any_selected_term                  = true;
			} else {
				$this->filter_params[ $query_key ]['selected_value'] = '';
				$this->filter_params[ $query_key ]['selected_array'] = [];
			}

			if ( $query_key == 'search' && ! empty( $_GET[ $query_key ] ) ) {
				$filter_terms                                        = $this->term_clean( wp_unslash( $_GET[ $query_key ] ) );
				$this->filter_params[ $query_key ]['selected_value'] = $filter_terms;
				$this->filter_params[ $query_key ]['selected_array'] = explode( ',', $filter_terms );
				$this->filter_has_any_selected_term                  = true;
			}

		endforeach;
	}

	public function get_filters() {
		return $this->filter_params;
	}

	public function get_filter_selected() {
		global $wpdb;

		foreach ( $this->array_filters as $query_key => $data ) :

			if ( ! empty( $_GET[ $this->query_prefix . $query_key ] ) ) {
				$filter_terms                                        = $this->term_clean( wp_unslash( $_GET[ $this->query_prefix . $query_key ] ) );
				$this->filter_params[ $query_key ]['selected_value'] = $filter_terms;
				$this->filter_params[ $query_key ]['selected_array'] = explode( ',', $filter_terms );
			} else {
				/*$this->filter_params[ $query_key ]['selected_value'] = '';
				$this->filter_params[ $query_key ]['selected_array'] = [];*/
			}

			if ( $query_key == 'search' && ! empty( $_GET[ $query_key ] ) ) {
				$filter_terms                                        = $this->term_clean( wp_unslash( $_GET[ $query_key ] ) );
				$this->filter_params[ $query_key ]['selected_value'] = $filter_terms;
			} else {
				//$this->filter_params[ $query_key ]['selected_value'] = '';
			}

		endforeach;

		return $this->filter_params;
	}

	public function term_clean( $var ) {
		$var = str_replace( '/', '', $var );

		if ( is_array( $var ) ) :
			return array_map( [ $this, 'term_clean' ], $var );
		else :
			return is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
		endif;
	}


	public function get_terms( $taxonomy, $query_key, $args ) {
		global $wpdb, $wp_query;

		$terms = get_terms( [
			'taxonomy'   => $taxonomy,
			'order'      => 'asc',
			'orderby'    => 'title',
			'hide_empty' => true,
		] );

		if ( empty( $terms ) ) {
			return [];
		}

		$term_ids = wp_list_pluck( $terms, 'term_id' );

		$query_object = get_queried_object();
		$tax_query    = $this->get_main_tax_query();
		$meta_query   = $this->get_main_meta_query();
		$custom_query = $this->get_main_custom_query();

		if ( $query_object instanceof \WP_Term && $query_object->taxonomy == $taxonomy ) {
			return [];
		}

		if ( $query_object instanceof \WP_Term && $query_object->taxonomy != $taxonomy ) {
			$tax_query = $this->unset_taxonomy_meta_query( $tax_query, $taxonomy );
		}

		if ( isset( $args['condition'] ) && $args['condition'] == 'OR' ) {
			$tax_query = $this->unset_taxonomy_meta_query( $tax_query, $taxonomy );
		}

		$tax_query        = new \WP_Tax_Query( $tax_query );
		$meta_query       = new \WP_Meta_Query( $meta_query );
		$tax_query_sql    = $tax_query->get_sql( $wpdb->posts, 'ID' );
		$meta_query_sql   = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
		$custom_query_sql = $this->get_custom_query_sql( $custom_query, true );
		$wpml_query_sql   = $this->get_wpml_query_sql( $custom_query, true );
		$search_query_sql = '';

		if ( isset( $this->filter_params['search'] ) && isset( $_GET[ $this->filter_params['search']['query_key'] ] ) &&
		     ( isset( $args['independent_data_search'] ) && $args['independent_data_search'] !== true ) ) {
			$search_param = $this->filter_params['search'];
			$keyword      = $_GET[ $this->filter_params['search']['query_key'] ];

			$keyword = stripslashes( $keyword );
			$keyword = urldecode( $keyword );
			$keyword = str_replace( array( "\r", "\n" ), '', $keyword );

			if ( preg_match_all( '/".*?("|$)|((?<=[\t ",+])|^)[^\t ",+]+/', $keyword, $matches ) ) {
				$keywords = $this->parse_search_terms( $matches[0] );
				// If the search string has only short terms or stopwords, or is 10+ terms long, match it as sentence.
				if ( empty( $keywords ) || count( $keywords ) > 9 ) {
					$keywords = array( $keyword );
				}
			} else {
				$keywords = array( $keyword );
			}

			if ( ! empty( $keywords ) ) {
				$n         = ! empty( $search_param['exact'] ) ? '' : '%';
				$searchand = '';

				foreach ( $keywords as $term ) {
					$like_op  = 'LIKE';
					$andor_op = 'OR';
					$like     = $n . $wpdb->esc_like( $term ) . $n;

					$search_sql   = [];
					$search_sql[] = "({$wpdb->posts}.post_title $like_op '$like')";
					$search_sql[] = "({$wpdb->posts}.post_excerpt $like_op '$like')";
					$search_sql[] = "({$wpdb->posts}.post_content $like_op '$like')";

					$search_query_sql .= "{$searchand}(" . join( " {$andor_op} ", $search_sql ) . ")";

					$searchand = ' AND ';
				}

				$search_query_sql = ' AND ' . $search_query_sql;
			}
		}

		// Generate query.
		$query           = [];
		$query['select'] = "SELECT COUNT( DISTINCT {$wpdb->posts}.ID ) as term_count, terms.term_id as term_count_id";
		$query['from']   = "FROM {$wpdb->posts}";
		$query['join']   = "
			INNER JOIN {$wpdb->term_relationships} AS term_relationships ON {$wpdb->posts}.ID = term_relationships.object_id
			INNER JOIN {$wpdb->term_taxonomy} AS term_taxonomy USING( term_taxonomy_id )
			INNER JOIN {$wpdb->terms} AS terms USING( term_id )
			" . $tax_query_sql['join'] . $meta_query_sql['join'] . $custom_query_sql['join'] . $wpml_query_sql['join'];

		$terms_ids = implode( ',', array_map( 'absint', $term_ids ) );

		$query['where'] = "
			WHERE {$wpdb->posts}.post_type IN ( '" . esc_sql( $this->post_type ) . "' )
			AND {$wpdb->posts}.post_status = 'publish'"
		                  . $search_query_sql . $tax_query_sql['where']
		                  . $meta_query_sql['where'] . $custom_query_sql['where'] . $wpml_query_sql['where'] .
		                  ' AND (terms.term_id IN (' . $terms_ids . ') OR term_taxonomy.parent IN (' . $terms_ids . '))';

		$query['group_by'] = 'GROUP BY term_count_id';

		$query = implode( ' ', $query );

		// We have a query - let's see if cached results of this query already exist.
		$query_hash = md5( $query );

		if ( true === static::$cache ) {
			$cached_counts = (array) get_transient( 'app_filter_terms_counts_' . sanitize_title( $taxonomy ) );
		} else {
			$cached_counts = [];
		}

		if ( ! isset( $cached_counts[ $query_hash ] ) ) {
			$results                      = $wpdb->get_results( $query, ARRAY_A ); // @codingStandardsIgnoreLine
			$term_counts                  = array_map( 'absint', wp_list_pluck( $results, 'term_count', 'term_count_id' ) );
			$cached_counts[ $query_hash ] = $term_counts;

			if ( true === static::$cache ) {
				set_transient( 'app_filter_terms_counts_' . sanitize_title( $taxonomy ), $cached_counts, DAY_IN_SECONDS );
			}
		}

		$term_counts = array_map( 'absint', (array) $cached_counts[ $query_hash ] );

		$new_terms       = [];
		$filter_selected = $this->get_filter_selected();
		$filter_selected = $filter_selected[ $query_key ]['selected_array'];

		/*usort( $terms, function ( $term_a, $term_b ) {
			$sort = $term_a->parent <=> $term_b->parent;

			if ( $sort === 0 ) {
				$sort = $term_a->name <=> $term_b->name;
			}

			return $sort;
		} );*/

		foreach ( $terms as $term ) {
			$term_count = ! empty( $term_counts[ $term->term_id ] ) ? $term_counts[ $term->term_id ] : 0;

			if ( $term->parent == 0 ) {
				$term_counts[ $term->term_id ] = $term_count;
			} else {
				$term_counts[ $term->parent ] = ( $term_counts[ $term->parent ] ?? 0 ) + $term_count;
			}
		}

		foreach ( $terms as $term ) {
			$option_is_set = ! empty( $filter_selected ) && in_array( $term->slug, $filter_selected, true );
			$count         = $term_counts[ $term->term_id ] ?? 0;

			if ( isset( $args['hide_count_zero'] ) && $args['hide_count_zero'] === true ) {
				if ( 0 === $count && ! $option_is_set ) {
					continue;
				}
			}

			$term->count = $count;
			$new_terms[] = $term;
		}

		unset( $terms );

		return $new_terms;
	}

	public function unset_taxonomy_meta_query( &$tax_query, $taxonomy ) {
		if ( empty( $tax_query ) ) {
			return $tax_query;
		}

		foreach ( $tax_query as $key => &$tax ) {
			if ( is_array( $tax ) && isset( $tax['taxonomy'] ) ) {
				if ( $taxonomy == $tax['taxonomy'] ) {
					unset( $tax_query[ $key ] );
				}
			} elseif ( is_array( $tax ) ) {
				$this->unset_taxonomy_meta_query( $tax, $taxonomy );
			}
		}

		return $tax_query;
	}

	public function get_custom_db_table_terms( $data ) {
		global $wpdb;

		if ( empty( $data['data_db_table'] ) ) {
			return [];
		}

		$query_table      = $wpdb->prefix . esc_sql( $data['data_db_table'] );
		$query_select_key = esc_sql( $data['data_field_key'] );
		$query_select_id  = esc_sql( $data['data_field_id'] );

		$query           = [];
		$query['select'] = "SELECT {$query_select_id} as id, {$query_select_key} as name, COUNT( DISTINCT {$query_select_key} ) as count";
		$query['from']   = "FROM {$query_table}";
		$query['where']  = "WHERE $query_select_key != ''";
		$query['group']  = "GROUP BY {$query_select_key}";

		$query = implode( ' ', $query );

		$wpdb->show_errors( true );
		$results = $wpdb->get_results( $query );

		if ( ! empty( $results ) ) {
			foreach ( $results as &$result ) {
				$result->slug = sanitize_title( $result->name );
			}
		}

		return $results;
	}

	public function get_custom_query_sql( $query, $join = false ) {
		return [
			'join'  => '',
			'where' => ''
		];
	}

	public function get_wpml_query_sql( $query, $join = false ) {
		global $wpdb;

		$join = $where = '';

		if ( defined( 'ICL_LANGUAGE_CODE' ) && ! empty( $current_lang = apply_filters( 'wpml_current_language', null ) ) ) {

			$join = "JOIN {$wpdb->prefix}icl_translations wpml_translations
				ON {$wpdb->posts}.ID = wpml_translations.element_id
				   AND wpml_translations.element_type = CONCAT('post_',{$wpdb->posts}.post_type)";

			$where = " AND (wpml_translations.language_code = '" . esc_sql( $current_lang ) . "'
		OR(wpml_translations.language_code = '" . esc_sql( $current_lang ) . "'
			AND {$wpdb->posts}.post_type IN('" . esc_sql( $this->post_type ) . "')
			AND(((
				SELECT
					COUNT(element_id)
					FROM {$wpdb->prefix}icl_translations
				WHERE
					trid = wpml_translations.trid
					AND language_code = '" . esc_sql( $current_lang ) . "') = 0)
			OR((
				SELECT
					COUNT(element_id)
					FROM {$wpdb->prefix}icl_translations t2
					JOIN {$wpdb->posts} p ON p.id = t2.element_id
				WHERE
					t2.trid = wpml_translations.trid
					AND t2.language_code = '" . esc_sql( $current_lang ) . "'
					AND(p.post_status = 'publish'
						OR p.post_status = 'private'
						OR(p.post_type = 'attachment'
							AND p.post_status = 'inherit'))) = 0))))";

		}

		return [
			'join'  => $join,
			'where' => $where
		];
	}

	public function custom_filter_query_post_clauses( $clauses, $query ) {
		return $clauses;
	}

	public function filter_tax_query( $tax_query, $taxonomy ) {
		foreach ( $tax_query->queries as $key => $query ) {
			if ( ! empty( $query['taxonomy'] ) && $query['taxonomy'] == $taxonomy ) {
				unset( $tax_query->queries[ $key ] );
			}
		}

		if ( isset( $tax_query->queried_terms[ $taxonomy ] ) ) {
			unset( $tax_query->queried_terms[ $taxonomy ] );
		}

		return $tax_query;
	}

	public function clear_filter_cache( $post_id ) {
		$post = get_post( $post_id );

		/*if ( $post && $post->post_type == $this->post_type ) {
			delete_transient( $this->filter_cache_data_key );
		}*/
	}


	public function clear_filter_terms_cache( $post_id ) {
		$post = get_post( $post_id );

		if ( $post && $post->post_type == $this->post_type ) {
			foreach ( $this->array_filters as $filter ) {
				if ( isset( $filter['taxonomy'] ) ) {
					delete_transient( 'app_filter_terms_counts_' . sanitize_title( $filter['taxonomy'] ) );
				} elseif ( isset( $filter['data_db_table'] ) ) {
					delete_transient( 'app_filter_terms_counts_' . sanitize_title( $filter['data_db_table'] ) );
				}
			}
		}
	}

	public function clear_filter_tax_terms_cache( $object_id, $term_id, $tt_id, $taxonomy ) {
		foreach ( $this->array_filters as $filter ) {
			if ( isset( $filter['taxonomy'] ) && $filter['taxonomy'] == $taxonomy ) {
				delete_transient( 'app_filter_terms_counts_' . $filter['taxonomy'] );
			}
		}
	}

	public function get_query() {
		return $this->query;
	}

	public function get_main_tax_query() {
		global $wp_query;

		$tax_query = isset( $wp_query->tax_query->queries ) ? $wp_query->tax_query->queries : array();

		return $tax_query;
	}

	/**
	 * Get the meta query which was used by the main query.
	 *
	 * @return array
	 */
	public static function get_main_meta_query() {
		global $wp_query;

		$args       = $wp_query->query_vars;
		$meta_query = isset( $args['meta_query'] ) ? $args['meta_query'] : array();

		return $meta_query;
	}

	/**
	 * Get the meta query which was used by the main query.
	 *
	 * @return array
	 */
	public static function get_main_custom_query() {
		global $wp_query;

		$args         = $wp_query->query_vars;
		$custom_query = isset( $args['custom_query'] ) ? $args['custom_query'] : array();

		return $custom_query;
	}

	public function filter_item_hidden_field( $item ) {

		return sprintf( '<input class="taxonomy-terms-value" type="hidden" name="%s" value="%s" %s>',
			$item['query_key'],
			$item['selected_value'],
			disabled( empty( $item['selected_value'] ), true, false )
		);
	}

	public function filter_item_term_field( $term, $item, $args = [], $sub_item = '' ) {
		$default = [
			'type'         => 'checkbox',
			'show_count'   => false,
			'class'        => 'posts-filter__terms-item',
			'label_before' => '',
			'label_after'  => '',
			'attr'         => []
		];

		$args = wp_parse_args( $args, $default );

		if ( count( $item['selected_array'] ) == 0 && ! empty( $item['default_value'] ) ) {
			$checked = $term->slug == $item['default_value'];
		} else {
			$checked = in_array( $term->slug, $item['selected_array'] );
		}

		if ( ! empty( $item['choose_type'] ) && $item['choose_type'] == 'single' ) {
			$span_class = 'custom-radio';
		} else {
			$span_class = 'custom-checkbox';
		}

		$value = $term->slug;

		$term_name = $term->name;
		$disabled  = false;

		if ( property_exists( $term, 'count' ) ) {
			if ( $args['show_count'] ) {
				$term_name = sprintf( '%s (%s)', $term_name, $term->count );
			}

			if ( isset( $item['hide_count_zero'] ) && true === $item['hide_count_zero'] && $term->count == 0 ) {
				$disabled      = true;
				$args['class'] = $args['class'] . ' disabled';
			}

			if ( isset( $item['hide_count_zero'] ) && false === $item['hide_count_zero'] && $term->count == 0 ) {
				$disabled      = true;
				$args['class'] = $args['class'] . ' inactive';
			}
		}

		$attrs        = [
			'class'     => esc_attr( $args['class'] ),
			'data-name' => esc_attr( $term->name )
		];
		$args['attr'] = array_merge( $attrs, $args['attr'] );

		$checkbox = sprintf( '<input id="%s" class="term" value="%s" type="%s"  %s %s>',
			esc_attr( $term->slug ),
			esc_attr( $value ),
			esc_attr( $args['type'] ),
			checked( $checked, true, false ),
			disabled( $disabled, true, false )
		);

		$label = sprintf( '%s<label for="%s" class="term-label%s%s">%s<span class="label">%s</span>%s%s</label>',
			$checkbox,
			esc_attr( $term->slug ),
			$checked ? ' chosen' : '',
			$sub_item ? ' term-label-toggle opened' : '',
			$args['label_before'],
			$term_name,
			$args['label_after'],
			$sub_item ? '<span class="icon-arrow"></span>' : '',
		);

		$wrap = sprintf( '<li %s><span class="%s">%s%s</span></li>', Helper::render_attributes( $args['attr'] ), $span_class, $label, $sub_item );

		return $wrap;
	}


	/**
	 * Check if the terms are suitable for searching.
	 *
	 * Uses an array of stopwords (terms) that are excluded from the separate
	 * term matching when searching for posts. The list of English stopwords is
	 * the approximate search engines list, and is translatable.
	 *
	 * @param string[] $terms Array of terms to check.
	 *
	 * @return string[] Terms that are not stopwords.
	 * @since 3.7.0
	 *
	 */
	protected function parse_search_terms( $terms ) {
		$strtolower = function_exists( 'mb_strtolower' ) ? 'mb_strtolower' : 'strtolower';
		$checked    = array();

		$stopwords = $this->get_search_stopwords();

		foreach ( $terms as $term ) {
			// Keep before/after spaces when term is for exact match.
			if ( preg_match( '/^".+"$/', $term ) ) {
				$term = trim( $term, "\"'" );
			} else {
				$term = trim( $term, "\"' " );
			}

			// Avoid single A-Z and single dashes.
			if ( ! $term || ( 1 === strlen( $term ) && preg_match( '/^[a-z\-]$/i', $term ) ) ) {
				continue;
			}

			if ( in_array( call_user_func( $strtolower, $term ), $stopwords, true ) ) {
				continue;
			}

			$checked[] = $term;
		}

		return $checked;
	}

	/**
	 * Retrieve stopwords used when parsing search terms.
	 *
	 * @return string[] Stopwords.
	 * @since 3.7.0
	 *
	 */
	protected function get_search_stopwords() {
		if ( isset( $this->stopwords ) ) {
			return $this->stopwords;
		}

		/*
		 * translators: This is a comma-separated list of very common words that should be excluded from a search,
		 * like a, an, and the. These are usually called "stopwords". You should not simply translate these individual
		 * words into your language. Instead, look for and provide commonly accepted stopwords in your language.
		 */
		$words = explode(
			',',
			_x(
				'about,an,are,as,at,be,by,com,for,from,how,in,is,it,of,on,or,that,the,this,to,was,what,when,where,who,will,with,www',
				'Comma-separated list of search stopwords in your language'
			)
		);

		$stopwords = array();
		foreach ( $words as $word ) {
			$word = trim( $word, "\r\n\t " );
			if ( $word ) {
				$stopwords[] = $word;
			}
		}

		/**
		 * Filters stopwords used when parsing search terms.
		 *
		 * @param string[] $stopwords Array of stopwords.
		 *
		 * @since 3.7.0
		 *
		 */
		$this->stopwords = apply_filters( 'wp_search_stopwords', $stopwords );

		return $this->stopwords;
	}
}