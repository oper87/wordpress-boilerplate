<?php
/**
 * Abstract Post Type Class
 */

namespace App\Abstracts;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class Post {

	/**
	 * @var string
	 */
	protected string $taxonomy;

	/**
	 * @var string
	 */
	protected string $post_type;

	protected function __construct( $post_type, $args = [] ) {
		$this->post_type = $post_type;

		add_filter( 'manage_edit-' . $this->post_type . '_columns', [ $this, 'add_custom_column' ] );
		add_action( 'manage_' . $this->post_type . '_posts_custom_column', [ $this, 'add_custom_column_output' ], 10, 2 );
	}

	public function register_post_type() {
		add_action( 'init', [ $this, 'init_register_post_type' ] );
	}

	public function init_register_post_type() {
		$args           = $this->get_settings();
		$args['labels'] = $this->get_labels();

		register_post_type( $this->post_type, $args );
	}

	/**
	 * @return array
	 */
	protected function get_labels(): array {
		return [
			'name'                  => _x( 'Posts', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Post', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Posts', 'text_domain' ),
			'name_admin_bar'        => __( 'Posts', 'text_domain' ),
			'archives'              => __( 'Item Archives', 'text_domain' ),
			'attributes'            => __( 'Item Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'All Items', 'text_domain' ),
			'add_new_item'          => __( 'Add New Item', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Item', 'text_domain' ),
			'edit_item'             => __( 'Edit Item', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'View Item', 'text_domain' ),
			'view_items'            => __( 'View Items', 'text_domain' ),
			'search_items'          => __( 'Search Item', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		];
	}

	/**
	 * @return array
	 */
	protected function get_settings(): array {
		return [
			'supports'            => [ 'title', 'thumbnail', 'custom-fields' ],
			//'taxonomies'          => [ self::TAXONOMY ],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-palmtree',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		];
	}

	/**
	 * Add custom column
	 */
	public function add_custom_column( $columns ) {
		return $columns;
	}

	/**
	 * Add custom column callback
	 *
	 * @param $column_name
	 * @param $post_id
	 */
	public function add_custom_column_output( $column_name, $post_id ) {

	}

	/**
	 * Add sortable custom columns
	 *
	 * @param $columns
	 *
	 * @return mixed
	 */
	public function add_custom_sortable_columns( $columns ) {
		return $columns;
	}

	/**
	 * Add order query for sortable custom columns
	 *
	 * @param $query
	 */
	public function make_custom_column_sortable( $query ) {
		return;
	}

	/**
	 * Related posts
	 */
	protected function get_related( $args = [] ) {
		global $wpdb;

		$default = [
			'per_page'    => 3,
			'term_ids'    => [],
			'exclude_ids' => [],
			'tax_query'   => []
		];

		$args = wp_parse_args( $args, $default );

		$limit            = $args['per_page'] + 10;
		$include_term_ids = array_merge( $args['term_ids'], [] );
		$exclude_ids      = $args['exclude_ids'];

		$tax_query     = new \WP_Tax_Query( $args['tax_query'] );
		$tax_query_sql = $tax_query->get_sql( 'p', 'ID' );

		if ( empty( $include_term_ids ) ) {
			return false;
		}

		$sql = "
			SELECT * FROM ( SELECT p.ID, p.post_name, taxonomy
			FROM {$wpdb->posts} p
			INNER JOIN ( 
			    SELECT object_id, taxonomy 
			    FROM {$wpdb->term_relationships} 
		        INNER JOIN {$wpdb->term_taxonomy} using( term_taxonomy_id ) 
			    WHERE term_id IN ( " . implode( ',', array_map( 'absint', $include_term_ids ) ) . " ) 
		    ) AS include_join ON include_join.object_id = p.ID
		    " . $tax_query_sql['join'] . "
		    WHERE 1=1
			AND p.post_status = 'publish'
			AND p.post_type = '%s'
			AND p.ID NOT IN ( " . implode( ',', array_map( 'absint', $exclude_ids ) ) . " )
			" . $tax_query_sql['where'] . "
			ORDER BY taxonomy DESC, p.post_date DESC ) AS tmp_table GROUP BY tmp_table.ID
			LIMIT %d";

		$post_ids = $wpdb->get_col( $wpdb->prepare( $sql, $this->post_type, absint( $limit ) ) );

		shuffle( $post_ids );

		return array_slice( $post_ids, 0, $args['per_page'] );
	}
}