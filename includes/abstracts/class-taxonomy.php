<?php
/**
 * Abstract Taxonomy Class
 */

namespace App\Abstracts;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class Taxonomy {

	/**
	 * @var string
	 */
	protected string $taxonomy;

	/**
	 * @var array
	 */
	protected array $object_types;

	public function __construct( $taxonomy, $object_types ) {
		$this->taxonomy     = $taxonomy;
		$this->object_types = is_string( $object_types ) ? (array) $object_types : $object_types;
	}

	public function register_taxonomy() {
		add_action( 'init', [ $this, 'init_register_taxonomy' ] );
	}

	public function init_register_taxonomy() {
		$args           = $this->get_settings();
		$args['labels'] = $this->get_labels();

		register_taxonomy( $this->taxonomy, $this->object_types, $args );
	}

	/**
	 * @return array
	 */
	protected function get_labels(): array {
		return [
			'name'                       => _x( 'Category', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Categories', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Category', 'text_domain' ),
			'all_items'                  => __( 'All Items', 'text_domain' ),
			'parent_item'                => __( 'Parent Item', 'text_domain' ),
			'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
			'new_item_name'              => __( 'New Item Name', 'text_domain' ),
			'add_new_item'               => __( 'Add New Item', 'text_domain' ),
			'edit_item'                  => __( 'Edit Item', 'text_domain' ),
			'update_item'                => __( 'Update Item', 'text_domain' ),
			'view_item'                  => __( 'View Item', 'text_domain' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
			'popular_items'              => __( 'Popular Items', 'text_domain' ),
			'search_items'               => __( 'Search Items', 'text_domain' ),
			'not_found'                  => __( 'Not Found', 'text_domain' ),
			'no_terms'                   => __( 'No items', 'text_domain' ),
			'items_list'                 => __( 'Items list', 'text_domain' ),
			'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
		];
	}

	/**
	 * @return array
	 */
	protected function get_settings(): array {
		return [];
	}

	/**
	 * Get term
	 *
	 * @param $term
	 * @param string $output
	 * @param string $filter
	 *
	 * @return \WP_Term|false
	 */
	public function get_term( $term, string $output = OBJECT, string $filter = 'raw' ) {
		$term = get_term( $term, $this->taxonomy, $output, $filter );

		if ( empty( $term ) || is_wp_error( $term ) ) {
			return false;
		}

		return $term;
	}

	/**
	 * Get term
	 *
	 * get_term_by( $field, $value, $taxonomy = '', $output = OBJECT, $filter = 'raw' ) {
	 *
	 * @param $field
	 * @param $value
	 * @param string $output
	 * @param string $filter
	 *
	 * @return \WP_Term|false
	 */
	public function get_term_by( $field, $value, string $output = OBJECT, string $filter = 'raw' ) {
		$term = get_term_by( $field, $value, $this->taxonomy, $output, $filter );

		if ( empty( $term ) || is_wp_error( $term ) ) {
			return false;
		}

		return $term;
	}

	/**
	 * Get terms
	 *
	 * @param array $args
	 *
	 * @return \WP_Error|\WP_Term[]
	 */
	public function get_terms( array $args = [] ) {
		$default = [
			'taxonomy' => $this->taxonomy
		];

		$args = wp_parse_args( $args, $default );

		return get_terms( $args );
	}

	/**
	 * Get terms
	 *
	 * @param $post_id
	 *
	 * @return array
	 */
	public function get_the_terms( $post_id, $primary_order = false ): array {
		$terms = get_the_terms( $post_id, $this->taxonomy );

		if ( is_wp_error( $terms ) || empty( $terms ) ) {
			return [];
		}

		if ( $primary_order ) {
			$primary_id = get_post_meta( $post_id, '_yoast_wpseo_primary_' . $this->taxonomy, true );

			$terms      = array_map( function ( $term ) use ( $primary_id ) {
				$term->primary = $primary_id == $term->term_id ? 1 : 0;

				return $term;
			}, $terms );

			usort( $terms, function ( $term_1, $term_2 ) {
				return $term_1->primary <= $term_2->primary;
			} );
		}

		return $terms;
	}

	/**
	 * Get first term
	 *
	 * @param $post_id
	 *
	 * @return \WP_Term|bool
	 */
	public function get_single_term( $post_id ) {
		return current( self::get_the_terms( $post_id ) );
	}

	/**
	 * Get terms
	 *
	 * @param $post_id
	 * @param array $args
	 *
	 * @return array
	 */
	public function get_posts_terms( $post_id, array $args = [] ): array {
		$terms = wp_get_post_terms( $post_id, $this->taxonomy, $args );

		if ( is_wp_error( $terms ) || empty( $terms ) ) {
			return [];
		}

		return $terms;
	}

	/**
	 * @param $term
	 * @param $post_id
	 *
	 * @return bool
	 */
	public function has_term( $term, $post_id ): bool {
		return has_term( $term, $this->taxonomy, $post_id );
	}
}