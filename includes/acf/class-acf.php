<?php

namespace App\Acf;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Acf {

	public function __construct() {
		//new Group_Field();

		add_action( 'acf/include_field_types', array( $this, 'include_field' ) );
	}

	public function include_field( $version = false ) {
		if ( class_exists( 'WPCF7_ContactForm' ) ) {
			new Field_Contact_Form7();
		}

		if ( function_exists( 'WC' ) ) {
			new Field_Product_Object();
		}
	}
}