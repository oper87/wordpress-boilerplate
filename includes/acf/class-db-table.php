<?php

/**
 * https://github.com/eduardo-marcolino/acf-fields-in-custom-table
 * https://anchor.host/acf-custom-fields-stored-in-custom-table/
 */

namespace App\Acf;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class Db_Table {

	public $wpdb;

	protected string $post_type = '';

	protected string $table_name = '';

	protected $fields_map = [];
	protected $acf_fields = [];
	protected $def_fields = [];

	protected static $def_repeater_fields = [];

	protected static $repeater_db_table = [];

	public array $key_fields = [];

	public array $fields = [];

	public array $repeater_fields = [];

	public function __construct() {
		global $wpdb;

		$this->wpdb = $wpdb;

		$this->set_fields();
		$this->set_def_fields( $this->get_fields() );
		$this->set_map_fields( $this->get_fields() );

		/**
		 * Process of replacing
		 */
		add_filter( 'acf/pre_update_value', [ $this, 'disabled_acf_field_update_value' ], 10, 4 );
		add_action( 'acf/save_post', [ $this, 'store_fields_in_custom_table' ], 1 );
		add_filter( 'acf/load_value', [ $this, 'load_field_from_custom_table' ], 11, 3 );
		add_filter( 'acf/pre_load_reference', [ $this, 'pre_load_reference_from_custom_table' ], 11, 3 );

		add_action( 'deleted_post', [ $this, 'delete_post_values_in_custom_table' ], 10, 2 );
	}

	public function disabled_acf_field_update_value( $check, $value, $post_id, $field ) {
		$post = get_post( $post_id );

		/**
		 * Detect type of entity
		 */
		if ( empty( $post ) ) {
			return $check;
		}

		if ( $this->post_type != $post->post_type ) {
			return $check;
		}

		// Disregard updating certain fields as they've already been stored in a custom table.
		$match_field = array_map( function ( $key ) use ( $field ) {
			return ( strpos( $field['name'], $key ) !== false );
		}, $this->get_map_fields() );//$this->get_map_fields() static::$fields_map

		$match_field = array_filter( $match_field );

		if ( ! empty( $match_field ) ) {
			$check = true;
		}

		$match_field = array_map( function ( $key ) use ( $field ) {
			return ( strpos( $field['name'], $key ) !== false );
		}, static::$repeater_db_table );

		$match_field = array_filter( $match_field );

		if ( ! empty( $match_field ) ) {
			$check = true;
		}

		return $check;
	}

	public function store_fields_in_custom_table( $post_id ) {
		global $wpdb;

		$post = get_post( $post_id );

		/**
		 * Detect type of entity
		 */
		if ( empty( $post ) ) {
			return;
		}

		if ( $this->post_type != $post->post_type ) {
			return;
		}

		// bail early if no ACF data
		if ( empty( $_POST['acf'] ) ) {
			return;
		}

		// array of field values
		$fields = $_POST['acf'];

		$_fields = $this->match_field_values( $fields );

		$this->prepare_field_values( $_fields );

		if ( $this->fields || $this->repeater_fields ) {
			$values = stripslashes_deep( $this->fields );

			$data = $this->wpdb->get_row( $this->wpdb->prepare(
				"SELECT * FROM {$this->table_name()} WHERE post_id = %d", $post_id
			) );

			$values = apply_filters( 'acf_db_table_update_values', $values, $post_id, $this->table_name() );

			if ( empty( $data ) ) {
				$values['post_id'] = $post_id;
				$this->wpdb->insert( $this->table_name(), $values );
			} else {
				$where = [ 'post_id' => $post_id ];
				$where = apply_filters( 'acf_db_table_update_values_where', $where, $post_id, $this->table_name() );

				$this->wpdb->update( $this->table_name(), $values, $where );
			}

			if ( $this->repeater_fields ) {
				$this->store_repeater_field_in_custom_table( $this->repeater_fields, $post_id );
			}

			wp_cache_delete( "format_value/post_id={$post_id}", 'acf' );
		}
	}

	public function store_repeater_field_in_custom_table( $repeater_fields, $post_id, $parent_row_id = false ) {
		global $wpdb;

		$this->wpdb->show_errors( true );

		foreach ( $repeater_fields as $repeater_key => $repeater_rows ) {
			$table_name = $this->repeater_table_name( $repeater_key );

			$this->delete_repeater_field_in_custom_table( $repeater_key, $repeater_rows, $post_id, $parent_row_id );

			foreach ( $repeater_rows as $order => $row_values ) {
				$sql = [
					'select' => 'SELECT * ',
					'from'   => 'FROM ' . $table_name,
					'where'  => $this->wpdb->prepare( 'WHERE post_id = %d AND row_order = %d', $post_id, $order ),
					'limit'  => 'LIMIT 1'
				];

				if ( false !== $parent_row_id ) {
					$sql['where'] .= $this->wpdb->prepare( ' AND row_parent_id = %d', $parent_row_id );
				}

				$data = $this->wpdb->get_row( join( ' ', $sql ) );

				$values = array_filter( $row_values, function ( $value ) {
					return empty( $value ) || ! is_array( $value );
				} );

				$values = stripslashes_deep( $values );

				$child_values = array_filter( $row_values, function ( $value ) {
					return is_array( $value );
				} );

				$foreign_key_id = $order;

				if ( empty( $data ) ) {
					$values['post_id']   = $post_id;
					$values['row_order'] = $order;

					if ( false !== $parent_row_id ) {
						$values['row_parent_id'] = $parent_row_id;
					}

					$this->wpdb->show_errors( true );
					$this->wpdb->insert(
						$table_name,
						$values,
						$this->store_repeater_field_value_format( $values )
					);
				} else if ( ! empty( $values ) ) {
					$where_value = [
						'post_id'   => $post_id,
						'row_order' => $order
					];

					if ( false !== $parent_row_id ) {
						$where_value['row_parent_id'] = $parent_row_id;
					}

					$this->wpdb->update(
						$table_name,
						$values,
						$where_value,
						$this->store_repeater_field_value_format( $values ),
						$this->store_repeater_field_value_format( $where_value )
					);
				}

				if ( ! empty( $child_values ) ) {
					$this->store_repeater_field_in_custom_table( $child_values, $post_id, $foreign_key_id );
				}
			}
		}
	}

	public function delete_repeater_field_in_custom_table( $repeater_key, $repeater_rows, $post_id, $parent_row_id = false ) {
		global $wpdb;

		$table_name = $this->repeater_table_name( $repeater_key );

		$row_sql = [
			'select' => 'SELECT * ',
			'from'   => 'FROM ' . $table_name,
			'where'  => $this->wpdb->prepare( 'WHERE post_id = %d', $post_id ),
		];

		if ( false !== $parent_row_id ) {
			$row_sql['where'] .= $this->wpdb->prepare( ' AND row_parent_id = %d', $parent_row_id );
		}

		$row_data = $this->wpdb->get_results( join( ' ', $row_sql ) );

		if ( count( $row_data ) > count( $repeater_rows ) ) {
			$sql = [
				'select' => 'DELETE',
				'from'   => 'FROM ' . $table_name,
				'where'  => $this->wpdb->prepare( 'WHERE post_id = %d AND row_order >= %d', $post_id, count( $repeater_rows ) ),
			];

			if ( false !== $parent_row_id ) {
				$sql['where'] .= $this->wpdb->prepare( ' AND row_parent_id = %d', $parent_row_id );
			}

			$this->wpdb->get_results( join( ' ', $sql ) );

			if ( isset( static::$def_repeater_fields[ $repeater_key ] ) ) {
				$child_repeater_key  = array_shift( static::$def_repeater_fields[ $repeater_key ] );
				$child_repeater_rows = $repeater_rows[ static::$def_repeater_fields[ $repeater_key ] ] ?? [];

				for ( $i = count( $repeater_rows ); $i < count( $row_data ); $i ++ ) {
					$this->delete_repeater_field_in_custom_table( $child_repeater_key, $child_repeater_rows, $post_id, $i );
				}
			}
		}
	}

	public function store_repeater_field_value_format( $values ): array {
		return array_map( function ( $key, $val ) {
			switch ( $key ) {
				case 'row_parent_id':
				case 'post_id':
				case 'row_order':
					$f = '%d';
					break;
				default:
					$f = '%s';
					break;
			}

			return $f;
		}, array_keys( $values ), array_values( $values ) );
	}

	public function match_field_values( $fields ): array {
		$n = [];

		foreach ( $fields as $key => $val ) {
			$field = get_field_object( sanitize_key( $key ) );

			if ( empty( $field ) ) {
				$name = $key;
			} else {
				$name = $field['name'];
			}

			if ( ! in_array( $name, $this->get_map_fields() ) ) {
				continue;
			}

			if ( is_array( $val ) ) {
				if ( empty( $field ) ) {
					$n[ $name ] = $this->match_field_values( $val );
				} else if ( $field['type'] == 'group' || $field['type'] == 'repeater' ) {
					$n[ $name ] = [
						'type' => $field['type'],
						'key'  => $field['key'],
						'rows' => $this->match_field_values( $val )
					];
				} else if ( $field ) {
					$n[ $name ] = [
						'type'  => $field['type'],
						'key'   => $field['key'],
						'value' => $val
					];
				}
			} else if ( empty( $val ) && $field['type'] == 'repeater' ) {
				$n[ $name ] = [
					'type' => $field['type'],
					'key'  => $field['key'],
					'rows' => []
				];
			} else {
				$n[ $name ] = [
					'type'  => $field['type'],
					'key'   => $field['key'],
					'value' => $val
				];
			}
		}

		return $n;
	}

	public function prepare_field_values( $fields, $prefix = '' ) {

		foreach ( $fields as $name => $field ) {
			$_name = sanitize_key( $name );
			$_name = str_replace( '-', '_', $_name );

			if ( ! empty( $prefix ) ) {
				$_name = $prefix . '_' . $name;
			}

			switch ( $field['type'] ) :
				case 'group':
					$this->key_fields[ $_name ] = $field['key'];

					if ( ! empty( $field['rows'] ) ) {
						$this->prepare_field_values( $field['rows'], $name );
					}
					break;
				case 'repeater':
					$this->key_fields[ $_name ] = $field['key'];

					$this->repeater_fields[ $_name ] = $this->prepare_repeater_field_values( $field );
					break;
				default:
					$this->fields[ $_name ]     = $field['value'];
					$this->key_fields[ $_name ] = $field['key'];
					break;
			endswitch;
		}
	}

	public function prepare_repeater_field_values( $repeater_field ): array {
		$values = [];

		$i = 0;
		foreach ( $repeater_field['rows'] as $row_key => $rows ) {
			foreach ( $rows as $name => $row ) {
				$_name = sanitize_key( $name );
				$_name = str_replace( '-', '_', $_name );

				$this->key_fields[ $_name ] = $row['key'];

				if ( $row['type'] == 'repeater' ) {
					$values[ $i ][ $_name ] = $this->prepare_repeater_field_values( $row );
				} else {
					if ( is_array( $row['value'] ) ) {
						$value = maybe_serialize( $row['value'] );
					} else {
						$value = $row['value'];
					}

					$values[ $i ][ $_name ] = $value;
				}
			}
			$i ++;
		}

		return $values;
	}

	public function pre_load_reference_from_custom_table( $reference, $field_name, $post_id ) {
		$post = get_post( $post_id );

		if ( $post && $post->post_type == $this->post_type ) {
			$acf_store_file = 'acf-' . $this->post_type . '-fields.json';
			$acf_store_path = ABSPATH . 'wp-content/uploads/acf-stores/';

			if ( file_exists( $acf_store_path . $acf_store_file ) ) {
				$acf_fields_stored = file_get_contents( $acf_store_path . $acf_store_file );
				$acf_fields_stored = json_decode( $acf_fields_stored, JSON_OBJECT_AS_ARRAY );

				if ( key_exists( $field_name, $acf_fields_stored ) ) {
					return $acf_fields_stored[ $field_name ];
				}
			}
		}

		return $reference;
	}

	public function load_field_from_custom_table( $value, $post_id, $field ) {
		$post = get_post( $post_id );

		/**
		 * Detect type of entity
		 */
		if ( empty( $post ) ) {
			return $value;
		}

		if ( $this->post_type != $post->post_type ) {
			return $value;
		}

		if ( $this->is_field_value( $field ) ) {
			return $this->load_field_value( $value, $post_id, $field );
		} else if ( $this->is_repeater_field_value( $field ) ) {
			return $this->load_repeater_field_value( $value, $post_id, $field );
		}

		return $value;
	}

	public function is_field_value( $field ): bool {
		if ( in_array( $field['name'], $this->get_def_fields() ) ) {
			return true;
		}

		return false;
	}

	public function load_field_value( $value, $post_id, $field ) {
		global $post;

		$table_name  = $this->table_name();
		$column_name = sanitize_key( $field['name'] );

		/*$post->post_content = '';
		echo '<pre>'; print_r( $post ); echo '</pre>';
		echo '<pre>'; var_dump($field['name']); echo '</pre>';
		echo '<pre>'; var_dump($post->{$field['name']}); echo '</pre>';
		echo '<pre>'; var_dump($post->short_title); echo '</pre>';
		echo '<pre>'; var_dump(isset( $post->{$field['name']} )); echo '</pre>';
		echo '<pre>'; var_dump(property_exists($post, $field['name'])); echo '</pre>';*/

		if ( is_object( $post ) && property_exists( $post, $field['name'] ) ) {
			return $post->{$field['name']};
		}

		$found     = false;
		$db_values = wp_cache_get( "format_value/post_id={$post_id}", 'acf', false, $found );

		if ( ! $found ) {
			$db_values = apply_filters( 'acf_db_table_select_values', null, $column_name, $post_id, $field );

			if ( $db_values === null ) {
				$db_values = $this->wpdb->get_row( $this->wpdb->prepare(
					"SELECT * FROM $table_name WHERE post_id = %d LIMIT 1", $post_id
				), ARRAY_A );
			}

			wp_cache_set( "format_value/post_id={$post_id}", $db_values, 'acf', false );
		}

		if ( ! is_null( $db_values ) && isset( $db_values[ $column_name ] ) ) {
			$value = $db_values[ $column_name ];
		}

		return $value;
	}

	public function is_repeater_field_value( $field ): bool {
		$found = array_filter( array_keys( self::$def_repeater_fields ), function ( $key ) use ( $field ) {
			return preg_match( "/^" . $field['name'] . "/i", $key );
		} );

		if ( ! empty( $found ) ) {
			return true;
		}

		return false;
	}

	public function load_repeater_field_value( $value, $post_id, $field, $s = false ) {
		if ( empty( $field['sub_fields'] ) || $field['type'] != 'repeater' ) {
			return $value;
		}

		$data = $this->get_repeater_field_data( $field, $post_id );

		if ( ! $data ) {
			return $value;
		}

		return $this->match_repeater_field_data_value( $data, $field );
	}

	public function get_repeater_field_data( $field, $post_id, $parent_data = [] ) {
		$table_name = $this->repeater_table_name( $field['name'] );

		$sql = [
			'select' => 'SELECT * ',
			'from'   => 'FROM ' . $table_name,
			'where'  => $this->wpdb->prepare( 'WHERE post_id = %d', $post_id ),
		];

		$data = $this->wpdb->get_results( join( ' ', $sql ), ARRAY_A );

		if ( empty( $data ) ) {
			return false;
		}

		if ( ! empty( static::$def_repeater_fields[ $field['name'] ] ) ) {
			foreach ( $field['sub_fields'] as $sub_field ) {
				if ( ! empty( $sub_field['sub_fields'] ) && $sub_field['type'] == 'repeater' ) {
					$data = $this->get_repeater_field_data( $sub_field, $post_id, $data );
				}
			}
		}

		if ( ! empty( $parent_data ) ) {
			foreach ( $parent_data as &$parent_val ) {
				foreach ( $data as $val ) {
					if ( $parent_val['row_order'] == $val['row_parent_id'] ) {
						$parent_val[ $field['name'] ][ $val['row_order'] ] = $val;
					}
				}
			}

			$data = $parent_data;
		}

		return $data;
	}

	public function match_repeater_field_data_value( $data, $field, $parent = false ) {

		$new_value = [];

		foreach ( $data as $key => $row_data ) {
			foreach ( $row_data as $field_name => $field_value ) {
				if ( $field_name == 'post_id' || $field_name == 'row_order' || $field_name == 'row_parent_id' ) {
					continue;
				}

				$find_field = $this->find_repeater_field_key( $field, $field_name );

				if ( ! empty( $find_field['sub_fields'] ) ) {
					$new_value[ $key ][ $find_field['key'] ] = $this->match_repeater_field_data_value( $field_value, $find_field, true );
				} else {
					if ( is_serialized( $field_value ) ) {
						$field_value = maybe_unserialize( $field_value );
					}

					$new_value[ $key ][ $find_field['key'] ] = $field_value;
				}
			}
		}

		return $new_value;
	}

	public function find_repeater_field_key( $field, $field_name ) {
		$find_field = array_map( function ( $field ) use ( $field_name ) {
			return $field['name'] == $field_name ? $field : false;
		}, $field['sub_fields'] );

		if ( empty( $find_field ) ) {
			return false;
		}

		$find_field = array_filter( $find_field );
		$find_field = array_shift( $find_field );

		return $find_field;
	}

	public function delete_post_values_in_custom_table( $post_id, $post ) {
		if ( $this->post_type != $post->post_type ) {
			return;
		}

		$this->wpdb->query( $this->wpdb->prepare( "DELETE FROM {$this->table_name()} WHERE post_id = %d", $post_id ) );
	}

	public function table_name(): string {
		return $this->wpdb->base_prefix . $this->table_name;
	}

	public function repeater_table_name( $name ): string {
		if ( ! isset( static::$repeater_db_table[ $name ] ) ) {
			return false;
		}

		return $this->wpdb->base_prefix . $this->table_name . '_' . static::$repeater_db_table[ $name ];
	}

	public function table_sql() {
		return false;
	}

	/**
	 * @param $field_group
	 *
	 * @return void
	 */
	public function create_or_update_table( $field_group ) {
		$columns = [];
		$fields  = acf_get_fields( $field_group );

		/*echo '<pre>';
		print_r( $fields );
		echo '</pre>';*/

		$data_fields = $this->pre_prepare_fields( $fields );

		$acf_store_file = 'acf-' . $this->post_type . '-fields.json';
		$acf_store_path = ABSPATH . 'wp-content/uploads/acf-stores/';

		if ( ! file_exists( $acf_store_path ) && ! is_dir( $acf_store_path ) ) {
			wp_mkdir_p( $acf_store_path );
		}

		if ( file_exists( $acf_store_path . $acf_store_file ) ) {
			$acf_fields_stored = file_get_contents( $acf_store_path . $acf_store_file );
			$acf_fields_stored = json_decode( $acf_fields_stored, JSON_OBJECT_AS_ARRAY );

			if ( ! empty( $acf_fields_stored ) ) {
				foreach ( $acf_fields_stored as $acf_field_name => $acf_field_key ) {
					$field = acf_get_field( $acf_field_key );

					if ( ! $field ) {
						unset( $acf_fields_stored[ $acf_field_name ] );
					}
				}

				$data_fields = array_merge( $acf_fields_stored, $data_fields );
			}
		}

		$fp = fopen( $acf_store_path . $acf_store_file, 'w+' );
		fwrite( $fp, json_encode( $data_fields ) );
		fclose( $fp );

		global $wpdb;

		$this->wpdb->show_errors( true );

		$sql = $this->tabel_sql();

		if ( ! $sql ) {
			return;
		}

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta( $sql );
		$success = empty( $this->wpdb->last_error );
	}

	public function set_fields() {
		$this->acf_fields = [];
	}

	public function get_fields(): array {
		return $this->acf_fields;
	}

	public function set_def_fields( $fields, $prefix = false ) {
		foreach ( $fields as $key => $field ) {
			if ( is_array( $field ) ) {
				$this->set_def_fields( $field, $key );
			} elseif ( false !== $prefix ) {
				$this->def_fields[] = $prefix . '_' . $field;
			} else {
				$this->def_fields[] = $field;
			}
		}
	}

	public function get_def_fields(): array {
		return $this->def_fields;
	}

	public function set_map_fields( $fields ) {
		foreach ( $fields as $key => $field ) {
			if ( is_array( $field ) ) {
				$this->fields_map[] = $key;
				$this->set_map_fields( $field );
			} else {
				$this->fields_map[] = $field;
			}
		}
	}

	public function get_map_fields(): array {
		return $this->fields_map;
	}

	public function pre_prepare_fields( $fields, $field_name = false ) {
		$matched_fields = [];

		foreach ( $fields as $field ) {

			if ( $field['type'] == 'tab' ) {
				continue;
			}

			$fname = $field['name'];

			if ( $field_name ) {
				$fname = $field_name . '_' . $fname;
			}

			$matched_fields[ $fname ] = $field['key'];

			if ( ! empty( $field['sub_fields'] ) ) {
				if ( $field['type'] == 'repeater' ) {
					$fname = false;
				}
				$matched_fields = array_merge( $matched_fields, $this->pre_prepare_fields( $field['sub_fields'], $fname ) );
			}
		}

		return $matched_fields;
	}

}