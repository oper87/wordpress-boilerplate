<?php

namespace App\Acf;

use WPCF7_ContactForm;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'acf_field' ) ) {
	exit; // Exit if accessed directly
}

class Field_Contact_Form7 extends \acf_field {


	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	29/03/2022
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function __construct() {

		$this->name     = 'acf_cf7';
		$this->label    = __( 'Contact form 7', 'ruby_studio_admin' );
		$this->category = 'basic';

		$this->defaults = [
			'return_format' => 'shortcode',
		];

		$this->settings = [
			'version' => '1.0.2'
		];

		// do not delete!
		parent::__construct();

	}


	/**
	 * render_field_settings()
	 *
	 * @param $field
	 *
	 * @return void
	 */
	function render_field_settings( $field ) {

		// layout
		acf_render_field_setting(
			$field,
			array(
				'label'        => __( 'Return', 'acf' ),
				'instructions' => '',
				'class'        => 'acf-repeater-layout',
				'type'         => 'radio',
				'name'         => 'return_format',
				'layout'       => 'horizontal',
				'choices'      => array(
					'id'        => __( 'Contact from ID', 'acf' ),
					'shortcode' => __( 'Shortcode', 'acf' ),
				),
			)
		);

	}

	/**
	 * @param $field
	 *
	 * @return void
	 */
	function render_field( $field ) {

		if ( ! class_exists( 'WPCF7_ContactForm' ) ) {
			echo 'Contact Form isn\'t installed or activated';

			return;
		}

		$contact_forms = WPCF7_ContactForm::find();

		$select = [
			'id'               => $field['id'],
			'class'            => $field['class'],
			'name'             => $field['name'],
			'data-ui'          => 1,
			'data-ajax'        => 0,
			'data-multiple'    => 0,
			'data-placeholder' => __( 'Select form', 'ruby_studio_admin' ),
			'data-allow_null'  => 0,
		];

		$select['value']      = acf_get_array( $field['value'] );
		$select['choices'][0] = __( 'Select form', 'ruby_studio_admin' );

		foreach ( $contact_forms as $form ) {
			$select['choices'][ $form->id() ] = $form->title();
		}

		acf_select_input( $select );
	}

	function format_value( $value, $post_id, $field ) {

		// bail early if no value
		if ( empty( $value ) ) {
			return false;
		}

		// bail early if not numeric (error message)
		if ( ! is_numeric( $value ) ) {
			return false;
		}

		// convert to int
		$value = intval( $value );

		// format
		if ( $field['return_format'] == 'shortcode' && class_exists( 'WPCF7_ContactForm' ) ) {
			//$contact_forms = WPCF7_ContactForm::find();

			return do_shortcode( '[contact-form-7 id="' . $value . '"]' );

		}

		// return
		return $value;

	}
}