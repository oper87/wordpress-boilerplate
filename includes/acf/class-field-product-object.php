<?php

namespace App\Acf;

use App\Scripts;
use App\WooCommerce\Product;

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'acf_field' ) ) {
	exit;
}

class Field_Product_Object extends \acf_field {

	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function initialize() {

		// vars
		$this->name     = 'wc_product_object';
		$this->label    = __( "WC Product Object", 'acf' );
		$this->category = 'relational';
		$this->defaults = array(
			'post_type'     => Product::POST_TYPE,
			'taxonomy'      => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'return_format' => 'object',
			'ui'            => 1,
		);


		// extra
		add_action( 'wp_ajax_acf/fields/product_object/query', array( $this, 'ajax_query' ) );
		add_action( 'wp_ajax_nopriv_acf/fields/product_object/query', array( $this, 'ajax_query' ) );

	}


	/*
	*  ajax_query
	*
	*  description
	*
	*  @type	function
	*  @date	24/10/13
	*  @since	5.0.0
	*
	*  @param	$post_id (int)
	*  @return	$post_id (int)
	*/

	function ajax_query() {

		// validate
		if ( ! acf_verify_ajax() ) {
			die();
		}

		// get choices
		$response = $this->get_ajax_query( $_POST );

		// return
		acf_send_ajax_results( $response );

	}


	/*
	*  get_ajax_query
	*
	*  This function will return an array of data formatted for use in a select2 AJAX response
	*
	*  @type	function
	*  @date	15/10/2014
	*  @since	5.0.9
	*
	*  @param	$options (array)
	*  @return	(array)
	*/

	function get_ajax_query( $options = array() ) {

		// defaults
		$options = acf_parse_args( $options, array(
			'post_id'   => 0,
			's'         => '',
			'field_key' => '',
			'paged'     => 1
		) );


		// load field
		$field = acf_get_field( $options['field_key'] );
		if ( ! $field ) {
			return false;
		}


		// vars
		$results   = array();
		$args      = array();
		$s         = false;
		$is_search = false;


		// paged
		$args['posts_per_page'] = 20;
		$args['paged']          = $options['paged'];


		// search
		if ( $options['s'] !== '' ) {

			// strip slashes (search may be integer)
			$s = wp_unslash( strval( $options['s'] ) );


			// update vars
			$args['s'] = $s;
			$is_search = true;

		}

		// post_type
		$args['post_type'] = Product::POST_TYPE;

		// post_status
		$args['post_status'] = 'publish';

		// meta_query
		$args['meta_query'] = [
			[
				'key'     => '_stock_status',
				'value'   => 'outofstock',
				'compare' => 'NOT LIKE',
			]
		];

		// meta_query
		/*$args['tax_query'] = [
			[
				'taxonomy' => 'product_visibility',
				'field'    => 'name',
				'terms'    => 'exclude-from-catalog',
				'operator' => 'NOT IN',
			]
		];*/


		// filters
		$args = apply_filters( 'acf/fields/product_object/query', $args, $field, $options['post_id'] );
		$args = apply_filters( 'acf/fields/product_object/query/name=' . $field['name'], $args, $field, $options['post_id'] );
		$args = apply_filters( 'acf/fields/product_object/query/key=' . $field['key'], $args, $field, $options['post_id'] );


		// get posts grouped by post type
		$groups = acf_get_grouped_posts( $args );


		// bail early if no posts
		if ( empty( $groups ) ) {
			return false;
		}


		// loop
		foreach ( array_keys( $groups ) as $group_title ) {
			// vars
			$posts = acf_extract_var( $groups, $group_title );

			// data
			$data = array(
				'text'     => $group_title,
				'children' => array()
			);

			// convert post objects to post titles
			foreach ( array_keys( $posts ) as $post_id ) {
				$posts[ $post_id ] = $this->get_post_title( $posts[ $post_id ], $field, $options['post_id'], $is_search );
			}

			// order posts by search
			if ( $is_search && empty( $args['orderby'] ) ) {
				$posts = acf_order_by_search( $posts, $args['s'] );
			}

			// append to $data
			foreach ( array_keys( $posts ) as $post_id ) {
				$data['children'][] = $this->get_post_result( $post_id, $posts[ $post_id ] );
			}

			// append to $results
			$results[] = $data;
		}

		// optgroup or single
		$post_type = acf_get_array( $args['post_type'] );
		if ( count( $post_type ) == 1 ) {
			$results = $results[0]['children'];
		}

		// vars
		$response = array(
			'results' => $results,
			'limit'   => $args['posts_per_page']
		);

		// return
		return $response;

	}


	/*
	*  get_post_result
	*
	*  This function will return an array containing id, text and maybe description data
	*
	*  @type	function
	*  @date	7/07/2016
	*  @since	5.4.0
	*
	*  @param	$id (mixed)
	*  @param	$text (string)
	*  @return	(array)
	*/

	function get_post_result( $id, $text ) {

		// vars
		$result = array(
			'id'   => $id,
			'text' => $text
		);


		// look for parent
		$search = '| ' . __( 'Parent', 'acf' ) . ':';
		$pos    = strpos( $text, $search );

		if ( $pos !== false ) {

			$result['description'] = substr( $text, $pos + 2 );
			$result['text']        = substr( $text, 0, $pos );

		}


		// return
		return $result;

	}

	/*
	*  get_post_title
	*
	*  This function returns the HTML for a result
	*
	*  @type	function
	*  @date	1/11/2013
	*  @since	5.0.0
	*
	*  @param	$post (object)
	*  @param	$field (array)
	*  @param	$post_id (int) the post_id to which this value is saved to
	*  @return	(string)
	*/

	function get_post_title( $post, $field, $post_id = 0, $is_search = 0 ) {

		// get post_id
		if ( ! $post_id ) {
			$post_id = acf_get_form_data( 'post_id' );
		}

		$thumbnail_image = wc_get_product_image_src( $post_id, 'woocommerce_thumbnail' );

		// vars
		if ( ! empty( $thumbnail_image ) ) {
			$title = acf_get_post_title( $post, $is_search ) . '|' . $thumbnail_image[0];
		} else {
			$title = acf_get_post_title( $post, $is_search ) . '|';
		}

		// filters
		$title = apply_filters( 'acf/fields/post_object/result', $title, $post, $field, $post_id );
		$title = apply_filters( 'acf/fields/post_object/result/name=' . $field['_name'], $title, $post, $field, $post_id );
		$title = apply_filters( 'acf/fields/post_object/result/key=' . $field['key'], $title, $post, $field, $post_id );


		// return
		return $title;
	}


	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field - an array holding all the field's data
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function render_field( $field ) {

		// Change Field into a select
		$field['type']        = 'select';
		$field['ui']          = 1;
		$field['ajax']        = 1;
		$field['choices']     = array();
		$field['ajax_action'] = 'acf/fields/product_object/query';

		// load posts
		$posts = $this->get_posts( $field['value'], $field );

		if ( $posts ) {
			foreach ( array_keys( $posts ) as $i ) {
				// vars
				$post = acf_extract_var( $posts, $i );

				// append to choices
				$field['choices'][ $post->ID ] = $this->get_post_title( $post, $field );
			}
		}

		// render
		acf_render_field( $field );
	}


	/**
	 *  render_field_settings()
	 *
	 *  Create extra options for your field. This is rendered when editing a field.
	 *  The value of $field['name'] can be used (like bellow) to save extra data to the $field
	 *
	 * @type    action
	 *
	 * @param    $field - an array holding all the field's data
	 *
	 * @since    3.6
	 * @date    23/01/13
	 *
	 */
	function render_field_settings( $field ) {

		// allow_null
		acf_render_field_setting( $field, array(
			'label'        => __( 'Allow Null?', 'acf' ),
			'instructions' => '',
			'name'         => 'allow_null',
			'type'         => 'true_false',
			'ui'           => 1,
		) );


		// multiple
		acf_render_field_setting( $field, array(
			'label'        => __( 'Select multiple values?', 'acf' ),
			'instructions' => '',
			'name'         => 'multiple',
			'type'         => 'true_false',
			'ui'           => 1,
		) );


		// return_format
		acf_render_field_setting( $field, array(
			'label'        => __( 'Return Format', 'acf' ),
			'instructions' => '',
			'type'         => 'radio',
			'name'         => 'return_format',
			'choices'      => array(
				'object' => __( "Post Object", 'acf' ),
				'id'     => __( "Post ID", 'acf' ),
			),
			'layout'       => 'horizontal',
		) );

	}


	/**
	 *  load_value()
	 *
	 *  This filter is applied to the $value after it is loaded from the db
	 *
	 * @type    filter
	 *
	 * @param    $value (mixed) the value found in the database
	 * @param    $post_id (mixed) the $post_id from which the value was loaded
	 * @param    $field (array) the field array holding all the field options
	 *
	 * @return    $value
	 * @since    3.6
	 * @date    23/01/13
	 *
	 */
	function load_value( $value, $post_id, $field ) {

		// ACF4 null
		if ( $value === 'null' ) {
			return false;
		}


		// return
		return $value;

	}

	/**
	 *  format_value()
	 *
	 *  This filter is appied to the $value after it is loaded from the db and before it is returned to the template
	 *
	 * @type    filter
	 *
	 * @param    $value (mixed) the value which was loaded from the database
	 * @param    $post_id (mixed) the $post_id from which the value was loaded
	 * @param    $field (array) the field array holding all the field options
	 *
	 * @return    $value (mixed) the modified value
	 * @since    3.6
	 * @date    23/01/13
	 *
	 */
	function format_value( $value, $post_id, $field ) {

		// numeric
		$value = acf_get_numeric( $value );

		// bail early if no value
		if ( empty( $value ) ) {
			return false;
		}

		// load posts if needed
		if ( $field['return_format'] == 'object' ) {
			$value = $this->get_posts( $value, $field );
		}

		// convert back from array if neccessary
		if ( ! $field['multiple'] && is_array( $value ) ) {
			$value = current( $value );
		}

		// return value
		return $value;
	}


	/**
	 *  update_value()
	 *
	 *  This filter is appied to the $value before it is updated in the db
	 *
	 * @type    filter
	 *
	 * @param    $value - the value which will be saved in the database
	 * @param    $post_id - the $post_id of which the value will be saved
	 * @param    $field - the field array holding all the field options
	 *
	 * @return    $value - the modified value
	 * @since    3.6
	 * @date    23/01/13
	 *
	 */
	function update_value( $value, $post_id, $field ) {
		// Bail early if no value.
		if ( empty( $value ) ) {
			return $value;
		}

		// Format array of values.
		// - ensure each value is an id.
		// - Parse each id as string for SQL LIKE queries.
		if ( acf_is_sequential_array( $value ) ) {
			$value = array_map( 'acf_idval', $value );
			$value = array_map( 'strval', $value );

			// Parse single value for id.
		} else {
			$value = acf_idval( $value );
		}

		// Return value.
		return $value;
	}


	/**
	 *  get_posts
	 *
	 *  This function will return an array of posts for a given field value
	 *
	 * @type    function
	 * @date    13/06/2014
	 *
	 * @param    $value (array)
	 *
	 * @return    $value
	 * @since    5.0.0
	 *
	 */
	function get_posts( $value, $field ) {

		// numeric
		$value = acf_get_numeric( $value );

		// bail early if no value
		if ( empty( $value ) ) {
			return false;
		}

		// get posts
		$posts = acf_get_posts( array(
			'post__in'  => $value,
			'post_type' => $field['post_type']
		) );

		// return
		return $posts;
	}

	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/
	function input_admin_enqueue_scripts() {

		// register & include style
		wp_register_style( $this->name, Scripts::get_admin_url( '/acf/field-product-object.css' ), '', '1.0.1' );
		wp_enqueue_style( $this->name );

		// register & include JS
		wp_register_script( $this->name, Scripts::get_admin_url( '/acf/field-product-object.js' ), [
			'wp-util',
			'acf-input',
		], '1.0.1' );
		wp_enqueue_script( $this->name );

	}


	/**
	 *  input_admin_head()
	 *
	 *  This action is called in the admin_head action on the edit screen where your field is created.
	 *  Use this action to add CSS and JavaScript to assist your render_field() action.
	 *
	 * @type    action (admin_head)
	 *
	 * @param n/a
	 *
	 * @return    n/a
	 * @since    3.6
	 * @date    23/01/13
	 *
	 */
	function input_admin_head() {
		?>
        <style>
            .wc-product-object-acf li.select2-selection__choice {
                display: flex;
                align-content: center;
                align-items: center;
                flex-direction: row;
                flex-wrap: nowrap;
            }

            .select2-container--default .select2-results__option {
                display: flex;
                align-items: center;
                align-content: center;
            }

            .select2-container.-acf .select2-selection--multiple .select2-selection__choice {
                border: none;
            }

            span.wc-product-item {
                display: flex;
                align-items: center;
                align-content: center;
            }

            span.wc-product-item img {
                width: 40px;
                height: auto;
                margin: 5px;
                padding: 0 6px 0 0;
            }

            span.wc-product-item span {
                margin-right: 5px;
            }
        </style>
		<?php
	}
}