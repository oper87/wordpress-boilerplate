<?php
/**
 * Early beta version
 */

namespace App\Acf;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Group_Field {

	private static $enable_key = 'acf_custom_table_enabled';

	private static $table_key = 'acf_custom_table_key';

	public function __construct() {
		add_filter( 'acf/field_group/additional_group_settings_tabs', [ $this, 'additional_group_settings_tabs' ] );
		add_action( 'acf/field_group/render_group_settings_tab/app_custom_table', [ $this, 'group_settings_tab_callback' ] );
		add_action( 'acf/update_field_group', [ $this, 'update_field_group' ] );
	}

	public function additional_group_settings_tabs( $settings_tabs ) {
		$settings_tabs['app_custom_table'] = esc_html__( 'Custom DB Table', 'ruby_studio_admin' );

		return $settings_tabs;
	}

	public function group_settings_tab_callback() {
		global $field_group;

		acf_render_field_wrap( [
			'label'        => __( 'Enabled', 'ruby_studio_admin' ),
			'instructions' => __( 'Enable Store some fields in custom table for this field group?', 'ruby_studio_admin' ),
			'type'         => 'true_false',
			'name'         => static::$enable_key,
			'key'          => static::$enable_key,
			'prefix'       => 'acf_field_group',
			'value'        => esc_attr( acf_maybe_get( $field_group, static::$enable_key, false ) ),
			'ui'           => 1,
		] );

		acf_render_field_wrap( [
			'label'             => __( 'ACF Custom Table Key', 'ruby_studio_admin' ),
			'instructions'      => __( 'Define Table Key', 'ruby_studio_admin' ),
			'type'              => 'text',
			'name'              => static::$table_key,
			'prefix'            => 'acf_field_group',
			'value'             => esc_attr( acf_maybe_get( $field_group, static::$table_key, false ) ),
			'required'          => true,
			'wrapper'           => [
				'class' => static::$table_key . '_wrapper'
			],
			'conditional_logic' => [
				'field'    => static::$enable_key,
				'operator' => '==',
				'value'    => '1',
			]
		] );

		?>
		<script type="text/javascript">
            if (typeof acf !== 'undefined') {
                acf.newPostbox({
                    'id': 'acf-field-custom-table',
                    'label': 'left'
                });
            }
		</script>
		<?php
	}

	/**
	 * @param $field_group
	 *
	 * @return void
	 */
	public function update_field_group( $field_group ) {
		if ( ! isset( $field_group[ self::$table_key ] ) ) {
			return;
		}

		$table_key = $field_group[ self::$table_key ];

		if ( ! method_exists( $table_key, 'create_or_update_table' ) ) {
			return;
		}

		$table_key::getInstance()->create_or_update_table( $field_group );

	}

}