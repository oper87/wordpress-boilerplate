<?php
/**
 * Wordpress Ajax class
 *
 * @version 1.0.0
 */


namespace App\Admin;

use App\Helper;
use App\Abstracts\Ajax as Abstract_Ajax;
use App\Newsletter;
use WP_Query;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Ajax extends Abstract_Ajax {

	public function __construct() {
		parent::__construct( 'admin' );
	}

}