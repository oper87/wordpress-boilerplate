<?php
/**
 * Admin functions class
 */

namespace App\Admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Functions {

	public function __construct() {

		//add_filter( 'wp_link_query', [ $this, 'extend_wp_link_query' ], 10, 2 );

		//add_filter( 'acf/update_value/name=adstxt', [ $this, 'update_local_ads_txt_file' ], 10, 3 );
	}

	/**
	 * Extend WordPress Link Query
	 *
	 * @param $results
	 * @param $query
	 *
	 * @return array
	 */
	public function extend_wp_link_query( $results, $query ) {

		$cat_results = [];

		$categories = get_terms(
			[
				'taxonomy'   => [ \App\Post_CPT\Post::PROGRAMMER_TAXONOMY, \App\Post_CPT\Post::PERSON_TAXONOMY ],
				'name__like' => $query['s'],
				'number'     => $query['posts_per_page'],
				'offset'     => $query['offset'],
				'hide_empty' => false,
			] );

		if ( $categories ) {
			foreach ( $categories as $term ) {

				if ( $term->taxonomy === \App\Post_CPT\Post::PROGRAMMER_TAXONOMY ) {
					$info = 'Programmer';
				} else {
					$info = 'Personer';
				}

				$cat_results[] = [
					'ID'        => $term->term_id,
					'title'     => trim( esc_html( strip_tags( $term->name ) ) ),
					'permalink' => get_term_link( $term->term_id, $term->taxonomy ),
					'info'      => $info,
				];

			}
		}

		return array_merge( $cat_results, $results );
	}

	/**
	 * Rewrite ads.txt local file
	 *
	 * @param $value
	 * @param $post_id
	 * @param $field
	 *
	 * @return mixed
	 */
	public function update_local_ads_txt_file( $value, $post_id, $field ) {
		$file_name = ABSPATH . '/ads.txt';

		file_put_contents( $file_name, $value );

		return $value;
	}
}