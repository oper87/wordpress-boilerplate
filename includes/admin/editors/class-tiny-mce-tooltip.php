<?php

namespace App\Editors;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Scripts;

class Tiny_Mce_Tooltip {

	private static $tooltip_dialog_printed = false;

	public function __construct() {
		add_action( 'admin_init', [ $this, 'init_tooltip_button' ] );
	}

	public function init_tooltip_button() {
		// check user permissions
		if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
			return;
		}

		// check if WYSIWYG is enabled
		if ( get_user_option( 'rich_editing' ) != 'true' ) {
			return;
		}

		add_filter( 'mce_external_plugins', [ $this, 'mce_external_plugins' ] );
		add_filter( 'mce_buttons', [ $this, 'mce_buttons' ] );
		add_filter( 'teeny_mce_buttons', [ $this, 'mce_buttons' ] );

		wp_enqueue_script( 'underscore' );
		wp_enqueue_style( 'app_tooltip_button', Scripts::get_admin_url( 'editors/tooltip.css' ), null, true );
		wp_localize_script( 'wp-util', '_appTooltipButton', [
			'site_url'            => site_url(),
			'tooltip_editor_html' => get_template_directory_uri() . '/includes/editors/tooltip.html'
		] );

		add_editor_style( Scripts::get_admin_url( 'editors/tooltip-editor.css' ) );
		//add_action( 'print_default_editor_scripts', [ $this, 'add_tooltip_dialog_html' ] );
	}

	public function mce_external_plugins( $plugins ) {
		$plugins['app_tooltip_button'] = Scripts::get_admin_url( 'editors/tooltip.js' );

		return $plugins;
	}

	public function mce_buttons( $buttons ) {
		array_push( $buttons, 'app_tooltip_button' );

		return $buttons;
	}

	public function add_tooltip_dialog_html() {
		if ( self::$tooltip_dialog_printed ) {
			return;
		}

		self::$tooltip_dialog_printed = true;

		?>
        <div id="app-tooltip-dialog-backdrop" style="display: none"></div>
        <div id="app-tooltip-dialog-wrap" class="wp-core-ui" style="display: none" role="dialog"
             aria-labelledby="link-modal-title">
            <form id="app-tooltip-dialog" tabindex="-1">
            </form>
        </div>
		<?php

	}

}