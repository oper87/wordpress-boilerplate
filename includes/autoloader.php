<?php
/**
 * WordPress Theme Autoloader
 *
 * @version 1.0.0
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Autoloader {

	public function __construct() {
		if ( function_exists( "__autoload" ) ) {
			spl_autoload_register( "__autoload" );
		}

		spl_autoload_register( [ $this, 'autoload' ] );
	}

	/**
	 * Auto-load WC classes on demand to reduce memory consumption.
	 *
	 * @param string $class_name
	 */
	public function autoload( $class_name ) {
		$located    = false;
		$class_name = strtolower( $class_name );

		if ( strpos( $class_name, 'app\\' ) === false ) {
			return;
		}

		$name_object = explode( "\\", $class_name );
		$namespace   = isset( $name_object[0] ) ? $name_object[0] : false;
		$classname   = array_slice( $name_object, - 1 );
		$classname   = array_shift( $classname );
		$folder_name = $this->get_folder_name( $name_object, true );

		if ( ! $classname && $namespace != 'app' ) {
			return;
		}

		$folder_name = str_replace( '_', '-', $folder_name );
		$file_class  = str_replace( '_', '-', $classname );
		$file_path   = "/includes{$folder_name}" . "class-{$file_class}.php";

		if ( file_exists( STYLESHEETPATH . $file_path ) ) {
			$located = STYLESHEETPATH . $file_path;
		} elseif ( file_exists( TEMPLATEPATH . $file_path ) ) {
			$located = TEMPLATEPATH . $file_path;
		}

		if ( $located ) {
			include_once $located;
		}
	}

	public function get_folder_name( $folders, $first ) {

		if ( $first ) {
			$folders = array_slice( $folders, 1 );
			$folders = array_slice( $folders, 0, - 1 );
		}

		if ( empty( $folders ) ) {
			return '/';
		}

		return '/' . join( '/', $folders ) . '/';
	}

}

new Autoloader();