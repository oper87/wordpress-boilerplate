<?php
/**
 * ACF helper
 *
 * @version 1.0.0
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Acf_Helper {

	public static function get_text_field( $selector, $before = '', $after = '', $post_id = null, $format_value = true ): string {
		$text = '';

		if ( $value = get_field( $selector, $post_id, $format_value ) ) {
			$text = $before . $value . $after;
		}

		return $text;
	}

	public static function get_text_sub_field( $selector, $before = '', $after = '', $format_value = true ): string {
		$text = '';

		if ( $value = get_sub_field( $selector, $format_value ) ) {
			$text = $before . $value . $after;
		}

		return $text;
	}

	public static function get_image_field( $selector, $before = '', $after = '', $post_id = null, $format_value = true ): string {
		$text = '';

		if ( $value = get_field( $selector, $post_id, $format_value ) ) {
			$text = $before . $value . $after;
		}

		return $text;
	}

	public static function get_image_sub_field( $selector, $before = '', $after = '', $post_id = null, $format_value = true ): string {
		$text = '';

		if ( $value = get_sub_field( $selector, $post_id, $format_value ) ) {
			$text = $before . $value . $after;
		}

		return $text;
	}

	/**
	 * @param $selector
	 * @param string $size
	 * @param null $post_id
	 * @param bool $format_value
	 *
	 * @return boolean|array
	 */
	public static function get_image_src( $selector, $size = 'thumbnail', $post_id = null, $format_value = true ) {

		if ( ! $image = get_field( $selector, $post_id, $format_value ) ) {
			return false;
		}

		if ( isset( $image['sizes'][ $size ] ) ) {
			return [
				$image['sizes'][ $size ],
				$image['sizes'][ $size . '-width' ],
				$image['sizes'][ $size . '-height' ],
			];
		} else {
			return [
				$image['url'],
				$image['width'],
				$image['height'],
			];
		}
	}

}
