<?php
/**
 * Wordpress Ajax class
 *
 * @version 1.0.0
 */


namespace App;

use App\Abstracts\Ajax as Abstract_Ajax;
use App\Extensions\Newsletter;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Ajax extends Abstract_Ajax {

	public function __construct( $ajax_key = false ) {
		add_filter( 'theme_locale', [ $this, 'theme_locale' ], 10, 2 );

		parent::__construct( $ajax_key );
	}

	/**
	 * Fix theme locale for AJAX requests
	 *
	 * @param $locale
	 * @param $text_domain
	 *
	 * @return mixed|string
	 */
	public function theme_locale( $locale, $text_domain ) {
		if ( $text_domain == 'ruby_studio' && defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			$locale = get_locale();
		}

		return $locale;
	}

	/**
	 * Get Dynamic Contents
	 */
	public function get_dynamic_contents() {
		$ar_qrcode = filter_input( INPUT_POST, 'augmented_reality_qrcode', FILTER_UNSAFE_RAW );

		$fragments = [];

		if ( ! empty( $ar_qrcode ) ) {
			$qrcode_text = $this->generate_qr_code( $ar_qrcode );

			ob_start();
			echo '<div class="augmented-reality__qr-code" data-text="' . esc_attr( $ar_qrcode ) . '">';
			echo '<img src="' . $qrcode_text . '" alt="QR Code" width="300" height="300">';
			echo '</div>';

			$fragments['div.augmented-reality__qr-code'] = ob_get_clean();
		}

		if ( ! empty( $_POST['single-dimensions'] ) ) {
			$product_id = intval( $_POST['single-dimensions']['product_id'] );
			$format     = strip_tags( $_POST['single-dimensions']['format'] );

			if ( ( $product = wc_get_product( $product_id ) ) ) {
				$dimensions = Product::get_dimensions( $product, $format );

				$fragments[ '.single-dimensions[data-product_id="' . $product_id . '"]' ] = '<div class="single-product__meta-content single-dimensions" data-product_id="' . esc_attr( $product_id ) . '" data-format="' . esc_attr( $format ) . '">' . $dimensions . '</div>';
			}
		}

		if ( ! empty( $_POST['loop_dimensions'] ) && is_array( $_POST['loop_dimensions'] ) ) {
			foreach ( $_POST['loop_dimensions'] as $val ) {
				$product_id = intval( $val['product_id'] );
				$format     = strip_tags( $val['format'] );

				if ( ( $product = wc_get_product( $product_id ) ) ) {
					$dimensions = Product::get_dimensions( $product, $format );

					$fragments[ '.loop-dimensions[data-product_id="' . $product_id . '"]' ] = '<div class="woocommerce-loop-product__meta-dimensions loop-dimensions" data-product_id="' . esc_attr( $product_id ) . '" data-format="' . esc_attr( $format ) . '">' . $dimensions . '</div>';
				}
			}
		}

		$fragments = apply_filters( 'app_theme_dynamic_content_fragments', $fragments );
		$response  = apply_filters( 'app_theme_dynamic_content_response', [
			'fragments' => $fragments
		] );

		do_action( 'app_theme_dynamic_content' );

		wp_send_json_success( $response );
	}

	public function site_search() {
		$keyword = filter_input( INPUT_POST, 'keyword', FILTER_SANITIZE_STRING );


		wp_send_json_success( [] );
	}

    /**
     * Subscribe a customer to Mailchimp newsletter
     *
     * @return void
     */
    public function app_newsletter_send() {
        $name  = filter_input( INPUT_POST, 'name', FILTER_SANITIZE_EMAIL );
        $email = filter_input( INPUT_POST, 'email', FILTER_SANITIZE_EMAIL );

        if ( ! is_email( $email ) ) {
            wp_send_json_error( 'Email is not valid' );
        }

        $subscribed = Newsletter\Mailchimp::getInstance()->subscribe_contact( [
            'EMAIL' => $email,
            'FNAME' => $name
        ] );

        if ( is_wp_error( $subscribed ) ) {
            wp_send_json_error( $subscribed->get_error_message() );
        }

        wp_send_json_success( esc_html__( 'Subscribed successfully', 'ruby_studio' ) );
    }
}