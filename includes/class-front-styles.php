<?php
/**
 * Front Styles Class
 */

namespace App;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Front_Styles {

	protected function __construct() {

		//add_action( 'template_redirect', [ $this, 'set_custom_style' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_custom_style' ] );

	}

	public function add_custom_style() {
		$custom_style = Helper::get_template_part( 'assets/css/custom-style.css', null, null, false );

		wp_add_inline_style( 'main', $custom_style );
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new static();
		}

		return $self;
	}
}