<?php
/**
 * Wordpress function
 *
 * @version 1.0.0
 */


namespace App;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Functions {

	public function __construct() {
		add_action( 'wp_before_load_template', [ $this, 'wp_before_load_template' ], 10, 3 );
		add_action( 'wp_after_load_template', [ $this, 'wp_after_load_template' ], 10, 3 );

		add_filter( 'get_pagenum_link', [ $this, 'remove_pagenum_link_get_variables' ] );
		//add_filter( 'wp_terms_checklist_args', [ $this, 'wp_terms_checklist_args' ], 10, 2 );

		add_filter( 'wp_get_attachment_image', [ $this, 'wp_get_attachment_image' ], 10, 5 );
		add_filter( 'wp_nav_menu_objects', [ $this, 'wp_nav_menu_submenu_items' ], 10, 2 );
	}

	/**
	 *
	 *
	 * @param $_template_file
	 *
	 * @return void
	 */
	public function wp_before_load_template( $_template_file ) {
		$theme_path = get_template_directory() . '/';
		$theme_path = str_replace( '/', '\/', $theme_path );

		if ( THEME_AJAX && preg_match( '/^' . $theme_path . '(header|footer)(-.*)?.php$/', $_template_file ) ) {
			ob_start();
		}
	}

	public function wp_after_load_template( $_template_file, $load_once, $args ) {
		static $i;

		$theme_path = get_template_directory() . '/';
		$theme_path = str_replace( '/', '\/', $theme_path );

		if ( THEME_AJAX && preg_match( '/^' . $theme_path . '(header|footer)(-.*)?.php$/', $_template_file, $matches ) ) {
			ob_clean();

			$i = is_null( $i ) ? 1 : $i + $i;

			if ( $matches[1] == 'header' && $i == 1 ) {
				echo '<!doctype html>';
				echo '<html ' . get_language_attributes() . '>';
				echo '<body>';
				self::ajax_header_response();
			} elseif ( $matches[1] == 'footer' && $i == 2 ) {
				echo '</body></html>';
			}
		}
	}

	public static function ajax_header_response() {

		$queried_id     = $queried_type = $queried_name = false;
		$queried_object = get_queried_object();

		if ( is_front_page() ) {
			$queried_id   = get_option( 'page_on_front' );
			$queried_type = 'front_page';
			$queried_name = 'front_page';
		} elseif ( is_a( $queried_object, 'WP_Term' ) ) {
			$queried_id   = $queried_object->term_id;
			$queried_type = 'taxonomy';
			$queried_name = $queried_object->taxonomy;
		} elseif ( is_a( $queried_object, 'WP_Post' ) ) {
			$queried_id   = $queried_object->ID;
			$queried_type = 'post_type';
			$queried_name = $queried_object->post_type;
		}

		ob_start();
		wp_robots();
		$wp_robots = ob_get_clean();

		echo sprintf( '<span id="wp_title">%s</span>', wp_title( ':', false, 'right' ) ) . "\n";
		echo sprintf( '<span id="wp_body">%s</span>', esc_attr( implode( ' ', get_body_class() ) ) ) . "\n";
		if ( class_exists('Main_Menu') && $menu_item = Main_Menu::get_curent_menu_item() ) {
			echo sprintf( '<span id="wp_menu_current_item">%s</span>', esc_attr( $menu_item['current_item_id'] ) ) . "\n";
			echo sprintf( '<span id="wp_menu_parent_item">%s</span>', esc_attr( $menu_item['parent_item_id'] ) ) . "\n";
		}
		echo sprintf( '<span id="wp_robots">%s</span>', $wp_robots ) . "\n";

		if ( $queried_id ) {
			echo sprintf( '<span id="queried_object_id">%s</span>', $queried_id ) . "\n";
			echo sprintf( '<span id="queried_object_type">%s</span>', $queried_type ) . "\n";
			echo sprintf( '<span id="queried_object_name">%s</span>', $queried_name ) . "\n";
		}

		do_action( 'ruby_theme_ajax_header_response' );

	}

	public function remove_pagenum_link_get_variables( $url ) {
		$url = remove_query_arg( '_ajax', $url );
		$url = remove_query_arg( '_load_more', $url );

		return $url;
	}

	/**
	 * Add classes to post tag links
	 *
	 * @param array $links
	 *
	 * @return string[]|\string[][]
	 */
	public static function add_tag_class( array $links ) {
		$links = array_map( function ( $link ) {
			return str_replace( 'rel="tag"', 'class="btn btn-label" rel="tag"', $link );
		}, $links );

		return $links;
	}

	public static function get_the_term_list_label( $post_id, $taxonomy, $sep = '' ) {
		add_filter( 'term_links-' . $taxonomy, [ self::class, 'add_tag_class' ] );
		$list = get_the_term_list( $post_id, $taxonomy, '', $sep, '' );
		remove_filter( 'term_links-' . $taxonomy, [ self::class, 'add_tag_class' ] );

		return $list;
	}

	public function wp_terms_checklist_args( $args, $post_id ) {
		if ( is_admin() && $args['taxonomy'] == Tax_Retailer::TAXONOMY ) {
			$args['checked_ontop'] = false;
		}

		return $args;
	}

	public function wp_get_attachment_image( $html, $attachment_id, $size, $icon, $attr ) {
		$image = wp_get_attachment_image_src( $attachment_id, $size, $icon );

		if ( $image ) {
			list( $src, $width, $height ) = $image;

			$ext = pathinfo( $src, PATHINFO_EXTENSION );

			if ( $ext == 'svg' ) {
				if ( is_array( $size ) ) {
					$image_size = [
						'width'  => $size[0],
						'height' => $size[1]
					];
				} else {
					$image_size = Helper::get_thumbnail_image_size( $size );
				}

				$class = isset( $attr['class'] ) ? 'attachment-svg ' . $attr['class'] : '';
				$lazy  = isset( $attr['loading'] ) ? 'loading="lazy"' : '';
				//$html  = sprintf( '<object data="%s" class="%s" %s>&nbsp;</object>', $src, $class, $lazy );
				$html = sprintf( '<img width="%s" height="%s" src="%s" class="%s" %s>', $image_size['width'], $image_size['height'], $src, $class, $lazy );
			}
		}

		return $html;
	}

	/**
	 * @param array $sorted_menu_items
	 * @param object $args
	 *
	 * @return array
	 */
	public function wp_nav_menu_submenu_items( array $sorted_menu_items, object $args ): array {
		if ( isset( $args->parent_of ) ) {
			$menu_item_parent = 0;

			foreach ( $sorted_menu_items as $menu_item ) {
				if ( $args->parent_of == $menu_item->object_id ) {
					$menu_item_parent = $menu_item->ID;
					break;
				}
			}

			$sorted_menu_items = array_filter( $sorted_menu_items, function ( $menu_item ) use ( $menu_item_parent ) {
				return $menu_item->menu_item_parent == $menu_item_parent ? $menu_item : false;
			} );

			if ( $menu_item_parent == 0 ) {
				$sorted_menu_items = [];
			}
		}

		return $sorted_menu_items;
	}

}
