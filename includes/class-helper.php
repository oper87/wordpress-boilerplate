<?php
/**
 * Theme helper
 *
 * @version 1.0.0
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Helper {

	/**
	 * Get upload directory
	 *
	 * @return false|string|array
	 */
	public static function upload_dir( $key = null ) {
		$upload_dir = wp_upload_dir();

		if ( is_null( $key ) ) {
			return $upload_dir;
		} elseif ( isset( $upload_dir[ $key ] ) ) {
			return $upload_dir[ $key ];
		} else {
			return false;
		}
	}

	/**
	 * Include template
	 *
	 * @param $slug
	 * @param null $name
	 * @param array $args
	 * @param bool $echo
	 *
	 * @return bool|false|string
	 */
	public static function get_template_part( $slug, $name = null, $args = [], $echo = true ) {

		if ( is_array( $args ) ) {
			extract( $args, EXTR_SKIP );
		}

		$templates = [];
		$name      = (string) $name;
		if ( '' !== $name ) {
			$templates[] = "{$slug}-{$name}.php";
		}

		$templates[] = "{$slug}.php";

		$_template_file = locate_template( $templates, false, false );

		if ( empty( $_template_file ) ) {
			return false;
		}

		if ( $echo == false ) {
			ob_start();
			require( $_template_file );

			return ob_get_clean();
		} else {
			require( $_template_file );
		}
	}

	/**
	 * Insert array element into array
	 *
	 * @return array
	 */
	public static function array_insert( $array, $array_insert_key, $array_insert, $before_after = 'after' ) {
		$array_keys = array_keys( $array );
		$array_key  = array_search( $array_insert_key, $array_keys );

		if ( $before_after == 'before' ) {
			$array_before = array_slice( $array, 0, $array_key );
			$array_after  = array_slice( $array, $array_key );
		} else {
			$array_before = array_slice( $array, 0, $array_key + 1 );
			$array_after  = array_slice( $array, $array_key + 1 );
		}

		return array_merge( $array_before, $array_insert, $array_after );
	}

	/**
	 * Prepare inline styles
	 *
	 * @param $selector
	 * @param $properties
	 * @param bool $echo
	 *
	 * @return string
	 */
	public static function render_style( $selector, $properties, $echo = false ) {

		$style = '';

		if ( ! empty( $selector ) ) {

			if ( is_array( $selector ) ) {
				$selector = join( ',' . "\n", $selector );
			}

			$style .= "{$selector} {";

			if ( ! empty( $properties ) ) {
				foreach ( $properties as $property => $value ) {
					$style .= "{$property}:{$value};";
				}
			}

			$style .= "}" . "\n";
		}

		if ( $echo ) {
			echo $style;
		} else {
			return $style;
		}
	}

	/**
	 * Prepare inline styles
	 *
	 * @param $properties
	 * @param bool $echo
	 * @param bool $wrap
	 *
	 * @return string
	 */
	public static function render_inline_style( $properties, bool $echo = true, bool $wrap = true ) {
		array_walk( $properties, function ( &$value, $key ) {
			if ( $value !== '' ) {
				$value = esc_attr( $key . ':' . $value );
			} else {
				$value = '';
			}
		} );

		$properties = array_filter( $properties );

		if ( $wrap && ! empty( $properties ) ) {
			$style = sprintf( 'style="%s"', join( '; ', $properties ) );
		} else if ( ! $wrap && ! empty( $properties ) ) {
			$style = join( '; ', $properties );
		} else {
			$style = '';
		}

		if ( $echo ) :
			echo $style;
		else :
			return $style;
		endif;
	}

	/**
	 * Output class
	 *
	 * @param array $classes
	 *
	 * @return string
	 */
	public static function render_class( array $classes, $echo = true ) {
		$class = 'class="' . esc_attr( join( ' ', $classes ) ) . '"';

		if ( $echo ) {
			echo $class;
		} else {
			return $class;
		}
	}

	/**
	 *
	 * @param $video
	 * @param string $video_attr
	 * @param null $type
	 *
	 * @return string
	 */
	public static function render_video( $video, $video_attr = '', $type = null ) {
		$video_html = '';
		$video_info = pathinfo( $video );

		$video_lazy = sprintf( '<video class="rocket-lazyload" %s>
                        <source data-lazy-src="%s" type="video/%s">
                    </video>', $video_attr, $video, $video_info['extension'] );

		$video_non_lazy = sprintf( '<video %s>
                        <source src="%s" type="video/%s">
                    </video>', $video_attr, $video, $video_info['extension'] );

		if ( defined( 'ROCKET_LAZYLOAD' ) && ROCKET_LAZYLOAD ) {
			$video_html = $video_lazy . sprintf( '<noscript>%s</noscript>', $video_non_lazy );
		} else {
			$video_html = $video_non_lazy;
		}

		return $video_html;
	}

	/**
	 * @param array $image
	 * @param string $size
	 *
	 * @return boolean|array
	 */
	public static function acf_get_image_src( $image, $size = 'thumbnail' ) {

		if ( empty( $image ) ) {
			return false;
		}

		if ( isset( $image['sizes'][ $size ] ) ) {
			return [
				$image['sizes'][ $size ],
				$image['sizes'][ $size . '-width' ],
				$image['sizes'][ $size . '-height' ],
			];
		} else {
			return [
				$image['url'],
				$image['width'],
				$image['height'],
			];
		}
	}

	/**
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function render_attributes( $atts = [] ) {
		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
				$value      = ( 'href' === $attr || 'src' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		return $attributes;
	}

	/**
	 * Escape JSON for use on HTML or attribute text nodes.
	 *
	 * @param string $json JSON to escape.
	 * @param bool $html True if escaping for HTML text node, false for attributes. Determines how quotes are handled.
	 *
	 * @return string Escaped JSON.
	 * @since 3.5.5
	 */
	public static function wp_esc_json( $json, $html = false ) {
		return _wp_specialchars(
			$json,
			$html ? ENT_NOQUOTES : ENT_QUOTES, // Escape quotes in attribute nodes only.
			'UTF-8',                           // json_encode() outputs UTF-8 (really just ASCII), not the blog's charset.
			true                               // Double escape entities: `&amp;` -> `&amp;amp;`.
		);
	}

	public static function get_svg_tag( $el ) {
		$svg = file_get_contents( $el );
		preg_match( '/<svg[\s\S]*\/svg>/m', $svg, $matches );

		return $matches[0] ?? $svg;
	}

	public static function get_svg( $tag, $echo = true ) {
		$file = get_template_directory() . '/assets/images/' . $tag . '.svg';

		if ( file_exists( $file ) ) {
			if ( $echo ) {
				echo self::get_svg_tag( $file );
			} else {
				return self::get_svg_tag( $file );
			}
		}
	}

	public static function get_thumbnail_image_size( $s = '' ) {
		global $_wp_additional_image_sizes;

		$_sizes = $_wp_additional_image_sizes;

		// vars
		$data = array(
			'width'  => isset( $_sizes[ $s ]['width'] ) ? $_sizes[ $s ]['width'] : get_option( "{$s}_size_w" ),
			'height' => isset( $_sizes[ $s ]['height'] ) ? $_sizes[ $s ]['height'] : get_option( "{$s}_size_h" ),
		);

		// return
		return $data;
	}
}