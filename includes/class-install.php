<?php
/**
 * Install
 *
 * @version 1.0.0
 */

namespace App;

use Exception;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Install {

	/**
	 * Theme version wp_options key
	 */
	private string $option_key = '';

	/**
	 * Theme DB version wp_options key
	 */
	private string $option_db_key = '';

	public function __construct() {
		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'after_switch_theme', [ $this, 'setup_theme' ] );
		add_action( 'admin_notices', [ $this, 'add_notices' ] );
	}

	public function admin_init() {
		$this->option_key    = wp_get_theme()->template . '_theme_version';
		$this->option_db_key = wp_get_theme()->template . '_theme_db_version';

		$action = sanitize_text_field( wp_unslash( filter_input( INPUT_GET, '_wpaction', FILTER_UNSAFE_RAW ) ) );
		$nonce  = sanitize_text_field( wp_unslash( filter_input( INPUT_GET, '_wpnonce', FILTER_UNSAFE_RAW ) ) );

		if ( $action == 'app_theme_update' && wp_verify_nonce( $nonce, 'app_theme_update' ) ) {
			$this->run();
			wp_redirect( wp_get_referer() );
			exit;
		}
	}

	public function add_notices() {
		$upgrade_url = add_query_arg( array(
			'_wpaction' => 'app_theme_update',
			'_wpnonce'  => wp_create_nonce( 'app_theme_update' ),
		), admin_url() );

		if ( version_compare( get_option( $this->option_key, 0 ), APP_THEME_VERSION, '<' ) ) {
			echo sprintf( '<div class="error"><p>Theme <strong>"%s"</strong> is outdated. Click here to <a class="" href="%s">update the theme</a>. Current version is %s. New version - %s.</p></div>',
				wp_get_theme()->Name,
				$upgrade_url,
				get_option( $this->option_key, 0 ),
				APP_THEME_VERSION );
		}

		if ( APP_THEME_DB_VERSION > 0 ) {
			if ( version_compare( get_option( $this->option_db_key, 0 ), APP_THEME_DB_VERSION, '<' ) ) {
				echo sprintf( '<div class="error"><p>The <strong>"%s" database</strong> is outdated. Click here to <a class="" href="%s">update the theme database </a>. Current version is %s. New version - %s.</p></div>',
					wp_get_theme()->Name,
					$upgrade_url,
					get_option( $this->option_db_key, 0 ),
					APP_THEME_DB_VERSION );
			}
		}

		if ( get_transient( 'app_migration_run' ) ) {
			$notice       = get_transient( 'app_migration_run' );
			$status_class = $notice['status'] == 'success' ? 'success' : 'warning';

			echo '<div class="notice notice-' . $status_class . ' is-dismissible">';
			echo '<p>' . $notice['message'] . '</p>';
			echo isset( $notice['errors'] ) ? '<p>Errors: ' . $notice['errors'] . '</p>' : '';
			echo '</div>';

			delete_transient( 'app_migration_run' );
		}
	}

	public function setup_theme() {
		$this->run();
	}

	protected function run() {

		try {

			if ( version_compare( get_option( $this->option_key, 0 ), APP_THEME_VERSION, '<' ) ) {
				do_action( 'app_migration_run' );
			}

			if ( APP_THEME_DB_VERSION > 0 ) {
				if ( version_compare( get_option( $this->option_db_key, 0 ), APP_THEME_DB_VERSION, '<' ) ) {
					$this->setup_tables();

					update_option( $this->option_db_key, APP_THEME_DB_VERSION );
				}
			}

			update_option( $this->option_key, APP_THEME_VERSION );

			set_transient( 'app_migration_run', [
				'status'  => 'success',
				'message' => __( 'The theme was successfully updated.', 'ruby_studio_admin' )
			] );
		} catch ( Exception $e ) {
			set_transient( 'app_migration_run', [
				'status'  => 'fail',
				'message' => __( 'There was some errors with updating the theme.', 'ruby_studio_admin' ),
				'errors'  => $e->getMessage()
			] );
		}

	}

	private function setup_tables() {
		global $wpdb;

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		$wpdb->show_errors();

		dbDelta( self::get_schema(), true );

		if ( $wpdb->last_error ) {
			throw new Exception( $wpdb->last_error );
		}
	}

	/**
	 * Get Table schema.
	 *
	 * @return string
	 */
	private static function get_schema(): string {
		global $wpdb;

		$collate = '';

		if ( $wpdb->has_cap( 'collation' ) ) {
			$collate = $wpdb->get_charset_collate();
		}

		$tables = "
CREATE TABLE {$wpdb->prefix}sample_table (
  order_id bigint(20) NOT NULL AUTO_INCREMENT,
  order_key varchar(255) NOT NULL default '',
  date_created datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  date_created_gtm datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  date_modified datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  date_modified_gtm datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  user_id bigint(20) unsigned NOT NULL DEFAULT '0',
  order_status ENUM('pending', 'reviewing', 'completed', 'canceled') NOT NULL default 'pending',
  total_expected_credits varchar(10) NOT NULL default '0',
  total_actual_credits varchar(10) NOT NULL default '0',
  shipping_method varchar(255) NOT NULL default '',
  PRIMARY KEY  (order_id),
  KEY user_id (user_id)
) $collate;
CREATE TABLE {$wpdb->prefix}sample_table_items (
  order_item_id bigint(20) NOT NULL AUTO_INCREMENT,
  order_id bigint(20) unsigned NOT NULL,
  item_name text NOT NULL,
  item_category_id bigint(20) unsigned NOT NULL,
  item_condition varchar(255) NOT NULL default '',
  item_quality varchar(255) NOT NULL default '',
  item_comment varchar(255) NOT NULL default '',
  item_expected_credits varchar(10) NOT NULL default '0',
  item_actual_credits varchar(10) NOT NULL default '0',
  item_exported int(1) NOT NULL default '0',
  PRIMARY KEY  (order_item_id),
  KEY order_id (order_id)
) $collate;
";

		return $tables;
	}

}