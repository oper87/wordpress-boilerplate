<?php
/**
 * Maintenance Mode
 *
 * Used DEFINED variables
 * WP_ENVIRONMENT_TYPE = production
 * WP_COMING_SOON = page_id
 *
 * To disable maintenance remove WP_COMING_SOON on production
 *
 * @version 1.0.0
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Maintenance_Mode {

	private $active;

	public function __construct() {

		if ( function_exists( 'get_field' ) ) {
			$this->active = get_field( 'coming_soon_page', 'options' );
		}

		if ( $this->active ) {
			add_action( 'wp', [ $this, 'add_safe_redirect' ] );
			add_action( 'pre_get_posts', [ $this, 'pre_get_coming_soon_page' ] );
			add_filter( 'rest_authentication_errors', [ $this, 'disable_rest' ] );
		}
	}

	public function is_active() {

	}

	/**
	 * Redirect to HOME page non-logged users
	 */
	public function add_safe_redirect() {
		if ( ! is_admin() && wp_get_environment_type() == 'production' && ( defined( 'WP_COMING_SOON' ) && WP_COMING_SOON ) ) {
			$newsletter_page_id = (int) 182;

			if ( ( ! is_user_logged_in() || ! current_user_can( 'administrator' ) ) && ! is_page( WP_COMING_SOON ) && ! is_page( $newsletter_page_id ) ) {
				wp_safe_redirect( site_url() );
			}
		}
	}

	/**
	 * Replace WP_Query to show coming soon page
	 *
	 * @param $query
	 */
	public function pre_get_coming_soon_page( $query ) {

		if ( ! is_admin() && wp_get_environment_type() == 'production' && ( defined( 'WP_COMING_SOON' ) && WP_COMING_SOON ) && ( ! is_user_logged_in() || ! current_user_can( 'administrator' ) ) ) {
			$policy_page_id = (int) get_option( 'wp_page_for_privacy_policy' );

			if ( $query->is_main_query() && $query->get( 'pagename' ) !== 'newsletter' ) {
				$query->set( 'page_id', WP_COMING_SOON );
			}
		}
	}

	/**
	 * Disable Rest API for maintenance mode
	 *
	 * @param $result
	 *
	 * @return mixed|\WP_Error
	 */
	public function disable_rest( $result ) {
		if ( ! empty( $result ) ) {
			return $result;
		}

		if ( wp_get_environment_type() == 'production' && ( defined( 'WP_COMING_SOON' ) && WP_COMING_SOON ) && ( ! is_user_logged_in() || ! current_user_can( 'administrator' ) ) ) {
			return new \WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
		}

		return $result;
	}
}