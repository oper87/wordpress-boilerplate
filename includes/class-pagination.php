<?php
/**
 * Custom Pagination rules
 *
 * @version 1.0.0
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Pagination {

	/**
	 * __construct
	 */
	public function __construct() {
		/**
		 * Add custom pagination rules
		 */
		add_filter( 'get_pagenum_link', [ $this, 'customize_pagination_url' ], 10, 2 );
		add_action( 'pre_get_posts', [ $this, 'pagination_change_main_query' ], 10, 1 );
		add_action( 'wp_head', [ $this, 'pagination_add_head_meta_tags' ] );
		add_action( 'template_redirect', [ $this, 'redirect_pagination_url_to_new_type' ] );
		add_action( 'redirect_canonical', [ $this, 'pagination_remove_trailing_slash' ], 10, 2 );
		add_filter( 'wpseo_next_rel_link', '__return_false' );
		add_filter( 'wpseo_prev_rel_link', '__return_false' );
	}


	/**
	 * Change deafual pagiation URL
	 *
	 * @param $result
	 *
	 * @return string|string[]|null
	 */
	public function customize_pagination_url( $result, $pagenum ) {
		global $custom_pagenum;

		if ( $custom_pagenum ) {
			$result = remove_query_arg( 'page', $result );

			$url_query = [];
			$parse_url = wp_parse_url( $result );

			if ( isset( $parse_url['query'] ) ) {
				parse_str( $parse_url['query'], $url_query );
			}
			$result = remove_query_arg( array_keys( $url_query ), $result );

			if ( ! empty( $url_query ) ) {
				$result = preg_replace( '~/page/(\d+)/?~', '?page=\1', $result );
				$result = add_query_arg( $url_query, $result );
				$result = rtrim( $result, '/' ) . '/';
			} else {
				$result = preg_replace( '~/page/(\d+)/?~', '?page=\1\/', $result );
			}
		}

		return $result;
	}

	/**
	 * Change main query for pagination
	 *
	 * @param $query
	 */
	public function pagination_change_main_query( $query ) {
		if ( is_admin() ) {
			return;
		}

		if ( $query->is_main_query() && $paged = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT ) ) {
			$query->set( 'paged', $paged );
		}
	}


	/**
	 * Adding pagination meta tags to the site header
	 */
	public function pagination_add_head_meta_tags() {
		global $wp_query, $custom_pagenum;

		$max_page = 0;
		$paged    = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT );

		if ( ! $max_page ) {
			$max_page = $wp_query->max_num_pages;
		}

		if ( ! $paged ) {
			$paged = 1;
		}

		if ( $max_page && ! is_single() ) {
			$prev_page      = intval( $paged ) - 1;
			$next_page      = intval( $paged ) + 1;
			$custom_pagenum = true;

			if ( $prev_page ) {
				echo '<link rel="prev" href="' . esc_url( get_pagenum_link( $prev_page ) ) . '" />';
			}

			echo '<link rel="canonical" href="' . esc_url( get_pagenum_link( $paged ) ) . '" />';

			if ( $next_page && $next_page <= $max_page ) {
				echo '<link rel="next" href="' . esc_url( get_pagenum_link( $next_page ) ) . '" />';
			}
		}

		$custom_pagenum = false;
	}


	/**
	 * Creating pagination redirect from old URL to the new one type
	 */
	public function redirect_pagination_url_to_new_type() {
		global $wp, $wp_query;

		if ( is_paged() && ( is_home() || is_category() || is_archive() ) ) {
			if ( strpos( $wp->request, '/page/' ) !== false ) {
				$new_url = str_replace( '/page/', '?page=', $wp->request );

				wp_redirect( site_url( $new_url . '/' ), 301 );
				exit;
			}
		}
	}

	/**
	 * Remove trailing slash from pagination url
	 *
	 * @param $redirect
	 * @param $request
	 *
	 * @return false
	 */
	public function pagination_remove_trailing_slash( $redirect, $request ) {
		if ( isset( $_GET['page'] ) ) {
			return false;
		}

		return $redirect;
	}

}