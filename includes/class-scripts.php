<?php
/**
 * Theme scripts and styles
 *
 * @version 1.0.0
 */

namespace App;

use ShipmondoForWooCommerce\Plugin\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Scripts {

	static string $suffix = '';

	static string $stylesheet = '';

	public function __construct() {

		self::$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		self::$stylesheet = get_stylesheet_directory_uri();

		/**
		 * Enqueue styles
		 */
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles' ] );

		/**
		 * Enqueue scripts
		 */
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

		/**
		 * Enqueue secondary styles
		 */
		add_action( 'wp_footer', [ $this, 'secondary_enqueue_styles' ] );

		/**
		 * Preloading fonts
		 */
		add_action( 'wp_head', [ $this, 'preloading_fonts' ], 7 );

		/**
		 * Deregister scripts and stylesheets
		 */
		add_action( 'wp_head', [ $this, 'dequeue_scripts' ], 1000 );

		/**
		 * Set defer or async attributes
		 */
		//add_action( 'script_loader_tag', [ $this, 'script_loader_tag' ], 10, 2 );

		/**
		 * Enqueue login scripts
		 */
		add_action( 'login_enqueue_scripts', [ $this, 'login_design' ] );

		/**
		 * Enqueue admin scripts
		 */
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_scripts' ] );
		add_action( 'admin_footer', [ $this, 'admin_footer_scripts' ] );
	}

	/**
	 * WP enqueue styles
	 */
	public function enqueue_styles() {

		// Themes styles
		wp_register_style( 'main-tmp', get_stylesheet_uri(), null, APP_SCRIPTS_VERSION );
		wp_register_style( 'main', $this->get_stylesheet_url( 'css/main.css' ), null, APP_SCRIPTS_VERSION );
	}

	public function secondary_enqueue_styles() {
		// Dependence styles
		//wp_enqueue_style( 'fancybox', $this->get_dependence_url( 'fancybox/jquery.fancybox.min.css' ), null, '3.5.7' );
	}

	/**
	 * WP dequeue and deregister scripts and styles
	 */
	public function dequeue_scripts() {
		global $wp_styles;

		// Remove Gutenberg registered styles
		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library-theme' );
		wp_dequeue_style( 'classic-theme-styles' );
		wp_dequeue_style( 'wc-blocks-vendors-style' ); // Remove WooCommerce block CSS
		wp_dequeue_style( 'wc-blocks-style' ); // Remove WooCommerce block CSS

		// Remove plugins registered styles
		//wp_dequeue_style( 'wordpress-popular-posts-css' );
		//wp_dequeue_style( 'woocommerce-currency-switcher' );
		//wp_deregister_script( 'wc-price-slider_33' );
	}

	/**
	 * WP enqueue scripts
	 */
	public function enqueue_scripts() {
		global $wp_scripts, $sitepress;

		$this->move_script( 'jquery', 'jquery-core' );
		$this->move_script( 'wpp-js' );

		// Dependence libraries
		//wp_register_script( 'gsap', $this->get_dependence_url( 'gsap/gsap.min.js' ), [ 'jquery' ], '3.3.4', true );
		//wp_register_script( 'swiper', $this->get_dependence_url( 'swiper/swiper-bundle.min.js' ), [ 'jquery' ], '9.1.0', true );
		//wp_register_script( 'fancybox', $this->get_dependence_url( 'fancybox/jquery.fancybox.min.js' ), [ 'jquery' ], '3.5.7', true );

		// Themes scripts
		wp_register_script( 'plugins', $this->get_stylesheet_url( 'js/plugins.js' ), [ 'jquery' ], APP_SCRIPTS_VERSION, true );
		wp_register_script( 'main', $this->get_stylesheet_url( 'js/main.js' ), [ 'jquery' ], APP_SCRIPTS_VERSION, true );
		wp_register_script( 'wc-main', $this->get_stylesheet_url( 'js/wc-main.js' ), [], APP_SCRIPTS_VERSION, true );
		wp_register_script( 'wc-shop', $this->get_stylesheet_url( 'js/wc-shop.js' ), [], APP_SCRIPTS_VERSION, true );
		wp_register_script( 'main-filter', $this->get_stylesheet_url( 'js/main-filter.js' ), [], APP_SCRIPTS_VERSION, true );

		//wp_enqueue_script( 'gsap' );
		//wp_enqueue_script( 'swiper' );

		// Threaded comment reply styles.
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		$ajax_url       = admin_url( 'admin-ajax.php' );
		$theme_ajax_url = add_query_arg( 'wp-theme-ajax', '%%endpoint%%', home_url( '/', 'relative' ) );

		if ( ! empty( $sitepress ) && ( $sitepress->get_current_language() != $sitepress->get_default_language() ) ) {
			$ajax_url       = add_query_arg( 'lang', $sitepress->get_current_language(), $ajax_url );
			$theme_ajax_url = add_query_arg( 'lang', $sitepress->get_current_language(), $theme_ajax_url );
		}

		$theme_params = [
			'ajax_url'       => $ajax_url,
			'theme_ajax_url' => esc_url_raw( urldecode( $theme_ajax_url ) ),
			'site_url'       => home_url(),
			'cache_enabled'  => defined( 'WP_CACHE' ) && WP_CACHE ? '1' : '0',
			'texts'          => [
				'contact_form_button_send' => esc_html__( 'Message sent', 'ruby_studio' )
			],
		];

		wp_localize_script( 'plugins', 'theme_params', $theme_params );

		if ( function_exists( 'WC' ) ) {
			if ( is_product() ) {
				//wp_enqueue_script( 'selectWoo' );
				//wp_enqueue_style( 'select2' );
			}

			wp_enqueue_script( 'wc-cart-fragments' );
			wp_enqueue_script( 'wc-main' );

			if ( is_shop() || is_product_category() ) {
				wp_enqueue_script( 'wc-shop' );
			}

			wp_localize_script( 'wc-main', 'wc_params', [
				'woocommerce_errors' => [
					'wc_coupon_please_enter' => __( 'Please enter a coupon code.', 'woocommerce' ),
				],
			] );
		}

		wp_enqueue_style( 'main' );
		wp_enqueue_style( 'main-tmp' );
		wp_enqueue_script( 'plugins' );
		wp_enqueue_script( 'main' );
	}

	/**
	 * Preloading theme fonts
	 */
	public function preloading_fonts() {

		$fonts = [
			/*[ $this->get_stylesheet_url( 'fonts/SteelfishRg-Regular.woff2' ), 'woff2' ]*/
		];

		foreach ( $fonts as $font ) {
			echo sprintf( '<link rel="preload" href="%s" as="font" type="font/%s" crossorigin>',
					$font[0],
					$font[1]
			     ) . "\n";
		}

		/*$styles = [
			'https://use.typekit.net/ixu8kkw.css',
			$this->get_stylesheet_url( 'css/main.css' ) . '?ver=' . APP_SCRIPTS_VERSION,
		];

		foreach ( $styles as $style ) {
			echo sprintf( '<link rel="preload" href="%s" as="style">', $style ) . "\n";
		}*/

	}

	/**
	 * Add attributes for scripts
	 *
	 * @param $tag
	 * @param $handle
	 *
	 * @return mixed|string|string[]
	 */
	public function script_loader_tag( $tag, $handle ) {

		/** Set DEFER attribute */
		if ( 'scrollreveal' == $handle ) {
			//$tag = str_replace( ' src', ' defer src', $tag ); // defer the script
		}

		/** Set ASYNC attribute */
		if ( 'scrollreveal' == $handle ) {
			//$tag = str_replace( ' src', ' async src', $tag ); // defer the script
		}

		return $tag;
	}

	/**
	 * Login design
	 */
	public function login_design() {

		if ( file_exists( get_stylesheet_directory() . '/assets/css/login-style.css' ) ) {
			$login_style = file_get_contents( get_stylesheet_directory() . '/assets/css/login-style.css' );
			$login_style = str_replace( '[STYLESHEET_DIRECTORY_URI]', get_stylesheet_directory_uri(), $login_style );
			printf( '<style type="text/css">%s</style>', $login_style );
		}
	}

	/**
	 * Admin scripts
	 */
	public function admin_scripts() {
		global $current_screen;

		wp_enqueue_style( 'wp-admin-style', $this->get_admin_url( 'admin-style.css' ) );
	}

	/**
	 * Admin footer scripts
	 */
	public function admin_footer_scripts() {
		global $current_screen;

		/*if ( isset( $current_screen ) && $current_screen->parent_file == 'edit.php?post_type=product' ) {
			wp_enqueue_script( 'wc-product-js', $this->get_admin_url( 'wc-product.js' ) );
		}*/

		wp_enqueue_script( 'wp-admin-js', $this->get_admin_url( 'admin-theme.js' ) );
	}

	public function move_script( $handler, $registered = null, $footer = true ) {
		global $wp_scripts;

		$registered = is_null( $registered ) ? $handler : $registered;

		if ( ! empty( $wp_scripts->registered[ $registered ] ) ) {
			$js = $wp_scripts->registered[ $registered ];

			wp_deregister_script( $handler );
			wp_register_script( $handler, $js->src, [], $js->ver, $footer );
		}

	}

	/**
	 * @return string
	 */
	public static function get_stylesheet_url( $slug ) {
		return self::$stylesheet . '/assets/' . $slug;
	}

	/**
	 * @return string
	 */
	public static function get_dependence_url( $slug ) {
		return self::$stylesheet . '/assets/dependence/' . $slug;
	}

	/**
	 * @return string
	 */
	public static function get_admin_url( $slug ) {
		return self::$stylesheet . '/assets/admin/' . $slug;
	}
}
