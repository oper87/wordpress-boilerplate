<?php
/**
 * Theme settings
 *
 * @version 1.0.0
 */

namespace App;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Settings {

	public function __construct() {

		// Remove Admin bar
		add_filter( 'show_admin_bar', '__return_false' );

		// Add options page
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page( [
				'page_title' => 'General Settings',
				'menu_title' => 'Site Options',
				'menu_slug'  => 'theme-general-settings',
				'capability' => 'edit_posts',
				'redirect'   => false,
			] );

			add_filter( 'acf/settings/enable_post_types', '__return_false' );
			add_filter( 'acf/settings/enable_options_pages_ui', '__return_false' );

			add_filter( 'acf/settings/save_json', [ $this, 'acf_settings_save_json' ] );
		}

		// Init setting after_setup_theme action
		add_action( 'after_setup_theme', [ $this, 'theme_setup' ] );

		// Init languages
		add_action( 'after_setup_theme', [ $this, 'load_theme_textdomain' ] );

		// Extend body classes
		add_filter( 'body_class', [ $this, 'add_body_class' ] );

		// Remove emojis and improve page loads
		add_action( 'init', [ $this, 'theme_disable_emojis' ] );

		add_action( 'admin_init', [ $this, 'hide_wordpress_editor' ] );

		//add_action( 'admin_menu', [ $this, 'change_admin_menu' ] );

		add_filter( 'wp_title', [ $this, 'wp_title' ], 10, 2 );

		add_filter( 'upload_mimes', [ $this, 'extend_upload_mimes' ] );
		add_filter( 'wp_check_filetype_and_ext', [ $this, 'extend_check_filetype_and_ext' ], 10, 4 );
	}

	/**
	 *
	 */
	public function theme_setup() {
		// Register WP Menu
		register_nav_menus( [
			'header' => 'Header menu',
			'footer' => 'Footer menu',
		] );

		// Add theme supports
		add_theme_support( 'html5', [ 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script' ] );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		// Add custom images sizes
		/*add_image_size( 'ruby_thumb', 80, 80, true );*/

		// Disable Gutenberg editor
		add_filter( 'use_block_editor_for_post', '__return_false', 10 );

		// Disabled scales images
		add_filter( 'big_image_size_threshold', '__return_false' );
	}

	/**
	 * Save ACF Groups as json file
	 *
	 * @param $path
	 *
	 * @return string
	 */
	public function acf_settings_save_json( $path ) {
		return get_stylesheet_directory() . '/acf-json';
	}

	/**
	 * Load languages
	 * You can replace "ruby_studio" file name with the theme name.
	 * Then we need to use wp-cli to generate pot file.
	 */
	public function load_theme_textdomain() {
		$locale = apply_filters( 'theme_locale', determine_locale(), 'ruby_studio' );
		load_textdomain( 'ruby_studio', get_template_directory() . '/languages/ruby_studio-' . $locale . '.mo' );
	}

	/**
	 * Add page slug to body class
	 *
	 * @return array
	 */
	public function add_body_class( $classes ) {
		global $post;

		if ( is_home() ) {
			$key = array_search( 'blog', $classes );
			if ( $key > - 1 ) {
				unset( $classes[ $key ] );
			}
		} elseif ( is_front_page() ) {
			$classes[] = 'frontpage';
		} elseif ( is_page() ) {
			$classes[] = sanitize_html_class( $post->post_name );
		} elseif ( is_singular() ) {
			$classes[] = sanitize_html_class( $post->post_name );
		} elseif ( is_search() && function_exists( 'WC' ) ) {
			$classes[] = 'woocommerce';
		}

		return $classes;
	}

	/**
	 * Disable the emoji's
	 */
	public function theme_disable_emojis() {
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		add_filter( 'tiny_mce_plugins', [ $this, 'disable_emojis_tinymce' ] );
	}

	/**
	 * Filter function used to remove the tinymce emoji plugin.
	 *
	 * @param array $plugins
	 *
	 * @return array Difference betwen the two arrays
	 */
	public function disable_emojis_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, [ 'wpemoji' ] );
		} else {
			return [];
		}
	}

	/**
	 * Disable WordPress Editor on Frontpage
	 */
	public function hide_wordpress_editor() {
		$post_id = ( ! empty( $_GET['post'] ) && ! is_array( $_GET['post'] ) ) ? strip_tags( $_GET['post'] ) : false;

		if ( $post_id == false ) {
			$post_id = ! empty( $_POST['post_ID'] ) ? strip_tags( $_POST['post_ID'] ) : false;
		}

		if ( ! isset( $post_id ) ) {
			return;
		}

		$template_file = get_post_meta( $post_id, '_wp_page_template', true );

		// Display editor for Front Page
		if ( $template_file == 'front-page.php' || $post_id == get_option( 'page_on_front' ) ) {
			remove_post_type_support( 'page', 'editor' );
		}

		if ( $template_file == 'templates/template-contact.php' || $template_file == 'templates/template-consortium-members.php' ) {
			remove_post_type_support( 'page', 'editor' );
		}
	}

	public function change_admin_menu() {
		global $menu, $submenu;

		if ( isset( $menu['25'] ) && in_array( 'edit-comments.php', $menu['25'] ) ) {
			unset( $menu['25'] );
		}
	}

	/**
	 * Create a nicely formatted and more specific title element text for output
	 * in head of document, based on current view.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 *
	 * @return string The filtered title.
	 * @global int $page WordPress paginated post page count.
	 *
	 * @global int $paged WordPress archive pagination page count.
	 */
	public function wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $title;
		}

		// Add the site name.
		$title .= get_bloginfo( 'name', 'display' );

		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title = "$title $sep " . sprintf( __( 'Page %s', 'nix' ), max( $paged, $page ) );
		}

		return $title;
	}

	/**
	 * Add support for external files
	 *
	 * @param $mimes
	 *
	 * @return mixed
	 */
	public function extend_upload_mimes( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';

		/*$mimes['js']   = 'text/javascript';
		$mimes['usdz'] = 'model/vnd.usdz+zip';
		$mimes['gltf'] = 'model/gltf+json';
		$mimes['glb']  = 'model/gltf-binary';*/

		return $mimes;
	}

	/**
	 * Add support for external files
	 *
	 * @param $data
	 * @param $file
	 * @param $filename
	 * @param $mimes
	 *
	 * @return array
	 */
	public function extend_check_filetype_and_ext( $data, $file, $filename, $mimes ): array {
		if ( ! empty( $data['ext'] ) && ! empty( $data['type'] ) ) {
			return $data;
		}

		$registered_file_types = [
			'svg' => 'image/svg+xml',
			/*'js'   => 'text/javascript',
			'usdz' => 'model/vnd.usdz+zip',
			'gltf' => 'model/gltf+json',
			'glb'  => 'model/gltf-binary'*/
		];

		$filetype = wp_check_filetype( $filename, $mimes );

		if ( ! isset( $registered_file_types[ $filetype['ext'] ] ) ) {
			return $data;
		}

		return [
			'ext'             => $filetype['ext'],
			'type'            => $filetype['type'],
			'proper_filename' => $data['proper_filename'],
		];
	}

}