<?php
/**
 * App User code
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Users {

	const SITE_ADMIN_ROLE = 'site_administrator';

	public function __construct() {

		add_action( 'app_migration_run', [ $this, 'setup_user_roles' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu_visibility' ] );
		add_action( 'wp_before_admin_bar_render', [ $this, 'admin_bar_visibility' ] );

		add_filter( 'pre_site_transient_update_core', [ $this, 'remove_core_updates' ] );
		add_filter( 'pre_site_option_auto_core_update_failed', [ $this, 'remove_core_updates' ] );
		add_filter( 'pre_site_option_auto_core_update_failed', [ $this, 'remove_core_update_failed' ] );

		if ( defined( 'REDIRECTION_FILE' ) ) {
			add_filter( 'redirection_role', [ $this, 'modify_redirection_capabilities' ] );
			add_filter( 'redirection_capability_check', [ $this, 'modify_redirection_capabilities' ] );
		}

		add_filter( 'woocommerce_current_user_can_edit_customer_meta_fields', [ $this, 'wc_current_user_can_edit_customer_meta_fields' ], 10, 2 );
		add_filter( 'woo_preview_emails_min_capability', [ $this, 'woo_preview_emails_min_capability' ], 10, 2 );

		add_filter( 'get_avatar', [ $this, 'acf_profile_avatar' ], 10, 5 );

		add_filter( 'map_meta_cap', [ $this, 'map_meta_cap_privacy_policy' ], 10, 4 );
	}

	public function setup_user_roles() {
		$role = get_role( self::SITE_ADMIN_ROLE );

		if ( empty( $role ) ) {
			add_role( self::SITE_ADMIN_ROLE, 'Site Administrator' );
		}

		$role = get_role( self::SITE_ADMIN_ROLE );

		if ( ! empty( $role ) ) {
			/**
			 * Inherit Editor role capabilities
			 */
			$role->add_cap( 'edit_others_pages' );
			$role->add_cap( 'edit_published_pages' );
			$role->add_cap( 'publish_pages' );
			$role->add_cap( 'delete_pages' );
			$role->add_cap( 'delete_others_pages' );
			$role->add_cap( 'delete_published_pages' );
			$role->add_cap( 'delete_posts' );
			$role->add_cap( 'delete_others_posts' );
			$role->add_cap( 'delete_published_posts' );
			$role->add_cap( 'delete_private_posts' );
			$role->add_cap( 'edit_private_posts' );
			$role->add_cap( 'read_private_posts' );
			$role->add_cap( 'delete_private_pages' );
			$role->add_cap( 'edit_private_pages' );
			$role->add_cap( 'read_private_pages' );
			$role->add_cap( 'moderate_comments' );
			$role->add_cap( 'manage_categories' );
			$role->add_cap( 'manage_links' );
			$role->add_cap( 'upload_files' );
			$role->add_cap( 'unfiltered_html' );
			$role->add_cap( 'edit_posts' );
			$role->add_cap( 'edit_others_posts' );
			$role->add_cap( 'edit_published_posts' );
			$role->add_cap( 'publish_posts' );
			$role->add_cap( 'edit_pages' );
			$role->add_cap( 'read' );
			$role->add_cap( 'level_7' );
			$role->add_cap( 'level_6' );
			$role->add_cap( 'level_5' );
			$role->add_cap( 'level_4' );
			$role->add_cap( 'level_3' );
			$role->add_cap( 'level_2' );
			$role->add_cap( 'level_1' );
			$role->add_cap( 'level_0' );

			/**
			 * Site Administrator role capabilities
			 */
			$role->add_cap( 'list_users' );
			$role->add_cap( 'edit_users' );
			$role->add_cap( 'delete_users' );
			$role->add_cap( 'create_users' );
			$role->add_cap( 'edit_theme_options' );
			$role->add_cap( 'edit_themes', false );
			$role->add_cap( 'customize', false );
			$role->add_cap( 'switch_themes', false );
			$role->add_cap( 'view_site_health_checks', false );
			$role->add_cap( 'edit_theme_options' );
			$role->add_cap( 'promote_users' );

			if ( class_exists( 'woocommerce' ) ) {
				/**
				 * WooCommerce Core capabilities
				 * @link https://github.com/woocommerce/woocommerce/blob/ee01d4219282387c2975ef4594677453c1dd7a0e/includes/class-wc-install.php#L1114
				 */
				$role->add_cap( 'manage_woocommerce' );
				$role->add_cap( 'view_woocommerce_reports' );

				/**
				 * WooCommerce Types capabilities
				 * All types: 'product', 'shop_order', 'shop_coupon'
				 */
				$woo_types = [ 'product', 'shop_order', 'shop_coupon' ];

				foreach ( $woo_types as $type ):
					$role->add_cap( "edit_{$type}" );
					$role->add_cap( "read_{$type}" );
					$role->add_cap( "delete_{$type}" );
					$role->add_cap( "edit_{$type}s" );
					$role->add_cap( "edit_others_{$type}s" );
					$role->add_cap( "publish_{$type}s" );
					$role->add_cap( "read_private_{$type}s" );
					$role->add_cap( "delete_{$type}s" );
					$role->add_cap( "delete_private_{$type}s" );
					$role->add_cap( "delete_published_{$type}s" );
					$role->add_cap( "delete_others_{$type}s" );
					$role->add_cap( "edit_private_{$type}s" );
					$role->add_cap( "edit_published_{$type}s" );

					// Terms.
					$role->add_cap( "manage_{$type}_terms" );
					$role->add_cap( "edit_{$type}_terms" );
					$role->add_cap( "delete_{$type}_terms" );
					$role->add_cap( "assign_{$type}_terms" );
				endforeach;
			}

			if ( class_exists( 'SitePress' ) ) {
				/**
				 * WPML Translator capabilities
				 * @link https://wpml.org/documentation/support/wpml-admin-capabilities/
				 */
				//$role->add_cap( 'manage_translations' );
				//$role->add_cap( 'wpml_manage_translation_management' );
				//$role->add_cap( 'wpml_manage_languages' );
				//$role->add_cap( 'wpml_manage_translation_options' );
				//$role->add_cap( 'wpml_manage_troubleshooting' );
				$role->add_cap( 'wpml_manage_taxonomy_translation' );
				//$role->add_cap( 'wpml_manage_wp_menus_sync' );
				$role->add_cap( 'wpml_manage_translation_analytics' );
				$role->add_cap( 'wpml_manage_string_translation' );
				$role->add_cap( 'wpml_manage_sticky_links' );
				$role->add_cap( 'wpml_manage_navigation' );
				//$role->add_cap( 'wpml_manage_theme_and_plugin_localization' );
				$role->add_cap( 'wpml_manage_media_translation' );
				$role->add_cap( 'wpml_manage_support' );
				//$role->add_cap( 'wpml_manage_woocommerce_multilingual' );
				//$role->add_cap( 'wpml_operate_woocommerce_multilingual' );
			}

			/**
			 * WP Rocket plugin
			 */
			$role->add_cap( 'rocket_purge_cache' );
			$role->add_cap( 'rocket_purge_opcache' );
			$role->add_cap( 'rocket_purge_cloudflare_cache' );
			$role->add_cap( 'rocket_purge_sucuri_cache' );
			$role->add_cap( 'rocket_preload_cache' );

			/**
			 * Redirection plugin
			 */
			$role->add_cap( 'redirection_manage' );

			/**
			 * SEO by Yoast Roles
			 */
			$role->add_cap( 'wpseo_manage_options' );
			//$role->add_cap( 'wpseo_manager' );
			//$role->add_cap( 'wpseo_editor' );
		}

	}

	/**
	 * Hide menu items in Admin Menu
	 */
	public function admin_menu_visibility() {
		global $submenu;

		if ( $this->current_user_has_role( self::SITE_ADMIN_ROLE ) ) {
			remove_submenu_page( 'themes.php', 'themes.php' );
			remove_submenu_page( 'themes.php', 'customize.php' );

			if ( ! defined( 'REDIRECTION_FILE' ) && current_user_can( 'redirection_manage' ) ) {
				remove_menu_page( 'tools.php' );
			} else {
				remove_submenu_page( 'tools.php', 'tools.php' );
			}

			// Remove Customize from the Appearance submenu
			if ( isset( $submenu['themes.php'][6] ) && $submenu['themes.php'][6][1] == 'customize' ) {
				unset( $submenu['themes.php'][6] );
			}
		}

	}

	/**
	 * Hide menu items in Admin Bar
	 */
	public function admin_bar_visibility() {
		global $wp_admin_bar;

		if ( $this->current_user_has_role( self::SITE_ADMIN_ROLE ) ) {
			$wp_admin_bar->remove_menu( 'comments' );
			$wp_admin_bar->remove_menu( 'customize' );
			$wp_admin_bar->remove_menu( 'search' );
		}
	}

	/**
	 * @return false|object
	 */
	public function remove_core_updates() {
		global $wp_version;

		if ( $this->current_user_has_role( self::SITE_ADMIN_ROLE ) ) {
			return (object) [
				'last_checked'    => time(),
				'version_checked' => $wp_version,
			];
		}

		return false;
	}

	/**
	 * @return array|false
	 */
	public function remove_core_update_failed() {

		if ( $this->current_user_has_role( self::SITE_ADMIN_ROLE ) ) {
			return [
				'attempted' => 0,
				'critical'  => '>',
			];
		}

		return false;
	}

	public function modify_redirection_capabilities( $default_cap ) {
		if ( $this->current_user_has_role( self::SITE_ADMIN_ROLE ) && current_user_can( 'redirection_manage' ) ) {
			return 'redirection_manage';
		} else {
			return $default_cap;
		}
	}

	/**
	 * @param $user_can
	 * @param $user_id
	 *
	 * @return bool|mixed
	 */
	public function wc_current_user_can_edit_customer_meta_fields( $user_can, $user_id ) {
		if ( self::current_user_has_role( self::SITE_ADMIN_ROLE ) ) {
			$user_can = true;
		}

		return $user_can;
	}

	/**
	 * Add manage_woocommerce capability to Woo preview emails plugin
	 *
	 * @return string
	 */
	public function woo_preview_emails_min_capability() {
		return 'manage_woocommerce';
	}

	/**
	 * Add Edit capability for Privacy Policy page
	 *
	 * @param $caps
	 * @param $cap
	 * @param $user_id
	 * @param $args
	 *
	 * @return array|mixed
	 */
	public function map_meta_cap_privacy_policy( $caps, $cap, $user_id, $args ) {
		$privacy_policy_page_id = (int) get_option( 'wp_page_for_privacy_policy' );
		$post_id                = isset( $args[0] ) ? $args[0] : false;

		if ( $cap === 'edit_post' && self::user_has_role( $user_id, static::SITE_ADMIN_ROLE ) && $privacy_policy_page_id == $post_id ) {
			$manage_name = is_multisite() ? 'manage_network' : 'manage_options';
			$caps        = array_diff( $caps, [ $manage_name ] );
		}

		return $caps;
	}

	/**
	 * Checks if the current user has a role.
	 *
	 * @param string $role The role.
	 *
	 * @return bool
	 */
	public static function current_user_has_role( $role ): bool {
		return self::user_has_role( wp_get_current_user(), $role );
	}

	/**
	 * Checks if a user has a role.
	 *
	 * @param int|\WP_User $user The user.
	 * @param string $role The role.
	 *
	 * @return bool
	 */
	public static function user_has_role( $user, $role ): bool {
		if ( ! is_object( $user ) ) {
			$user = get_userdata( $user );
		}

		if ( ! $user || ! $user->exists() ) {
			return false;
		}

		return in_array( $role, $user->roles, true );
	}

	/**
	 * Use ACF image field as avatar
	 * @author Mike Hemberger
	 * @link http://thestizmedia.com/acf-pro-simple-local-avatars/
	 * @uses ACF Pro image field (tested return value set as Array )
	 */
	public function acf_profile_avatar( $avatar, $id_or_email, $size, $default, $alt ) {

		$user = '';

		// Get user by id or email
		if ( is_numeric( $id_or_email ) ) {

			$id   = (int) $id_or_email;
			$user = get_user_by( 'id', $id );

		} elseif ( is_object( $id_or_email ) ) {

			if ( ! empty( $id_or_email->user_id ) ) {
				$id   = (int) $id_or_email->user_id;
				$user = get_user_by( 'id', $id );
			}

		} else {
			$user = get_user_by( 'email', $id_or_email );
		}

		if ( ! $user ) {
			return $avatar;
		}

		// Get the user id
		$user_id = $user->ID;

		// Get the file id
		$image_id = get_user_meta( $user_id, 'avatar', true ); // CHANGE TO YOUR FIELD NAME

		// Bail if we don't have a local avatar
		if ( ! $image_id ) {
			return $avatar;
		}

		// Get the file size
		$image_url = wp_get_attachment_image_src( $image_id, 'thumbnail' ); // Set image size by name
		// Get the file url
		$avatar_url = $image_url[0];
		// Get the img markup
		$avatar = '<img alt="' . $alt . '" src="' . $avatar_url . '" class="avatar avatar-' . $size . '" height="' . $size . '" width="' . $size . '"/>';

		// Return our new avatar
		return $avatar;
	}
}