<?php
/**
 * App User code
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use WP_CLI;

class WP_Console {

	/**
	 * Send password resete email for one or more users.
	 *
	 * ## OPTIONS
	 * <user>...
	 * : one or more user logins or IDs.
	 *
	 * [--timeout]
	 * : Set timeout between sending emails
	 *
	 * ## EXAMPLES
	 *
	 *     wp app user_reset_password 12345
	 *     wp app user_reset_password 12345 --timeout=14
	 *
	 */
	public function user_reset_password( $args, $assoc_args ) {
		$assoc_args = wp_parse_args(
			$assoc_args,
			array(
				'timeout' => 0,
			)
		);

		foreach ( $args as $user_id_name ) {

			if ( intval( $user_id_name ) !== 0 ) {
				$user = get_user_by( 'ID', $user_id_name );
			} else {
				$user = get_user_by( 'login', $user_id_name );
			}

			if ( empty( $user ) ) {
				if ( intval( $user_id_name ) !== 0 ) {
					WP_CLI::log( __( sprintf( __( "User ID #%s does not exits.", 'ruby_studio_admin' ), $user_id_name ) ) );
				} else {
					WP_CLI::log( __( sprintf( __( "Username '%s' does not exits.", 'ruby_studio_admin' ), $user_id_name ) ) );
				}
			} else {

				if ( get_user_meta( $user->ID, '_reset_email_sent', true ) == 'yes' ) {
					WP_CLI::log( sprintf( __( "Reset email for user '%s' is already sent.", 'ruby_studio_admin' ), $user->user_email ) );
				} else {

					if ( $assoc_args['timeout'] > 0 ) {
						WP_CLI::log( __( sprintf( __( "Reset email will be sent to '%s' in %s seconds.", 'ruby_studio_admin' ), $user_id_name, $assoc_args['timeout'] ) ) );
					}

					/**
					 * Set timeout before sending the retrieve_password
					 */
					sleep( $assoc_args['timeout'] );

					if ( retrieve_password( $user->user_login ) ) {
						add_user_meta( $user->ID, '_reset_email_sent', 'yes' );

						WP_CLI::success( sprintf( __( "Reset email is sent to '%s'.", 'ruby_studio_admin' ), $user->user_email ) );
					} else {
						WP_CLI::log( __( sprintf( __( "Failed to send reset email to '%s'.", 'ruby_studio_admin' ), $user_id_name ) ) );
					}
				}

			}
		}


	}


	public function import_gallery( $args, $assoc_args ) {

		$assoc_args = wp_parse_args(
			$assoc_args,
			array(
				'timeout' => 0,
			)
		);

		$media = new Import_Media( 'https://jdku.dk/' );

		foreach ( $args as $post_id ) {
			$post = get_post( $post_id );

			if ( ! empty( $post ) ) {
				$thumbnail_id   = (int) get_post_meta( $post->ID, '_thumbnail_id', true );
				$gallery_images = get_post_meta( $post->ID, 'gallery_images', true );

				$new_thumbnail_id = $media->import_media( $thumbnail_id );

				if ( ! is_wp_error( $new_thumbnail_id ) ) {

					WP_CLI::log( __( sprintf( __( "Thumbnail was updated to new image '#%s'", 'ruby_studio_admin' ), $new_thumbnail_id ) ) );

					update_post_meta( $post->ID, '_thumbnail_id', $new_thumbnail_id );
				} else {

					WP_CLI::log( __( sprintf( __( "Thumbnail import error '#%s'", 'ruby_studio_admin' ), $new_thumbnail_id->get_error_message() ) ) );

				}

				$new_gallery_images = [];

				foreach ( $gallery_images as $gallery_image ) {
					$new_gallery_image = $media->import_media( $gallery_image );

					if ( ! is_wp_error( $new_gallery_image ) ) {

						WP_CLI::log( __( sprintf( __( "Gallery image was updated to new image '#%s'", 'ruby_studio_admin' ), $new_gallery_image ) ) );

						$new_gallery_images[] = $new_gallery_image;
					} else {
						WP_CLI::log( __( sprintf( __( "Gallery image import error '#%s'", 'ruby_studio_admin' ), $new_gallery_image->get_error_message() ) ) );
					}
				}

				if ( ! empty( $new_gallery_images ) ) {
					update_post_meta( $post->ID, 'gallery_images', $new_gallery_images );

					WP_CLI::success( __( "Gallery post was updated", 'ruby_studio_admin' ) );
				}
			}
		}
	}


	/**
	 * woocommerce_order_resend_emails.
	 *
	 * ## OPTIONS
	 * <ids>...
	 * : one or more order IDs.
	 *
	 * [--timeout]
	 * : Set timeout between sending emails
	 *
	 * ## EXAMPLES
	 *
	 *     wp app woocommerce_order_resend_email 12345
	 *     wp app woocommerce_order_resend_email 12345 --timeout=14
	 *
	 */
	public function woocommerce_order_resend_email( $args, $assoc_args ) {
		$assoc_args = wp_parse_args(
			$assoc_args,
			array(
				'timeout' => 0,
			)
		);

		foreach ( $args as $order_id ) {
			$order = wc_get_order( $order_id );

			if ( empty( $order ) ) {
				WP_CLI::log( __( sprintf( __( "Order ID #%s does not exits.", 'ruby_studio_admin' ), $order_id ) ) );
			} else {

				/**
				 * Set timeout before sending the retrieve_password
				 */
				if ( $assoc_args['timeout'] > 0 ) {
					WP_CLI::log( __( sprintf( __( "Emails for order '%s' will be resent in %s seconds at %s.", 'ruby_studio_admin' ), $order_id, $assoc_args['timeout'], date( 'd-m-Y H:i:s' ) ) ) );
				}

				sleep( $assoc_args['timeout'] );

				WC()->mailer()->emails['WC_Email_Customer_Completed_Order']->trigger( $order->get_id(), $order, true );

				WP_CLI::success( sprintf( __( "Completed order email for Order ID #%s was resent at %s.", 'ruby_studio_admin' ), $order_id, date( 'd-m-Y H:i:s' ) ) );

				if ( ! empty( $participant_id = \App\WooCommerce\Order::getInstance()->get_order_participant_id( $order ) ) ) {
					$participant = \App\Participant\Participant::getInstance()->model->get_by_id( $participant_id );
					//do_action( 'customer_registration_completed', $participant_id, $participant );*/

					WC()->mailer()->emails['WC_Email_Customer_Registration_Completed']->trigger( $participant_id, $participant );

					WP_CLI::success( sprintf( __( "Participant registration email for Order ID #%s and Participant #%s was resent at %s.", 'ruby_studio_admin' ), $order_id, $participant_id, date( 'd-m-Y H:i:s' ) ) );
				}

			}
		}

	}

	/**
	 *
	 *  ## EXAMPLES
	 *
	 *      wp app user_list --field=ID
	 *      wp app user_list --fields='full_name,user_email,user_pass,billing_full_name'
	 *
	 * @param $args
	 * @param $assoc_args
	 *
	 * @return void
	 */
	public function user_list( $args, $assoc_args ) {

		$display_fields = array(
			'ID',
			'user_login',
			'display_name',
			'user_email',
			'user_registered',
			'roles',
			'user_pass',
			'user_nicename',
			'meta_input',
			'full_name'
			//'user_url',
			//'user_activation_key',
			//'user_status',
			//'spam',
			//'deleted',
			//'caps',
			//'cap_key',
			//'allcaps',
			//'filter',
			//'url',
		);

		// Initialize the class.
		$formatter = new WP_CLI\Formatter( $assoc_args, $display_fields, 'user' );

		if ( WP_CLI\Utils\get_flag_value( $assoc_args, 'network' ) ) {
			if ( ! is_multisite() ) {
				WP_CLI::error( 'This is not a multisite installation.' );
			}
			$assoc_args['blog_id'] = 0;
			if ( isset( $assoc_args['fields'] ) ) {
				$fields               = explode( ',', $assoc_args['fields'] );
				$assoc_args['fields'] = array_diff( $fields, [ 'roles' ] );
			} else {
				$assoc_args['fields'] = array_diff( $this->obj_fields, [ 'roles' ] );
			}
		}

		if ( in_array( $formatter->format, [ 'ids', 'count' ], true ) ) {
			$assoc_args['fields'] = 'ids';
		} else {
			$assoc_args['fields'] = 'all_with_meta';
		}

		$assoc_args['count_total'] = false;
		$assoc_args                = self::process_csv_arguments_to_arrays( $assoc_args );

		if ( ! empty( $assoc_args['role'] ) && 'none' === $assoc_args['role'] ) {
			$norole_user_ids = wp_get_users_with_no_role();

			if ( ! empty( $norole_user_ids ) ) {
				$assoc_args['include'] = $norole_user_ids;
				unset( $assoc_args['role'] );
			}
		}

		$users = get_users( $assoc_args );

		if ( $users ) {
			cache_users( wp_list_pluck( $users, 'ID' ) );

			$r = array();
			foreach ( $users as $user ) {
				$r[ $user->ID ]                    = $user;
				$r[ $user->ID ]->full_name         = get_user_meta( $user->ID, 'first_name', true ) . ' ' . get_user_meta( $user->ID, 'last_name', true );
				$r[ $user->ID ]->billing_full_name = get_user_meta( $user->ID, 'billing_first_name', true ) . ' ' . get_user_meta( $user->ID, 'billing_last_name', true );
			}

			$users = $r;
		} else {
			$users = [];
		}


		if ( 'ids' === $formatter->format ) {
			echo implode( ' ', $users );
		} elseif ( 'count' === $formatter->format ) {
			$formatter->display_items( $users );
		} else {
			$iterator = WP_CLI\Utils\iterator_map(
				$users,
				function ( $user ) {
					if ( ! is_object( $user ) ) {
						return $user;
					}

					$user->roles = implode( ',', $user->roles );
					$user->url   = get_author_posts_url( $user->ID, $user->user_nicename );

					return $user;
				}
			);

			$formatter->display_items( $iterator );
		}
	}

	/**
	 * Transforms arguments with '__' from CSV into expected arrays
	 *
	 * @param array $assoc_args
	 *
	 * @return array
	 */
	protected static function process_csv_arguments_to_arrays( $assoc_args ) {
		foreach ( $assoc_args as $k => $v ) {
			if ( false !== strpos( $k, '__' ) ) {
				$assoc_args[ $k ] = explode( ',', $v );
			}
		}

		return $assoc_args;
	}

	protected static function wc_clean( $var ) {
		if ( is_array( $var ) ) {
			return array_map( [ static::class, 'wc_clean' ], $var );
		} else {
			return is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
		}
	}
}
