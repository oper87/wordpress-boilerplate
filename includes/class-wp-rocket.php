<?php
/**
 * WP Rocket plugin snipets and helpers
 */

namespace App;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WP_Rocket {


	public function __construct() {
		add_action( 'edit_post', [ $this, 'rocket_clean_post_on_edit' ], 10, 2 );
	}

	public function rocket_clean_post_on_edit( $post_id, $post ) {
		//$allowed_post_types = apply_filters( "app_rocket_clean_post_on_edit__post_types", [] );

		if ( ! function_exists( 'rocket_clean_post' ) ) {
			return;
		}

		if ( $post->post_type == Jobs\Post::POST_TYPE ) {
			rocket_clean_post( Jobs\Post::get_page( 'ID' ) );
		}
	}

}