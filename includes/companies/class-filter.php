<?php

namespace App\Companies;

use App\Companies\Post;
use App\Helper;
use App\Scorecard;
use WP_Meta_Query;
use WP_Tax_Query;

class Filter extends \App\Abstracts\Filter {

	private static $cache = true;

	private $active = false;

	private $filters = [];

	private function __construct() {
		$args = [
			'insurer_type' => [
				'name'        => __( 'Insurer Type', 'ruby_studio' ),
				'query_from'  => 'taxonomy',
				'data_from'   => 'taxonomy',
				'taxonomy'    => Tax_Insurer_Type::TAXONOMY,
				'choose_type' => 'multiple',
			],
			'countries'    => [
				'name'        => __( 'Countries', 'ruby_studio' ),
				'query_from'  => 'taxonomy',
				'data_from'   => 'taxonomy',
				'taxonomy'    => Tax_Country::TAXONOMY,
				'choose_type' => 'multiple',
			],
		];

		parent::__construct( $args, Post::POST_TYPE );

		add_action( 'wp', [ $this, 'activate_filter' ] );
		add_action( 'app_theme_scorecard_after_main_content', [ $this, 'init_filter' ] );

		add_action( 'save_post_' . Post::POST_TYPE, [ $this, 'clear_filter_terms_cache' ], 10, 1 );
		add_action( 'trashed_post', [ $this, 'clear_filter_terms_cache' ], 10, 1 );
		add_action( 'untrash_post', [ $this, 'clear_filter_terms_cache' ], 10, 1 );
		add_action( 'set_object_terms', [ $this, 'clear_filter_tax_terms_cache' ], 10, 4 );
		//add_action( 'delete_term', [ $this, 'clear_filter_tax_terms_cache' ], 10, 3 );

		$this->hook_init();
	}

	public function is_run_pre_get_posts( $query ) {
		global $post;

		//$scorecard = Scorecard::get_page();
		// We only want to affect the main query.
		if ( $query->get('app_post_type_filter') && $this->post_type == $query->get( 'post_type' ) ) {
			return true;
		}

		return false;
	}

	function activate_filter() {
		if ( is_page( \App\Scorecard::get_page() ) && ! is_admin() ) :
			$self          = self::getInstance();
			$this->filters = $self->get_filters();

			if ( $this->filter_has_any_term ) {
				$this->active = true;
			}

		endif;
	}

	public function is_active_filter() {
		return $this->active;
	}

	/**
	 * Statement condition
	 *
	 * @return bool
	 */
	static public function is_filter_page() {
		if ( filter_input( INPUT_GET, 'filter_diet' ) ||
		     filter_input( INPUT_GET, 'filter_season' ) ||
		     filter_input( INPUT_GET, 'filter_tid' ) ) {
			return true;
		}

		return false;
	}

	public function init_filter( $companies ) {
		global $wp_query;

		if ( ! $this->is_active_filter() ) {
			return;
		}

		if ( empty( $this->filter_params ) ) {
			return;
		}

		$archive    = get_queried_object();
		$action_url = Scorecard::get_page_url();

		get_template_part( 'template-parts/scorecard/filter', 'sidebar', [
			'action_url'  => $action_url,
			'filters'     => $this->filters,
			'found_posts' => count( $companies )
		] );
	}

	public function filter_head() {
		if ( ! $this->is_active_filter() ) {
			return;
		}

		if ( empty( $this->filter_params ) ) {
			return;
		}

		Helper::get_template_part( '/template-parts/archive/filter-head', null, [
			'filter_params' => $this->filter_params
		] );

	}

	public function get_terms( $taxonomy, $query_key = false ) {
		global $wpdb;

		$terms = get_terms( [
			'taxonomy'   => $taxonomy,
			'order'      => 'asc',
			'orderby'    => 'title',
			'hide_empty' => true,
		] );

		if ( empty( $terms ) ) {
			return [];
		}

		$term_ids = wp_list_pluck( $terms, 'term_id' );

		$query_object  = get_queried_object();
		$main_query    = $this->get_query();
		$tax_query_sql = $meta_query_sql = [ 'join' => '', 'where' => '' ];

		if ( isset( $main_query['tax_query'] ) ) {
			foreach ( $main_query['tax_query'] as $key => $tax ) {
				if ( is_array( $main_query['tax_query'] ) && is_array( $tax ) && $query_object->taxonomy !== $tax['taxonomy'] ) {
					unset( $main_query['tax_query'][ $key ] );
				}
			}

			$tax_query     = new WP_Tax_Query( $main_query['tax_query'] );
			$tax_query_sql = $tax_query->get_sql( $wpdb->posts, 'ID' );
		}

		if ( isset( $main_query['meta_query'] ) ) {
			$meta_query     = new WP_Meta_Query( $main_query['meta_query'] );
			$meta_query_sql = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
		}

		// Generate query.
		$query           = [];
		$query['select'] = "SELECT COUNT( DISTINCT {$wpdb->posts}.ID ) as term_count, terms.term_id as term_count_id";
		$query['from']   = "FROM {$wpdb->posts}";
		$query['join']   = "
			INNER JOIN {$wpdb->term_relationships} AS term_relationships ON {$wpdb->posts}.ID = term_relationships.object_id
			INNER JOIN {$wpdb->term_taxonomy} AS term_taxonomy USING( term_taxonomy_id )
			INNER JOIN {$wpdb->terms} AS terms USING( term_id )
			" . $tax_query_sql['join'] . $meta_query_sql['join'];

		$query['where'] = "
			WHERE {$wpdb->posts}.post_type IN ( '" . Post::POST_TYPE . "' )
			AND {$wpdb->posts}.post_status = 'publish'"
		                  . $tax_query_sql['where'] . $meta_query_sql['where'] .
		                  ' AND terms.term_id IN (' . implode( ',', array_map( 'absint', $term_ids ) ) . ')';

		$query['group_by'] = 'GROUP BY terms.term_id';

		$query = implode( ' ', $query );

		// We have a query - let's see if cached results of this query already exist.
		$query_hash = md5( $query );

		if ( true === static::$cache ) {
			$cached_counts = (array) get_transient( 'app_filter_terms_counts_' . sanitize_title( $taxonomy ) );
		} else {
			$cached_counts = [];
		}

		if ( ! isset( $cached_counts[ $query_hash ] ) ) {
			$results                      = $wpdb->get_results( $query, ARRAY_A ); // @codingStandardsIgnoreLine
			$term_counts                  = array_map( 'absint', wp_list_pluck( $results, 'term_count', 'term_count_id' ) );
			$cached_counts[ $query_hash ] = $term_counts;

			if ( true === static::$cache ) {
				set_transient( 'app_filter_terms_counts_' . sanitize_title( $taxonomy ), $cached_counts, DAY_IN_SECONDS );
			}
		}

		$term_counts = array_map( 'absint', (array) $cached_counts[ $query_hash ] );

		$new_terms       = [];
		$filter_selected = $this->get_filter_selected();

		foreach ( $terms as $key => $term ) {
			$option_is_set = ! empty( $filter_selected[ $query_key ]['selected_array'] ) ? in_array( $term->slug, $filter_selected[ $query_key ]['selected_array'], true ) : false;
			$count         = isset( $term_counts[ $term->term_id ] ) ? $term_counts[ $term->term_id ] : 0;

			if ( 0 > $count ) {
				continue;
			} elseif ( 0 === $count && ! $option_is_set ) {
				continue;
			}

			$new_terms[] = $term;
		}

		return $new_terms;
	}

	public function clear_filter_terms_cache( $post_id ) {
		$post = get_post( $post_id );

		if ( $post && $post->post_type == $this->post_type ) {
			delete_transient( 'app_filter_terms_counts_' . Tax_Country::TAXONOMY );
			delete_transient( 'app_filter_terms_counts_' . Tax_Insurer_Type::TAXONOMY );
			delete_transient( 'app_filter_recipe_total_time' );
		}
	}

	public function clear_filter_tax_terms_cache( $object_id, $term_id, $tt_id, $taxonomy ) {
		if ( $taxonomy == Tax_Country::TAXONOMY ) {
			delete_transient( 'app_filter_terms_counts_' . Tax_Country::TAXONOMY );
		}

		if ( $taxonomy == Tax_Insurer_Type::TAXONOMY ) {
			delete_transient( 'app_filter_terms_counts_' . Tax_Insurer_Type::TAXONOMY );
		}
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}