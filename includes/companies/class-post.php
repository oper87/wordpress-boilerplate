<?php
/**
 * Register post_type
 *
 * Init class add to functions.php - new App\Post_Type\Sample_Post();
 * Use method in the theme - App\Post_Type\Sample_Post::getInstance()->get_posts(-1);
 *
 * @version 1.0.0
 */

namespace App\Companies;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Post extends \App\Abstracts\Post {

	/**
	 * Post-type constant
	 */
	const POST_TYPE = 'company';

	protected function __construct() {
		parent::__construct( static::POST_TYPE );

		Tax_Country::getInstance()->register_taxonomy();

		parent::register_post_type();
	}

	/**
	 * @return array
	 */
	protected function get_labels(): array {
		return [
			'name'          => _x( 'Companies', 'Post Type General Name', 'text_domain' ),
			'singular_name' => _x( 'Company', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'     => __( 'Companies', 'text_domain' ),
		];
	}

	/**
	 * @return array
	 */
	protected function get_settings(): array {
		return [
			'supports'            => [ 'title', 'custom-fields' ],
			'taxonomies'          => [ Tax_Country::TAXONOMY ],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-building',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		];
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}