<?php
/**
 * Abstract Taxonomy Class
 */

namespace App\Companies;

use App\Companies\Post;
use App\Abstracts\Taxonomy;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Tax_Country extends Taxonomy {

	const TAXONOMY = 'country';

	private function __construct() {
		parent::__construct( static::TAXONOMY, [ Post::POST_TYPE ] );
	}

	/**
	 * @return array
	 */
	protected function get_labels(): array {
		return wp_parse_args( [
			'name'          => _x( 'Countries', 'Taxonomy General Name', 'text_domain' ),
			'singular_name' => _x( 'Country', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'     => __( 'Countries', 'text_domain' ),
		], parent::get_labels() );
	}

	/**
	 * @return array[]
	 */
	protected function get_settings(): array {
		return [
			'hierarchical'      => true,
			'public'            => false,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => false,
			'show_tagcloud'     => false,
			'show_in_rest'      => true,
		];
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}