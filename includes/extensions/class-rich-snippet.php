<?php
/**
 * Rich Snippet Class
 */

namespace App\Extensions;

class Rich_Snippet {

	/**
	 * Data
	 *
	 * @var array
	 */
	private array $data = [];

	/**
	 * Construct
	 *
	 * @param string $type
	 * @param bool $init
	 */
	public function __construct( string $type, bool $init = true ) {
		if ( $init ) {
			$this->set_data( '@context', 'https://schema.org' );
		}

		$this->set_data( '@type', $type );
	}

	/**
	 * Set data
	 *
	 * @param $key
	 * @param $value
	 *
	 * @return void
	 */
	public function set_data( $key, $value ) {
		if ( ! $key ) {
			return;
		}

		$this->data[ $key ] = $value;
	}

	/**
	 * Get data
	 *
	 * @return array
	 */
	public function get_data(): array {
		return $this->data;
	}

	/**
	 * Get embed json data format
	 *
	 * @return string
	 */
	public function get_data_json(): string {
		return sprintf( '<script type="application/ld+json">%s</script>', wp_json_encode( $this->data ) );
	}

	/**
	 * Convert time to RichSnippet time format
	 *
	 * @param string $string
	 *
	 * @return string
	 */
	protected function str_to_time( string $string ): string {
		$string        = mb_strtolower( $string );
		$string        = str_replace( ' ', '', $string );
		$minutesLabels = [ "m" ];
		$hoursLabels   = [ "t", "h" ];
		$minutes       = "";
		$hours         = "";

		foreach ( $minutesLabels as $minutesLabel ) {
			$matches = null;
			preg_match( "/(\d+?)" . $minutesLabel . "/", $string, $matches );

			if ( ! empty( $matches[1] ) ) {
				$minutes = $matches[1];
				break;
			}
		}

		foreach ( $hoursLabels as $hoursLabel ) {
			$matches = null;
			preg_match( "/(\d+?)" . $hoursLabel . "/", $string, $matches );

			if ( ! empty( $matches[1] ) ) {
				$hours = $matches[1];
				break;
			}
		}

		$return = "PT";

		if ( ! empty( $hours ) ) {
			$return .= $hours . "H";
		}

		if ( ! empty( $minutes ) ) {
			$return .= $minutes . "M";
		}

		return $return;
	}

	/**
	 * Detect google bots
	 *
	 * @return bool
	 */
	protected static function detect_google_bots(): bool {
		// User lowercase string for comparison.
		$user_agent = isset( $_SERVER['HTTP_USER_AGENT'] ) ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) : '';

		// A list of some common words used only for bots and crawlers.
		$bot_identifiers = [
			'googlebot',
			'adsbot',
			'google.com',
		];

		// See if one of the identifiers is in the UA string.
		foreach ( $bot_identifiers as $identifier ) {
			if ( strpos( $user_agent, $identifier ) !== false ) {
				return true;
			}
		}

		return false;
	}
}