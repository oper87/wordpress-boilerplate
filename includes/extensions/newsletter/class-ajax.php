<?php
/**
 * Wordpress Ajax class
 *
 * @version 1.0.0
 */


namespace App\Extensions\Newsletter;

use App\Abstracts\Ajax as Abstract_Ajax;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Ajax extends Abstract_Ajax {

	public function __construct() {
		parent::__construct( 'newsletter_admin' );
	}

	public function mailchimp_save_settings() {
		$api_key = filter_input( INPUT_POST, 'api_key', FILTER_SANITIZE_STRING );

		if ( empty( $api_key ) ) {
			Mailchimp::getInstance()->update_mailchimp_settings( Mailchimp::getInstance()->get_default_settings() );
			wp_send_json_error();
		}


		if ( strpos( $api_key, '******************' ) === false ) {
			$config = Mailchimp::parse_mailchimp_key( $api_key );
			Mailchimp::getInstance()->set_credential( $config['api_key'], $config['server'] );
		} else {
			Mailchimp::getInstance()->get_mailchimp_settings();
			$config = [];
		}

		$connection = Mailchimp::getInstance()->get_connection();

		if ( ! is_wp_error( $connection ) ) {
			$mailchimp_get_lists     = Mailchimp::getInstance()->mailchimp_get_lists();
			$mailchimp_get_templates = Mailchimp::getInstance()->mailchimp_get_templates();

			if ( ! is_wp_error( $mailchimp_get_lists ) ) {
				$config['lists'] = $mailchimp_get_lists;
			}

			if ( ! is_wp_error( $mailchimp_get_templates ) ) {
				$config['templates'] = $mailchimp_get_templates;
			}

			Mailchimp::getInstance()->update_mailchimp_settings( $config );

			ob_start();
			get_template_part( 'template-parts/admin/newsletter/account', 'settings', [
				'mailchimp_settings' => Mailchimp::getInstance()->get_mailchimp_settings()
			] );

			$account_template = ob_get_clean();

			wp_send_json_success( [
				'connection'       => $connection,
				'account_template' => $account_template
			] );
		} else {
			Mailchimp::getInstance()->update_mailchimp_settings( Mailchimp::getInstance()->get_default_settings() );
			wp_send_json_error( $connection->get_error_message() );
		}
	}

	public function mailchimp_save_account() {
		$list_id            = filter_input( INPUT_POST, 'list_id', FILTER_SANITIZE_STRING );
		$wc_list_id         = filter_input( INPUT_POST, 'wc_list_id', FILTER_SANITIZE_STRING );
		$wc_subscribe_event = filter_input( INPUT_POST, 'wc_subscribe_event', FILTER_SANITIZE_STRING );
		$template_id        = filter_input( INPUT_POST, 'template_id', FILTER_SANITIZE_STRING );
		$double_optin       = filter_input( INPUT_POST, 'double_optin', FILTER_SANITIZE_NUMBER_INT );

		$segments   = [];
		Mailchimp::getInstance()->get_mailchimp_settings();
		$connection = Mailchimp::getInstance()->get_connection();

		if ( ! is_wp_error( $connection ) ) {
			$segments = Mailchimp::getInstance()->mailchimp_get_segments( $list_id );

			if ( is_wp_error( $segments ) ) {
				$segments = [];
			}

			$groups = Mailchimp::getInstance()->mailchimp_get_groups( $list_id );

			if ( is_wp_error( $groups ) ) {
				$groups = [];
			}

			$merge_fields = Mailchimp::getInstance()->mailchimp_get_merge_fields( $list_id );

			if ( is_wp_error( $merge_fields ) ) {
				$merge_fields = [];
			}

			if ( $wc_list_id ) {
				$wc_merge_fields = Mailchimp::getInstance()->mailchimp_get_merge_fields( $wc_list_id );

				if ( is_wp_error( $merge_fields ) ) {
					$wc_merge_fields = [];
				}
			} else {
				$wc_merge_fields = [];
			}
		}

		Mailchimp::getInstance()->update_mailchimp_settings( [
			'list_id'            => $list_id,
			'wc_list_id'         => $wc_list_id,
			'wc_subscribe_event' => $wc_subscribe_event,
			'wc_merge_fields'    => $wc_merge_fields,
			'template_id'        => $template_id,
			'double_optin'       => $double_optin,
			'merge_fields'       => $merge_fields,
			'segments'           => $segments,
			'groups'             => $groups
		] );

		ob_start();
		get_template_part( 'template-parts/admin/newsletter/account', 'merge-fields', [
			'merge_fields' => $merge_fields
		] );
		$merge_fields = ob_get_clean();

		ob_start();
		get_template_part( 'template-parts/admin/newsletter/account', 'merge-fields', [
			'title'        => 'WooCommerce merge fields',
			'merge_fields' => $wc_merge_fields
		] );
		$wc_merge_fields = ob_get_clean();

		ob_start();
		get_template_part( 'template-parts/admin/newsletter/account', 'segments', [
			'segments' => $segments
		] );
		$segments = ob_get_clean();

		ob_start();
		get_template_part( 'template-parts/admin/newsletter/account', 'groups', [
			'groups' => $groups
		] );
		$groups = ob_get_clean();

		wp_send_json_success( [
			'message'         => 'Saved!',
			'merge_fields'    => $merge_fields,
			'wc_merge_fields' => $wc_merge_fields,
			'segments'        => $segments,
			'groups'          => $groups
		] );
	}

	public function mailchimp_fetch_account() {
		$list_id     = filter_input( INPUT_POST, 'list_id', FILTER_SANITIZE_STRING );
		$wc_list_id  = filter_input( INPUT_POST, 'wc_list_id', FILTER_SANITIZE_STRING );
		$template_id = filter_input( INPUT_POST, 'template_id', FILTER_SANITIZE_STRING );

		Mailchimp::getInstance()->get_mailchimp_settings();
		$connection = Mailchimp::getInstance()->get_connection();

		if ( ! is_wp_error( $connection ) ) {
			$settings = [
				'list_id'     => $list_id,
				'wc_list_id'  => $wc_list_id,
				'template_id' => $template_id
			];

			$mailchimp_get_lists     = Mailchimp::getInstance()->mailchimp_get_lists();
			$mailchimp_get_templates = Mailchimp::getInstance()->mailchimp_get_templates();

			if ( ! is_wp_error( $mailchimp_get_lists ) ) {
				$settings['lists'] = $mailchimp_get_lists;
			}

			if ( ! is_wp_error( $mailchimp_get_templates ) ) {
				$settings['templates'] = $mailchimp_get_templates;
			}

			$segments = Mailchimp::getInstance()->mailchimp_get_segments( $list_id );

			if ( is_wp_error( $segments ) ) {
				$settings['segments'] = [];
			} else {
				$settings['segments'] = $segments;
			}

			$groups = Mailchimp::getInstance()->mailchimp_get_groups( $list_id );

			if ( is_wp_error( $groups ) ) {
				$settings['groups'] = [];
			} else {
				$settings['groups'] = $groups;
			}

			$merge_fields = Mailchimp::getInstance()->mailchimp_get_merge_fields( $list_id );

			if ( is_wp_error( $merge_fields ) ) {
				$settings['merge_fields'] = [];
			} else {
				$settings['merge_fields'] = $merge_fields;
			}

			if ( $wc_list_id ) {
				$wc_merge_fields = Mailchimp::getInstance()->mailchimp_get_merge_fields( $wc_list_id );

				if ( is_wp_error( $wc_merge_fields ) ) {
					$settings['wc_merge_fields'] = [];
				} else {
					$settings['wc_merge_fields'] = $wc_merge_fields;
				}
			} else {
				$settings['wc_merge_fields'] = [];
			}

			Mailchimp::getInstance()->update_mailchimp_settings( $settings );

			ob_start();
			get_template_part( 'template-parts/admin/newsletter/account', 'settings', [
				'mailchimp_settings' => Mailchimp::getInstance()->get_mailchimp_settings()
			] );
			$account_template = ob_get_clean();

			wp_send_json_success( [
				'account_template' => $account_template,
				'message'          => 'Fetched!'
			] );
		} else {
			wp_send_json_error( $connection->get_error_message() );
		}
	}

	public function newsletter_send_campaign() {

		if ( ! function_exists( 'acf_save_post' ) ) {
			wp_send_json_error( 'ACF could not save data' );
		}

		if ( acf_verify_nonce( 'options' ) ) {
			if ( acf_validate_save_post( true ) ) {
				acf_save_post( $_POST['_acf_post_id'] );
			}
		}

		$email_data = [
			'email_from_name'           => get_field( 'from_name', 'options' ),
			'email_from_email'          => get_field( 'from_email_address', 'options' ),
			'email_subject'             => get_field( 'email_subject', 'options' ),
			'email_content_image'       => get_field( 'email_content_image', 'options' ),
			'email_content_title'       => get_field( 'email_content_title', 'options' ),
			'email_content_text'        => get_field( 'email_content_text', 'options' ),
			'email_products'            => get_field( 'email_products', 'options' ),
			'email_exhibition'          => get_field( 'email_exhibition', 'options' ),
			'email_content_more_button' => get_field( 'email_content_more_button', 'options' ),
			'test_email'                => filter_input( INPUT_POST, 'test_email', FILTER_SANITIZE_EMAIL ),
			'send_as_test'              => filter_input( INPUT_POST, 'send_as_test', FILTER_SANITIZE_NUMBER_INT ),
		];

		$mailchimp_settings = Mailchimp::getInstance()->get_mailchimp_settings();

		$send_as_test = false;

		if ( $email_data['send_as_test'] == 1 && ! empty( $email_data['test_email'] ) ) {
			$send_as_test = true;
		}

		$send_campaign = Mailchimp::getInstance()->send_newsletter_campaign( $email_data, $send_as_test );

		if ( is_wp_error( $send_campaign ) ) {
			wp_send_json_error( $send_campaign->get_error_message() );
		}

		if ( $send_as_test ) {
			$campaign_overview_url = 'https://' . $mailchimp_settings['server'] . '.admin.mailchimp.com/campaigns/';
			$campaign_edit_url     = 'https://' . $mailchimp_settings['server'] . '.admin.mailchimp.com/campaigns/edit?id=' . $send_campaign->web_id;

			$campaign_send_button   = '<button type="button" class="button button-primary final_send_campaign" data-campaign="' . $send_campaign->id . '">Send Campaign</button>';
			$campaign_delete_button = '<button type="button" class="button button-delete final_delete_campaign" data-campaign="' . $send_campaign->id . '">Delete Campaign</button>';

			$response_message = '<p>Test email has been successfully sent. Please check your email client.</p>';
			$response_message .= '<p>You can review the campaign setting by following this link <a href="' . $campaign_edit_url . '">' . $send_campaign->settings->title . '</a>.</p>';
			$response_message .= '<p>If everything is ok you can sent it to real subscribers or delete campaign.</p>';
			$response_message .= '<p>' . $campaign_send_button . ' ' . $campaign_delete_button . '</p>';
			//$response_message .= '<p>' . print_r($send_campaign, true) . '</p>';
		} else {
			$response_message = '<p>Campaign has been successfully sent.</p>';
		}

		wp_send_json_success( $response_message );
	}

	public function newsletter_finally_send_campaign() {
		$campaign_id = filter_input( INPUT_POST, 'campaign_id', FILTER_SANITIZE_STRING );

		$send_campaign = Mailchimp::getInstance()->send_campaign( $campaign_id );

		if ( is_wp_error( $send_campaign ) ) {
			wp_send_json_error( $send_campaign->get_error_message() );
		}

		wp_send_json_success( 'Campaign has been successfully sent.' );
	}

	public function newsletter_finally_delete_campaign() {
		$campaign_id = filter_input( INPUT_POST, 'campaign_id', FILTER_SANITIZE_STRING );

		$send_campaign = Mailchimp::getInstance()->delete_campaign( $campaign_id );

		if ( is_wp_error( $send_campaign ) ) {
			wp_send_json_error( $send_campaign->get_error_message() );
		}

		wp_send_json_success( 'Campaign has been successfully deleted.' );
	}
}