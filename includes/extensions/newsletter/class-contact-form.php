<?php
/**
 * Contact_Form
 *
 * @version 1.0.0
 */

namespace App\Extensions\Newsletter;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Contact_Form {

	/**
	 * Contact_Form constructor.
	 */
	public function __construct() {

		/**
		 * Contact From 7 Integration
		 */
		add_filter( 'wpcf7_load_css', '__return_false' );
		add_action( 'wpcf7_mail_sent', [ $this, 'contact_form_mail_sent' ], 1 );
		//add_action( 'wpcf7_init', [ $this, 'register_form_tag_wc_product' ] );
		//add_action( 'wpcf7_before_send_mail', [ $this, 'contact_form_before_send_mail' ], 10, 3 );

		add_filter( 'wpcf7_form_tag', [ $this, 'form_tag_pipes' ], 10, 2 );
		add_filter( 'wpcf7_form_tag_data_option', [ $this, 'form_tag_data_option' ], 10, 3 );
	}

	public function form_tag_pipes( $scanned_tag, $replace ) {
		if ( in_array( 'use_keys_values', $scanned_tag['options'] ) ) {
			$scanned_tag['values'] = $scanned_tag['pipes']->collect_befores();
			$scanned_tag['labels'] = $scanned_tag['pipes']->collect_afters();
		}

		return $scanned_tag;
	}

	/**
	 * @param $contact_form
	 *
	 * @return bool
	 */
	public function contact_form_mail_sent( $contact_form ) {
		$settings = Mailchimp::getInstance()->get_mailchimp_settings();
		$prefix   = 'MC-';
		$length   = strlen( $prefix );
		$inputs   = array_change_key_case( $_REQUEST, CASE_UPPER );
		$fields   = [];

		foreach ( $inputs as $key => $value ) {
			if ( strpos( $key, $prefix ) === 0 ) {
				$new_key = substr( $key, $length );
				if ( is_array( $value ) ) {
					$fields[ $new_key ] = array_map( 'sanitize_text_field', $value );
				} else {
					$fields[ $new_key ] = sanitize_text_field( $value );
				}
			}
		}

		if ( empty( (int) $fields['SUBSCRIBE'] ) && (int) $fields['SUBSCRIBE'] !== 1 ) {
			return false;
		}

		if ( empty( $fields['EMAIL'] ) && ! is_string( $fields['EMAIL'] ) && ! is_email( $fields['EMAIL'] ) ) {
			return false;
		}

		if ( ! empty( $fields['FNAME'] ) && is_string( $fields['FNAME'] ) ) {
			$fields['FNAME'] = ucfirst( $fields['FNAME'] );
		}

		if ( ! empty( $fields['INTERESTS'] ) ) {
			$mc_groups = $settings['groups'];
			$interests = explode( ',', $fields['INTERESTS'] );

			if ( ! empty( $interests ) ) {
				$fields['INTERESTS'] = [];

				foreach ( $mc_groups as $group ) {
					foreach ( $group['interests'] as $interest ) {
						$search_key = array_search( $interest['name'], $interests );

						if ( false !== $search_key ) {
							$interests[ $search_key ] = $interest['id'];
						}
					}
				}


				foreach ( $interests as $interest ) {
					$fields['INTERESTS'][ $interest ] = true;
				}
			}
		}

		unset( $fields['SUBSCRIBE'] );

		$response = Mailchimp::getInstance()->subscribe_contact( $fields );

		return true;
	}

	/**
	 * Add form tag for form
	 */
	public function register_form_tag_wc_product() {
		//wpcf7_add_form_tag( 'product_id', [ $this, 'wc_product_id_tag_handler' ] );
		//wpcf7_add_form_tag( 'exhibition_term', [ $this, 'wc_exhibition_term_tag_handler' ] );
		//wpcf7_add_form_tag( 'exhibition_interest', [ $this, 'wc_exhibition_interest_tag_handler' ] );
	}

	/**
	 * @param $tag
	 *
	 * @return int
	 */
	public function wc_product_id_tag_handler( $tag ) {
		global $product;
		if ( is_singular( 'product' ) ) {
			return ! empty( $product ) ? $product->get_id() : '';
		} else {
			return '';
		}
	}

	/**
	 * @param $tag
	 *
	 * @return int
	 */
	public function wc_exhibition_term_tag_handler( $tag ) {
		global $product;

		if ( is_singular( 'product' ) ) {
			$terms = get_the_terms( $product->get_id(), 'exhibition' );

			if ( is_wp_error( $terms ) || empty( $terms ) ) {
				return '';
			}

			$term = array_shift( $terms );

			return ! empty( $term ) ? $term->term_id : '';
		} else if ( is_tax( 'exhibition' ) ) {
			$term = get_queried_object();

			return ! empty( $term ) ? $term->term_id : '';
		} else {
			return '';
		}
	}

	/**
	 * @param $tag
	 *
	 * @return int
	 */
	public function wc_exhibition_interest_tag_handler( $tag ) {
		global $product;

		if ( is_singular( 'product' ) ) {
			$terms = get_the_terms( $product->get_id(), 'exhibition' );

			if ( is_wp_error( $terms ) || empty( $terms[0] ) ) {
				return '';
			}

			$term     = array_shift( $terms );
			$interest = get_field( 'exhibition_interest', $term );

			return ! empty( $interest ) ? $interest : '';
		} else if ( is_tax( 'exhibition' ) ) {
			$term = get_queried_object();

			if ( empty( $term ) ) {
				return '';
			}

			$interest = get_field( 'exhibition_interest', $term );

			return ! empty( $interest ) ? $interest : '';
		} else {
			return '';
		}
	}

	/**
	 * Replace content of the form before send
	 *
	 * @param $contact_form
	 * @param $abort
	 * @param $submission
	 */
	public function contact_form_before_send_mail( $contact_form, $abort, $submission ) {
		$replaced_content = false;
		$form_properties  = $contact_form->get_properties();

		if ( ! empty( $product_id = $submission->get_posted_data( 'product_id' ) ) ) {
			$product_title = get_field( 'product_title', $product_id );
			$product_title = empty( $product_title ) ? get_the_title( $product_id ) : $product_title;
			$product_link  = get_the_permalink( $product_id );

			$form_properties['mail']['subject'] = str_replace( '[product_title]', $product_title, $form_properties['mail']['subject'] );
			$form_properties['mail']['body']    = str_replace( '[product_title]', $product_title, $form_properties['mail']['body'] );
			$form_properties['mail']['body']    = str_replace( '[product_link]', $product_link, $form_properties['mail']['body'] );

			$replaced_content = true;
		}

		if ( ! empty( $exhibition_term = $submission->get_posted_data( 'exhibition_term' ) ) ) {
			$exhibition      = get_term( $exhibition_term, 'exhibition' );
			$exhibition_link = get_term_link( $exhibition, 'exhibition' );

			$form_properties['mail']['subject'] = str_replace( '[exhibition_title]', $exhibition->name, $form_properties['mail']['subject'] );
			$form_properties['mail']['body']    = str_replace( '[exhibition_title]', $exhibition->name, $form_properties['mail']['body'] );
			$form_properties['mail']['body']    = str_replace( '[exhibition_link]', $exhibition_link, $form_properties['mail']['body'] );

			$replaced_content = true;
		}

		if ( $replaced_content ) {
			$contact_form->set_properties( [
				'mail' => $form_properties['mail']
			] );
		}
	}

	public function form_tag_data_option( $data, $options, $args ) {
		$args = wp_parse_args( $args, array() );

		$contact_form   = wpcf7_get_current_contact_form();
		$args['locale'] = $contact_form->locale();

		foreach ( (array) $options as $option ) {
			$option        = explode( '.', $option );
			$type          = $option[0];
			$args['group'] = isset( $option[1] ) ? $option[1] : null;

			if ( $type == 'countries' ) {
				$countries = include get_theme_file_path( 'i18n/countries.php' );

				if ( ! empty( $data ) ) {
					$data = array_merge( $data, $countries );
				} else {
					$data = $countries;
				}
			}
		}

		return $data;
	}

}