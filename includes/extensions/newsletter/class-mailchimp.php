<?php
/**
 * Mailchimp
 *
 * @version 1.0.0
 */

namespace App\Extensions\Newsletter;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Scripts;
use App\Extensions\Newsletter\Ajax;
use App\Extensions\Newsletter\Contact_Form;

class Mailchimp {

	const MAILCHIMP_KEY = 'app_mailchimp';

	private $api_key = '';
	private $server = '';
	private $mailchimp = '';

	protected function __construct() {
		$this->mailchimp = new \MailchimpMarketing\ApiClient();

		/**
		 * Register ACF option for Newsletter and Mailchimp
		 */
		add_action( 'init', [ $this, 'init_options' ], 100 );
		add_action( 'acf/input/admin_head', [ $this, 'add_acf_metabox' ], 10 );
		add_action( 'acf/save_post', [ $this, 'acf_save_option_newsletter_settings' ], 20 );
		add_filter( 'acf/prepare_field/type=message', [ $this, 'acf_prepare_newsletter_action_field' ] );

		new Ajax();
		new Contact_Form();

		if ( function_exists( 'WC' ) ) {
			Woocommerce::getInstance();
		}
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}

	public function init_options() {
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page( [
				'page_title' => 'Newsletter Settings',
				'menu_title' => 'Mailchimp',
				'menu_slug'  => 'theme-newsletter-settings',
				'capability' => 'edit_posts',
				'redirect'   => false,
				'icon_url'   => 'dashicons-email-alt'
			] );
		}
	}

	public function add_acf_metabox() {

		if ( ! acf_is_screen( 'toplevel_page_theme-newsletter-settings' ) ) {
			return;
		}

		wp_enqueue_script( 'mailchimp-settings', Scripts::get_admin_url( 'newsletter/mailchimp.js' ), '' );
		wp_enqueue_style( 'mailchimp-settings', Scripts::get_admin_url( 'newsletter/mailchimp.css' ), '' );

		add_meta_box(
			'mailchimp_settings',
			__( 'Mailchimp Settings', 'textdomain' ),
			[ $this, 'mailchimp_settings_metabox' ],
			'acf_options_page',
			'normal'
		);
	}

	/**
	 * Display custom metabox on an ACF options page
	 */
	public function mailchimp_settings_metabox() {
		//https://mailchimp.com/developer/marketing/api/campaign-content/set-campaign-content/
		//https://mailchimp.com/help/create-editable-content-areas-with-mailchimps-template-language/#Repeating_content_area
		//https://stackoverflow.com/questions/29366766/mailchimp-api-not-replacing-mcedit-content-sections-using-ruby-library

		$mailchimp_settings = $this->get_mailchimp_settings();
		$connection         = $this->get_connection();

		get_template_part( 'template-parts/admin/newsletter/settings', null, [
			'connection'         => $connection,
			'mailchimp_settings' => $mailchimp_settings
		] );
	}

	public function get_default_settings() {
		return [
			'api_key'            => '',
			'server'             => '',
			'lists'              => [],
			'templates'          => [],
			'merge_fields'       => [],
			'segments'           => [],
			'groups'             => [],
			'list_id'            => '',
			'wc_list_id'         => '',
			'wc_subscribe_event' => '',
			'wc_merge_fields'    => '',
			'template_id'        => '',
			'double_optin'       => '1',
		];
	}

	public function get_mailchimp_settings() {

		$default_settings = $this->get_default_settings();
		$settings         = get_option( self::MAILCHIMP_KEY, [] );

		$this->api_key = $settings['api_key'] ?? '';
		$this->server  = $settings['server'] ?? '';

		return wp_parse_args( $settings, $default_settings );
	}

	public function update_mailchimp_settings( $settings ) {
		$saved = $this->get_mailchimp_settings();

		$settings = wp_parse_args( $settings, $saved );

		update_option( self::MAILCHIMP_KEY, $settings, 'no' );
	}

	public static function parse_mailchimp_key( $api_key ) {
		list( , $server ) = explode( '-', $api_key );

		return [
			'api_key' => $api_key,
			'server'  => $server
		];
	}

	public static function mask_key( $string ) {
		$length            = strlen( $string );
		$obfuscated_length = ceil( $length / 2 );
		$string            = str_repeat( '*', $obfuscated_length ) . substr( $string, $obfuscated_length );

		return $string;
	}

	/**
	 * @return mixed|string
	 */
	public function set_credential( $api_key, $server ) {
		$this->api_key = $api_key;
		$this->server  = $server;

		return $this->get_connection();
	}

	public function get_connection() {
		/*$mailchimp_settings = $this->get_mailchimp_settings();

		$this->api_key = $mailchimp_settings['api_key'] ?? '';
		$this->server  = $mailchimp_settings['server'] ?? '';*/

		if ( empty( $this->api_key ) && empty( $this->server ) ) {
			return new \WP_Error( 'api_key_required', 'Mailchimp API Key is required' );
		}

		try {
			$this->mailchimp->setConfig( [
				'apiKey' => $this->api_key,
				'server' => $this->server
			] );

			return $this->mailchimp->ping->get();
		} catch ( \GuzzleHttp\Exception\ClientException $e ) {
			return new \WP_Error( 'mailchimp_connection', $e->getMessage() );
		}
	}

	public function mailchimp_get_lists() {
		try {
			$lists         = [];
			$fetched_lists = $this->mailchimp->lists->getAllLists();

			foreach ( $fetched_lists->lists as $list ) {
				$lists[] = [
					'id'   => $list->id,
					'name' => $list->name,
				];
			}

			return $lists;

		} catch ( \GuzzleHttp\Exception\ClientException $e ) {
			return new \WP_Error( 'mailchimp_lists', $e->getMessage() );
		}
	}

	public function mailchimp_get_templates() {
		try {
			$templates         = [];
			$fetched_templates = $this->mailchimp->templates->list( null, null, 0, 0, null, null, null, 'user' );

			foreach ( $fetched_templates->templates as $template ) {
				$templates[] = [
					'id'   => $template->id,
					'name' => $template->name,
				];
			}

			return $templates;
		} catch ( \GuzzleHttp\Exception\ClientException $e ) {
			return new \WP_Error( 'mailchimp_lists', $e->getMessage() );
		}
	}

	public function mailchimp_get_groups( $list_id ) {
		try {
			$categories         = [];
			$fetched_categories = $this->mailchimp->lists->getListInterestCategories( $list_id );

			foreach ( $fetched_categories->categories as $category ) {
				$interests          = [];
				$category_interests = $this->mailchimp->lists->listInterestCategoryInterests(
					$list_id,
					$category->id
				);

				foreach ( $category_interests->interests as $interest ) {
					$interests[] = [
						'id'   => $interest->id,
						'name' => $interest->name
					];
				}

				$categories[] = [
					'id'        => $category->id,
					'title'     => $category->title,
					'interests' => $interests,
				];
			}

			return $categories;
		} catch ( \GuzzleHttp\Exception\ClientException $e ) {
			return new \WP_Error( 'mailchimp_lists', $e->getMessage() );
		}
	}

	public function mailchimp_get_segments( $list_id ) {
		try {
			$segments         = [];
			$fetched_segments = $this->mailchimp->lists->listSegments( $list_id );

			foreach ( $fetched_segments->segments as $segment ) {
				$segments[] = [
					'id'   => $segment->id,
					'name' => $segment->name,
				];
			}

			return $segments;
		} catch ( \GuzzleHttp\Exception\ClientException $e ) {
			return new \WP_Error( 'mailchimp_lists', $e->getMessage() );
		}
	}

	public function mailchimp_get_merge_fields( $list_id ) {
		try {
			$fields         = [];
			$fetched_fields = $this->mailchimp->lists->getListMergeFields( $list_id );

			foreach ( $fetched_fields->merge_fields as $field ) {
				$fields[] = [
					'name' => $field->name,
					'tag'  => $field->tag,
					'type' => $field->type,
				];
			}

			return $fields;
		} catch ( \GuzzleHttp\Exception\ClientException $e ) {
			return new \WP_Error( 'mailchimp_lists', $e->getMessage() );
		}
	}

	public function acf_prepare_newsletter_action_field( $field ) {

		if ( $field['message'] === '[newsletter_action]' ) {
			ob_start();
			get_template_part( 'template-parts/admin/mailchimp', 'newsletter-action' );
			$field['message'] = ob_get_clean();
		}

		return $field;
	}

	public function acf_save_option_newsletter_settings( $post_id ) {
		$screen = get_current_screen();

		if ( ! empty( $screen ) && property_exists( $screen, 'id' ) && strpos( $screen->id, 'toplevel_page_theme-newsletter-settings' ) === false ) {
			return;
		}
	}

	public function send_newsletter_campaign( $data, $send_as_test = false ) {

		$connection = $this->get_connection();
		$settings   = $this->get_mailchimp_settings();

		if ( is_wp_error( $connection ) ) {
			return $connection->get_error_message();
		}

		try {
			$campaign = $this->mailchimp->campaigns->create( [
				'type'       => 'regular',
				'recipients' => [
					'list_id' => $settings['list_id']
				],
				'settings'   => [
					'subject_line'     => $data['email_subject'],
					'preview_text'     => $data['email_content_title'],
					'title'            => $data['email_content_title'],
					'template_id'      => (int) $settings['template_id'],
					'reply_to'         => $data['email_from_email'],
					'from_name'        => $data['email_from_name'],
					'use_conversation' => true,
					'auto_footer'      => false,
					'inline_css'       => false,
					'auto_tweet'       => false,
					'drag_and_drop'    => false,
				]
			] );


			$template_products_html = '<table border="0" cellpadding="0" cellspacing="0" width="100%">';

			if ( ! empty( $data['email_products'] ) ) {
				$template_products_html .= '<tr><td>';

				foreach ( $data['email_products'] as $key => $post_id ) {
					$num = ( $key % 2 );

					$product               = wc_get_product( $post_id );
					$template_product_html = '<table align="' . ( $num == 0 ? 'left' : 'right' ) . '" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td>
								<a href="' . get_the_permalink( $product->get_id() ) . '" title="' . get_the_title( $product->get_id() ) . '" target="_blank" >
									' . $product->get_image() . '
								</a>
							</td>
						</tr>
					</table>';

					$template_products_html .= $template_product_html;

					if ( $num == 1 && ( count( $data['email_products'] ) !== ( $key + 1 ) ) ) {
						$template_products_html .= '</td></tr><tr><td>';
					}
				}

				$template_products_html .= '</td></tr>';
			}

			$template_products_html .= '</table>';

			$email_style = '<style type="text/css">';

			$header_logo_url = 'https://mcusercontent.com/c83a3930c46eaee3130efbdf5/images/b9a52a83-7bf3-40a5-9992-55eaa9f1afdb.png';
			$footer_logo_url = 'https://mcusercontent.com/c83a3930c46eaee3130efbdf5/images/4628d755-af70-8e59-1fc0-0770bfa37a6f.png';

			if ( ! empty( $data['email_exhibition'] ) ) {
				$primary_color   = get_field( 'primary_color', $data['email_exhibition'] );
				$secondary_color = get_field( 'secondary_color', $data['email_exhibition'] );
				$tertiary_color  = get_field( 'tertiary_color', $data['email_exhibition'] );
				$text_color      = get_field( 'text_color', $data['email_exhibition'] );

				$email_style .= '
					/* 
					    #dddddd - primary
					    #eeeeee - secondary
						#999999 - tertiary
						#000000 - text
					*/
					body, #bodyTable{
						background-color: ' . $primary_color . ';
					}
					#templateBodyFooter {
						background-color: ' . $secondary_color . ';
					}
					#headerMenu a,
					#templateSectionHead h3,
					#templateSectionHead p {
						color: ' . $text_color . ';
					}
					#footerContent, #footerContent p, #utilityBar, #utilityBar p,
					#footerContent a, #footerContent p a, #utilityBar a, #utilityBar p a {
						color: ' . $text_color . ';
					}
					#templateProductSectionMore a {
						background-color: ' . $tertiary_color . ';
						color: ' . $text_color . ';
					}
					#templateProductSectionTable a {
						background: ' . $tertiary_color . ';
						display: block;
					}';

				if ( $text_color == '#ffffff' ) {
					$header_logo_url = 'https://mcusercontent.com/c83a3930c46eaee3130efbdf5/images/44ca0e08-2c16-5a26-b2ea-efa8d4d9ee47.png';
				}

				if ( $text_color != '#ffffff' ) {
					$footer_logo_url = 'https://mcusercontent.com/c83a3930c46eaee3130efbdf5/images/8627de5c-d632-0de2-8ca2-823a6eb483f1.png';
				}
			}

			$email_style .= '</style>';

			$this->mailchimp->campaigns->setContent( $campaign->id, [
				'template' => [
					'id'       => (int) $settings['template_id'],
					'sections' => [
						'email_style'              => $email_style,
						'header_logo'              => '<img src="' . $header_logo_url . '" alt="" style="border: 0px; height: 17px; width: 252px; margin: 0; padding: 0;" width="252" height="17">',
						'footer_logo'              => '<img src="' . $footer_logo_url . '" style="border: 0px; width: 133px; height: 133px; margin: 0px;" width="133">',
						'body_featured_image'      => wp_get_attachment_image( $data['email_content_image']['id'], 'large' ),
						'products_section_title'   => sprintf( '<h3>%s</h3>', $data['email_content_title'] ),
						'products_section_text'    => sprintf( '<p>%s</p>', $data['email_content_text'] ),
						'products_section_content' => $template_products_html,
						'products_section_more'    => sprintf( '<a href="%s" target="%s">%s</a>', $data['email_content_more_button']['url'], $data['email_content_more_button']['target'], $data['email_content_more_button']['title'] ),
					],
				]
			] );

			if ( $send_as_test ) {
				$this->mailchimp->campaigns->sendTestEmail( $campaign->id, [
					"test_emails" => [ $data['test_email'] ],
					"send_type"   => "html",
				] );
			} else {
				$this->mailchimp->campaigns->send( $campaign->id );
			}

			return $campaign;

		} catch ( \GuzzleHttp\Exception\BadResponseException $e ) {
			return new \WP_Error( 'mailchimp_connection', $e->getResponse() );
		}
	}

	public function send_campaign( $campaign_id ) {
		$connection = $this->get_connection();

		if ( is_wp_error( $connection ) ) {
			return $connection->get_error_message();
		}

		try {
			$this->mailchimp->campaigns->send( $campaign_id );

			return true;
		} catch ( \GuzzleHttp\Exception\BadResponseException $e ) {
			return new \WP_Error( 'mailchimp_connection', $e->getMessage() );
		}
	}

	public function delete_campaign( $campaign_id ) {
		$connection = $this->get_connection();

		if ( is_wp_error( $connection ) ) {
			return $connection->get_error_message();
		}

		try {
			$this->mailchimp->campaigns->remove( $campaign_id );

			return true;
		} catch ( \GuzzleHttp\Exception\BadResponseException $e ) {
			return new \WP_Error( 'mailchimp_connection', $e->getMessage() );
		}
	}

	public function subscribe_contact( $contact_fields, $list_id = false ) {
		$connection = $this->get_connection();
		$settings   = $this->get_mailchimp_settings();

		if ( false === $list_id ) {
			$list_id = $settings['list_id'];
		}

		if ( is_wp_error( $connection ) ) {
			return $connection->get_error_message();
		}

		if ( empty( $contact_fields['EMAIL'] ) ) {
			return new \WP_Error( 'subscribe_contact', 'Email is required field' );
		}

		$contact_email = $contact_fields['EMAIL'];
		unset( $contact_fields['EMAIL'] );

		if ( ! empty( $contact_fields['SEGMENT'] ) ) {
			$segment = $contact_fields['SEGMENT'];
			unset( $contact_fields['SEGMENT'] );
		} else {
			$segment = false;
		}

		if ( ! empty( $contact_fields['INTERESTS'] ) ) {
			$interests = array_map( function ( $v ) {
				return ! empty( $v );
			}, $contact_fields['INTERESTS'] );

			unset( $contact_fields['INTERESTS'] );
		} else {
			$interests = false;
		}

		$subscriber_hash = strtolower( md5( $contact_email ) );

		try {
			$contact_data = [
				'email_address' => $contact_email,
				'status_if_new' => $settings['double_optin'] ? 'pending' : 'subscribed',
				'status'        => $settings['double_optin'] ? 'pending' : 'subscribed',
				'email_type'    => 'html',
				'ip_signup'     => $this->get_request_ip_address()
			];

			$merge_fields = array_filter( $contact_fields );

			if ( ! empty( $merge_fields ) ) {
				$contact_data['merge_fields'] = $merge_fields;
			}

			if ( $interests ) {
				$contact_data['interests'] = $interests;
			}

			$subscriber = $this->mailchimp->lists->setListMember( $list_id, $subscriber_hash, $contact_data );

			if ( $segment ) {
				$this->mailchimp->lists->createSegmentMember( $list_id, $segment, [
					"email_address" => $contact_email,
				] );
			}

			return $subscriber;
		} catch ( \GuzzleHttp\Exception\BadResponseException $e ) {
			return new \WP_Error( 'subscribe_contact', $e->getMessage() );
		}
	}

	protected function get_request_ip_address() {
		if ( isset( $_SERVER['X-Forwarded-For'] ) ) {
			return $_SERVER['X-Forwarded-For'];
		}

		if ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}

		if ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
			return $_SERVER['REMOTE_ADDR'];
		}

		return null;
	}

}