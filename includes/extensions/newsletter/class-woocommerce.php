<?php
/**
 * Contact_Form
 *
 * @version 1.0.0
 */

namespace App\Extensions\Newsletter;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Woocommerce {

	/**
	 * @var array
	 */
	protected array $mc_settings = [];

	/**
	 * @var array
	 */
	protected array $wc_order_statuses = [];

	const customer_opt_in_key = 'wc_mailchip_customer_opt_in';

	const customer_subscribed_key = 'wc_mailchip_customer_subscribed';

	/**
	 * Contact_Form constructor.
	 */
	protected function __construct() {
		$this->wc_order_statuses = [ 'pending', 'processing', 'completed' ];

		add_action( 'woocommerce_checkout_update_order_meta', [ $this, 'checkout_update_order_meta' ], 10, 2 );
		add_action( 'woocommerce_order_status_changed', [ $this, 'order_status_changed' ], 10, 4 );
		add_action( 'wc_order_mailchimp_maybe_subscribe', [ $this, 'order_mailchimp_maybe_subscribe' ] );

		add_action( 'woocommerce_after_checkout_account_form', [ $this, 'output_checkout_field' ] );
	}

	public function set_settings() {
		$this->mc_settings = Mailchimp::getInstance()->get_mailchimp_settings();
	}

	/**
	 * Check if MC is ready for WooCommerce
	 *
	 * @return bool
	 */
	public function is_active() {
		$this->set_settings();

		if ( ! empty( $this->mc_settings['api_key'] ) && ! empty( $this->mc_settings['wc_list_id'] ) ) {
			return true;
		}

		return false;
	}

	public function get_subscribe_event() {
		if ( ! empty( $this->mc_settings['wc_subscribe_event'] ) ) {
			return $this->mc_settings['wc_subscribe_event'];
		}

		return 'pending';
	}

	/**
	 * Schedule subscribe event for mailchimp subscription
	 *
	 * @param $order_id
	 * @param $data
	 *
	 * @return void
	 */
	public function checkout_update_order_meta( $order_id, $data ) {
		if ( $this->is_active() ) {
			$order = wc_get_order( $order_id );
			$value = isset( $_POST[ self::customer_opt_in_key ] ) ? 'yes' : 'no';

			$order->update_meta_data( '_' . self::customer_opt_in_key, $value );
			$order->save();
		}
	}

	/**
	 * Schedule subscribe event for mailchimp subscription
	 *
	 * @param $order_id
	 * @param $status_from
	 * @param $status_to
	 * @param $order
	 *
	 * @return void
	 */
	public function order_status_changed( $order_id, $status_from, $status_to, $order ) {
		if ( $order_id && ! is_a( $order, 'WC_Order' ) ) {
			$order = wc_get_order( $order_id );
		}

		if ( $this->is_active() && $order->get_meta( '_' . self::customer_opt_in_key ) == 'yes' ) {
			$need_to_subscribe   = false;
			$customer_subscribed = $order->get_meta( '_' . self::customer_subscribed_key );

			if ( $this->get_subscribe_event() == $status_to ) {
				$need_to_subscribe = true;
			} else if ( $this->get_subscribe_event() == 'processing' && $status_to == 'completed' && ! $customer_subscribed ) {
				$need_to_subscribe = true;
			}

			if ( $need_to_subscribe ) {
				wp_schedule_single_event( strtotime( 'now' ), 'wc_order_mailchimp_maybe_subscribe', [ $order_id ] );
			}
		}
	}

	/**
	 * Subscribe customer on scheduled subscribe event
	 *
	 * @param $order_id
	 *
	 * @return void
	 */
	public function order_mailchimp_maybe_subscribe( $order_id ) {
		$order = wc_get_order( $order_id );

		if ( $this->is_active() && ! $order->get_meta( '_' . self::customer_subscribed_key ) ) {

			$fields = [
				'EMAIL' => $order->get_billing_email(),
				'FNAME' => $order->get_billing_first_name(),
				'LNAME' => $order->get_billing_last_name(),
			];

			$response = Mailchimp::getInstance()->subscribe_contact( $fields, $this->mc_settings['wc_list_id'] );

			if ( is_wp_error( $response ) ) {
				$order->add_order_note( __( 'Error during Customer subscribing to Mailchimp list #' . $this->mc_settings['wc_list_id'] . '.', 'woocommerce' ) . ': ' . $response->get_error_message() );
			} else {
				$order->update_meta_data( '_' . self::customer_subscribed_key, 'yes' );
				$order->add_order_note( __( 'Customer was successfully subscribed to Mailchimp list #' . $this->mc_settings['wc_list_id'] . '.', 'woocommerce' ) );
			}

			$order->delete_meta_data( '_' . self::customer_opt_in_key );
			$order->save();
		}
	}

	public function output_checkout_field() {
		if ( $this->is_active() ) {
			$args = [
				'type'    => 'checkbox',
				'label'   => __( 'Subscribe to newsletter', 'ruby_studio' ),
				'id'      => self::customer_opt_in_key,
				'default' => '',
			];

			woocommerce_form_field( self::customer_opt_in_key, $args, '' );
		}
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}

}