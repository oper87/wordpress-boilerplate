<?php


namespace App\Extensions\Progressive_Images;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Image {

	/**
	 * @var array
	 */
	public $upload_dir;

	public $placeholder_image = '';

	public $placeholder_dir;

	public $placeholder_url;

	protected function __construct() {
		$this->upload_dir        = wp_upload_dir();
		$this->placeholder_dir   = $this->upload_dir['basedir'] . '/placeholders/';
		$this->placeholder_url   = $this->upload_dir['baseurl'] . '/placeholders/';
		$this->placeholder_image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';

		add_filter( 'wp_get_attachment_image', [ $this, 'wp_get_attachment_image' ], 0, 5 );
		add_action( 'delete_attachment', [ $this, 'wp_delete_attachment' ], 10, 2 );
	}

	public function lazy_load_images( $html, $clean_buffer ) {
		if ( ! preg_match_all( '#<img(?<atts>\s.+)\s?/?>#iUs', $clean_buffer, $images, PREG_SET_ORDER ) ) {
			return $html;
		}

		$images = array_unique( $images, SORT_REGULAR );

		foreach ( $images as $image ) {
			$image = $this->can_lazy_load( $image );

			if ( ! $image ) {
				continue;
			}

			$replaced_image = $this->replace_image( $image );
			$replaced_image .= $this->noscript( $image[0] );
			$html           = str_replace( $image[0], $replaced_image, $html );

			unset( $replaced_image );
		}

		return $html;
	}

	public function wp_get_attachment_image( $html, $attachment_id, $size, $icon, $attr ) {

		if ( false === Lazy_Load::lazy_load_allowed() ) {
			return $html;
		}

		if ( ! $this->can_lazy_load_attachment( $attr ) ) {
			return $html;
		}

		$image = wp_get_attachment_image_src( $attachment_id, $size, $icon );
		list( $src, $width, $height ) = $image;

		$hwstring = image_hwstring( $width, $height );

		$image_lazy_load = rtrim( "<img $hwstring" );

		$attr['src']      = $this->get_placeholder_image_src( $attr, $width, $height );
		$attr['data-src'] = $src;
		$class            = $attr['class'] ?? '';

		$image_lazy_load .= " class=" . '"placeholder"';

		foreach ( $attr as $name => $value ) {
			$name = str_replace( 'srcset', 'data-srcset', $name );
			$name = str_replace( 'sizes', 'data-sizes', $name );
			$name = str_replace( 'class', 'data-class', $name );

			$image_lazy_load .= " $name=" . '"' . $value . '"';
		}

		$image_lazy_load .= ' />';

		$media = $this->wrap_image( $image_lazy_load, $width, $height, $class );
		$media .= $this->noscript( $html );

		return $media;
	}

	public function wp_delete_attachment( $post_id, $post ) {
		$meta = wp_get_attachment_metadata( $post_id );

		if ( empty( $meta['sizes'] ) ) {
			return;
		}

		$image_path = pathinfo( $meta['file'] );

		foreach ( $meta['sizes'] as $size => $image ) {
			$thumb_path           = pathinfo( $image['file'] );
			$placeholder_basename = "{$image_path['dirname']}/{$thumb_path['filename']}-ph.{$thumb_path['extension']}";

			if ( file_exists( $this->placeholder_dir . $placeholder_basename ) ) {
				wp_delete_file( $this->placeholder_dir . $placeholder_basename );
			}
		}
	}

	private function replace_image( $image ) {
		$width  = 0;
		$height = 0;
		$class  = '';

		if ( preg_match( '@[\s"\']width\s*=\s*(\'|")(?<width>.*)\1@iUs', $image['atts'], $atts ) ) {
			$width = absint( $atts['width'] );
		}

		if ( preg_match( '@[\s"\']height\s*=\s*(\'|")(?<height>.*)\1@iUs', $image['atts'], $atts ) ) {
			$height = absint( $atts['height'] );
		}

		if ( preg_match( '/[\s"\']class\s*=\s*(\'|")(?<class>.*)\1/iUs', $image['atts'], $atts ) ) {
			$class = trim( $atts['class'] );
		}

		$placeholder_atts = preg_replace( '@\ssrc\s*=\s*(\'|")(?<src>.*)\1@iUs', ' src="' . $this->get_placeholder_image_src( $image, $width, $height ) . '"', $image['atts'] );

		$image_lazy_load = str_replace( $image['atts'], $placeholder_atts . ' data-src="' . $image['src'] . '"', $image[0] );

		/*if ( ! preg_match( '@\sloading\s*=\s*(\'|")(?:lazy|auto)\1@i', $image_lazy_load ) ) {
			$image_lazy_load = str_replace( '<img', '<img loading="lazy"', $image_lazy_load );
		}*/

		$image_lazy_load = $this->lazy_load_responsive_attributes( $image_lazy_load );
		$image_lazy_load = str_replace( ' class="' . $class . '"', '', $image_lazy_load );

		return $this->wrap_image( $image_lazy_load, $width, $height, $class );
	}

	private function wrap_image( $image, $width, $height, $class = '' ) {

		$image_height = ( $height / $width ) * 100;
		$image_height = round( $image_height, 2 );
		$class        = $class != '' ? ' ' . $class : '';

		$image_html = sprintf( '<span class="aspect-ratio-fill" style="padding-bottom: %s%%;"></span>', $image_height );

		return sprintf( '<span class="progressive-media img%s">%s %s</span>', $class, $image, $image_html );
	}

	private function lazy_load_responsive_attributes( $html ) {
		$html = preg_replace( '/[\s|"|\'](srcset)\s*=\s*("|\')([^"|\']+)\2/i', ' data-$1=$2$3$2', $html );
		$html = preg_replace( '/[\s|"|\'](sizes)\s*=\s*("|\')([^"|\']+)\2/i', ' data-$1=$2$3$2', $html );

		return $html;
	}

	private function get_placeholder_image_src( $image, $width = 0, $height = 0 ) {
		$width  = 0 === $width ? 0 : absint( $width );
		$height = 0 === $height ? 0 : absint( $height );

		[ , $image_basename ] = explode( 'uploads/', $image['src'] );
		$image_pathinfo = pathinfo( $image_basename );

		if ( $this->placeholder_image ) {
			return $this->placeholder_image;
		}

		$image_path           = $this->upload_dir['basedir'] . '/' . $image_basename;
		$placeholder_basename = "{$image_pathinfo['dirname']}/{$image_pathinfo['filename']}-ph.{$image_pathinfo['extension']}";
		$placeholder_path     = $this->placeholder_dir . $placeholder_basename;
		$placeholder_url      = $this->placeholder_url . $placeholder_basename;

		if ( file_exists( $placeholder_path ) ) {
			return $placeholder_url;
		}

		if ( ! is_dir( $this->placeholder_dir ) ) {
			wp_mkdir_p( $this->placeholder_dir );
		}

		if ( ! is_dir( $this->placeholder_dir . $image_pathinfo['dirname'] ) ) {
			wp_mkdir_p( $this->placeholder_dir . $image_pathinfo['dirname'] );
		}

		$gen_low_image = wp_get_image_editor( $image_path );

		if ( ! is_wp_error( $gen_low_image ) ) {
			$mime_type = mime_content_type( $image_path );
			$gen_low_image->resize( 60, null, false );
			$gen_low_image->set_quality( 20 );

			$tmp_low_image_name = $gen_low_image->generate_filename( 'ph' );
			$tmp_image          = $gen_low_image->save( $tmp_low_image_name );
			$move_image         = rename( $tmp_image['path'], $placeholder_path );

			if ( $move_image ) {
				return $placeholder_url;
			}
		}

		return $image['src'];
	}

	private function can_lazy_load( $image ) {
		if ( $this->is_excluded( $image['atts'], $this->get_excluded_attributes() ) ) {
			return false;
		}

		if ( ! preg_match( '@\ssrc\s*=\s*(\'|")(?<src>.*)\1@Uis', $image['atts'], $atts ) ) {
			return false;
		}

		$image['src'] = trim( $atts['src'] );

		if ( '' === $image['src'] ) {
			return false;
		}

		if ( $this->is_excluded( $image['src'], $this->get_excluded_src() ) ) {
			return false;
		}

		if ( $this->is_excluded( $image['src'], $this->get_excluded_extension() ) ) {
			return false;
		}

		return $image;
	}

	private function can_lazy_load_attachment( $attachment_attr ) {
		$skip_attr = [ 'src', 'srcset', 'sizes' ];

		foreach ( $attachment_attr as $attr => $value ) {
			if ( in_array( $attr, $skip_attr ) ) {
				continue;
			}

			if ( $this->is_excluded( $attr, $this->get_excluded_attributes() ) ) {
				return false;
			}

			if ( $this->is_excluded( $value, $this->get_excluded_attributes() ) ) {
				return false;
			}
		}

		if ( '' === $attachment_attr['src'] ) {
			return false;
		}

		if ( $this->is_excluded( $attachment_attr['src'], $this->get_excluded_src() ) ) {
			return false;
		}

		if ( $this->is_excluded( $attachment_attr['src'], $this->get_excluded_extension() ) ) {
			return false;
		}

		return true;
	}

	private function noscript( $element ) {
		return '<noscript>' . $element . '</noscript>';
	}

	private function is_excluded( string $string, $excluded_values ) {
		if ( ! is_array( $excluded_values ) ) {
			$excluded_values = (array) $excluded_values;
		}

		if ( empty( $excluded_values ) ) {
			return false;
		}

		foreach ( $excluded_values as $excluded_value ) {
			if ( str_contains( $string, $excluded_value ) ) {
				return true;
			}
		}

		return false;
	}

	private function get_excluded_attributes() {
		return [
			'data-src=',
			'data-srcset=',
			'data-skip-lazy',
		];
	}

	private function get_excluded_src() {
		return [
			'data:image/',
			'/wpcf7_captcha/',
			'timthumb.php?src',
			'woocommerce/assets/images/placeholder.png',
		];
	}

	private function get_excluded_extension() {
		return [
			'.svg',
			'.gif',
		];
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}