<?php
/**
 * Wordpress Lazy Load Class
 *
 * @version 1.0.0
 */


namespace App\Extensions\Progressive_Images;

use App\Helper;
use App\Scripts;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class Lazy_Load {

	protected function __construct() {
		$this->init();
	}

	private function init() {
		Image::getInstance();

		add_filter( 'body_class', [ $this, 'body_class' ], 10, 2 );
		add_filter( 'wp_lazy_loading_enabled', [ $this, 'wp_native_lazy_loading' ], 10, 2 );
		add_action( 'template_redirect', [ $this, 'init_buffer' ], 2 );
		add_action( 'wp_enqueue_scripts', [ $this, 'wp_enqueue_scripts' ], 1000 );
		add_action( 'wp_footer', [ $this, 'wp_footer_scripts' ], 1000 );
	}

	/**
	 * Disable WP Native lazy-load
	 *
	 * @param $default
	 * @param $tag_name
	 *
	 * @return false
	 */
	public function body_class( $classes ) {
		$classes[] = 'lazy-image';

		return $classes;
	}

	/**
	 * Disable WP Native lazy-load
	 *
	 * @param $default
	 * @param $tag_name
	 *
	 * @return false
	 */
	public function wp_native_lazy_loading( $default, $tag_name ) {
		return false;
	}

	/**
	 * Add Progressive Image Style and JS
	 */
	public function wp_enqueue_scripts() {
		$inline_style = file_get_contents( get_theme_file_path( 'assets/css/progressive-images.css' ) );

		wp_add_inline_style( 'main', $inline_style );

		$inline_style = file_get_contents( get_theme_file_path( 'assets/js/progressive-images.js' ) );

		wp_add_inline_script( 'main', $inline_style );
	}

	/**
	 * Add inline lazyload script to the end of body
	 */
	public function wp_footer_scripts() {
		echo '<script data-no-minify="1" async src="' . Scripts::get_dependence_url( 'lazyload/lazyload.min.js' ) . '"></script>';
	}

	public static function lazy_load_allowed() {

		if ( ( defined( 'DOING_AJAX' ) && DOING_AJAX ) && isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'theme_ajax' ) {
			return true;
		}

		if ( isset( $_REQUEST['wc-ajax'] ) ) {
			return false;
		}

		if ( is_admin() || is_feed() || is_preview() || ( defined( 'REST_REQUEST' ) && REST_REQUEST ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Init buffer
	 *
	 * @return false|void
	 */
	public function init_buffer() {
		if ( false === self::lazy_load_allowed() ) {
			return false;
		}

		ob_start( [ $this, 'load_buffer' ] );
	}

	/**
	 * Load buffer and replace HTML of the images
	 *
	 * @param $html
	 *
	 * @return array|mixed|string|string[]
	 */
	private function load_buffer( $html ) {
		$clean_buffer = $this->ignore_html_special_tags( $html );

		return Image::getInstance()->lazy_load_images( $html, $clean_buffer );
	}

	/**
	 * Ignore HTML tags
	 *
	 * @param $html
	 *
	 * @return array|string|string[]|null
	 */
	private function ignore_html_special_tags( $html ) {
		$html = preg_replace( '/<script\b(?:[^>]*)>(?:.+)?<\/script>/Umsi', '', $html );
		$html = preg_replace( '#<noscript>(?:.+)</noscript>#Umsi', '', $html );
		$html = preg_replace( '#<span class="progressive-media img">(?:.+)</span>#Umsi', '', $html );

		return $html;
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}