<?php
/**
 * Helpers
 *
 */

if ( ! function_exists( 'str_contains' ) ) {
	/**
	 * Check if substring is contained in string
	 *
	 * @param $haystack
	 * @param $needle
	 *
	 * @return bool
	 */
	function str_contains( $haystack, $needle ) {
		return ( strpos( $haystack, $needle ) !== false );
	}
}

if ( ! function_exists( 'pp' ) ) {
	/**
	 * print_r helper wrapped in <pre> tag
	 *
	 * @return void
	 */
	function pp() {
		foreach ( func_get_args() as $x ) {
			echo '<pre>';
			print_r( $x );
			echo '</pre>';
		}
		die;
	}
}


if ( ! function_exists( 'dd' ) ) {
	/**
	 * var_dump helper
	 *
	 * @return void
	 */
	function dd() {
		foreach ( func_get_args() as $x ) {
			var_dump( $x );
		}
		die;
	}
}

if ( ! function_exists( 'wp_esc_json' ) ) {
	/**
	 * Escape JSON for use on HTML or attribute text nodes.
	 *
	 * @param string $json JSON to escape.
	 * @param bool $html True if escaping for HTML text node, false for attributes. Determines how quotes are handled.
	 *
	 * @return string Escaped JSON.
	 * @since 3.5.5
	 */
	function wp_esc_json( $json, $html = false ) {
		return _wp_specialchars(
			$json,
			$html ? ENT_NOQUOTES : ENT_QUOTES, // Escape quotes in attribute nodes only.
			'UTF-8', // json_encode() outputs UTF-8 (really just ASCII), not the blog's charset.
			true // Double escape entities: `&amp;` -> `&amp;amp;`.
		);
	}
}
