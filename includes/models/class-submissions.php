<?php
/**
 * DB Model
 *
 * @version 1.0.0
 */

namespace App\Models;

use Exception;
use App\Abstracts\DB_Model;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Submissions extends DB_Model {

	/**
	 * @var string
	 */
	protected string $table_name_items;

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct( 'submissions' );

		$this->table_name_items = $this->generate_table_name( 'submission_items' );
	}

	/**
	 * Get submission statuses
	 *
	 * @return array
	 */
	public static function get_statuses() {
		return [
			'pending'   => _x( 'Pending review', 'Submissions', 'ruby_studio' ),
			'completed' => _x( 'Completed', 'Submissions', 'ruby_studio' ),
			'canceled'  => _x( 'Canceled', 'Submissions', 'ruby_studio' ),
		];
	}

	/**
	 * Get shipping methods
	 *
	 * @return array
	 */
	public static function get_shipping_methods() {
		return [
			'dao'      => _x( 'Med POST (DAO)', 'Submissions', 'ruby_studio' ),
			'in_store' => _x( 'I butikken', 'Submissions', 'ruby_studio' ),
		];
	}

	public function set_categories( $selected_cats ) {
		update_option( 'submissions_categories', $selected_cats, 'no' );
	}

	public function get_categories() {
		return get_option( 'submissions_categories', array() );
	}

	public function set_conditions_text( $conditions ) {
		update_option( 'submissions_conditions', $conditions, 'no' );
	}

	public function get_conditions_text() {
		return get_option( 'submissions_conditions', array() );
	}

	public function set_qualities_text( $conditions ) {
		update_option( 'submissions_qualities', $conditions, 'no' );
	}

	public function get_qualities_text() {
		return get_option( 'submissions_qualities', array() );
	}

	public function get_category_reward_credits( $term_id ) {
		return get_term_meta( $term_id, 'reward_credits', true );
	}

	public function set_category_reward_credits( $term_id, $reward_credits ) {
		return update_term_meta( $term_id, 'reward_credits', $reward_credits );
	}

	public function get_mathed_reward_credits( $child_term_id, $condition, $quality = false ) {
		$category = get_term( $child_term_id );
		$prices   = $this->get_category_reward_credits( $category->parent );

		if ( ! empty( $quality ) ) {
			return $prices[ $condition ][ $quality ];
		} else if ( ! empty( $condition ) ) {
			return $prices[ $condition ];
		} else {
			return 0;
		}
	}

	/**
	 * @param int $term_id
	 *
	 * @return array
	 */
	public function get_category_carbon_dioxide_saver( int $term_id ) {
		$defaults = [ 'co2' => '', 'coffee' => '', 'streaming' => '', 'chicken' => '' ];

		return wp_parse_args( get_term_meta( $term_id, 'carbon_dioxide_saver', true ), $defaults );
	}

	/**
	 * @param int $term_id
	 * @param array $value
	 *
	 * @return void
	 */
	public function set_category_carbon_dioxide_saver( int $term_id, array $value ) {
		$defaults = [ 'co2' => '', 'coffee' => '', 'streaming' => '', 'chicken' => '' ];
		$value    = wp_parse_args( $value, $defaults );

		update_term_meta( $term_id, 'carbon_dioxide_saver', $value );
	}

	/**
	 * Get entries
	 *
	 * @param $args
	 *
	 * @return \stdClass
	 */
	public function get_entries( $args = [] ) {
		$defaults = [
			'user_id'  => '',
			'orderby'  => 'date_created',
			'order'    => 'DESC',
			'paged'    => 1,
			'per_page' => 50
		];

		$args = wp_parse_args( $args, $defaults );

		$pg = $limit = '';
		if ( $args['paged'] !== false && $args['per_page'] != '-1' ) {
			$limit = ( $args['paged'] - 1 ) * $args['per_page'];
			$pg    = "LIMIT {$limit}, {$args['per_page']}";
		}

		$where = [];

		if ( ! empty( $args['customer'] ) ) {
			$where[] = $this->db->prepare( "user_id = %d", $args['customer'] );
		}

		$where = ! empty( $where ) ? ' AND ' . join( ' AND ', $where ) : '';

		$sql = "SELECT SQL_CALC_FOUND_ROWS {$this->table_name}.*, users.display_name as user_full_name,
                	(SELECT COUNT(*) as total FROM {$this->table_name_items} WHERE {$this->table_name}.order_id = {$this->table_name_items}.order_id) as total_order_items
					FROM {$this->table_name}
					LEFT JOIN {$this->db()->users} users ON ({$this->table_name}.user_id=users.ID)
					WHERE 1=1 {$where}
					ORDER BY {$args['orderby']} {$args['order']} {$pg}";

		$items = $this->db->get_results( $sql, ARRAY_A );
		$total = $this->db->get_var( "SELECT FOUND_ROWS() as total" );

		$results                = new \stdClass();
		$results->items         = $items;
		$results->total         = $total;
		$results->max_num_pages = ceil( $total / $args['per_page'] );

		return $results;
	}

	public function get_order( $order_id ) {
		return $this->db()->get_row(
			$this->db()->prepare(
				"SELECT * FROM {$this->table_name} WHERE order_id = %s LIMIT 1",
				$order_id
			), ARRAY_A
		);
	}

	public function delete_order( $order_id ) {
		$this->db()->delete( $this->table_name, [ 'order_id' => $order_id ] );
		$this->db()->delete( $this->table_name_items, [ 'order_id' => $order_id ] );
	}

	public function create_order( $data ) {
		$insert = $this->db()->insert( $this->table_name,
			[
				'date_created'           => current_time( 'mysql' ),
				'date_created_gtm'       => current_time( 'mysql', true ),
				'user_id'                => $data['user_id'],
				'order_status'           => $data['order_status'],
				'total_expected_credits' => $data['total_expected_credits'],
				'total_actual_credits'   => $data['total_actual_credits'],
				'shipping_method'        => $data['shipping_method'],
			]
		);

		if ( false === $insert ) {
			return false;
		} else {
			return $this->db->insert_id;
		}
	}

	public function update_order( $order_id, $data ) {
		$update = $this->db()->update( $this->table_name,
			[
				'date_modified'     => current_time( 'mysql' ),
				'date_modified_gtm' => current_time( 'mysql', true ),
				'order_status'      => $data['order_status'],
				'shipping_method'   => $data['shipping_method'],
			], [
				'order_id' => $order_id
			]
		);

		if ( false === $update ) {
			return false;
		} else {
			return $this->db->insert_id;
		}
	}

	public function add_order_item( $order_id, $order_item_data ) {
		$insert = $this->db()->insert( $this->table_name_items,
			[
				'order_id'              => $order_id,
				'item_name'             => $order_item_data['item_name'],
				'item_category_id'      => $order_item_data['item_category_id'],
				'item_condition'        => $order_item_data['item_condition'],
				'item_quality'          => $order_item_data['item_quality'],
				'item_expected_credits' => $order_item_data['item_expected_credits'],
				'item_actual_credits'   => $order_item_data['item_actual_credits'],
			]
		);

		if ( false === $insert ) {
			return false;
		} else {
			return $this->db->insert_id;
		}
	}

	/**
	 * Update order item
	 *
	 * @param $order_id
	 * @param $order_item_id
	 * @param array $order_item_data
	 *
	 * @return void
	 */
	public function update_order_item( $order_id, $order_item_id, array $order_item_data ) {
		$this->db()->update( $this->table_name_items, $order_item_data, [
				'order_id'      => $order_id,
				'order_item_id' => $order_item_id,
			]
		);
	}

	public function remove_order_item( $order_id, $order_item_id ) {
		$this->db()->delete( $this->table_name_items,
			[
				'order_id'      => $order_id,
				'order_item_id' => $order_item_id,
			]
		);
	}

	public function update_order_total_credits( $order_id ) {
		$order_items = $this->get_order_items( $order_id );

		$total_actual_credits = $total_expected_credits = 0;

		foreach ( $order_items as $order_item ) {
			$total_expected_credits += $order_item['item_expected_credits'];
			$total_actual_credits   += $order_item['item_actual_credits'];
		}

		$this->db()->update( $this->table_name,
			[
				'total_expected_credits' => $total_expected_credits,
				'total_actual_credits'   => $total_actual_credits,
			], [
				'order_id' => $order_id
			]
		);
	}

	public function get_order_items( $order_id ) {
		return $this->db()->get_results(
			$this->db()->prepare(
				"SELECT * FROM {$this->table_name_items} WHERE order_id = %s",
				$order_id
			), ARRAY_A
		);
	}

	public function get_order_item( $order_item_id ) {
		return $this->db()->get_row(
			$this->db()->prepare(
				"SELECT * FROM {$this->table_name_items} WHERE order_item_id = %s LIMIT 1",
				$order_item_id
			), ARRAY_A
		);
	}
}