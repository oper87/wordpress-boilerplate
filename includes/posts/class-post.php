<?php
/**
 * Register post_type
 *
 * Init class add to functions.php - new App\Post_Type\Sample_Post();
 * Use method in the theme - App\Post_Type\Sample_Post::getInstance()->get_posts(-1);
 *
 * @version 1.0.0
 */

namespace App\Posts;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use WP_Query;
use App\Abstracts\Post as Abstract_Post;

class Post extends Abstract_Post {

	/**
	 * Post-type constant
	 */
	const POST_TYPE = 'post';

	/**
	 * Except limit
	 */
	const EXCERPT_LIMIT = 50;

	protected function __construct() {
		parent::__construct( static::POST_TYPE );

		add_filter( 'post_type_labels_' . self::POST_TYPE, [ $this, 'modify_post_labels' ], 10, 1 );
	}

	/**
	 * Modify post labels
	 *
	 * @param $labels
	 *
	 * @return mixed
	 */
	public function modify_post_labels( $labels ) {
		$labels->name      = 'News';
		$labels->all_items = 'All posts';
		$labels->menu_name = 'News';

		return $labels;
	}

	public function get_page() {
		$page = get_post( get_option( 'page_for_posts' ) );

		return $page;
	}

	public function get_page_id() {
		$page = $this->get_page();

		return $page->ID;
	}

	public static function get_page_url() {
		return get_post_type_archive_link( static::POST_TYPE );
	}

	public function pre_get_posts( $q ) {
		if ( is_home() ) {
			$q->set( 'posts_per_page', - 1 );
		}
	}

	/**
	 * Related posts
	 */
	public function get_related_posts( $per_page = 9 ) {
		global $wpdb, $post;

		$limit = $per_page + 10;

		$categories = Tax_Category::getInstance()->get_the_terms( $post );
		$categories = ! empty( $categories ) && ! is_wp_error( $categories ) ? wp_list_pluck( $categories, 'term_id' ) : [];

		$include_term_ids = array_merge( $categories, [] );
		$exclude_ids      = [ $post->ID ];

		if ( empty( $include_term_ids ) ) {
			return false;
		}

		$sql = "
			SELECT * FROM ( SELECT p.ID, p.post_name, taxonomy
			FROM {$wpdb->posts} p
			INNER JOIN ( 
			    SELECT object_id, taxonomy 
			    FROM {$wpdb->term_relationships} 
		        INNER JOIN {$wpdb->term_taxonomy} using( term_taxonomy_id ) 
			    WHERE term_id IN ( " . implode( ',', array_map( 'absint', $include_term_ids ) ) . " ) 
		    ) AS include_join ON include_join.object_id = p.ID
		    WHERE 1=1
			AND p.post_status = 'publish'
			AND p.post_type = '%s'
			AND p.ID NOT IN ( " . implode( ',', array_map( 'absint', $exclude_ids ) ) . " )
			ORDER BY taxonomy DESC, p.post_date DESC ) AS tmp_table GROUP BY tmp_table.ID
			LIMIT %d";

		$post_ids = $wpdb->get_col( $wpdb->prepare( $sql, self::POST_TYPE, absint( $limit ) ) );

		shuffle( $post_ids );

		return array_slice( $post_ids, 0, $per_page );
	}

	/**
	 * Add limit of excerpt words
	 *
	 * @param $post_excerpt
	 * @param $post
	 *
	 * @return string
	 */
	public function get_the_excerpt( $post_excerpt, $post ) {

		if ( $post->post_type != self::POST_TYPE ) {
			return $post_excerpt;
		}

		$post_excerpt = strip_tags( $post_excerpt );
		$words        = explode( ' ', strip_tags( $post_excerpt ) );
		$return       = trim( implode( ' ', array_slice( $words, 0, self::EXCERPT_LIMIT ) ) );

		if ( strlen( $return ) < strlen( $post_excerpt ) ) {
			$return .= apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
		}

		return $return;
	}

	/**
	 * @param $excerpt_more
	 *
	 * @return string
	 */
	public function excerpt_more( $excerpt_more ) {
		return ' &hellip;';
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance(): ?Post {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}
