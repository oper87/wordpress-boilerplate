<?php
/**
 * Abstract Taxonomy Class
 */

namespace App\Posts;

use App\Abstracts\Taxonomy;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Tax_Category extends Taxonomy {
	/**
	 * Taxonomy name
	 */
	const TAXONOMY = 'category';

	/**
	 * Constructor
	 */
	private function __construct() {
		parent::__construct( static::TAXONOMY, [ Post::POST_TYPE ] );
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}