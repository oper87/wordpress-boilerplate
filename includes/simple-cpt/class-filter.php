<?php

namespace App\Posts;

use App\Simple_CPT\Post;
use App\Helper;
use WP_Meta_Query;
use WP_Tax_Query;

class Filter extends \App\Abstracts\Filter {

	const VISIBLE_COUNT = 10;

	private $active = false;

	private function __construct() {
		$args = [
			/*'search' => [
				'name'       => __( 'Search', 'ruby_studio' ),
				'query_from' => 'search',
				'data_from'  => 'search',
			],*/
			'banks'  => [
				'name'       => __( 'Category', 'ruby_studio' ),
				'query_from' => 'taxonomy',
				'data_from'  => 'taxonomy',
				'taxonomy'   => 'category',
			],
			'people' => [
				'name'       => __( 'Tags', 'ruby_studio' ),
				'query_from' => 'taxonomy',
				'data_from'  => 'taxonomy',
				'taxonomy'   => 'post_tag',
			],
		];

		parent::__construct( $args, Post::POST_TYPE );

		add_action( 'wp', [ $this, 'activate_filter' ] );
		add_action( 'app_theme_blog_after_main_content', [ $this, 'init_filter' ] );
		add_action( 'app_theme_blog_before_loop_posts', [ $this, 'show_selected_terms' ] );

		$this->hook_init();
	}

	function activate_filter() {

		if ( is_home() ) :
			$this->active = true;
		endif;
	}

	public function is_active_filter() {
		return $this->active;
	}

	/**
	 * Statement condition
	 *
	 * @return bool
	 */
	static public function is_filter_page() {
		if ( filter_input( INPUT_GET, 'search' ) ||
		     filter_input( INPUT_GET, 'filter_banks' ) ||
		     filter_input( INPUT_GET, 'filter_people' ) ) {
			return true;
		}

		return false;
	}

	public function init_filter() {
		global $wp_query;

		if ( ! $this->is_active_filter() ) {
			return;
		}

		$action_url = '';

		if ( is_home() ) :
			$archive    = get_queried_object();
			$action_url = get_permalink( $archive->ID );
		endif;

		$self    = self::getInstance();
		$filters = $self->get_filters();

		Helper::get_template_part( '/template-parts/news/sidebar', null, [
			'action_url'  => $action_url,
			'filters'     => $filters,
			'found_posts' => $wp_query->found_posts
		] );
	}

	public function show_selected_terms() {

		if ( empty( $this->filter_params ) ) {
			return;
		}

		Helper::get_template_part( '/template-parts/news/filter-selected-terms', null, [
			'filter_params' => self::getInstance()->get_filters()
		] );

	}

	public function get_terms( $taxonomy, $query_key = false ) {
		global $wpdb;

		$terms = get_terms( [
			'taxonomy'   => $taxonomy,
			'order'      => 'asc',
			'orderby'    => 'title',
			'hide_empty' => true,
		] );

		if ( empty( $terms ) ) {
			return [];
		}

		$term_ids = wp_list_pluck( $terms, 'term_id' );

		$main_query = $this->get_query();

		foreach ( $main_query['tax_query'] as $key => $tax ) {
			if ( is_array( $main_query['tax_query'] ) && is_array( $tax ) && $taxonomy === $tax['taxonomy'] ) {
				unset( $main_query['tax_query'][ $key ] );
			}
		}

		$meta_query     = new WP_Meta_Query( $main_query['meta_query'] );
		$tax_query      = new WP_Tax_Query( $main_query['tax_query'] );
		$meta_query_sql = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
		$tax_query_sql  = $tax_query->get_sql( $wpdb->posts, 'ID' );

		// Generate query.
		$query           = [];
		$query['select'] = "SELECT COUNT( DISTINCT {$wpdb->posts}.ID ) as term_count, terms.term_id as term_count_id";
		$query['from']   = "FROM {$wpdb->posts}";
		$query['join']   = "
			INNER JOIN {$wpdb->term_relationships} AS term_relationships ON {$wpdb->posts}.ID = term_relationships.object_id
			INNER JOIN {$wpdb->term_taxonomy} AS term_taxonomy USING( term_taxonomy_id )
			INNER JOIN {$wpdb->terms} AS terms USING( term_id )
			" . $tax_query_sql['join'] . $meta_query_sql['join'];

		$query['where'] = "
			WHERE {$wpdb->posts}.post_type IN ( '" . Post::POST_TYPE . "' )
			AND {$wpdb->posts}.post_status = 'publish'"
		                  . $tax_query_sql['where'] . $meta_query_sql['where'] .
		                  ' AND terms.term_id IN (' . implode( ',', array_map( 'absint', $term_ids ) ) . ')';

		$query['group_by'] = 'GROUP BY terms.term_id';

		$query = implode( ' ', $query );

		// We have a query - let's see if cached results of this query already exist.
		$query_hash = md5( $query );

		$results     = $wpdb->get_results( $query, ARRAY_A ); // @codingStandardsIgnoreLine
		$term_counts = array_map( 'absint', wp_list_pluck( $results, 'term_count', 'term_count_id' ) );

		$new_terms       = [];
		$filter_selected = $this->get_filter_selected();

		foreach ( $terms as $key => $term ) {
			$option_is_set = in_array( $term->slug, $filter_selected[ $query_key ]['selected_array'], true );
			$count         = isset( $term_counts[ $term->term_id ] ) ? $term_counts[ $term->term_id ] : 0;

			if ( 0 > $count ) {
				continue;
			} elseif ( 0 === $count && ! $option_is_set ) {
				continue;
			}

			$new_terms[] = $term;
		}

		return $new_terms;
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}