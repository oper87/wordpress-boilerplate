<?php
/**
 * Register post_type
 *
 * Init class add to functions.php - new App\Post_Type\Sample_Post();
 * Use method in the theme - App\Post_Type\Sample_Post::getInstance()->get_posts(-1);
 *
 * @version 1.0.0
 */

namespace App\Simple_CPT;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Helper;

class Post {

	/**
	 * Post-type constant
	 */
	const POST_TYPE = 'sample_posts';

	/**
	 * Taxonomy constant
	 */
	const TAXONOMY = 'sample_category';

	private function __construct() {
		add_action( 'init', [ $this, 'create_post_type' ] );
		add_action( 'init', [ $this, 'create_taxonomy' ] );

		add_filter( 'manage_edit-' . self::POST_TYPE . '_columns', [ $this, 'add_custom_column' ] );
		add_action( 'manage_' . self::POST_TYPE . '_posts_custom_column', [ $this, 'add_custom_column_output' ], 10, 2 );
	}

	/**
	 * Register post type
	 */
	public function create_post_type() {
		register_post_type(
			self::POST_TYPE, [
				'label'               => __( 'Case', 'text_domain' ),
				'description'         => __( 'Cases', 'text_domain' ),
				'labels'              => [
					'name'                  => _x( 'Cases', 'Post Type General Name', 'text_domain' ),
					'singular_name'         => _x( 'Case', 'Post Type Singular Name', 'text_domain' ),
					'menu_name'             => __( 'Cases', 'text_domain' ),
					'name_admin_bar'        => __( 'Cases', 'text_domain' ),
					'archives'              => __( 'Item Archives', 'text_domain' ),
					'attributes'            => __( 'Item Attributes', 'text_domain' ),
					'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
					'all_items'             => __( 'All Items', 'text_domain' ),
					'add_new_item'          => __( 'Add New Item', 'text_domain' ),
					'add_new'               => __( 'Add New', 'text_domain' ),
					'new_item'              => __( 'New Item', 'text_domain' ),
					'edit_item'             => __( 'Edit Item', 'text_domain' ),
					'update_item'           => __( 'Update Item', 'text_domain' ),
					'view_item'             => __( 'View Item', 'text_domain' ),
					'view_items'            => __( 'View Items', 'text_domain' ),
					'search_items'          => __( 'Search Item', 'text_domain' ),
					'not_found'             => __( 'Not found', 'text_domain' ),
					'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
					'featured_image'        => __( 'Featured Image', 'text_domain' ),
					'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
					'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
					'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
					'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
					'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
					'items_list'            => __( 'Items list', 'text_domain' ),
					'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
					'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
				],
				'supports'            => [ 'title', 'thumbnail', 'custom-fields' ],
				'taxonomies'          => [ self::TAXONOMY ],
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 5,
				'menu_icon'           => 'dashicons-palmtree',
				'show_in_admin_bar'   => true,
				'show_in_nav_menus'   => true,
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'page',
			]
		);

	}

	/**
	 * Register taxonomy for post
	 */
	public function create_taxonomy() {
		$args = [
			'labels'            => [
				'name'                       => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
				'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
				'menu_name'                  => __( 'Categories', 'text_domain' ),
				'all_items'                  => __( 'All Items', 'text_domain' ),
				'parent_item'                => __( 'Parent Item', 'text_domain' ),
				'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
				'new_item_name'              => __( 'New Item Name', 'text_domain' ),
				'add_new_item'               => __( 'Add New Item', 'text_domain' ),
				'edit_item'                  => __( 'Edit Item', 'text_domain' ),
				'update_item'                => __( 'Update Item', 'text_domain' ),
				'view_item'                  => __( 'View Item', 'text_domain' ),
				'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
				'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
				'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
				'popular_items'              => __( 'Popular Items', 'text_domain' ),
				'search_items'               => __( 'Search Items', 'text_domain' ),
				'not_found'                  => __( 'Not Found', 'text_domain' ),
				'no_terms'                   => __( 'No items', 'text_domain' ),
				'items_list'                 => __( 'Items list', 'text_domain' ),
				'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
			],
			'hierarchical'      => true,
			'public'            => false,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'show_in_rest'      => true,
		];

		register_taxonomy( self::TAXONOMY, [ self::POST_TYPE ], $args );
	}

	/**
	 * Add custom column
	 */
	public function add_custom_column( $columns ) {
		$columns = Helper::array_sort( $columns, false, [ 'thumbnail' => '' ] );

		//$columns = Helper::array_sort( $columns, 'comments', [ 'featured' => 'Featured' ] );

		return $columns;
	}

	/**
	 * Add custom column callback
	 *
	 * @param $column_name
	 * @param $post_id
	 */
	public function add_custom_column_output( $column_name, $post_id ) {

		if ( $column_name == 'thumbnail' && has_post_thumbnail( $post_id ) ) {
			echo get_the_post_thumbnail( $post_id, 'thumbnail' );
		}

		if ( $column_name == 'featured' ) {
			if ( get_post_meta( $post_id, 'featured_post', true ) ) {
				echo '<span class="dashicons dashicons-yes-alt"></span>';
			} else {
				echo '<span class="dashicons dashicons-no-alt"></span>';
			}
		}
	}

	/**
	 * Add sortable custom columns
	 *
	 * @param $columns
	 *
	 * @return mixed
	 */
	public function add_custom_sortable_columns( $columns ) {
		$columns['featured'] = 'featured';

		return $columns;
	}

	/**
	 * Add order query for sortable custom columns
	 *
	 * @param $query
	 */
	public function make_custom_column_sortable( $query ) {
		if ( ! is_admin() ) {
			return;
		}

		if ( 'featured' == $query->get( 'orderby' ) && self::POST_TYPE == $query->get( 'post_type' ) ) {
			$query->set( 'meta_key', 'featured_post' );
			$query->set( 'orderby', 'meta_value_num' );
		}
	}

	/**
	 * Get posts
	 */
	public function get_posts( $per_page = 10 ) {
		$args = [
			'post_type'      => self::POST_TYPE,
			'posts_per_page' => $per_page,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		];

		return get_posts( $args );
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}

}