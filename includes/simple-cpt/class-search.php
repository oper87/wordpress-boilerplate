<?php
/**
 * Extend standard search query
 * @version 1.0.0
 */

namespace App\Simple_CPT;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Simple_CPT\Post;

class Search {

	protected $taxonomies;

	protected $post_meta_keys;

	public function __construct() {

		$this->taxonomies = [
			//Post::PROGRAMMER_TAXONOMY,
			//Post::PERSON_TAXONOMY,
		];

		$this->post_meta_keys = [
			//'realitybyline',
		];

		/**
		 * Frontend
		 */
		add_action( 'pre_get_posts', [ $this, 'pre_get_posts' ], 10 );
		add_filter( 'posts_join', [ $this, 'search_posts_join' ], 10, 2 );
		add_filter( 'posts_search', [ $this, 'search_posts_search' ], 10, 2 );
		add_filter( 'posts_orderby', [ $this, 'search_posts_orderby' ], 10, 2 );
		add_filter( 'posts_groupby', [ $this, 'search_posts_groupby' ], 10, 2 );

	}

	public function pre_get_posts( $query ) {
		if ( is_admin() ) {
			return;
		}

		if ( is_search() && $query->is_main_query() ) {
			$query->set( 'post_type', Post::POST_TYPE );
		}
	}

	/**
	 * * Add posts_join sql query on search page
	 *
	 * @param $join
	 * @param $query
	 *
	 * @return string
	 */
	public function search_posts_join( $join, $query ) {
		global $wpdb;

		if ( is_admin() ) {
			return $join;
		}

		if ( is_search() && $query->is_main_query() ) {

			$join .= " 
				LEFT JOIN {$wpdb->term_relationships} tr1 
					ON ({$wpdb->posts}.ID = tr1.object_id)
				LEFT JOIN {$wpdb->term_taxonomy} tt1
					ON (tr1.term_taxonomy_id = tt1.term_taxonomy_id AND tt1.taxonomy IN ('" . join( "','", $this->taxonomies ) . "'))
				LEFT JOIN {$wpdb->terms} term1
					ON (tt1.term_taxonomy_id = term1.term_id)";

			$join .= " 
			INNER JOIN {$wpdb->postmeta} pm1
				ON ({$wpdb->posts}.ID = pm1.post_id AND pm1.meta_key IN ('" . join( "','", $this->post_meta_keys ) . "') )";

		}

		return $join;
	}

	public function search_posts_search( $search, $query ) {
		global $wpdb;

		if ( is_admin() ) {
			return $search;
		}

		if ( empty( $search ) ) {
			return $search; // skip processing - no search term in query
		}

		if ( is_search() && $query->is_main_query() ) {
			$q      = $query->query_vars;
			$n      = ! empty( $q['exact'] ) ? '' : '%';
			$search =
			$searchand = '';

			foreach ( (array) $q['search_terms'] as $term ) {
				$like_op  = 'LIKE';
				$andor_op = 'OR';
				$like     = '%' . $wpdb->esc_like( $term ) . '%';

				$search_sql   = [];
				$search_sql[] = $wpdb->prepare( "({$wpdb->posts}.post_title $like_op %s)", $like );
				$search_sql[] = $wpdb->prepare( "({$wpdb->posts}.post_excerpt $like_op %s)", $like );
				$search_sql[] = $wpdb->prepare( "({$wpdb->posts}.post_content $like_op %s)", $like );
				$search_sql[] = $wpdb->prepare( "(term1.name $like_op %s)", $like );
				$search_sql[] = $wpdb->prepare( "(pm1.meta_value $like_op %s)", $like );

				$search .= "{$searchand}(" . join( " {$andor_op} ", $search_sql ) . ")";

				$searchand = ' AND ';
			}

			if ( ! empty( $search ) ) {
				$search = " AND ({$search}) ";
				if ( ! is_user_logged_in() ) {
					$search .= " AND ($wpdb->posts.post_password = '') ";
				}
			}
		}

		return $search;
	}

	public function search_posts_orderby( $orderby, $query ) {
		global $wpdb;

		if ( is_admin() ) {
			return $orderby;
		}

		if ( is_search() && $query->is_main_query() ) {
			$orderby = $this->parse_search_order( $query->query_vars );
		}

		return $orderby;
	}

	protected function parse_search_order( $q ) {
		global $wpdb;

		if ( $q['search_terms_count'] > 1 ) {
			$num_terms = count( $q['search_orderby_title'] );

			// If the search terms contain negative queries, don't bother ordering by sentence matches.
			$like = '';
			if ( ! preg_match( '/(?:\s|^)\-/', $q['s'] ) ) {
				$like = '%' . $wpdb->esc_like( $q['s'] ) . '%';
			}

			$search_orderby = '';

			// sentence match in 'post_title'
			if ( $like ) {
				$search_orderby .= $wpdb->prepare( "WHEN {$wpdb->posts}.post_title LIKE %s THEN 1 ", $like );
			}

			// sanity limit, sort as sentence when more than 6 terms
			// (few searches are longer than 6 terms and most titles are not)
			if ( $num_terms < 7 ) {
				// all words in title
				$search_orderby .= 'WHEN ' . implode( ' AND ', $q['search_orderby_title'] ) . ' THEN 2 ';
				// any word in title, not needed when $num_terms == 1
				if ( $num_terms > 1 ) {
					$search_orderby .= 'WHEN ' . implode( ' OR ', $q['search_orderby_title'] ) . ' THEN 3 ';
				}
			}

			$search_orderby .= $wpdb->prepare( "WHEN term1.name LIKE %s THEN 4 ", $like );
			$search_orderby .= $wpdb->prepare( "WHEN pm1.meta_value LIKE %s THEN 5 ", $like );


			// Sentence match in 'post_content' and 'post_excerpt'.
			if ( $like ) {
				$search_orderby .= $wpdb->prepare( "WHEN {$wpdb->posts}.post_excerpt LIKE %s THEN 6 ", $like );
				$search_orderby .= $wpdb->prepare( "WHEN {$wpdb->posts}.post_content LIKE %s THEN 7 ", $like );
			}

			if ( $search_orderby ) {
				$search_orderby = '(CASE ' . $search_orderby . 'ELSE 8 END)';
			}

			$search_orderby = $search_orderby . ", {$wpdb->posts}.post_date DESC";

		} else {
			// single word or sentence search
			$like = '';
			if ( ! preg_match( '/(?:\s|^)\-/', $q['s'] ) ) {
				$like = '%' . $wpdb->esc_like( $q['s'] ) . '%';
			}

			$like_sql = [];

			$like_sql[] = $wpdb->prepare( "{$wpdb->posts}.post_title LIKE %s DESC", $like );
			$like_sql[] = $wpdb->prepare( "term1.name LIKE %s DESC", $like );
			$like_sql[] = $wpdb->prepare( "pm1.meta_value LIKE %s DESC", $like );
			$like_sql[] = $wpdb->prepare( "{$wpdb->posts}.post_excerpt LIKE %s DESC", $like );
			$like_sql[] = $wpdb->prepare( "{$wpdb->posts}.post_content LIKE %s DESC", $like );

			$like_sql[] = "{$wpdb->posts}.post_date DESC";

			$search_orderby = join( ', ', $like_sql );
		}


		return $search_orderby;
	}

	/**
	 * * Add posts_groupby sql query on search page
	 *
	 * @param $join
	 * @param $query
	 *
	 * @return string
	 */
	public function search_posts_groupby( $groupby, $query ) {
		global $wpdb;

		if ( is_admin() ) {
			return $groupby;
		}

		if ( is_search() && $query->is_main_query() ) {

			$groupby .= "{$wpdb->posts}.ID";

		}

		return $groupby;
	}

}