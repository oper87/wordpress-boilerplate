<?php
/**
 * Walkers
 *
 * @version 1.0.0
 */

namespace App\Walkers;

use App\Helper;
use App\WooCommerce\Product;
use Walker_Nav_Menu;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Main_Menu extends Walker_Nav_Menu {

	const theme_location = 'header';

	public function __construct() {
		add_filter( 'nav_menu_item_title', [ $this, 'nav_menu_item_title' ], 10, 3 );
		add_filter( 'nav_menu_css_class', [ $this, 'nav_menu_css_class' ], 10, 4 );
	}

	public function nav_menu_item_title( $title, $menu_item, $args ) {

		if ( $args->walker !== '' && $args->theme_location == self::theme_location && $menu_item->menu_item_parent == 0 ) {
			$title = '<span class="regular">' . $title . '</span><span class="bold">' . $title . '</span>';
		}

		return $title;
	}

	/**
	 * @param array $classes
	 * @param \WP_Post $item
	 * @param \stdClass $args
	 * @param int $depth
	 */
	public function nav_menu_css_class( $classes, $item, $args, $depth ) {
		$queried_object_id = get_queried_object_id();

		/*if ( is_singular( Exhibitions\Post::POST_TYPE ) && Exhibitions\Post::get_page_id( 'ID' ) == $item->object_id ) {
			$classes[] = 'current-menu-item';
		} elseif ( is_singular( Product::POST_TYPE ) ) {
			// Mark change the mind so fast we leave Artist in logic
			$artist   = false;//Tax_Artist::getInstance()->get_single_term( $queried_object_id );
			$category = Tax_Category::getInstance()->get_single_term( $queried_object_id );

			if ( $artist ) {
				if ( $artist->term_id == $item->object_id && $item->object == Tax_Artist::TAXONOMY ) {
					$classes[] = 'current-menu-item';
				}

				if ( get_field( 'pages_artist', 'options', false ) == $item->object_id ) {
					$classes[] = 'current-menu-ancestor';
					$classes[] = 'current-menu-item';
				}

			} else if ( $category ) {
				if ( wc_get_page_id( 'shop' ) == $item->object_id ) {
					$classes[] = 'current-menu-ancestor';
					$classes[] = 'current-menu-item';
				}

				if ( $category->term_id == $item->object_id && $item->object == Tax_Category::TAXONOMY ) {
					$classes[] = 'current-menu-item';
				}
			} else if ( wc_get_page_id( 'shop' ) == $item->object_id ) {
				$classes[] = 'current-menu-item';
			}
		}*/

		return $classes;
	}

	public static function get_current_menu_item() {
		$menu              = false;
		$current_menu_item = false;

		$locations = get_nav_menu_locations();
		if ( ! $menu && self::theme_location && $locations && isset( $locations[ self::theme_location ] ) ) {
			$menu = wp_get_nav_menu_object( $locations[ self::theme_location ] );
		}

		if ( ! $menu ) {
			return '';
		}

		if ( $menu_items = wp_get_nav_menu_items( $menu->term_id ) ) {
			$queried_object_id = get_queried_object_id();

			if ( is_front_page() ) {
				$queried_object_id = get_option( 'page_on_front' );
			}

			// Loop over menu items
			foreach ( $menu_items as $item ) {
				// Compare menu object with current page menu object
				if ( is_singular( Product::POST_TYPE ) ) {
					/*$category = Tax_Category::getInstance()->get_single_term( $queried_object_id );

					if ( isset( $_GET['_menu_item'] ) && intval( $_GET['_menu_item'] ) == $item->ID ) {
						$current_menu_item = $item;
					} elseif ( $artist && $artist->term_id == $item->object_id && $item->object == Tax_Artist::TAXONOMY ) {
						$current_menu_item = $item;
					} else if ( $category && $category->term_id == $item->object_id && $item->object == Tax_Category::TAXONOMY ) {
						$current_menu_item = $item;
					} else if ( wc_get_page_id( 'shop' ) == $item->object_id ) {
						$current_menu_item = $item;
					}*/
				} if ( $item->object_id == $queried_object_id ) {
					$current_menu_item = $item;
				}
			}
		}

		if ( $current_menu_item ) {
			return [
				'current_item_id' => $current_menu_item->ID,
				'parent_item_id'  => $current_menu_item->menu_item_parent,
			];
		}

		return false;
	}

}
