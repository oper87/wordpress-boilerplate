<?php
/**
 * Woocommerce Product ACF BD Table Class
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Acf\Db_Table;

class Acf_DB_Table extends Db_Table {

	protected string $post_type = 'product';

	protected string $table_name = 'wc_product_acf_lookup';

	private function __construct() {
		parent::__construct();
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}

	public function set_fields() {
		$this->acf_fields = [
			'hide_outofstock',
		];
	}

	public function tabel_sql(): string {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "";
		$sql .= "CREATE TABLE `{$this->table_name()}` (
				`post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
			  	`hide_outofstock` int(1) NOT NULL DEFAULT '0',
				UNIQUE KEY `post_id` (`post_id`) USING BTREE,
                KEY `hide_outofstock` (`hide_outofstock`)
				) $charset_collate;";

		return $sql;
	}

}