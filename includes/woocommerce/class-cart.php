<?php
/**
 * Woocommerce Functions Class
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Cart {

	/**
	 * Cart constructor.
	 */
	private function __construct() {

		// Add Cart Panel to wp_footer
		add_action( 'wp_footer', [ $this, 'footer_add_cart_contents' ], 0 );

		add_filter( 'woocommerce_get_cart_url', [ $this, 'woocommerce_get_cart_url' ] );

		//Replace Cart fragments data
		add_filter( 'woocommerce_add_to_cart_fragments', [ $this, 'add_to_cart_fragments' ], 10 );

		//Modify Script Data Parameters
		add_filter( 'woocommerce_get_script_data', [ $this, 'add_script_data_params' ], 10, 2 );

		//AJAX Request: Update cart item
		add_action( 'wc_ajax_update_cart_item', [ $this, 'update_cart_item' ] );

		add_filter( 'woocommerce_shipping_rate_label', [ $this, 'shipping_rate_label' ] );
		add_filter( 'woocommerce_cart_shipping_method_full_label', [
			$this,
			'cart_shipping_method_full_label'
		], 10, 2 );

		add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
		add_filter( 'woocommerce_is_attribute_in_product_name', '__return_empty_string' );
		//add_filter( 'woocommerce_cart_item_name', [ $this, 'cart_item_name' ], 10, 3 );
		//add_filter( 'woocommerce_is_attribute_in_product_name', [ $this, 'is_attribute_in_product_name' ], 10, 3 );
	}

	/**
	 * Add to footer Cart Panel Widget
	 */
	public function footer_add_cart_contents() {
		wc_get_template_part( 'cart/cart-panel' );
	}

	public function woocommerce_get_cart_url() {
		if ( wc_get_page_id( 'checkout' ) == wc_get_page_id( 'cart' ) || wc_get_page_id( 'cart' ) === - 1 ) {
			return wc_get_checkout_url();
		}
	}

	/**
	 * Get cart count HTML
	 *
	 * @return string
	 */
	public static function get_cart_contents_count() {
		$cart_count  = WC()->cart->cart_contents_count;
		$count_class = ( $cart_count > 0 ) ? '' : ' cart-count-zero';

		return sprintf( '<span class="shop-cart-count%s">%s</span>', esc_attr( $count_class ), esc_html( $cart_count ) );
	}

	/**
	 * Replace Cart fragments data
	 *
	 * @param $fragments
	 *
	 * @return array
	 */
	public function add_to_cart_fragments( $fragments ) {

		ob_start();
		woocommerce_mini_cart();
		$mini_cart = ob_get_clean();

		$fragments                         = [];
		$fragments['span.shop-cart-count'] = self::get_cart_contents_count();
		$fragments['div.cart-panel-body']  = '<div class="sidebar-body cart-panel-body">' . $mini_cart . '</div>';

		return $fragments;
	}

	/**
	 * Modify Script Data Parameters
	 *
	 * @param $params
	 * @param $handle
	 *
	 * @return mixed
	 */
	public function add_script_data_params( $params, $handle ) {
		if ( $handle == 'woocommerce' ) {
			$params['update_cart_item_nonce'] = wp_create_nonce( 'update_cart_item' );
		}

		return $params;
	}

	/**
	 * AJAX Request: Update cart item
	 */
	public function update_cart_item() {

		check_ajax_referer( 'update_cart_item', 'security' );

		$cart_updated = false;
		$cart_totals  = isset( $_POST['cart'] ) ? wp_unslash( $_POST['cart'] ) : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

		if ( ! WC()->cart->is_empty() && is_array( $cart_totals ) ) {
			foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

				$_product = $values['data'];

				// Skip product if no updated quantity was posted.
				if ( ! isset( $cart_totals[ $cart_item_key ] ) || ! isset( $cart_totals[ $cart_item_key ]['qty'] ) ) {
					continue;
				}

				// Sanitize.
				$quantity = apply_filters( 'woocommerce_stock_amount_cart_item', wc_stock_amount( preg_replace( '/[^0-9\.]/', '', $cart_totals[ $cart_item_key ]['qty'] ) ), $cart_item_key );

				if ( '' === $quantity || $quantity === $values['quantity'] ) {
					continue;
				}

				// Update cart validation.
				$passed_validation = apply_filters( 'woocommerce_update_cart_validation', true, $cart_item_key, $values, $quantity );

				// is_sold_individually.
				if ( $_product->is_sold_individually() && $quantity > 1 ) {
					/* Translators: %s Product title. */
					wc_add_notice( sprintf( __( 'You can only have 1 %s in your cart.', 'woocommerce' ), $_product->get_name() ), 'error' );
					$passed_validation = false;
				}

				if ( $passed_validation ) {
					WC()->cart->set_quantity( $cart_item_key, $quantity, false );
					$cart_updated = true;
				}
			}
		}

		// Trigger action - let 3rd parties update the cart if they need to and update the $cart_updated variable.
		$cart_updated = apply_filters( 'woocommerce_update_cart_action_cart_updated', $cart_updated );

		if ( $cart_updated ) {
			WC()->cart->calculate_totals();
			wc_add_notice( __( 'Cart updated.', 'woocommerce' ), apply_filters( 'woocommerce_cart_updated_notice_type', 'success' ) );
		}

		wc_print_notices();
		wp_die();
	}

	public function wc_cart_display_tax_totals_html() {
		$value = '';

		if ( wc_tax_enabled() && WC()->cart->display_prices_including_tax() ) {
			$tax_string_array = array();
			$cart_tax_totals  = WC()->cart->get_tax_totals();

			if ( get_option( 'woocommerce_tax_total_display' ) === 'itemized' ) {
				foreach ( $cart_tax_totals as $code => $tax ) {
					$tax_string_array[] = sprintf( '%s %s', $tax->formatted_amount, $tax->label );
				}
			} elseif ( ! empty( $cart_tax_totals ) ) {
				$tax_string_array[] = sprintf( '%s', wc_price( WC()->cart->get_taxes_total( true, true ) ) );//, WC()->countries->tax_or_vat()
			}

			if ( ! empty( $tax_string_array ) ) {
				$taxable_address = WC()->customer->get_taxable_address();
				if ( WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping() ) {
					$country = WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ];
					/* translators: 1: tax amount 2: country name */
					$tax_text = wp_kses_post( sprintf( __( 'includes %1$s estimated for %2$s', 'woocommerce' ), implode( ', ', $tax_string_array ), $country ) );
				} else {
					/* translators: %s: tax amount */
					$tax_text = wp_kses_post( sprintf( __( 'Including %s in taxes.', 'ruby_studio' ), implode( ', ', $tax_string_array ) ) );
				}

				$value .= '<small class="includes_tax">' . $tax_text . '</small>';
			}
		}

		return $value;
	}

	public function wc_cart_display_shipping_totals_html() {
		$total_html     = '';
		$shipping_total = WC()->cart->get_shipping_total();

		if ( $shipping_total > 0 ) {

			if ( WC()->cart->display_prices_including_tax() ) {
				$total_html = wc_price( $shipping_total + WC()->cart->get_shipping_tax() );

				if ( WC()->cart->get_shipping_tax() > 0 && ! wc_prices_include_tax() ) {
					$total_html .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
				}

			} else {
				$total_html = wc_price( $shipping_total );

				if ( WC()->cart->get_shipping_tax() > 0 && wc_prices_include_tax() ) {
					$total_html .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
				}
			}
		} elseif ( $shipping_total == 0 ) {
			$total_html = sprintf( '<span class="woocommerce-Price-amount amount"><bdi>%s</bdi></span>',
				esc_html__( __( 'Free', 'woocommerce' ) ) );
		}

		return $total_html;
	}

	/**
	 * Wrap Shipping label into span
	 *
	 * @param $label
	 *
	 * @return string
	 */
	public function shipping_rate_label( $label ) {
		return sprintf( '<span class="shipping-title">%s</span>|', $label );
	}

	/**
	 * Display "Free" text for zero shipping cost
	 *
	 * @param $label
	 * @param $method
	 *
	 * @return mixed|string
	 */
	public function cart_shipping_method_full_label( $label, $method ) {
		$label = str_replace( ':', '', $label );

		if ( $method->cost == 0 ) {
			$label .= sprintf( '<span class="woocommerce-Price-amount amount"><bdi>%s</bdi></span>',
				esc_html__( 'Free', 'ruby_studio' ) );
		} else if ( $method->get_shipping_tax() > 0 && WC()->cart->display_prices_including_tax() && wc_prices_include_tax() ) {
			$label .= wc_show_text_with_tax_prefix( '' );
		}

		[ $title, $cost ] = explode( '|', $label );

		return $title . '<span class="shipping-price">' . $cost . '</span>';
	}

	public function cart_item_name( $item_name, $cart_item, $cart_item_key ) {
		global $cart_display_item_meta;

		if ( $cart_item['data']->is_type( 'variation' ) ) {
			$cart_display_item_meta = true;
			$item_name              = $cart_item['data']->get_title();
		} else {
			$cart_display_item_meta = false;
		}

		return $item_name;
	}

	public function is_attribute_in_product_name( $is_in_name, $attribute, $name ) {
		global $cart_display_item_meta;

		if ( $cart_display_item_meta ) {
			$is_in_name = false;
		}

		return $is_in_name;
	}

	/**
	 * Get quantity of product in the Cart
	 *
	 * @param int $product_id
	 *
	 * @return int
	 */
	public static function get_product_cart_contains( int $product_id ): int {
		$contains = 0;

		if ( WC()->cart->get_cart_contents_count() > 0 ) {
			foreach ( WC()->cart->get_cart_contents() as $cart_item ) {
				if ( $product_id == $cart_item['data']->get_id() ) {
					$contains = $cart_item['quantity'];

					break;
				}
			}
		}

		return $contains;
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}