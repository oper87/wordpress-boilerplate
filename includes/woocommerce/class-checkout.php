<?php
/**
 * Woocommerce Functions Class
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Checkout {

	/**
	 * Cart constructor.
	 */
	private function __construct() {

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

		/**
		 * Add empty cart
		 */
		add_filter( 'the_content', [ $this, 'show_empty_checkout' ], 20 );

		/**
		 * Add redirect from cart page to checkout
		 */
		add_action( 'template_redirect', [ $this, 'cart_template_redirect' ] );

		/**
		 * Disable redirect from checkout page
		 */
		add_filter( 'woocommerce_checkout_redirect_empty_cart', [ $this, 'checkout_redirect_empty_cart' ] );

		/**
		 * Modify Billing Fields
		 */
		add_filter( 'woocommerce_default_address_fields', [ $this, 'default_address_fields' ], 10 );
		add_filter( 'woocommerce_checkout_fields', [ $this, 'checkout_billing_shipping_fields' ], 20, 1 );
		add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );

		remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
		//remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
		remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
		remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
		remove_action( 'woocommerce_checkout_terms_and_conditions', 'wc_terms_and_conditions_page_content', 30 );
		add_action( 'woocommerce_checkout_order_review_items', 'woocommerce_order_review_items', 10 );
		add_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );

		add_filter( 'woocommerce_order_button_html', [ $this, 'woocommerce_order_button_html' ], 10 );
		add_filter( 'woocommerce_update_order_review_fragments', [ $this, 'woocommerce_update_order_review_fragments' ] );
		add_filter( 'woocommerce_shipping_rate_label', [ $this, 'shipping_rate_label' ] );
		add_filter( 'woocommerce_cart_shipping_method_full_label', [ $this, 'shipping_method_full_label' ] );

	}

	public function enqueue_scripts() {
		if ( wc_get_page_id( 'checkout' ) == wc_get_page_id( 'cart' ) && is_checkout() ) {
			if ( is_cart() ) {
				wp_deregister_script( 'wc-cart' );
			}
		}
	}

	public function show_empty_checkout( $content ) {
		if ( function_exists( 'is_checkout' ) && function_exists( 'WC' ) ) {
			if ( ( is_checkout() && empty( get_query_var( 'order-pay' ) ) && empty( get_query_var( 'order-received' ) ) ) && WC()->cart->is_empty() ) {
				ob_start();
				wc_get_template( 'cart/cart-empty.php' );
				$content = ob_get_clean();
			}
		}

		return $content;
	}

	public function cart_template_redirect() {
		if ( ! is_checkout() && is_cart() && wc_get_page_id( 'cart' ) === - 1 ) {
			wp_safe_redirect( wc_get_checkout_url() );
			exit;
		}
	}

	public function checkout_redirect_empty_cart( $redirect ) {
		if ( wc_get_page_id( 'checkout' ) !== wc_get_page_id( 'cart' ) ) {
			$redirect = false;
		}

		return $redirect;
	}

	/**
	 * Update order review fragments
	 *
	 * @return void
	 */
	public function woocommerce_update_order_review_fragments( $fragments ) {
		ob_start();
		woocommerce_order_review_items();
		$woocommerce_order_review_items = ob_get_clean();

		$fragments['.woocommerce-checkout-review-order-items-table'] = $woocommerce_order_review_items;

		return $fragments;
	}

	public function woocommerce_order_button_html( $button_html ) {
		$button_html = str_replace( 'class="', 'class="button--color_green ', $button_html );

		return $button_html;
	}

	/**
	 * Override default address fields
	 *
	 * @param $fields
	 *
	 * @return array
	 */
	public function default_address_fields( $fields ): array {

		// Removes
		unset( $fields['address_2'] );

		// Labels
		$fields['company']['label']   = __( 'Company', 'ruby_studio' );
		$fields['address_1']['label'] = __( 'Address (full)', 'ruby_studio' );
		$fields['city']['label']      = __( 'City', 'ruby_studio' );
		$fields['country']['label']   = __( 'Country', 'ruby_studio' );
		$fields['postcode']['label']  = __( 'Zip code', 'ruby_studio' );

		// Placeholders
		$fields = array_map( [ $this, 'address_field_placeholder' ], $fields );

		// Priorities
		$fields['address_1']['priority'] = 40;
		$fields['city']['priority']      = 50;
		$fields['country']['priority']   = 60;
		$fields['postcode']['priority']  = 70;

		// Row width
		if ( ( $key = array_search( 'form-row-wide', $fields['country']['class'] ) ) !== false ) {
			unset( $fields['country']['class'][ $key ] );
		}

		if ( ( $key = array_search( 'form-row-wide', $fields['postcode']['class'] ) ) !== false ) {
			unset( $fields['postcode']['class'][ $key ] );
		}

		$fields['country']['class']  = array_merge( [ 'form-row-first' ], $fields['country']['class'] );
		$fields['postcode']['class'] = array_merge( [ 'form-row-last' ], $fields['postcode']['class'] );

		return $fields;
	}

	/**
	 * Override checkout billing and shipping fields
	 *
	 * @param $fields
	 *
	 * @return array
	 */
	public function checkout_billing_shipping_fields( $fields ): array {

		// Labels
		$fields['billing']['billing_email']['label'] = __( 'Email', 'ruby_studio' );

		// Placeholders
		$fields['billing'] = array_map( [ $this, 'address_field_placeholder' ], $fields['billing'] );

		return $fields;
	}

	/**
	 * Set field placeholders as labels
	 *
	 * @param $field
	 *
	 * @return mixed
	 */
	public function address_field_placeholder( $field ) {
		$field['placeholder'] = $field['label'];

		if ( ! $field['required'] ) {
			$field['placeholder'] .= ' ' . __( '(optional)', 'ruby_studio' );
		}

		return $field;
	}

	/**
	 * Wrap Shipping label into span
	 *
	 * @param $label
	 *
	 * @return string
	 */
	public function shipping_rate_label( $label ) {
		return sprintf( '<span class="shipping-title">%s</span>', $label );
	}

	/**
	 * Remove colon symbol
	 *
	 * @param $label
	 *
	 * @return string
	 */
	public function shipping_method_full_label( $label ) {
		return str_replace( ':', '', $label );
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}