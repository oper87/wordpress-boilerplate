<?php
/**
 * Core Class for Woocommerce
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

include_once 'wc-functions.php';

class Core {

	/**
	 * Settings constructor.
	 */
	public function __construct() {

		// Add theme supports
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );

		add_filter( 'woocommerce_enqueue_styles', [ $this, 'enqueue_front_styles' ] );
		add_filter( 'woocommerce_before_single_product', [ $this, 'set_single_product_loop_name' ], 10, 2 );
		add_filter( 'woocommerce_post_class', [ $this, 'add_columns_classes' ], 10, 2 );

		// Remove default wrappers.
		remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper' );
		remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end' );

		// Add custom wrappers.
		add_action( 'woocommerce_before_main_content', [ $this, 'output_content_wrapper' ] );
		add_action( 'woocommerce_after_main_content', [ $this, 'output_content_wrapper_end' ] );

		// Configure breadcrumb
		if ( defined( 'WC_BREADCRUMBS' ) && false == WC_BREADCRUMBS_ENABLED ) {
			remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
		} else {
			add_filter( 'woocommerce_breadcrumb_defaults', [ $this, 'breadcrumb_settings' ] );
			add_filter( 'woocommerce_get_breadcrumb', [ $this, 'breadcrumb_items' ] );

			if ( defined( 'WC_BREADCRUMBS' ) && false == WC_BREADCRUMBS ) {
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				add_action( 'woocommerce_before_main_content', [ $this, 'add_yoast_breadcrumbs' ], 20 );
			}
		}

		//add_filter( 'woocommerce_admin_features', [ $this, 'remove_admin_features' ] );

		/**
		 * Change a currency symbol
		 */
		add_filter( 'woocommerce_currency_symbol', [ $this, 'currency_symbol' ], 10, 2 );

		/**
		 * Disable dashboard metaboxes
		 */
		add_action( 'wp_dashboard_setup', [ $this, 'remove_dashboard_metaboxes' ], 100 );
	}

	/**
	 * Add Ruby Studio theme support
	 */
	public function add_theme_support() {

		/**
		 * Add Single Gallery options
		 */
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

		/**
		 * Add Catalog options
		 */
		add_theme_support(
			'woocommerce',
			[
				'thumbnail_image_width'         => 300,
				'single_image_width'            => 600,
				'gallery_thumbnail_image_width' => 150,
				'product_grid'                  => [
					'min_columns'     => 1,
					'max_columns'     => 6,
					'default_columns' => 4,
					'min_rows'        => 1,
					'max_rows'        => 6,
					'default_rows'    => 4,
				],
				'product_blocks'                => [
					'min_columns'     => 1,
					'max_columns'     => 6,
					'default_columns' => 4,
					'min_rows'        => 1,
					'max_rows'        => 6,
					'default_rows'    => 4,
				],
				'featured_block'                => [
					'min_height'     => 500,
					'default_height' => 500,
				],
			]
		);

		add_image_size( 'woocommerce_thumbnail_x2', 600, '', true );
		add_image_size( 'woocommerce_single_x2', 1200, '', false );

		add_filter('woocommerce_single_product_zoom_enabled', '__return_false');
		add_filter('woocommerce_single_product_photoswipe_enabled', '__return_false');

	}

	/**
	 * Enqueue WC Front Styles
	 *
	 * @param $styles
	 *
	 * @return mixed
	 */
	public function enqueue_front_styles( $styles ) {

		if ( isset( $styles['woocommerce-layout'] ) ) {
			unset( $styles['woocommerce-layout'] );
		}

		if ( isset( $styles['woocommerce-smallscreen'] ) ) {
			unset( $styles['woocommerce-smallscreen'] );
		}

		if ( isset( $styles['woocommerce-general'] ) ) {
			unset( $styles['woocommerce-general'] );
		}

		return $styles;
	}

	public function set_single_product_loop_name() {
		wc_set_loop_prop( 'name', 'single' );
	}

	/**
	 * WooCommerce Post Class filter.
	 *
	 * @param array $classes Array of CSS classes.
	 * @param object $product Product object.
	 *
	 * @return array
	 * @since 3.6.2
	 */
	public function add_columns_classes( array $classes, object $product ) {
		if ( wc_get_loop_prop( 'name' ) !== 'single' ) {
			$classes[] = 'xsmall-12';
			$classes[] = 'small-6';
			$classes[] = 'large-' . esc_attr( 12 / wc_get_loop_prop( 'columns' ) );
			$classes[] = 'columns';
		}

		return $classes;
	}

	/**
	 * Open the Ruby Studio wrapper.
	 */
	public function output_content_wrapper() {
		echo '<main id="main" class="site-main shop-page" role="main">';

		do_action( 'woocommerce_before_main_content_wrapper' );

		echo '<div class="container woocommerce-wrapper">';
	}

	/**
	 * Close the Ruby Studio wrapper.
	 */
	public function output_content_wrapper_end() {
		echo '</div>';

		do_action( 'woocommerce_after_main_content_wrapper' );

		echo '</main>';
	}

	public function subcategory_archive_thumbnail_size( $thumbnail_size ) {
		return 'thumb_category';
	}

	public function get_image_size_thumb_category( $size ) {
		$sizes = wp_get_additional_image_sizes();

		if ( isset( $sizes['thumb_category'] ) ) {
			$size = $sizes['thumb_category'];
		}

		return $size;
	}

	/**
	 * Change breadcrumbs arguments
	 *
	 * @param $args
	 *
	 * @return array
	 */
	public function breadcrumb_settings( $args ) {
		$args = [
			'delimiter'   => '&nbsp;›&nbsp;',
			'wrap_before' => '<p id="breadcrumbs" class="woocommerce-breadcrumbs">',
			'wrap_after'  => '</p>',
			'before'      => '',
			'after'       => '',
			'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
		];

		return $args;
	}

	/**
	 * Add Shop page to breadcrumbs
	 *
	 * @param $items
	 *
	 * @return array
	 */
	public function breadcrumb_items( $items ) {
		$shop_page_id = wc_get_page_id( 'shop' );

		if ( is_product() || is_product_category() ) {
			$items = Helper::array_insert( $items, 0, [
				[
					get_the_title( $shop_page_id ),
					get_permalink( $shop_page_id ),
				],
			] );
		}

		return $items;
	}

	/**
	 * Add Yoast breadcrumbs instead of default Woocommerce breadcrumbs
	 */
	public function add_yoast_breadcrumbs() {
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<p id="breadcrumbs" class="yoast-breadcrumbs">', '</p>' );
		}
	}

	/**
	 * Filter list of features and remove those not needed
	 *
	 * @param $features
	 *
	 * @return array
	 */
	public function remove_admin_features( $features ) {
		return array_values(
			array_filter( $features, function ( $feature ) {
				return $feature !== 'marketing' && $feature !== 'analytics';
			} )
		);
	}

	/**
	 * Change currency symbol to currency code
	 *
	 * @param $currency_symbol
	 * @param $currency
	 *
	 * @return mixed
	 */
	public function currency_symbol( $currency_symbol, $currency ) {
		switch ( $currency ) {
			case 'DKK':
			case 'EUR':
			case 'USD':
				$currency_symbol = $currency;
				break;
			default:
				//$currency_symbol = $currency;
				break;
		}

		return $currency_symbol;
	}

	public function remove_dashboard_metaboxes() {
		remove_meta_box( 'dashboard_rediscache', 'dashboard', 'normal' );
		remove_meta_box( 'woocommerce_dashboard_recent_reviews', 'dashboard', 'normal' );
		remove_meta_box( 'woocommerce_dashboard_status', 'dashboard', 'normal' );
	}

}