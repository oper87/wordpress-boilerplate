<?php
/**
 * Woocommerce Functions Class
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Functions {

	/**
	 * __construct
	 */
	public function __construct() {
		add_filter( 'woocommerce_form_field_checkbox', [ $this, 'form_field_checkbox' ], 10, 4 );
	}

	/**
	 * Modify checkbox field layout
	 *
	 * @param $field
	 * @param $key
	 * @param $args
	 * @param $value
	 *
	 * @return array|string|string[]
	 */
	public function form_field_checkbox( $field, $key, $args, $value ) {
		$field = str_replace( '"woocommerce-input-wrapper"', '"woocommerce-input-wrapper custom-checkbox"', $field );

		preg_match( '/<input(.*?)>/is', $field, $output_array );

		if ( ! empty( $output_array[0] ) ) {
			$field = str_replace( $output_array[0], '', $field );
			$field = str_replace( '<label', $output_array[0] . '<label for="' . $key . '"', $field );
		}

		$field = preg_replace( '/(&nbsp;<span class=\"optional\">(.*)<\/span>)/U', '', $field );
		$field = str_replace( $args['label'], '<span>' . $args['label'] . '</span>', $field );

		return $field;
	}

	/**
	 * Get user location
	 *
	 * @param $key
	 *
	 * @return array|string
	 */
	public static function get_user_geo_location( $key = false ) {
		$user_geolocate = \WC_Geolocation::geolocate_ip( '', true, true ); // Get geolocated user data.

		if ( defined( 'DEBUG_COUNTRY' ) && false !== DEBUG_COUNTRY ) {
			$user_geolocate['country'] = DEBUG_COUNTRY;
		}

		if ( false !== $key && isset( $user_geolocate[ $key ] ) ) {
			return $user_geolocate[ $key ];
		}

		return $user_geolocate;
	}

	/**
	 * Get user IP
	 *
	 * @return string
	 */
	public static function get_user_ip_address() {
		return \WC_Geolocation::get_ip_address(); // Get geolocated user data.
	}

	/**
	 * Get Next Product Category
	 *
	 * @param $term_id
	 * @param $category
	 *
	 * @return array|object|void|null
	 */
	public static function get_next_category( $term_id, $category ) {
		global $wpdb;

		$query = "
			SELECT t.*, tt.*
			FROM {$wpdb->terms} t
			INNER JOIN {$wpdb->term_taxonomy} tt ON (t.term_id = tt.term_id)
       		INNER JOIN {$wpdb->termmeta} tm ON (tm.term_id = tt.term_id AND tm.meta_key = 'order')
       		WHERE tt.taxonomy = %s AND tt.parent = %d AND tt.term_id > %d 
       			AND (t.slug NOT LIKE 'uncategorized%' 
       				AND t.slug NOT LIKE 'unkategorisiert%' 
       				AND t.slug NOT LIKE 'bez-kategorii%')
           	ORDER BY CAST(tm.meta_value AS SIGNED) ASC, tt.term_id ASC
           	LIMIT 1";

		return $wpdb->get_row( $wpdb->prepare( $query, $category->taxonomy, $category->parent, $term_id, $term_id ) );
	}

	/**
	 * Get children categories
	 *
	 * @param $product
	 *
	 * @return mixed|\WP_Term
	 */
	public static function get_product_child_term( $product ) {
		$terms = get_the_terms( $product->get_id(), 'product_cat' );

		$terms = array_map( function ( $term ) {
			return $term->parent != 0 ? $term : false;
		}, $terms );

		$terms = array_filter( $terms );

		return array_shift( $terms );
	}

}