<?php
/**
 * Order
 */

namespace App\WooCommerce;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Order {

	/**
	 * __construct
	 */
	protected function __construct() {
		add_filter( 'woocommerce_get_order_item_totals', [ $this, 'get_order_item_totals' ] );
		add_filter( 'woocommerce_order_item_get_formatted_meta_data', [ $this, 'order_item_get_formatted_meta_data' ], 10, 2 );
	}

	public function get_order_item_totals( $total_rows ) {
		if ( isset( $total_rows['payment_method'] ) ) {
			unset( $total_rows['payment_method'] );
		}

		return $total_rows;
	}

	public function order_item_get_formatted_meta_data( $formatted_meta, $order_item ) {

		foreach ( $formatted_meta as &$value ) {

			if ( $value->key == 'subscription' ) {
				$is_parent_order = wcs_order_contains_subscription( $order_item->get_order(), 'parent' );
				$subscriptions   = wcs_get_subscriptions_for_order( $order_item->get_order(), array( 'order_type' => 'any' ) );

				if ( ! empty( $subscriptions ) ) {
					$subscription = current( $subscriptions );

					if ( $is_parent_order && $subscription->get_time( 'next_payment' ) > 0 ) :
						$value->display_key   = '';
						$value->display_value = sprintf(
							esc_html__( 'Next payment: %s', 'woocommerce-subscriptions' ),
							esc_html( date_i18n( wc_date_format(), $subscription->get_time( 'next_payment', 'site' ) ) )
						);
					endif;
				}
				break;
			}

		}

		return $formatted_meta;
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}