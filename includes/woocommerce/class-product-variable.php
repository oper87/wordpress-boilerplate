<?php
/**
 * Sample Class for Woocommerce
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Product_Variable {

	private function __construct() {

		add_filter( 'woocommerce_variation_option_name', [ $this, 'display_price_in_variation_option_name' ], 10, 4 );

	}

	/**
	 * Add price to variation dropdown
	 *
	 * @param $term
	 *
	 * @return mixed|string
	 */
	public function display_price_in_variation_option_name( $option, $term, $attribute, $product ) {
		if ( empty( $option ) ) {
			return $option;
		}

		if ( ! $product->is_type( 'variable' ) ) {
			return $option;
		}

		$product_variations   = $product->get_children();
		$variation_attributes = $product->get_variation_attributes();

		if ( ! isset( $variation_attributes[ $attribute ] ) ) {
			return $option;
		}

		$variation_attributes = $variation_attributes[ $attribute ];
		$variation_attribute  = array_search( $option, $variation_attributes );

		if ( ! isset( $product_variations[ $variation_attribute ] ) ) {
			return $option;
		}

		$product = wc_get_product( $product_variations[ $variation_attribute ] );

		if ( $product ) {
			$option .= ' (' . wp_kses( wc_price( $product->get_price() ), [] ) . ')';
		}

		return $option;
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}