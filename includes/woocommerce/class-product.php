<?php
/**
 * Sample Class for Woocommerce
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Product {

	const POST_TYPE = 'product';

	private function __construct() {
		//add_filter( 'woocommerce_hide_invisible_variations', '__return_false' );
		remove_filter( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

		//add_filter( 'woocommerce_product_single_add_to_cart_text', [ $this, 'product_single_add_to_cart_text' ] );
		add_filter( 'woocommerce_quantity_input_args', [ $this, 'woocommerce_quantity_input_args' ], 10, 3 );
		add_filter( 'woocommerce_product_supports', [ $this, 'woocommerce_product_supports' ], 10, 3 );
		add_filter( 'woocommerce_after_add_to_cart_button', [ $this, 'add_ajax_attribute_to_cart_button' ] );
	}

	/**
	 * @param bool $supports
	 * @param $feature
	 * @param $product
	 *
	 * @return bool
	 */
	public function woocommerce_product_supports( bool $supports, $feature, $product ): bool {

		if ( $feature !== 'ajax_add_to_cart' ) {
			return $supports;
		}

		if ( defined( 'WC_AJAX_ADD_TO_CART' ) && ! WC_AJAX_ADD_TO_CART ) {
			$supports = false;
		}

		if ( $product->is_type( 'simple' ) || $product->is_type( 'variable' ) ) {
			$supports = true;
		}

		return $supports;
	}

	/**
	 * Change add to cart button text
	 *
	 * @param string $text
	 *
	 * @return string
	 */
	public function add_to_cart_text( string $text, $product ): string {

		if ( $product->get_stock_quantity() == get_product_cart_contains( $product->get_id() ) ) {
			return esc_html__( 'Added to basket', 'ruby_studio' );
		}

		return esc_html__( 'Purchase', 'ruby_studio' );
	}

	/**
	 * Determine where the quantity input should be hidden
	 *
	 * @param array $args
	 *
	 * @return array
	 */
	public function woocommerce_quantity_input_args( array $args ): array {
		$args['app_quantity_hidden'] = true;

		return $args;
	}

	/**
	 * Add hidden input
	 *
	 * @return void
	 */
	public function add_ajax_attribute_to_cart_button() {
		global $product;

		if ( $product->supports( 'ajax_add_to_cart' ) ) {
			echo '<input type="hidden" name="ajax_add_to_cart" value="1"/>';
		}
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}
