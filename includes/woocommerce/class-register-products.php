<?php
/**
 * Register Product Types for Woocommerce
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

use App\Helper;
use App\WooCommerce\Attributes;
use WC_Product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Register_Products extends WC_Product {

	public function __construct() {
		//add_action( 'init', [ $this, 'wc_init' ] );
		//add_filter( 'product_type_selector', [ $this, 'product_type_selector' ], 10 );
	}

	function product_type_selector( $types ) {
		$types['custom-product-type'] = __( 'Custom product', 'ruby_studio_tmp' );

		return $types;
	}

	public function wc_init() {
		//require_once 'class-wc-custom-product-type.php';
	}

}
