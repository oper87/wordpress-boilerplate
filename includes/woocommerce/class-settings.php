<?php
/**
 * Settings Class for Woocommerce
 *
 * @version 1.0.0
 */

namespace App\WooCommerce;

use App\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Settings {

	/**
	 * Settings constructor.
	 */
	public function __construct() {
		add_filter( 'woocommerce_countries', [ $this, 'woocommerce_countries' ] );
	}

	/**
	 * @param $countries
	 *
	 * @return mixed
	 */
	function woocommerce_countries( $countries ) {
		foreach ( $countries as &$country ) {
			$country = trim( str_replace( [ '(UK)', '(US)' ], '', $country ) );
		}

		return $countries;
	}

}