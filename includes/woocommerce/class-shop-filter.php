<?php
/**
 * WooCommerce Shop Filter Class
 */

namespace App\WooCommerce;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Abstracts;
use App\Helper;
use WP_Meta_Query;
use WP_Tax_Query;

class Shop_Filter extends Abstracts\Filter {

	public function __construct() {
		parent::__construct( Product::POST_TYPE );
		$this->hook_init();

		add_action( 'wp', [ $this, 'activate_filter' ], 10 );
		//add_action( 'pre_get_posts', [ $this, 'set_posts_per_page' ] );
		add_action( 'ruby_theme_ajax_header_response', [ $this, 'ajax_header_response' ] );
	}

	public function set_array_filters() {
		global $wpdb;

		$this->array_filters = [
			'categories' => [
				'name'        => __( 'Filter by Category', 'ruby_studio' ),
				'query_from'  => 'taxonomy',
				'data_from'   => 'taxonomy',
				'taxonomy'    => 'product_cat',
				'choose_type' => 'multiple',
			],
			'sizes'      => [
				'name'        => __( 'Filter by Size', 'ruby_studio' ),
				'query_from'  => 'taxonomy',
				'data_from'   => 'taxonomy',
				'taxonomy'    => 'pa_stoerrelse',
				'choose_type' => 'multiple',
			],
			'colors'     => [
				'name'        => __( 'Filter by Color', 'ruby_studio' ),
				'query_from'  => 'taxonomy',
				'data_from'   => 'taxonomy',
				'taxonomy'    => 'pa_farve',
				'choose_type' => 'multiple',
			],
			'designers'  => [
				'name'        => __( 'Filter by Designer', 'ruby_studio' ),
				'query_from'  => 'taxonomy',
				'data_from'   => 'taxonomy',
				'taxonomy'    => 'pa_designer',
				'choose_type' => 'multiple',
			],
			'prices'     => [
				'name'           => __( 'Filter by Price', 'ruby_studio' ),
				'query_from'     => 'custom_db_table',
				'data_from'      => 'custom_db_table',
				'data_db_table'  => 'wc_product_meta_lookup',
				'data_field_id'  => 'product_id',
				'data_field_key' => 'min_max_price',
				'choose_type'    => 'multiple',
			],
		];
	}

	public function activate_filter() {
		global $wp_query;

		if ( is_admin() ) {
			return;
		}

		if ( ! $wp_query->get( 'app_filter_posts' ) ) {
			return;
		}


		self::getInstance()->set_filters();

		if ( $this->filter_has_any_term ) {
			$this->set_active( true );
		}
	}

	public function is_run_pre_get_posts( $q ) {
		// We only want to affect the main query.

		if ( is_admin() ) {
			return false;
		}

		if ( ! $q->is_main_query() ) {
			return false;
		}

		if ( ! $q->is_post_type_archive( Product::POST_TYPE ) && ! $q->is_tax( get_object_taxonomies( Product::POST_TYPE ) ) ) {
			return false;
		}

		self::getInstance()->set_array_filters();

		return true;
	}

	public function ajax_header_response() {
		echo sprintf( '<span id="wp_shop_data_filters">%s</span>', esc_attr( wp_json_encode( $this->get_filters() ) ) ) . "\n";
	}

	public function get_terms( $taxonomy, $query_key = false ) {
		$terms = parent::get_terms( $taxonomy, $query_key );

		if ( $taxonomy == 'product_cat' ) {
			$_terms = [];

			foreach ( $terms as $term ) {
				if ( $term->parent == 0 ) {
					$_terms[] = $term;
				}
				$term = null;
			}

			$terms = array_values( $_terms );
		}

		if ( $taxonomy == 'pa_stoerrelse' ) {
			$hidden_terms = [ '0', '36', '36-5', '37', '37-5', '38', '38-5', '39', '39-5', '40', '40-5', 'dk32', 'us10', 'us4', 'us6', 'us8' ];

			foreach ( $terms as $k => $term ) {
				if ( in_array( $term->slug, $hidden_terms ) ) {
					unset( $terms[ $k ] );
				}
			}
		}

		return $terms;
	}

	public function get_custom_prices_db_table_terms( $data ) {
		global $wpdb;

		$query_object = get_queried_object();

		$query_table      = $wpdb->prefix . esc_sql( $data['data_db_table'] );
		$query_select_key = esc_sql( $data['data_field_key'] );
		$query_select_id  = esc_sql( $data['data_field_id'] );

		$query_object = get_queried_object();
		$tax_query    = $this->get_main_tax_query();
		$meta_query   = $this->get_main_meta_query();
		//$custom_query = $this->get_main_custom_query();

		if ( $tax_query ) {
			//echo '<pre>'; print_r( $tax_query ); echo '</pre>';
			//$tax_query = $this->unset_taxonomy_meta_query($tax_query, Tax_Diet::TAXONOMY);
			//$tax_query = $this->unset_taxonomy_meta_query($tax_query, Tax_Season::TAXONOMY);
		}

		$tax_query      = new \WP_Tax_Query( $tax_query );
		$meta_query     = new \WP_Meta_Query( $meta_query );
		$tax_query_sql  = $tax_query->get_sql( $wpdb->posts, 'ID' );
		$meta_query_sql = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
		//$custom_query_sql = $this->get_custom_query_sql( $custom_query );

		$sql = "
			SELECT min( min_price ) as min_price, MAX( max_price ) as max_price
			FROM {$wpdb->wc_product_meta_lookup}
			WHERE product_id IN (
				SELECT ID FROM {$wpdb->posts}
				" . $tax_query_sql['join'] . $meta_query_sql['join'] . "
				WHERE {$wpdb->posts}.post_type IN ('" . implode( "','", array_map( 'esc_sql', apply_filters( 'woocommerce_price_filter_post_type', array( 'product' ) ) ) ) . "')
				AND {$wpdb->posts}.post_status = 'publish'
				" . $tax_query_sql['where'] . $meta_query_sql['where'] . '
			)';

		$prices    = $wpdb->get_row( $sql );
		$min_price = $prices->min_price;
		$max_price = $prices->max_price;

		// Check to see if we should add taxes to the prices if store are excl tax but display incl.
		$tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

		/*if ( wc_tax_enabled() && ! wc_prices_include_tax() && 'incl' === $tax_display_mode ) {
			$tax_class = apply_filters( 'woocommerce_price_filter_widget_tax_class', '' ); // Uses standard tax class.
			$tax_rates = \WC_Tax::get_rates( $tax_class );

			if ( $tax_rates ) {
				$min_price += \WC_Tax::get_tax_total( \WC_Tax::calc_exclusive_tax( $min_price, $tax_rates ) );
				$max_price += \WC_Tax::get_tax_total( \WC_Tax::calc_exclusive_tax( $max_price, $tax_rates ) );
			}
		}*/

		$options    = [];
		$values     = [
			'less-100'  => [ 'less', 100 ],
			'100-600'   => [ 100, 600 ],
			'600-1100'  => [ 600, 1100 ],
			'1100-1600' => [ 1100, 1600 ],
			'1600-2100' => [ 1600, 2100 ],
			'2100-more' => [ 2100, 'more' ]
		];
		$price_args = [ 'decimals' => 0 ];

		foreach ( $values as $slug => $option ) {

			if ( $option[0] == 'less' && ( $max_price - $option[1] ) > 0 && $min_price <= $option[1] ) {
				$options[] = (object) [
					'slug' => $slug,
					'name' => sprintf( '%s - %s', wc_price( 10, $price_args ), wc_price( $option[1], $price_args ) )
				];
			} else if ( $option[1] == 'more' && $max_price > $option[0] ) {
				$options[] = (object) [
					'slug' => $slug,
					'name' => sprintf( '%s+', wc_price( $option[0], $price_args ) )
				];
			} else if(  $option[0] != 'less' &&  $option[1] != 'more' ) {
				if ( ( ( $max_price - $option[1] ) > 0 || ( $max_price >= $option[0] ) ) && $min_price <= $option[1] ) {
					$options[] = (object) [
						'slug' => $slug,
						'name' => sprintf( '%s - %s', wc_price( $option[0], $price_args ), wc_price( $option[1], $price_args ) )
					];
				}
			}
		}

		return $options;
	}

	public function custom_filter_query_post_clauses( $args, $query ) {
		if ( $query->is_main_query() && $query->get( 'custom_query' ) ) {
			$custom_query_sql = $this->get_custom_query_sql( $query->get( 'custom_query' ) );

			$args['join']  .= $custom_query_sql['join'];
			$args['where'] .= $custom_query_sql['where'];
		}

		return $args;
	}

	public function get_custom_query_sql( $query ) {
		global $wpdb;

		$join_sql  = [];
		$where_sql = [];

		if ( empty( $query ) ) {
			return parent::get_custom_query_sql( $query );
		}

		foreach ( $query as $custom_query ) {
			$query_table     = esc_sql( $wpdb->prefix . $custom_query['data_db_table'] );
			$query_field_key = esc_sql( $custom_query['data_field_key'] );
			$query_field_id  = esc_sql( $custom_query['data_field_id'] );

			$join_sql[ $custom_query['data_db_table'] ] = " LEFT JOIN {$query_table} ON ({$wpdb->posts}.ID = {$query_table}.{$query_field_id}) ";

			$where_sql_values = [];

			/*echo '<pre>';
			print_r( $custom_query );
			echo '</pre>';*/

			if ( $custom_query['data_field_key'] == 'min_max_price' && ! empty( $custom_query['data_values'] ) ) {
				foreach ( $custom_query['data_values'] as $data_values ) {
					$values_cond = [];

					list( $min, $max ) = explode( '-', $data_values );

					if ( $min != 'less' ) {
						$min_cond      = $max == 'more' ? '>' : '>=';
						$values_cond[] = $wpdb->prepare( "({$query_table}.min_price {$min_cond} %d)", $min );
					}

					if ( $max != 'more' ) {
						$max_cond      = $min == 'less' ? '<' : '<=';
						$values_cond[] = $wpdb->prepare( "({$query_table}.max_price {$max_cond} %d)", $max );
					}

					$where_sql_values[] = "(" . join( ' AND ', $values_cond ) . ")";
				}
			}

			if ( ! empty( $where_sql_values ) ) {
				$where_sql[] = " AND (" . join( ' OR ', $where_sql_values ) . ")";
			}
		}


		return [
			'join'  => join( ' ', $join_sql ),
			'where' => join( ' ', $where_sql )
		];
	}


	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}

}