<?php
/**
 * Shop Class modifies WooCommerce Shop/Categories/Search pages
 */

namespace App\WooCommerce;

use App\WooCommerce\Product;
use App\WooCommerce\Functions;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Shop {

	/**
	 * Settings constructor.
	 */
	public function __construct() {

		//Shop_Filter::getInstance();

		// Remove Shop defaults
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

		add_action( 'woocommerce_before_main_content', [ $this, 'woocommerce_shop_page_title' ], 100 );

		// Modify query variables for search page
		//add_action( 'pre_get_posts', [ $this, 'add_search_only_by_shop' ], 0 );
		//add_filter( 'body_class', [ $this, 'add_woocommerce_class_on_search' ] );
	}

	/**
	 * Modify WordPress search and allow search only by product
	 *
	 * @param $query
	 */
	public function add_search_only_by_shop( $query ) {
		if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
			$query->set( 'post_type', Product::POST_TYPE );
			$query->set( 'posts_per_page', apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() ) );
		}
	}

	/**
	 * Add woocommerce to the body on search page.
	 * For search page when display only woocommerce products
	 *
	 * @param $classes
	 *
	 * @return mixed
	 */
	public function add_woocommerce_class_on_search( $classes ) {
		if ( is_search() ) {
			$classes[] = 'woocommerce';
		}

		return $classes;
	}

	/**
	 * Output page title
	 *
	 * @return string
	 */
	public function woocommerce_shop_page_title() {
		$object = get_queried_object();

		if ( is_shop() ) {
			$title = get_the_title( wc_get_page_id( 'shop' ) );
		} elseif ( is_a( $object, 'WP_Term' ) ) {
			$title = $object->name;
		}

		if ( ! empty( $title ) ) {
			echo '<h1 class="align-center">' . $title . '</h1>';
		}

	}
}
