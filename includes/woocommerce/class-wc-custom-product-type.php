<?php
/**
 * Furniture Product Type for Woocommerce
 *
 * @version 1.0.0
 */

//namespace App\WooCommerce;

use App\Helper;
use App\WooCommerce\Attributes;
use App\WooCommerce\Product;
use App\WooCommerce\Product_Filter;
use App\WooCommerce\Generate_Image;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WC_Product_Color_Samples extends WC_Product {

	public function __construct( $product ) {
		$this->product_type = 'color_samples';

		parent::__construct( $product );
	}

}