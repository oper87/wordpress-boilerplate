<?php
/**
 * Woocommerce Functions Class
 *
 * @version 1.0.0
 */

namespace App\WooCommerce\Emails;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Helper;

class Email_Settings {

	/**
	 * Cart constructor.
	 */
	function __construct() {

		$emails = [
			'customer_completed_order',
			'customer_processing_order',
		];

		foreach ( $emails as $email_id ) {
			add_filter( 'woocommerce_settings_api_form_fields_' . $email_id, function ( $feilds ) use ( $email_id ) {
				return $this->email_form_fields_add_content_editor( $feilds, $email_id );
			} );
		}

		add_filter( 'woocommerce_generate_content_wysiwyg_html', [ $this, 'content_wysiwyg_email_field' ], 10, 4 );
		add_filter( 'woocommerce_email_content', [ $this, 'woocommerce_email_content' ], 10, 4 );
	}

	public function email_form_fields_add_content_editor( $fields, $email_id ) {
		$placeholder_text = '';
		$placeholders     = [
			'{first_name}' => '',
			'{last_name}'  => '',
		];

		if ( $email_id == 'customer_completed_order' || $email_id == 'customer_processing_order' ) {
			$placeholders['{tracking_link content="" link_text=""}'] = '';
			$placeholders['{delivery_time content=""}']              = '';
		}

		if ( ! empty( $fields['heading']['description'] ) ) {
			$placeholder_text = $fields['heading']['description'];
		} elseif ( ! empty( $fields['heading_partial']['description'] ) ) {
			$placeholder_text = $fields['heading_partial']['description'];
		}

		if ( $placeholder_text ) {
			$placeholder_text = $placeholder_text . ', <code>' . implode( '</code>, <code>', array_keys( $placeholders ) ) . '</code>';
		} else {
			$placeholder_text = sprintf( __( 'Available placeholders: %s', 'woocommerce' ), '<code>' . implode( '</code>, <code>', array_keys( $placeholders ) ) . '</code>' );
		}

		$new_field = [
			'content' => array(
				'title'       => __( 'Email content', 'woocommerce' ),
				'type'        => 'content_wysiwyg',
				'description' => $placeholder_text,
				'default'     => '',
				'class'       => 'content_wysiwyg',
				'desc_tip'    => true,
			),
		];

		return Helper::array_insert( $fields, 'additional_content', $new_field, 'before' );
	}

	/**
	 * @param \WC_Order|object|array|null $order
	 * @param bool $sent_to_admin
	 * @param bool $plain_text
	 * @param \WC_Email $email
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function woocommerce_email_content( $order, $sent_to_admin, $plain_text, $email ) {
		$tracking_link = '';
		$delivery_time = '';

		if ( is_a( $order, 'WC_Order' ) ) {
			$first_name    = $order->get_billing_first_name();
			$last_name     = $order->get_billing_first_name();
			$tracking_link = $order->get_meta( 'tracking_link' );
			$delivery_time = $order->get_meta( 'delivery_time' );
		} elseif ( is_array( $order ) ) {
			$order         = wc_get_order( $order['id'] );
			$customer      = new \WC_Customer( $order['user_id'] );
			$first_name    = $customer->get_billing_first_name();
			$last_name     = $customer->get_billing_first_name();
			$tracking_link = $order->get_meta( 'tracking_link' );
			$delivery_time = $order->get_meta( 'delivery_time' );
		} elseif ( $email->id === 'customer_new_account' ) {
			$first_name = get_user_meta( $email->object->ID, 'first_name', true );
			$last_name  = get_user_meta( $email->object->ID, 'last_name', true );

			if ( empty( $first_name ) ) {
				$first_name = $email->user_login;
				$last_name  = '';
			}
		} else {
			$first_name = '';
			$last_name  = '';
		}

		$placeholders = [
			'first_name'    => $first_name,
			'last_name'     => $last_name,
			'tracking_link' => $tracking_link,
			'delivery_time' => $delivery_time,
		];

		$content = $email->get_option( 'content' );

		preg_match_all( '/{(.*?)(?: (.*?))?}/m', $content, $matches );

		if ( $matches ) {
			foreach ( $matches[0] as $key => $match ) {
				$shortcode_name = $matches[1][ $key ] ?? false;
				$shortcode_attr = [];

				if ( ! isset( $placeholders[ $shortcode_name ] ) ) {
					continue;
				}

				$shortcode_content = $placeholders[ $shortcode_name ];

				if ( ! empty( $matches[2][ $key ] ) ) {
					preg_match_all( '/(\w+)\=\"(.*?)\"/m', $matches[2][ $key ], $attr_matches );

					if ( $attr_matches ) {
						foreach ( $attr_matches[1] as $attr_key => $attr_name ) {
							$shortcode_attr[ $attr_name ] = $attr_matches[2][ $attr_key ];
						}
					}

					$new_shortcode_content = '';

					switch ( $shortcode_name ) {
						case 'tracking_link':
							if ( ! empty( $shortcode_content ) ) {
								if ( ! $plain_text ) {
									if ( $shortcode_attr['link_text'] ) {
										$new_shortcode_content = '<a href="' . $shortcode_content . '">' . esc_html( $shortcode_attr['link_text'] ) . '</a>';
									} else {
										$new_shortcode_content = '<a href="' . $shortcode_content . '">' . esc_html( $shortcode_content ) . '</a>';
									}
								} else {
									$new_shortcode_content = esc_html( $shortcode_content );
								}
							} else {
								if ( $shortcode_attr['link_text'] && empty( $shortcode_attr['content'] ) ) {
									$new_shortcode_content = $shortcode_attr['link_text'];
								}
							}
							break;
						default:
							if ( ! empty( $shortcode_attr['content'] ) ) {
								$new_shortcode_content = $shortcode_content;
							}
							break;
					}

					if ( ! empty( $shortcode_content ) && isset( $shortcode_attr['content'] ) && $shortcode_attr['content'] ) {
						$new_shortcode_content = $shortcode_attr['content'] . ' ' . $new_shortcode_content;
					}

					$shortcode_content = $new_shortcode_content;
				} else {
					switch ( $shortcode_name ) {
						case 'tracking_link':
							if ( ! empty( $shortcode_content ) ) {
								if ( ! $plain_text ) {
									$shortcode_content = '<a href="' . $shortcode_content . '">' . esc_html( $shortcode_content ) . '</a>';
								} else {
									$shortcode_content = esc_html( $shortcode_content );
								}
							}
							break;
					}
				}

				if ( empty( $shortcode_content ) ) {
					$content = str_replace( [ "\r\n", "\r" ], "\n", $content );
					$content = preg_replace( "/$match/", '{new_line}', $content );
					$content = str_replace( ['{new_line}' . "\n", '{new_line}'], "", $content );
				} else {
					$content = preg_replace( "/$match/", $shortcode_content, $content );
				}
			}
		}

		$content = $email->format_string( $content );

		echo '<div class="email-content">' . wpautop( $content ) . '</div>';
	}

	public function woocommerce_email_get_option( $value, $_email, $_value, $key, $empty_value ) {
		if ( $key == 'additional_content' && $value === $empty_value ) {
			$value = '';
		}

		return $value;
	}

	/**
	 * Add wysiwyg field type for WooCommerce settings
	 *
	 * @param $html
	 * @param $key
	 * @param $data
	 * @param $email
	 *
	 * @return false|string
	 */
	public function content_wysiwyg_email_field( $html, $key, $data, $email ) {
		$field_key = $email->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'content_wysiwyg',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		$data = wp_parse_args( $data, $defaults );


		$mce_buttons = array(
			'formatselect',
			'bold',
			'italic',
			'bullist',
			'numlist',
			'alignleft',
			'aligncenter',
			'alignright',
			'link',
			'spellchecker',
			'wp_adv',
		);

		$mce_buttons_2 = array(
			'strikethrough',
			'hr',
			'forecolor',
			'pastetext',
			'removeformat',
			'charmap',
			'outdent',
			'indent',
			'undo',
			'redo',
		);

		if ( ! wp_is_mobile() ) {
			$mce_buttons_2[] = 'wp_help';
		}

		$settings = [
			'media_buttons' => false,
			'teeny'         => false,
			'tinymce'       => array(
				'toolbar1' => implode( ',', $mce_buttons ),
				'toolbar2' => implode( ',', $mce_buttons_2 ),
				'toolbar3' => '',
			),
			'editor_class'  => 'woocommerce-email-editor'
		];

		ob_start(); ?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label
						for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?><?php echo $email->get_tooltip_html( $data ); // WPCS: XSS ok. ?></label>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span>
					</legend>
					<?php wp_editor( $email->get_option( $key ), $field_key, $settings ); ?>
					<?php echo $email->get_description_html( $data ); // WPCS: XSS ok. ?>
				</fieldset>
			</td>
		</tr>
		<?php
		$html = ob_get_clean();

		return $html;

	}
}