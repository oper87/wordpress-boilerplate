<?php
/**
 * Woocommerce Functions Class
 *
 * @version 1.0.0
 */

namespace App\WooCommerce\Emails;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use App\Helper;

class Email {

	/**
	 * Cart constructor.
	 */
	private function __construct() {
		//add_filter( 'woocommerce_email_actions', [ $this, 'email_actions' ] );
		//add_filter( 'woocommerce_email_classes', [ $this, 'email_classes' ] );
		add_filter( 'woocommerce_email_headers', [ $this, 'email_headers' ], 10, 4 );

		//add_filter( 'retrieve_password_message', [ $this, 'retrieve_password_message' ], 10, 4 );

		add_filter( 'woocommerce_email_get_option', [ $this, 'woocommerce_email_get_option' ], 10, 5 );

		new Email_Settings();
	}

	public function email_actions( $actions ) {

		/*$actions[] = 'customer_registration_completed';
		$actions[] = 'customer_registration_canceled';
		$actions[] = 'customer_registration_waiting_to_participant';
		$actions[] = 'admin_reset_customer_password';*/

		return $actions;
	}

	public function email_classes( $emails ) {

		/*$emails['WC_Email_Customer_Registration_Completed']              = include __DIR__ . '/emails/class-wc-email-customer-registration-completed.php';
		$emails['WC_Email_Customer_Registration_Canceled']               = include __DIR__ . '/emails/class-wc-email-customer-registration-canceled.php';
		$emails['WC_Email_Customer_Registration_Waiting_To_Participant'] = include __DIR__ . '/emails/class-wc-email-customer-registration-waiting-to-participant.php';
		$emails['WC_Email_Admin_Reset_Customer_Password']                = include __DIR__ . '/emails/class-wc-email-admin-reset-customer-password.php';*/

		return $emails;
	}

	public function email_headers( $header, $id, $object, $email ) {
		$email_from_address = get_option( 'woocommerce_email_from_address' );

		if ( ! empty( $email_from_address ) ) {
			$header .= 'List-Unsubscribe: <mailto: ' . $email_from_address . '?subject=unsubscribe>';
		}

		return $header;
	}

	public function woocommerce_email_get_option( $value, $_email, $_value, $key, $empty_value ) {
		if ( $key == 'additional_content' && $value === $empty_value ) {
			$value = '';
		}

		return $value;
	}

	/**
	 * Singleton pattern
	 */
	static public function getInstance() {
		static $self = null;

		if ( $self === null ) {
			$self = new self();
		}

		return $self;
	}
}