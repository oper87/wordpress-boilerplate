<?php
/**
 * Overwrite woocommerce functions
 */

use App\WooCommerce\Product;
use App\WooCommerce\Cart;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function get_product_cart_contains( $product_id ) {
	return Cart::get_product_cart_contains( $product_id );
}

function wc_get_product_image_src( $product_id, $size = 'woocommerce_thumbnail' ) {
	$thumbnail_id = get_post_thumbnail_id( $product_id );

	if ( empty( $thumbnail_id ) ) {
		$dimensions        = wc_get_image_size( $size );
		$placeholder_image = get_option( 'woocommerce_placeholder_image', 0 );

		if ( wp_attachment_is_image( $placeholder_image ) ) {
			$thumbnail_image = wp_get_attachment_image_src( $placeholder_image, $size );
		} else {
			$thumbnail_image   = [];
			$thumbnail_image[] = wc_placeholder_img_src( $size );
			$thumbnail_image[] = $dimensions['width'];
			$thumbnail_image[] = $dimensions['height'];
		}
	} else {
		$thumbnail_image = wp_get_attachment_image_src( $thumbnail_id, $size );
	}

	return $thumbnail_image;
}

if ( ! function_exists( 'woocommerce_default_product_tabs' ) ) {

	/**
	 * Add default product tabs to product pages.
	 *
	 * @param array $tabs Array of tabs.
	 *
	 * @return array
	 */
	function woocommerce_default_product_tabs( $tabs = array() ) {
		global $product, $post;

		$tabs['signing'] = array(
			'title'    => __( 'Book signing', 'ruby_studio' ),
			'priority' => 10,
			'callback' => 'woocommerce_product_description_tab',//'woocommerce_product_signing_tab',
		);

		// Description tab - shows product content.
		if ( $post->post_content ) {
			$tabs['description'] = array(
				'title'    => __( 'Details', 'ruby_studio' ),
				'priority' => 10,
				'callback' => 'woocommerce_product_description_tab',
			);
		}

		return $tabs;
	}
}

if ( ! function_exists( 'woocommerce_order_review_items' ) ) {

	/**
	 * Output the Order review table for the checkout.
	 */
	function woocommerce_order_review_items() {
		wc_get_template(
			'checkout/review-order-items.php',
			array(
				'checkout' => WC()->checkout(),
			)
		);
	}
}

/**
 * Show subcategory thumbnails.
 *
 * @param mixed $category Category.
 */
function woocommerce_subcategory_thumbnail( $category ) {
	$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'woocommerce_thumbnail' );
	$dimensions           = wc_get_image_size( $small_thumbnail_size );
	$thumbnail_id         = get_term_meta( $category->term_id, 'thumbnail_id', true );

	if ( $thumbnail_id ) {
		$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
		$image        = $image[0];
		$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
		$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
	} else {
		$image             = wc_placeholder_img_src( $small_thumbnail_size );
		$placeholder_image = get_option( 'woocommerce_placeholder_image', 0 );

		$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $placeholder_image, $small_thumbnail_size ) : false;
		$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $placeholder_image, $small_thumbnail_size ) : false;
	}

	if ( $image ) {
		// Prevent esc_url from breaking spaces in urls for image embeds.
		// Ref: https://core.trac.wordpress.org/ticket/23605.
		$image = str_replace( ' ', '%20', $image );

		// Add responsive image markup if available.
		if ( $image_srcset && $image_sizes ) {
			echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
		} else {
			echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" />';
		}
	}
}

/**
 * Output the related products.
 */
function woocommerce_output_related_products() {

	$args = array(
		'posts_per_page' => 3,
		'columns'        => 3,
		'orderby'        => 'rand', // @codingStandardsIgnoreLine.
	);

	woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
}

/**
 * Output the view cart button.
 */
function woocommerce_widget_shopping_cart_button_view_cart() {
	echo '<a href="' . esc_url( wc_get_cart_url() ) . '" class="button wc-forward">' . esc_html__( 'View cart', 'woocommerce' ) . '</a>';
}

/**
 * Output the proceed to checkout button.
 */
function woocommerce_widget_shopping_cart_proceed_to_checkout() {
	echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="button checkout wc-forward">' . esc_html__( 'Proceed to checkout', 'woocommerce' ) . '</a>';
}

/**
 * Output to view cart subtotal.
 *
 * @since 3.7.0
 */
function woocommerce_widget_shopping_cart_shipping() {

	if ( WC()->cart->get_cart_shipping_total() ) {
		$label = esc_html__( 'Shipping', 'ruby_studio' );

		echo '<p class="woocommerce-mini-cart__shipping total">';
		echo '<strong>' . $label . '</strong> ' . WC()->cart->get_cart_shipping_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo '</p>';
	}
}

/**
 * Output to view cart subtotal.
 *
 * @since 3.7.0
 */
function woocommerce_widget_shopping_cart_subtotal() {

	if ( WC()->cart->display_prices_including_tax() ) {
		$label = esc_html__( 'Subtotal (including taxes)', 'ruby_studio' );
	} else {
		$label = esc_html__( 'Subtotal', 'ruby_studio' );
	}

	echo '<strong>' . $label . '</strong> ' . WC()->cart->get_cart_subtotal(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

if ( ! function_exists( 'wc_cart_display_tax_totals_html' ) ) {

	/**
	 * Display tax totals separately from order totals on the checkout
	 *
	 * @return void
	 */
	function wc_cart_display_tax_totals_html() {
		echo \App\WooCommerce\Cart::getInstance()->wc_cart_display_tax_totals_html();
	}

}

if ( ! function_exists( 'wc_cart_display_shipping_totals_html' ) ) {

	/**
	 * Display shipping totals inc or excl the tax
	 *
	 * @return void
	 */
	function wc_cart_display_shipping_totals_html() {
		echo \App\WooCommerce\Cart::getInstance()->wc_cart_display_shipping_totals_html();
	}

}

if ( ! function_exists( 'wc_show_text_with_tax_prefix' ) ) {

	/**
	 * @param $text
	 * @param $tax_display
	 *
	 * @return string
	 */
	function wc_show_text_with_tax_prefix( $text, $tax_display = null, $html = true ): string {

		if ( is_null( $tax_display ) ) {
			$tax_display = wc_prices_include_tax();
		}

		if ( $tax_display ) {
			if ( $html ) {
				return $text . ' <small class="tax_label">' . __( '(incl. VAT)', 'woocommerce' ) . '</small>';
			} else {
				return $text . ' ' . __( '(incl. VAT)', 'woocommerce' );
			}
		} else {
			return $text;
		}
	}

}

add_filter( 'woocommerce_form_field', function ( $field, $key, $args, $value ) {
	if ( ! $args['required'] ) {
		$field = str_replace( '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>', '', $field );
	}

	return $field;
}, 10, 4 );
