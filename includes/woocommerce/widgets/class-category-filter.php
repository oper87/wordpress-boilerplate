<?php
/**
 * Categories filter Widget.
 *
 * @package RubyStudio/WooCommerce/Widgets
 * @version 1.0.0
 */

namespace App\WooCommerce\Widgets;

use WC_Widget;
use WP_Meta_Query;
use WP_Tax_Query;
use WC_Query;
use App\WooCommerce\Categories_Filter_Walker;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Widget' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Widget Categories filter.
 */
class Category_Filter extends WC_Widget {

	/**
	 * Category ancestors.
	 *
	 * @var array
	 */
	public $cat_ancestors;

	/**
	 * Current Category.
	 *
	 * @var bool
	 */
	public $current_cat;

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->widget_cssclass    = 'woocommerce widget-category-filter';
		$this->widget_description = __( 'A list of product categories.', 'woocommerce' );
		$this->widget_id          = 'widget_category_filter';
		$this->widget_name        = __( 'Filter Products by Categories', 'woocommerce' );
		$this->settings           = [
			'title'   => [
				'type'  => 'text',
				'std'   => __( 'Product categories', 'woocommerce' ),
				'label' => __( 'Title', 'woocommerce' ),
			],
			'orderby' => [
				'type'    => 'select',
				'std'     => 'name',
				'label'   => __( 'Order by', 'woocommerce' ),
				'options' => [
					'order' => __( 'Category order', 'woocommerce' ),
					'name'  => __( 'Name', 'woocommerce' ),
				],
			],
			/*'dropdown'           => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Show as dropdown', 'woocommerce' ),
			),*/
			'count'   => [
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Show product counts', 'woocommerce' ),
			],
			/*'hierarchical'       => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => __( 'Show hierarchy', 'woocommerce' ),
			),
			'show_children_only' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Only show children of the current category', 'woocommerce' ),
			),*/
			/*'hide_empty' => [
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Hide empty categories', 'woocommerce' ),
			],*/
			/*'max_depth'          => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Maximum depth', 'woocommerce' ),
			),*/
		];


		add_action( 'woocommerce_product_query', [ $this, 'categories_filter_product_query' ], 10, 2 );

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Widget instance.
	 *
	 * @see WP_Widget
	 */
	public function widget( $args, $instance ) {
		global $wp, $wp_query, $post;

		$count              = isset( $instance['count'] ) ? $instance['count'] : $this->settings['count']['std'];
		$hierarchical       = true;
		$show_children_only = 0;//isset( $instance['show_children_only'] ) ? $instance['show_children_only'] : $this->settings['show_children_only']['std'];
		$dropdown           = false;//isset( $instance['dropdown'] ) ? $instance['dropdown'] : $this->settings['dropdown']['std'];
		$orderby            = isset( $instance['orderby'] ) ? $instance['orderby'] : $this->settings['orderby']['std'];
		$hide_empty         = true;//isset( $instance['hide_empty'] ) ? $instance['hide_empty'] : $this->settings['hide_empty']['std'];
		$dropdown_args      = [
			'hide_empty' => $hide_empty,
		];

		$taxonomy_filter_name = 'product_cat';

		$list_args = [
			'show_count'   => false,
			'hierarchical' => true,
			'taxonomy'     => 'product_cat',
			'hide_empty'   => $hide_empty,
		];
		$max_depth = 2;

		$list_args['menu_order'] = false;
		$list_args['depth']      = $max_depth;

		if ( 'order' === $orderby ) {
			$list_args['orderby']  = 'meta_value_num';
			$list_args['meta_key'] = 'order';
		}

		$this->current_cat   = false;
		$this->cat_ancestors = [];

		if ( is_tax( 'product_cat' ) ) {
			$this->current_cat   = $wp_query->queried_object;
			$this->cat_ancestors = get_ancestors( $this->current_cat->term_id, 'product_cat' );

		} elseif ( is_singular( 'product' ) ) {
			$terms = wc_get_product_terms(
				$post->ID,
				'product_cat',
				apply_filters(
					'woocommerce_product_categories_widget_product_terms_args',
					[
						'orderby' => 'parent',
						'order'   => 'DESC',
					]
				)
			);

			if ( $terms ) {
				$main_term           = apply_filters( 'woocommerce_product_categories_widget_main_term', $terms[0], $terms );
				$this->current_cat   = $main_term;
				$this->cat_ancestors = get_ancestors( $main_term->term_id, 'product_cat' );
			}
		}

		// Show Siblings and Children Only.
		if ( $show_children_only && $this->current_cat ) {
			if ( $hierarchical ) {
				$include = array_merge(
					$this->cat_ancestors,
					[ $this->current_cat->term_id ],
					get_terms(
						'product_cat',
						[
							'fields'       => 'ids',
							'parent'       => 0,
							'hierarchical' => true,
							'hide_empty'   => false,
						]
					),
					get_terms(
						'product_cat',
						[
							'fields'       => 'ids',
							'parent'       => $this->current_cat->term_id,
							'hierarchical' => true,
							'hide_empty'   => false,
						]
					)
				);
				// Gather siblings of ancestors.
				if ( $this->cat_ancestors ) {
					foreach ( $this->cat_ancestors as $ancestor ) {
						$include = array_merge(
							$include,
							get_terms(
								'product_cat',
								[
									'fields'       => 'ids',
									'parent'       => $ancestor,
									'hierarchical' => false,
									'hide_empty'   => false,
								]
							)
						);
					}
				}
			} else {
				// Direct children.
				$include = get_terms(
					'product_cat',
					[
						'fields'       => 'ids',
						'parent'       => $this->current_cat->term_id,
						'hierarchical' => true,
						'hide_empty'   => false,
					]
				);
			}

			$list_args['include']     = implode( ',', $include );
			$dropdown_args['include'] = $list_args['include'];

			if ( empty( $include ) ) {
				return;
			}
		} elseif ( $show_children_only ) {
			$dropdown_args['depth']        = 1;
			$dropdown_args['child_of']     = 0;
			$dropdown_args['hierarchical'] = 1;
			$list_args['depth']            = 1;
			$list_args['child_of']         = 0;
			$list_args['hierarchical']     = 1;
		}

		$this->widget_start( $args, $instance );


		$list_args['walker']                     = new Categories_Filter_Walker();
		$list_args['title_li']                   = '';
		$list_args['pad_counts']                 = 1;
		$list_args['show_option_none']           = __( 'No product categories exist.', 'woocommerce' );
		$list_args['current_category']           = ( $this->current_cat ) ? $this->current_cat->term_id : '';
		$list_args['current_category_ancestors'] = $this->cat_ancestors;
		$list_args['current_categories']         = $this->get_current_categories();
		$list_args['max_depth']                  = $max_depth;


		if ( '' === get_option( 'permalink_structure' ) ) {
			$form_action = remove_query_arg( [ 'page', 'paged', '_ajax' ], add_query_arg( $wp->query_string, '', wc_get_page_permalink( 'shop' ) ) );
		} else {
			$form_action = preg_replace( '%\/page/[0-9]+%', '', wc_get_page_permalink( 'shop' ) );
			$form_action = remove_query_arg( [ '_ajax' ], $form_action );
		}

		echo '<div class="widget-filter-body">';

		echo '<form method="get" action="' . esc_url( $form_action ) . '" class="product-categories-filter-form" data-taxonomy="product_cats">';

		echo '<ul class="product-categories">';

		wp_list_categories( apply_filters( 'woocommerce_product_categories_widget_args', $list_args ) );

		echo '</ul>';

		echo '<input type="hidden" name="product_cats" value="' . esc_attr( implode( ',', $this->get_current_categories() ) ) . '" />';
		echo wc_query_string_form_fields( null, [ 'product_cats', '_ajax' ], '', true ); // @codingStandardsIgnoreLine

		echo '</form>';

		echo '</div>';

		$this->widget_end( $args );
	}


	public function get_current_categories() {
		global $wp_query;

		$filter_terms = [];

		if ( isset( $_GET['product_cats'] ) ) {
			$filter_terms = ! empty( $_GET['product_cats'] ) ? explode( ',', wc_clean( wp_unslash( $_GET['product_cats'] ) ) ) : [];
			$filter_terms = array_map( 'sanitize_title', $filter_terms ); // Ensures correct encoding.
		}

		if ( is_tax( 'product_cat' ) ) {
			$this->current_cat   = $wp_query->queried_object;
			$this->cat_ancestors = get_ancestors( $this->current_cat->term_id, 'product_cat' );

			$filter_terms[] = $this->current_cat->slug;
		}

		return $filter_terms;
	}


	public function categories_filter_product_query( $query, $wc_query ) {


		if ( ( ! $query->is_main_query() || ( $this->get_current_categories() ) ) && ! is_tax( 'product_cat' ) ) {
			$tax_query = $query->get( 'tax_query' );

			$tax_query[] = [
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $this->get_current_categories(),
			];

			$query->set( 'tax_query', $tax_query );
		}

	}
}
