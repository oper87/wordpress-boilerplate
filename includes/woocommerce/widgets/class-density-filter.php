<?php
/**
 * Density (Liter) filter Widget.
 *
 * @package RubyStudio/WooCommerce/Widgets
 * @version 1.0.0
 */

namespace App\WooCommerce\Widgets;

use WC_Widget;
use WP_Meta_Query;
use WP_Tax_Query;
use WC_Query;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Widget' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Widget Density filter.
 */
class Density_Filter extends WC_Widget {

	const DENSITY_FILTER_KEY = 'density';

	/**
	 * Constructor.
	 */
	public function __construct() {

		$this->widget_cssclass    = 'woocommerce widget_density_filter';
		$this->widget_description = __( 'Min/Max Density Filter.', 'woocommerce' );
		$this->widget_id          = 'woocommerce_density_filter';
		$this->widget_name        = __( 'Filter Products by Density (Liter)', 'woocommerce' );
		$this->settings           = [
			'title' => [
				'type'  => 'text',
				'std'   => __( 'Density (Liter)', 'woocommerce' ),
				'label' => __( 'Title', 'woocommerce' ),
			],
		];

		add_action( 'woocommerce_product_query', [ $this, 'density_filter_product_query' ], 10, 2 );

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @param array $args Arguments.
	 * @param array $instance Widget instance.
	 *
	 * @see WP_Widget
	 */
	public function widget( $args, $instance ) {
		global $wp;

		if ( ! is_shop() && ! is_product_taxonomy() ) {
			return;
		}

		if ( false == ( $min_max = $this->get_min_max_values( false ) ) ) {
			return;
		}

		wp_enqueue_script( 'jquery-ui-slider' );
		wp_enqueue_script( 'wc-jquery-ui-touchpunch' );

		$min_density = $min_max['min'];
		$max_density = $min_max['max'];

		// Round values to nearest 10 by default.
		$step = max( 10, 1 );

		$min_density = floor( $min_density / $step ) * $step;
		$max_density = ceil( $max_density / $step ) * $step;

		// If both min and max are equal, we don't need a slider.
		if ( $min_density === $max_density ) {
			return;
		}

		$current_min_density = isset( $_GET['min_density'] ) ? floor( floatval( wp_unslash( $_GET['min_density'] ) ) / $step ) * $step : $min_density; // WPCS: input var ok, CSRF ok.
		$current_max_density = isset( $_GET['max_density'] ) ? ceil( floatval( wp_unslash( $_GET['max_density'] ) ) / $step ) * $step : $max_density; // WPCS: input var ok, CSRF ok.

		$this->widget_start( $args, $instance );

		if ( '' === get_option( 'permalink_structure' ) ) {
			$form_action = remove_query_arg( [ 'page', 'paged', 'product-page' ], add_query_arg( $wp->query_string, '', home_url( $wp->request ) ) );
		} else {
			$form_action = preg_replace( '%\/page/[0-9]+%', '', home_url( trailingslashit( $wp->request ) ) );
		}

		echo '<div class="widget-filter-body">';

		wc_get_template( 'widgets/density-filter-widget.php', [
			'form_action'         => $form_action,
			'min_density'         => $min_density,
			'max_density'         => $max_density,
			'current_min_density' => $current_min_density,
			'current_max_density' => $current_max_density,
			'step'                => $step,
		] );

		echo '</div>';

		$this->widget_end( $args );
	}

	public function get_min_max_values( $show_available ) {
		global $wpdb;

		if ( true === $show_available ) {

			$args       = WC()->query->get_main_query()->query_vars;
			$tax_query  = isset( $args['tax_query'] ) ? $args['tax_query'] : [];
			$meta_query = isset( $args['meta_query'] ) ? $args['meta_query'] : [];

			if ( ! is_post_type_archive( 'product' ) && ! empty( $args['taxonomy'] ) && ! empty( $args['term'] ) ) {
				$tax_query[] = WC()->query->get_main_tax_query();
			}

			foreach ( $meta_query + $tax_query as $key => $query ) {
				if ( ! empty( $query[ self::DENSITY_FILTER_KEY ] ) || ! empty( $query['price_filter'] ) || ! empty( $query['rating_filter'] ) ) {
					unset( $meta_query[ $key ] );
				}
			}

			$meta_query = new WP_Meta_Query( $meta_query );
			$tax_query  = new WP_Tax_Query( $tax_query );
			$search     = WC_Query::get_main_search_query_sql();

			$meta_query_sql   = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
			$tax_query_sql    = $tax_query->get_sql( $wpdb->posts, 'ID' );
			$search_query_sql = $search ? ' AND ' . $search : '';

			$min = floor( $wpdb->get_var( "
				SELECT min(meta_value + 0)
				FROM {$wpdb->posts}
				" . $tax_query_sql['join'] . $meta_query_sql['join'] . "
				LEFT JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id
				WHERE meta_key IN ('" . self::DENSITY_FILTER_KEY . "') AND meta_value != ''
					AND {$wpdb->posts}.post_type IN ('" . implode( "','", array_map( 'esc_sql', apply_filters( 'woocommerce_price_filter_post_type', [ 'product' ] ) ) ) . "')
					AND {$wpdb->posts}.post_status = 'publish'
					" . $tax_query_sql['where'] . $meta_query_sql['where'] . $search_query_sql ) );

			$max = ceil( $wpdb->get_var( "
				SELECT max(meta_value + 0)
				FROM {$wpdb->posts}
				" . $tax_query_sql['join'] . $meta_query_sql['join'] . "
				LEFT JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id
				WHERE meta_key IN ('" . self::DENSITY_FILTER_KEY . "') AND meta_value != ''
					AND {$wpdb->posts}.post_type IN ('" . implode( "','", array_map( 'esc_sql', apply_filters( 'woocommerce_price_filter_post_type', [ 'product' ] ) ) ) . "')
					AND {$wpdb->posts}.post_status = 'publish'
					" . $tax_query_sql['where'] . $meta_query_sql['where'] . $search_query_sql ) );

		} else {

			$min = floor( $wpdb->get_var( "
				SELECT min(meta_value + 0)
				FROM {$wpdb->posts}
				LEFT JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id
				WHERE meta_key IN ('" . self::DENSITY_FILTER_KEY . "') AND meta_value != ''" ) );

			$max = ceil( $wpdb->get_var( "
				SELECT max(meta_value + 0)
				FROM {$wpdb->posts}
				LEFT JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id
				WHERE meta_key IN ('" . self::DENSITY_FILTER_KEY . "') AND meta_value != ''" ) );

		}

		return [ 'min' => $min, 'max' => $max ];
	}

	public function density_filter_product_query( $query, $wc_query ) {
		global $wpdb;

		if ( ! $query->is_main_query() || ( ! isset( $_GET['max_density'] ) && ! isset( $_GET['min_density'] ) ) ) {
			return '';
		}

		$current_min_density = isset( $_GET['min_density'] ) ? floatval( wp_unslash( $_GET['min_density'] ) ) : 0; // WPCS: input var ok, CSRF ok.
		$current_max_density = isset( $_GET['max_density'] ) ? floatval( wp_unslash( $_GET['max_density'] ) ) : PHP_INT_MAX; // WPCS: input var ok, CSRF ok.

		$meta_query = $query->get( 'meta_query' );

		$meta_query[] = [
			'key'     => self::DENSITY_FILTER_KEY,
			'value'   => [ $current_min_density, $current_max_density ],
			'type'    => 'numeric',
			'compare' => 'BETWEEN',
		];

		$query->set( 'meta_query', $meta_query );
	}
}