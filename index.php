<?php
/**
 * The template for displaying all posts
 */

get_header(); ?>

	<main id="main" class="site-main index-main">
		
			<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

                <article <?php post_class( 'container' ); ?>>

                    <header class="entry-header">
                        <h2 class="entry-title">
                            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ) ?>">
                                <?php the_title() ?>
                            </a>
                        </h2>
                    </header>

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>

                </article>

            <?php endwhile; ?>

        <?php endif; ?>
    </main><!-- #main -->

<?php get_footer(); ?>