<?php
/**
 * The template for displaying a default page template
 */

get_header();

?>

    <main id="main" class="site-main default-main">
		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post(); ?>

            <article <?php post_class( 'container' ); ?>>

                <div class="entry-header">
                    <h1 class="entry-title">
						<?php the_title(); ?>
                    </h1>
                </div>

                <div class="entry-content">
					<?php the_content(); ?>
                </div>

            </article>

		<?php endwhile; ?>

    </main>

<?php
get_footer();