# WordPress Starter Theme



## Getting Started

### AJAX Requests


#### 1st case

```javascript
theme.ajax.post({
    ajax_action: 'ajax_callback_endpoint',
    post_id: _button.dataset.post_id,
}).done(function (response) {
    //
}).fail(function (response) {
    //
});
```

#### 2st case

```javascript
$.ajax({
    method: 'POST', //or GET
    url: theme_params.theme_ajax_url.toString().replace('%%endpoint%%', 'ajax_callback_endpoint'),
    data: {
        post_id: _button.dataset.post_id
    },
    dataType: 'json'
}).done(function (response) {
    //
});
```

#### 3st case

```javascript
$.ajax({
    method: 'POST', //or GET
    url: theme_params.ajax_url,
    data: {
        action: 'theme_ajax'
        ajax_action: 'ajax_callback_endpoint',
        post_id: _button.dataset.post_id
    },
    dataType: 'json'
}).done(function (response) {
    //
});
```
