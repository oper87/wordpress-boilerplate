<?php
/**
 * Single post template
 */

if ( ! THEME_AJAX ) {
	get_header();
} else {
	\App\Functions::ajax_header_response();
}
?>

	<main id="main" class="site-main single-main">
		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post(); ?>

			<article <?php post_class( 'container' ); ?>>

				<div class="entry-header">
					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
				</div>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>

			</article>

		<?php endwhile; ?>

	</main>

<?php
if ( ! THEME_AJAX ) {
	get_footer();
}