<?php
/**
 * Mailchimp Account settings template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

if ( empty( $args['groups'] ) ) {
	return;
}

?>
<th scope="row">Groups</th>
<td class="mailchimp-table">
	<?php if ( ! empty( $args['groups'] ) ) { ?>
        <table>
            <tr>
                <th><strong>Title</strong></th>
                <th><strong>Interests</strong></th>
            </tr>
			<?php foreach ( $args['groups'] as $group ) { ?>
                <tr>
                    <td class="group-title">
                        <strong><?php echo $group['title'] ?></strong>
                        ID: <code><?php echo $group['id'] ?></code>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <th><strong>Name</strong></th>
                                <th><strong>ID</strong></th>
                            </tr>
							<?php foreach ( $group['interests'] as $interests ) : ?>
                                <tr>
                                    <td><?php echo $interests['name'] ?></td>
                                    <td><code><?php echo $interests['id'] ?></code></td>
                                </tr>
							<?php endforeach; ?>
                        </table>
                    </td>
                </tr>
			<?php } ?>
        </table>
	<?php } ?>
</td>