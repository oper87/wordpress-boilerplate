<?php
/**
 * Mailchimp Account settings template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

if ( empty( $args['merge_fields'] ) ) {
	return;
}

?>
<th scope="row">Merge fields</th>
<td class="mailchimp-table">
    <table>
        <tr>
            <th><strong>Name</strong></th>
            <th><strong>Tag</strong></th>
            <th><strong>Type</strong></th>
        </tr>
        <tr>
            <td>Email address<span class="wp-ui-text-notification">*</span></td>
            <td><code>EMAIL</code></td>
            <td>email</td>
        </tr>
		<?php foreach ( $args['merge_fields'] as $field ) { ?>
            <tr>
                <td><?php echo $field['name'] ?></td>
                <td><code><?php echo $field['tag'] ?></code></td>
                <td><?php echo $field['type'] ?></td>
            </tr>
		<?php } ?>
    </table>
    <p class="help">
        Use <code>mc-</code> prefix in field name - ex.: <code>mc-EMAIL</code>
        <br>
        And it should be added hidden field
        <code><?php esc_html_e( '<input type="hidden" name="mc-subscribe" value="1" />' ); ?></code>
    </p>
</td>
