<?php
/**
 * Mailchimp Account settings template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

if ( empty( $args['segments'] ) ) {
	return;
}

?>
<th scope="row">Segments/Tags</th>
<td class="mailchimp-table">
    <table>
        <tr>
            <th><strong>Name</strong></th>
            <th><strong>ID</strong></th>
        </tr>
		<?php foreach ( $args['segments'] as $segment ) { ?>
            <tr>
                <td><?php echo $segment['name'] ?></td>
                <td><code><?php echo $segment['id'] ?></code></td>
            </tr>
		<?php } ?>
    </table>
</td>
