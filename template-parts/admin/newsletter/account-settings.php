<?php
/**
 * Mailchimp Account settings template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

if ( empty( $args['mailchimp_settings'] ) ) {
	return;
}

$mailchimp_settings = $args['mailchimp_settings'];
$list_id            = $mailchimp_settings['list_id'];
$template_id        = $mailchimp_settings['template_id'];

?>
<hr>
<table class="form-table">
    <tr valign="top" class="acf-field">
        <th scope="row"><label for="mailchimp_list">Mailchimp lists</label></th>
        <td>
			<?php if ( ! empty( $mailchimp_settings['lists'] ) ) { ?>
                <select id="mailchimp_list">
                    <option value="">Select a list</option>
					<?php foreach ( $mailchimp_settings['lists'] as $list ) { ?>
                        <option value="<?php echo $list['id'] ?>" <?php selected( $list_id, $list['id'] ) ?>>
							<?php echo sprintf( '%s - %s', $list['name'], $list['id'] ) ?>
                        </option>
					<?php } ?>
                </select>
				<?php
			} ?>
        </td>
    </tr>
    <tr valign="top" class="acf-field">
        <th scope="row"><label for="mailchimp_template">Mailchimp templates</label></th>
        <td>
            <select id="mailchimp_template">
                <option value="">Select a list</option>
				<?php if ( ! empty( $mailchimp_settings['templates'] ) ) { ?>
					<?php foreach ( $mailchimp_settings['templates'] as $template ) { ?>
                        <option value="<?php echo $template['id'] ?>" <?php selected( $template_id, $template['id'] ) ?>>
							<?php echo sprintf( '%s - %s', $template['name'], $template['id'] ) ?>
                        </option>
					<?php } ?>
					<?php
				} ?>
            </select>
        </td>
    </tr>
    <tr valign="top" class="acf-field">
        <th scope="row">Double Opt-in</th>
        <td>
            <label><input class="double_optin" type="radio" name="double_optin"
                          value="1" <?php checked( $mailchimp_settings['double_optin'], '1' ) ?>> Yes</label>
            &nbsp;&nbsp;&nbsp;
            <label><input class="double_optin" type="radio" name="double_optin"
                          value="0" <?php checked( $mailchimp_settings['double_optin'], '0' ) ?>> No</label>
            <p class="help">Select "yes" if you want people to confirm their email address before being subscribed
                (recommended)</p>
        </td>
    </tr>

    <tr valign="top" class="acf-field mailchimp-merge-fields">
		<?php
		get_template_part( 'template-parts/admin/newsletter/account', 'merge-fields', [
			'merge_fields' => $mailchimp_settings['merge_fields']
		] );
		?>
    </tr>

    <tr valign="top" class="acf-field mailchimp-segments">
		<?php
		get_template_part( 'template-parts/admin/newsletter/account', 'segments', [
			'segments' => $mailchimp_settings['segments']
		] );
		?>
    </tr>

    <tr valign="top" class="acf-field mailchimp-groups">
	    <?php
	    get_template_part( 'template-parts/admin/newsletter/account', 'groups', [
		    'groups' => $mailchimp_settings['groups']
	    ] );
	    ?>
    </tr>

</table>
<p class="submit-action">
    <button type="button" class="button button-primary mailchimp_save_account">Save Changes</button>
    <button type="button" class="button mailchimp_fetch_account">Fetch lists and templates
    </button>
</p>
