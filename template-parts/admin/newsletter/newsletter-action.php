<?php
/**
 * Mailchimp Action template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

?>
<div id="newsletter-campaign-action">
	<p class="submit-action">
		<button type="button" class="button button-primary newsletter_send_campaign">Send Campaign</button>
		<button type="button" class="button newsletter_show_test_email">Send Test Campaign</button>
	</p>

	<div class="newsletter-test-email-field">
		<div class="acf-label">
			<label for="newsletter-test-email">Enter Email</label>
		</div>
		<div class="acf-input">
			<input id="newsletter-test-email" type="text" value="">
			<button type="button" class="button button-primary newsletter_send_test_campaign">Send</button>
		</div>
	</div>
	<div class="newsletter-campaign-response">

	</div>
</div>