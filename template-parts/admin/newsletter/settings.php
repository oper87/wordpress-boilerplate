<?php
/**
 * Mailchimp Account settings template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

$connection         = $args['connection'];
$mailchimp_settings = $args['mailchimp_settings'];

?>
<div class="mailchimp-settings">
    <table class="form-table">
        <tr valign="top" class="acf-field">
            <th scope="row">Status</th>
            <td>
				<?php if ( is_wp_error( $connection ) && $connection->get_error_message( 'api_key_required' ) ) {
					echo '<span class="connected-status negative">NOT CONNECTED</span>';
				} else if ( is_wp_error( $connection ) ) {
					echo '<span class="connected-status negative">NOT CONNECTED</span>';
					echo '<div class="acf-notice -error acf-error-message connected-error-message"><p>' . $connection->get_error_message() . '</p></div>';
				} else {
					echo '<span class="connected-status positive">' . $connection->health_status . ' - CONNECTED</span>';
				} ?>
            </td>
        </tr>
        <tr valign="top" class="acf-field">
            <th scope="row"><label for="mailchimp_api_key">API Key</label></th>
            <td>
                <input type="text" class="widefat"
                       placeholder="Your Mailchimp API key" id="mailchimp_api_key"
                       value="<?php echo App\Extensions\Newsletter\Mailchimp::mask_key( $mailchimp_settings['api_key'] ) ?>" required>
                <p class="help">
                    The API key for connecting with your Mailchimp account.
                    <a target="_blank" href="https://admin.mailchimp.com/account/api">
                        Get your API key here.
                    </a>
                </p>
            </td>
        </tr>
    </table>
    <p class="submit-action">
        <button type="button" class="button button-primary mailchimp_save_settings">Save Changes</button>
    </p>
</div>

<div class="mailchimp-account-settings">
	<?php if ( ! is_wp_error( $connection ) ) {
		get_template_part( 'template-parts/admin/newsletter/account', 'settings', [
			'mailchimp_settings' => $mailchimp_settings
		] );
	} ?>
</div>