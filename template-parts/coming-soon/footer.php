<?php
/**
 * The footer template for Coming Soon page
 */
?>

</div>
<!-- /wrapper -->

<!-- footer -->
<footer class="footer" role="contentinfo">
    <div class="row row-top">
        <div class="small-12 medium-4 large-3 columns footer-contact">
            <h3><?php the_field( 'contact_title', 'options' ); ?></h3>
            <div class="content-column">
                <p>
                    <strong><?php _e( 'Press inquiries', 'ruby_studio_tmp' ) ?></strong>
                    <br>
                    <a href="mailto:<?php the_field( 'contact_press_inquiries_email', 'options' ); ?>">
						<?php the_field( 'contact_press_inquiries_email', 'options' ); ?>
                    </a>
                </p>
            </div>
        </div>
        <div class="small-12 medium-3 large-3 columns footer-some">
            <h3><?php the_field( 'some_title', 'options' ); ?></h3>
            <div class="content-column">
				<?php $some = get_field( 'some', 'options' );

				$some_fields = [
					'twitter_global_url'  => [
						'name' => __( 'Twitter Global', 'ruby_studio_tmp' ),
						'icon' => 'twitter'
					],
					'twitter_us_url'      => [
						'name' => __( 'Twitter US', 'ruby_studio_tmp' ),
						'icon' => 'twitter'
					],
					'linkedin_global_url' => [
						'name' => __( 'LinkedIn Global', 'ruby_studio_tmp' ),
						'icon' => 'linkedin'
					],
					'linkedin_us_url'     => [
						'name' => __( 'LinkedIn US', 'ruby_studio_tmp' ),
						'icon' => 'linkedin'
					],
				];

				foreach ( $some_fields as $key => $val ) :
					if ( ! empty( $some[ $key ] ) ) : ?>
                        <p>
                            <a href="<?php echo esc_url( $some[ $key ] ); ?>">
								<?php get_template_part( 'assets/images/' . $val['icon'] . '.svg' ); ?>
								<?php echo $val['name'] ?>
                            </a>
                        </p>
					<?php endif; ?>
				<?php endforeach; ?>
            </div>
        </div>
        <div class="footer-newsletter">
            <h3><?php the_field( 'newsletter_title', 'options' ); ?></h3>
            <div class="content-column">
                <p>
                    <a class="btn btn-has-icon" data-ajax="false" href="<?php the_field( 'newsletter_page', 'options' ); ?>">
						<?php _e( 'Subscribe', 'ruby_studio_tmp' ) ?>
						<?php get_template_part( 'assets/images/arrow.svg' ); ?>
                    </a>
                </p>
            </div>
        </div>
    </div>

    <div class="row row-bottom">
        <!-- copyright -->
        <div class="small-12 medium-12 large-8 columns">
            <p class="text"><?php the_field( 'footer_text', 'options' ); ?></p>
        </div>
        <!-- /copyright -->

        <!-- powered by -->
        <div class="small-12 medium-12 large-4 columns">
            <p class="copyright">&copy; <?php echo date( 'Y' ); ?> Ruby Studio. All rights reserved.</p>
        </div>
        <!-- /powered by -->
    </div>

</footer>
<!-- /footer -->

<?php wp_footer(); ?>

</body>
</html>