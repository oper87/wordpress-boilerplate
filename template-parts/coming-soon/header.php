<?php
/**
 * The header template
 */
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php
/**
 * Head
 */
get_template_part( 'template-parts/global/head' );

?>
<body <?php body_class(); ?>>

<!-- header -->
<header class="header clear" role="banner">

	<?php get_template_part( 'template-parts/header/top-bar' ) ?>

    <div class="header-container">
        <!-- logo -->
        <div class="logo">
            <a data-ajax="false" href="<?php echo site_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="Logo" class="logo-img">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-white.png" alt="Logo" class="logo-img-mobile">
            </a>
        </div>
        <!-- /logo -->
    </div>
</header>
<!-- /header -->

<!-- wrapper -->
<div class="wrapper">