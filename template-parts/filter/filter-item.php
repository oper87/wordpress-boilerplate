<?php
/**
 * Filter item template
 *
 */

use App\Helper;
use App\Posts\Filter;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

$filterInstance = Filter::getInstance();

$key  = $args['key'];
$item = $args['item'];

$classes = [ 'posts-filter__item' ];

$classes[] = 'key-' . $key;
$classes[] = 'type-' . $item['type'];

if ( count( $item['terms'] ) == 0 && $item['type'] == 'taxonomy' ) {
	$classes[] = 'display-none';
}

if ( isset( $item['found_posts'] ) && $item['found_posts'] == 0 && $item['type'] == 'taxonomy' ) {
	$classes[] = 'inactive';
}

if ( count( $item['selected_array'] ) > 0 ) {
	$classes[] = 'selected';
}

$multiple = isset( $item['choose_type'] ) && $item['choose_type'] == 'multiple';

?>
<div class="<?php echo join( ' ', $classes ) ?>">

	<?php if ( ! empty( $args['title'] ) ) { ?>
        <h3 class="posts-filter__terms-title"><?php echo esc_html( $item['name'] ); ?></h3>
	<?php } ?>

    <div class="posts-filter__terms-container filter-terms-item posts-filter__key-<?php echo esc_attr( $key ) ?>"
         data-filter-item="<?php echo esc_attr( $key ); ?>" data-level="1">

		<?php echo $filterInstance->filter_item_hidden_field( $item ); ?>

        <div class="posts-filter__terms">
			<?php switch ( $item['view'] ) {
				case 'search':
					$search_word = '';

					if ( ( ! isset( $item['choose_type'] ) || $item['choose_type'] != 'multiple' ) && ! empty( $item['selected_value'] ) ) {
						$search_word = $item['selected_value'];
					}
					?>
                    <div class="posts-filter__terms-search">
                        <div class="posts-filter__terms-search-field">
                            <input class="search" type="text" value="<?php echo $search_word ? esc_attr( strip_tags( $search_word ) ) : '' ?>"
                                   placeholder="<?php esc_attr_e( 'Search for recipes by text&hellip;', 'ruby_studio' ); ?>">
                            <button type="button" class="button-icon search-button"><?php Helper::get_svg( 'search' ); ?></button>
                        </div>
						<?php if ( $multiple && ! empty( $item['selected_array'] ) ) { ?>
                            <div class="selected-terms-list">
								<?php foreach ( $item['selected_array'] as $search_val ) { ?>
                                    <button class="button button--size_small button--filter" data-action="unselect-term"
                                            data-value="<?php echo $search_val ?>"
                                            type="button">
										<?php echo $search_val ?>
										<?php Helper::get_svg( 'close-thin' ); ?>
                                    </button>
								<?php } ?>
                            </div>
						<?php } ?>
                    </div>
					<?php
					break;
				case 'term_autocomplete':

					$terms = array_map( function ( $term ) use ( $item ) {
						return [
							'id'       => $term->slug,
							'text'     => $term->name,
							//'selected' => in_array( $term->slug, $item['selected_array'] )
						];
					}, $item['terms'] );

					?>
                    <div class="posts-filter__terms-search">

                        <div class="posts-filter__terms-search-field">
                            <select class="terms-autocomplete" style="width: 100%"
								<?php echo $multiple ? 'data-multiple="1"' : '' ?>
                                    data-terms="<?php echo esc_attr( wp_json_encode( $terms ) ) ?>"
                                    data-selected_terms="<?php echo esc_attr( wp_json_encode( $item['selected_array'] ) ) ?>"
                                    data-placeholder="<?php esc_attr_e( 'Search for recipes by ingredients&hellip;', 'ruby_studio' ); ?>">
	                            <?php echo ! $multiple ? '<option value=""></option>': ''; ?>
                            </select>
                            <button type="button"
                                    class="button-icon search-button"><?php Helper::get_svg( 'search' ); ?></button>
                        </div>
						<?php if ( $item['selected_array'] ) { ?>
                            <div class="selected-terms-list">
								<?php
								foreach ( $item['terms'] as $term ) {
									$terms[ $term->slug ] = $term->name;
								}

								foreach ( $item['selected_array'] as $selected ) {
									if ( ! empty( $terms[ $selected ] ) ) { ?>
                                        <button class="button button--size_small button--filter" data-action="unselect-term"
                                                data-value="<?php echo esc_attr( $selected ) ?>"
                                                type="button">
											<?php echo $terms[ $selected ] ?>
											<?php Helper::get_svg( 'close-thin' ); ?>
                                        </button>
										<?php
									}
								} ?>
                            </div>
						<?php } ?>
                    </div>
				<?php
				case 'price_range':

					break;
				case 'term_list': ?>
                    <ul class="posts-filter__terms-items">
						<?php
						foreach ( $item['terms'] as $term ) {
							$args               = [];
							$args['show_count'] = $item['show_count'];
							$args['class']      = false;

							if ( $item['taxonomy'] == 'pa_colors' ) {
								$args['label_after']   = '<span class="cb-bg"></span>';
								$args['attr']['style'] = '--color:' . get_field( 'color', $term ) . ';';
								$args['class']         = wc_light_or_dark( get_field( 'color', $term ), 'dark', 'light' );
							}

							if ( isset( $item['hierarchical'] ) && $item['hierarchical'] ) {
								$sub_term_slugs = array_map( function ( $sub_term ) {
									return $sub_term->slug;
								}, $term->terms );

								// include parent slug
								$sub_term_slugs[] = $term->slug;

								$classes = [ 'posts-filter__terms-item' ];

								if ( count( array_uintersect( $sub_term_slugs, $item['selected_array'], 'strcasecmp' ) ) > 0 ) {
									$classes[] = 'selected';
								}

								?>
                                <li class="<?php echo join( ' ', $classes ) ?>"
                                    data-name="<?php echo esc_attr( $term->name ) ?>">
                                    <a href="#" class="posts-filter__terms-item-title">
										<?php echo esc_html( $term->name ); ?>
										<?php Helper::get_svg( 'arrow' ); ?>
                                    </a>
                                    <div class="posts-filter__terms-container" data-level="2">

                                        <div class="posts-filter__terms-head">
                                            <a class="posts-filter__terms-back" href="#">
												<?php Helper::get_svg( 'arrow' ); ?>
                                            </a>
                                            <h3 class="posts-filter__terms-title"><?php echo esc_html( $term->name ); ?></h3>
                                        </div>

                                        <ul class="posts-filter__terms-sub-items">
											<?php
											$term->name = __( 'Show all', 'ruby_studio' );
											echo $filterInstance->filter_item_term_field( $term, $item, [
												'class'      => 'posts-filter__terms-sub-item',
												'show_count' => $item['show_count']
											] );

											if ( ! empty( $term->terms ) ) {
												foreach ( $term->terms as $sub_term ):
													echo $filterInstance->filter_item_term_field( $sub_term, $item, [
														'class'      => 'posts-filter__terms-sub-item',
														'show_count' => $item['show_count']
													] );
												endforeach;
											} ?>
                                        </ul>
                                    </div>
                                </li>
								<?php
							} else {
								echo $filterInstance->filter_item_term_field( $term, $item, $args );
							}
						} ?>
                    </ul>
					<?php
					break;
			}
			?>
        </div>
    </div>
</div>