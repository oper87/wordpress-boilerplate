<?php
/**
 * Head template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link rel="preconnect" href="https://use.typekit.net/"/>
	<link rel="preconnect" href="https://fonts.googleapis.com/"/>
	<link rel="preconnect" href="https://fonts.gstatic.com/"/>

	<script type="text/javascript">
        (function () {
            document.documentElement.classList.add('js');
            document.documentElement.classList.remove('no-js');

            if( document.cookie.indexOf('disableTopBar') === -1 ){
                document.documentElement.classList.add('top-bar-active');
            }
        })();
	</script>

    <!--<script id="CookieConsent" src="https://policy.app.cookieinformation.com/uc.js" data-culture="EN" type="text/javascript"></script>-->

	<style>
        @import url('https://use.typekit.net/vhq7shq.css');
        @import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap');
	</style>

	<script type="text/javascript">
        WebFontConfig = {
            //typekit: {id: 'sku8uhg'},
            //google: {families: ['Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap']},
            custom: {
                families: [
                    'Font Awesome 5 Free', 'Font Awesome 5 Brands'
                ],
                urls: [
                    'https://use.fontawesome.com/releases/v5.8.0/css/all.css',
                ]
            }
        };

        (function (d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = 'https://cdn.jsdelivr.net/npm/webfontloader@1.6.28/webfontloader.min.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
	</script>
	<noscript>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css" media="all">
	</noscript>

	<?php wp_head(); ?>
</head>
