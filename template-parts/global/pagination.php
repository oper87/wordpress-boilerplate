<?php
/**
 * Pagination template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

global $wp_query;

$posts_query  = $args['posts_query'] ?? $wp_query;
$type         = ! empty( $args['type'] ) ? $args['type'] : 'standard';
$current_page = ! empty( $posts_query->query_vars['paged'] ) ? $posts_query->query_vars['paged'] : 1;
$loaded_posts = $posts_query->post_count * $current_page;
$found_posts  = $posts_query->found_posts;
$total_pages  = $posts_query->max_num_pages;

$load_more_title = ! empty( $args['load_more_title'] ) ? $args['load_more_title'] : esc_html__( 'Load more', 'ruby_studio' );

if ( $current_page === (int) $total_pages ) {
	$loaded_posts = $found_posts;
}

if ( $type == 'standard' ) {
	global $custom_pagenum;

	if ( $total_pages <= 1 ) {
		return;
	}

	$big = 999999999; // need an unlikely integer

	$custom_pagenum = true;

	if ( $custom_pagenum && filter_input( INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT ) ) {
		$current = max( 1, filter_input( INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT ) );
	} else {
		$current = max( 1, get_query_var( 'paged' ) );
	}

	$paginate_links = paginate_links( [
		'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'  => '?paged=%#%',
		'current' => $current,
		'total'   => $posts_query->max_num_pages,
	] );

	echo sprintf( '<div class="pagination">%s</div>', $paginate_links );

} elseif ( $type == 'load-more' ) { ?>

	<div class="load-more-pagination align-center" data-anim="fade-slide-up">
		<div class="load-more-progress">
			<p><?php 
			/* translators: %1$s loaded posts %2$s found posts */
			echo sprintf( __( 'You have seen %1$s of %2$s', 'ruby_studio_tmp' ), $loaded_posts, $found_posts ) ?></p>
			<div class="progress-bar" style="--loaded-percent: <?php echo ( $loaded_posts / $found_posts ) * 100 ?>%">
				<div class="loader-bar"></div>
			</div>
		</div>

		<?php if ( $current_page < $total_pages && $next_page_link = get_next_posts_page_link() ) : ?>
			<a class="button load-more-button" data-ajax="false" href="<?php echo esc_url( $next_page_link ) ?>">
                <span class="load-more-button__text"><?php _e( 'Load more', 'ruby_studio_tmp' ) ?></span>
				<?php \App\Helper::get_svg( 'loader' ); ?>
			</a>
		<?php endif; ?>
	</div>

	<?php
} elseif ( $type == 'load-more-infinity' ) {
	$next_page_link = get_next_posts_page_link();
	$next_page_link = remove_query_arg( 'pb_section_number', $next_page_link ) ?>

	<div class="load-more-pagination infinity">
		<?php if ( $current_page < $total_pages && $next_page_link ) : ?>
			<a class="button-icon load-more-button"
			   href="<?php echo esc_url( $next_page_link ) ?>"
			   data-ajax-url="<?php echo esc_url( $next_page_link ) ?>">
				<?php echo $load_more_title; ?>
				<span class="ajax-loader"></span>
			</a>
			<input type="hidden" value="<?php echo esc_url( $next_page_link ) ?>"/>
		<?php endif; ?>
	</div>
	<?php
}
