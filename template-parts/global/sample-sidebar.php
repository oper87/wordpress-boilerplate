<?php
/**
 * Search Sidebar template
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

?>
<div class="sidebar-modal right" id="search-sidebar">
    <div class="sidebar-modal-dialog">
        <div class="sidebar-modal-content">
            <div class="sidebar-header">
                <h3><?php _e( 'Search', 'ruby_studio_tmp' ) ?></h3>
                <button class="btn-close black" data-dismiss="sidebar-modal"
                        aria-label="<?php _e( 'Close', 'ruby_studio_tmp' ) ?>">
					<?php get_template_part( 'assets/images/close.svg' ); ?>
                </button>
                <div class="filter-search">
                    <form action="<?php echo esc_url( site_url() ) ?>" method="get">
                        <label>
                            <span class="screen-reader-text"><?php _e( 'Search for article…', 'ruby_studio_tmp' ) ?></span>
                            <button type="button" class="btn-icon btn-search">
								<?php get_template_part( 'assets/images/search.svg' ) ?>
                            </button>
                            <input id="site-search-content" class="input-search" type="text" name="s" value=""
                                   placeholder="<?php _e( 'What are you searching for?', 'ruby_studio_tmp' ) ?>">
                        </label>
                        <span class="show-more-results"><?php _e( 'Press enter for full results', 'ruby_studio_tmp' ) ?></span>
                    </form>
                </div>
            </div>
            <div class="sidebar-body">
                <div class="search-body">
                </div>
            </div>
        </div>
    </div>
</div>
