<?php
/**
 * Head top bar template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

if ( ! get_field( 'top_bar_enabled', 'options' ) ) {
	return;
}


if ( ! get_field( 'top_bar_text', 'options' ) ) {
	return;
}

?>
<div class="header-top-bar">
	<div class="header-top-bar__text"><?php the_field( 'top_bar_text', 'options' ); ?></div>
	<button class="btn-close" aria-label="Close Top Bar">
		<?php get_template_part( 'assets/images/close.svg' ); ?>
	</button>
</div>