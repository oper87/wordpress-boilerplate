<?php
/**
 * Newsletter Form popup
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

?>
<div id="newsletter-form-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?php _e( 'Popup title', 'ruby_studio_tmp' ) ?></h3>
                <div class="modal-text"><?php _e( 'Popup description', 'ruby_studio_tmp' ) ?></div>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">
					<?php get_template_part( 'assets/images/close.svg' ); ?>
                </button>
            </div>
            <div class="modal-body">
                Popup content
            </div>
        </div>
    </div>
</div>
