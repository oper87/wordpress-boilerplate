<?php
/**
 * Filter form template
 *
 */

use App\Helper;
use App\Posts\Filter;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}
global $wp_query;

$current_term    = get_queried_object();
$filterInstance  = Filter::getInstance();
$filter_items    = $filterInstance->get_filters();
$form_action     = $filterInstance->get_form_action();
$form_view_items = $filterInstance->get_filter_sidebar_view();
$auto_submit     = 1;
$is_sidebar      = true;

if ( is_search() ) {
	$form_view_items = $filterInstance->get_filter_view();
	$auto_submit     = 0;
	$is_sidebar      = false;
}

?>
<form name="posts-filter-form" class="filter-form" data-auto_submit="<?php echo esc_attr( $auto_submit ) ?>" action="<?php echo $form_action ?>"
      method="get">

    <div class="filter-body__wrap posts-filter__items">

		<?php


		foreach ( $form_view_items as $form_view_key => $form_items ) {


			switch ( $form_view_key ) {
				case 'tabs':
					if ( count( $form_items ) == 1 ) {
						foreach ( $form_items as $form_item => $arr ) {
							get_template_part( 'template-parts/filter/filter', 'item', [
								'key'   => $form_item,
								'item'  => $filter_items[ $form_item ] ?? false,
								'title' => false
							] );
						}
					} else {
						foreach ( array_keys( $form_items ) as $v ) {
							$active_tab = isset( $_GET[ $v ] ) ? $v : current( array_keys( $form_items ) );
						}
						?>
                        <div class="posts-filter__tabs">
                            <ul class="posts-filter__tabs-list">
                                <li class="<?php echo $active_tab == 'search' ? 'active' : '' ?>">
                                    <a href="#search_by_search"><?php _e( 'Recipes', 'ruby_studio' ) ?></a>
                                </li>
                                <li class="<?php echo $active_tab == 'ingredients' ? 'active' : '' ?>">
                                    <a href="#search_by_ingredients"><?php _e( 'Ingredients', 'ruby_studio' ) ?></a>
                                </li>
                            </ul>
							<?php
							foreach ( $form_items as $form_item => $arr ) { ?>
                                <div id="<?php echo 'search_by_' . $form_item ?>"
                                     class="posts-filter__tab<?php echo $active_tab == $form_item ? ' active' : '' ?>">
									<?php
									get_template_part( 'template-parts/filter/filter', 'item', [
										'key'   => $form_item,
										'item'  => $filter_items[ $form_item ] ?? false,
										'title' => false
									] ); ?>
                                </div>
								<?php
							} ?>
                        </div>
						<?php
					}

					break;
				case 'taxonomies': ?>

                    <div class="posts-filter__items-wrap">
                        <div class="posts-filter__items-inner">
							<?php
							echo '<div class="sizer"></div>';

							foreach ( $form_items as $form_item => $arr ) {
								get_template_part( 'template-parts/filter/filter', 'item', [
									'key'   => $form_item,
									'item'  => $filter_items[ $form_item ] ?? false,
									'title' => true
								] );
							}
							?>
                        </div>
                    </div>

					<?php
					break;
			}

		} ?>

    </div>

	<?php if ( is_search() ) { ?>
        <div class="filter-body__buttons-wrap">
            <button type="button"
                    class="button button--color_beige btn-reset-filter" <?php disabled( false, $filterInstance->filter_has_any_selected_term ) ?>>
				<?php _e( 'Clear filter', 'ruby_studio' ) ?>
            </button>
            <button type="submit"
                    class="button button--color_green btn-apply-filter" <?php disabled( false, $filterInstance->filter_has_any_selected_term ) ?>>
				<?php _e( 'Search recipes', 'ruby_studio' ) ?>
            </button>
        </div>
	<?php } ?>

</form>