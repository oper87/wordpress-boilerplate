<?php
/**
 * Filter selected terms template
 *
 */

use App\Helper;
use App\Posts\Filter;

global $wp_query;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

$filterInstance = Filter::getInstance();

?>
<div class="filter-selected-terms">
    <div class="selected-terms-list">
		<?php foreach ( $filterInstance->get_filters() as $key => $item ) {
			$terms = [];

			if ( $item['type'] == 'search' && ! empty( $item['selected_array'] ) ) {

				if ( is_array( $item['selected_array'] ) ) {
					foreach ( $item['selected_array'] as $value ) { ?>
                        <button class="button button--size_small" type="button"
                                data-action="unselect-term"
                                data-key="<?php echo esc_attr( $key ) ?>"
                                data-value="<?php echo esc_attr( $value ) ?>">
							<?php echo esc_html( $value ) ?>
							<?php Helper::get_svg( 'close-thin' ); ?>
                        </button>
						<?php
					}
				}


			} else if ( $item['choose_type'] == 'slider' && ! empty( $item['selected_value'] ) ) {
				list( $min, $max ) = explode( '-', $item['selected_value'] ); ?>
                <button class="button button--size_small" type="button"
                        data-action="unselect-term"
                        data-value="<?php echo esc_attr( $item['selected_value'] ) ?>">
					<?php echo $item['name'] . ' ' . $min . ' - ' . $max . ' ' . get_woocommerce_currency_symbol(); ?>
					<?php Helper::get_svg( 'close-thin' ); ?>
                </button>
				<?php
			} elseif ( $item['type'] == 'taxonomy' && ! empty( $item['selected_array'] ) ) {
				foreach ( $item['terms'] as $term ) {
					$terms[ $term->slug ] = $term->name;

					if ( ! empty( $term->terms ) && isset( $item['hierarchical'] ) && $item['hierarchical'] ) {
						foreach ( $term->terms as $sub_term ) {
							$terms[ $sub_term->slug ] = $sub_term->name;
						}
					}
				}

				foreach ( $item['selected_array'] as $selected ) :
					if ( ! empty( $terms[ $selected ] ) ) : ?>
                        <button class="button button--size_small" type="button"
                                data-action="unselect-term"
                                data-key="<?php echo esc_attr( $key ) ?>"
                                data-value="<?php echo esc_attr( $selected ) ?>">
							<?php echo esc_html( $terms[ $selected ] ) ?>
							<?php Helper::get_svg( 'close-thin' ); ?>
                        </button>
					<?php
					endif;
				endforeach;
			}
		} ?>
    </div>
</div>