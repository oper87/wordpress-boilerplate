<?php
/**
 * Posts filter sidebar template
 *
 */

use App\Helper;
use App\Posts\Tax_Category;
use App\Posts\Post;
use App\Posts\Filter;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

global $wp_query;

$filterInstance = Filter::getInstance();
$found_posts    = $wp_query->found_posts;

?>
<div class="sidebar-modal right" id="posts-filter-sidebar">
    <div class="sidebar-modal-dialog">
        <div class="sidebar-modal-content">
            <div class="sidebar-header">
                <h3><?php _e( 'Filter', 'ruby_studio' ) ?></h3>
                <button class="button-icon" data-dismiss="sidebar-modal"
                        aria-label="<?php _e( 'Close', 'ruby_studio' ) ?>">
					<?php Helper::get_svg( 'close-thin' ); ?>
                </button>
            </div>
            <div class="sidebar-body">
	            <?php get_template_part( 'template-parts/posts/filter', 'form' ); ?>
            </div>
            <div class="sidebar-footer shop-filter-footer">
	            <?php /* translators: %s: Number of found posts. */ ?>
                <p class="total-posts"><?php echo sprintf( _n( '%s recipe', '%s recipes', $found_posts, 'ruby_studio' ), number_format_i18n( $found_posts ) ) ?></p>
                <div class="buttons-wrap">
                    <button type="button"
                            class="button button--color_beige btn-reset-filter" <?php disabled( false, $filterInstance->filter_has_any_selected_term ) ?>>
				        <?php _e( 'Clear filter', 'ruby_studio' ) ?>
                    </button>
                    <button type="button" class="button button--color_green btn-apply-filter"
                            data-dismiss="sidebar-modal">
				        <?php _e( 'Search recipes', 'ruby_studio' ) ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>