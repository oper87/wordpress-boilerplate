<?php
/**
 * Post loop template
 *
 */

use App\Helper;
use App\Posts\Tax_Category;
use App\Posts\Post;
use App\Posts\Recipe_Schema;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

global $post;

$categories = Tax_Category::getInstance()->get_the_terms( $post->ID, [
	'orderby' => 'parent'
] );
$category   = is_array( $categories ) ? end( $categories ) : false;

?>
<article <?php post_class( 'xsmall-6 large-3 columns' ); ?>>

	<?php if ( Post::is_type( 'recipe', $post->ID ) ) {
		$schema = new Recipe_Schema( 'recipe' );
		echo $schema->generate( $post->ID );
	} ?>

    <div class="loop-post-container">

		<?php if ( Post::is_type( 'recipe', $post->ID ) && App\WooCommerce\Subscriber::getInstance()->is_user_subscribed() ) { ?>
            <div class="loop-post-buttons post-buttons-wrap" data-post_id="<?php the_ID(); ?>">
                <button class="button-icon post__button post__planner-button" data-action="add_to_planner"
                        data-post_id="<?php the_ID(); ?>"><?php Helper::get_svg( 'chef' ) ?></button>
                <button class="button-icon post__button post__board-button"
                        data-action="add_to_board"
                        data-post_id="<?php the_ID(); ?>"><?php Helper::get_svg( 'heart' ) ?></button>
            </div>
		<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
            <div class="post__thumbnail">
                <a class="post__image" href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'post_thumb' ) ?>
                </a>

				<?php if ( $category ) { ?>
                    <a class="post__category" href="<?php echo esc_url( get_category_link( $category ) ) ?>"><?php echo $category->name ?></a>
				<?php } ?>
            </div>
		<?php } ?>

        <a href="<?php the_permalink(); ?>">
            <div class="post__loop-title"><?php the_title() ?></div>
        </a>

    </div>

</article>