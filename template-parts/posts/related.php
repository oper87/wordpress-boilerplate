<?php
/**
 * Related post loop template
 *
 */

use App\Helper;
use App\Posts\Post;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

global $post;

$related_posts = Post::getInstance()->get_related( [ 'post_id' => $post->ID, 'per_page' => 4 ] );

?>
<section class="related-posts content-area">
    <div class="section-head align-center">
        <h2 class="section-title"><?php _e( 'Delicious recipes from the same category', 'ruby_studio' ) ?></h2>
    </div>

    <div class="content-area-inner posts-loop row">
		<?php foreach ( $related_posts as $post_id ):
			$post_object = get_post( $post_id );

			setup_postdata( $GLOBALS['post'] =& $post_object );

			get_template_part( 'template-parts/posts/loop' );
		endforeach;
		wp_reset_postdata(); ?>
    </div>

</section>
