<?php
/**
 * Share buttons template
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

global $post;

?>

<div class="entry-share">
    <p><?php _e( 'Share this article', 'ruby_studio_tmp' ) ?></p>
    <div class="share-buttons"
         data-title="<?php echo esc_attr( get_the_title() ) ?>"
         data-permalink="<?php echo esc_attr( get_permalink() ) ?>"
         data-thumbnail="<?php echo esc_attr( get_the_post_thumbnail_url() ) ?>">
        <a href="#" class="btn-share" data-type="twitter">
			<?php get_template_part( 'assets/images/twitter.svg' ) ?>
        </a>
        <a href="#" class="btn-share" data-type="linkedin">
			<?php get_template_part( 'assets/images/linkedin.svg' ) ?>
        </a>
        <a href="#" class="btn-share" data-type="facebook">
			<?php get_template_part( 'assets/images/facebook.svg' ) ?>
        </a>
        <a href="#" class="btn-share" data-type="email">
			<?php get_template_part( 'assets/images/email.svg' ) ?>
        </a>
    </div>
</div>
