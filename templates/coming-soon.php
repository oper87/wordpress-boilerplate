<?php
/**
 * Template Name: Coming Soon
 *
 * @package WordPress
 * @subpackage Ruby Studio
 * @since Ruby Studio 3.0
 */

get_template_part( 'template-parts/coming-soon/header' );

?>
    <main id="main" class="site-main coming-soon-main">
			<?php while ( have_posts() ) : the_post(); ?>

                <article <?php post_class( 'container' ); ?>>

                    <div class="entry-header">
                        <h1 class="entry-title">
							<?php the_title(); ?>
                        </h1>
                    </div>

                    <div class="entry-content">
						<?php the_content(); ?>
                    </div>

                </article>

			<?php endwhile; ?>
    </main>
<?php

get_template_part( 'template-parts/coming-soon/footer' );