<?php
/**
 * The front page template file
 *
 * Template Name: Front Page
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage Ruby Studio
 * @since Ruby Studio 3.0
 */

get_header();

?>
    <main id="main" class="site-main front-main">
		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post(); ?>

			<div class="container">
				<?php the_content(); ?>
            </div>

		<?php endwhile; ?>

    </main>

<?php

get_footer();