<?php
/**
 * The WooCommerce template file
 *
 * Template Name: WooCommerce
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage Ruby Studio
 * @since Ruby Studio 3.0
 */

get_header();

global $wp;

$class = 'container';

if ( isset( $wp->query_vars['pin-board'] ) || isset( $wp->query_vars['meal-plans'] ) || is_account_page() ) {
	$class = 'container--fullwidth';
}

?>

	<main id="main" class="site-main woocommerce-main">
		<?php while ( have_posts() ) : the_post(); ?>

			<div <?php post_class( $class ); ?>>
				<?php the_content(); ?>
			</div>

		<?php endwhile; ?>

	</main>

<?php
get_footer();