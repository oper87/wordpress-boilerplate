<?php
/**
 * Cart Panel template
 */

use App\Helper;

defined( 'ABSPATH' ) || exit; ?>

<div class="sidebar-modal right" id="cart-panel-modal">
    <div class="sidebar-modal-dialog">
        <div class="sidebar-modal-content cart-panel-content">
            <div class="sidebar-header cart-panel-header">
                <h3><?php _e( 'Cart', 'ruby_studio' ) ?></h3>
                <button class="button-icon" data-dismiss="sidebar-modal"
                        aria-label="<?php _e( 'Close', 'ruby_studio' ) ?>">
		            <?php Helper::get_svg( 'close-thin' ); ?>
                </button>
            </div>

            <div class="sidebar-body cart-panel-body">
				<?php woocommerce_mini_cart(); ?>
            </div>
        </div>
    </div>
</div>