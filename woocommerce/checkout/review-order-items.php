<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

use App\Helper;
use App\WooCommerce\Tax_Size;

defined( 'ABSPATH' ) || exit;

?>
<table class="shop_table woocommerce-checkout-review-order-items-table">
    <tbody>
	<?php
	do_action( 'woocommerce_review_order_before_cart_contents' );

	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			$product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );

            ?>
            <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                <td class="product-thumbnail">
					<?php echo apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key ); ?>
                </td>

                <td class="product-content">

                    <div class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
						<?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) ) . '&nbsp;'; ?>
                    </div>

					<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>

                    <div class="product-total">
						<?php echo apply_filters( 'woocommerce_cart_item_quantity', '<div class="cart-item-quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</div>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>

						<?php echo apply_filters( 'woocommerce_cart_item_subtotal', '<div class="cart-item-subtotal">' . WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ) . '</div>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
                    </div>

					<?php
					echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
						'<a href="%s" class="product-remove remove" title="%s" data-product_id="%s" data-product_sku="%s">%s</a>',
						esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
						__( 'Remove this item', 'woocommerce' ),
						esc_attr( $_product->get_id() ),
						esc_attr( $_product->get_sku() ),
						Helper::get_svg( 'close', '', false )
					), $cart_item_key );
					?>

					<?php do_action( 'woocommerce_review_order_cart_item_entry', $cart_item, $cart_item_key ); ?>

                </td>
            </tr>
			<?php
		}
	}

	do_action( 'woocommerce_review_order_after_cart_contents' );
	?>
    </tbody>
    <tfoot>

	<?php if ( apply_filters( 'woocommerce_cart_needs_show_subtotal', true ) ) : ?>

        <tr class="shop-table-row cart-subtotal">
            <th><?php esc_html_e( 'Product total', 'ruby_studio' ); ?></th>
            <td><?php wc_cart_totals_subtotal_html(); ?></td>
        </tr>

	<?php endif; ?>

    </tfoot>
</table>
