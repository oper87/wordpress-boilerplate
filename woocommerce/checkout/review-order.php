<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

defined( 'ABSPATH' ) || exit;

?>
<table class="shop_table woocommerce-checkout-review-order-table">
	<tfoot>

	<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

		<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

		<?php wc_cart_totals_shipping_html(); ?>

		<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_review_order_before_coupon_total' ); ?>

	<?php if ( wc_coupons_enabled() ) : ?>

		<tr class="cart-coupon-form">
			<td colspan="2">
				<div class="coupon">
					<label for="coupon_code" class="screen-reader-text"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label>
					<input type="text" name="coupon_code" class="input-text" id="coupon_code" value=""
					       placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>"/>
					<button type="button" class="button button--color_black" id="apply-coupon-btn" name="apply_coupon"
					        value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
					<?php do_action( 'woocommerce_cart_coupon' ); ?>
				</div>
				<div class="coupon-error"></div>
			</td>
		</tr>

		<?php if ( count( WC()->cart->get_coupons() ) > 0 ) : ?>
			<tr class="woocommerce-cart-discounts">
				<td colspan="2">
					<h3><?php esc_html_e( 'Coupons', 'ruby_studio' ); ?></h3>
					<ul class="cart-discount-coupons">
						<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
							<li class="shop-table-row cart-discount-coupon coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
								<div class="coupon-item">
									<span class="coupon-item__code"><?php echo $code ?></span>
									<span class="coupon-item__discount"><?php wc_cart_totals_coupon_html( $coupon ); ?></span>
								</div>
								<?php do_action( 'woocommerce_review_order_discount_coupon_item', $code, $coupon ); ?>
							</li>
						<?php endforeach; ?>
					</ul>
				</td>
			</tr>
		<?php endif; ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_review_order_after_coupon_total' ); ?>

	<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
		<tr class="shop-table-row fee">
			<th><?php echo esc_html( $fee->name ); ?></th>
			<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
		</tr>
	<?php endforeach; ?>

	<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
		<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
			<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
				<tr class="shop-table-row tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
					<th><?php echo esc_html( $tax->label ); ?></th>
					<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
				</tr>
			<?php endforeach; ?>
		<?php else : ?>
			<tr class="shop-table-row tax-total">
				<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
				<td><?php wc_cart_totals_taxes_total_html(); ?></td>
			</tr>
		<?php endif; ?>
	<?php endif; ?>

	<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

	<tr class="shop-table-row order-total">
		<th><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
		<td><?php wc_cart_totals_order_total_html(); ?></td>
	</tr>

	<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

	</tfoot>
</table>
