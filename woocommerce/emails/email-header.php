<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 7.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>"/>
    <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
    <meta name="color-scheme" content="light dark">
    <meta name="supported-color-schemes" content="light dark">
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div id="wrapper" class="darkmode" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
            <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_container">
                    <tr>
                        <td align="center" valign="top">
                            <!-- Header -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header">
                                <td id="template_header_image">
		                            <?php
		                            if ( $img = get_option( 'woocommerce_email_header_image' ) ) {
			                            echo '<p class="light-img" style="margin-top:0;margin-bottom:0;"><img src="' . esc_url( get_parent_theme_file_uri( $img ) ) . '" alt="' . get_bloginfo( 'name', 'display' ) . '" /></p>';

			                            if ( $img_dark = get_option( 'woocommerce_email_header_image_dark' ) ) { ?>

                                            <!--[if !mso]><! --><p class="dark-img" style="margin-top:0; margin-bottom:0; overflow:hidden; float:left; width:0; max-height:0; max-width:0; line-height:0; visibility:hidden;">
                                                <img src="<?php echo esc_url( get_parent_theme_file_uri( $img_dark ) ) ?>" alt="<?php echo get_bloginfo( 'name', 'display' ) ?>">
                                            </p><!--<![endif]-->
			                            <?php }
		                            } ?>
                                </td>
                                <!--<tr>
                                    <td id="header_wrapper">
                                        <h1><?php /*echo esc_html( $email_heading ); */?></h1>
                                    </td>
                                </tr>-->
                            </table>
                            <!-- End Header -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- Body -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_body">
                                <tr>
                                    <td valign="top" id="body_content">
                                        <!-- Content -->
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top">
                                                    <div id="body_content_inner">
