<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$text_align   = is_rtl() ? 'right' : 'left';
$text_align_r = is_rtl() ? 'left' : 'right';

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

<h2><?php esc_html_e( 'Order Details', 'woocommerce' ); ?></h2>

<table cellspacing="0" cellpadding="6" style="width: 100%;" border="1" class="order-details">
    <tbody>
        <?php
        echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            $order,
            array(
                'show_sku'      => $sent_to_admin,
                'show_image'    => true,
                'image_size'    => array( 96, 96 ),
                'plain_text'    => $plain_text,
                'sent_to_admin' => $sent_to_admin,
            )
        );
        ?>
    </tbody>
    <tfoot>
        <?php
        $item_totals = $order->get_order_item_totals();

        if ( $item_totals ) {
            $i = 0;
            foreach ( $item_totals as $key => $total ) {
                $i++;
                ?>
                <tr class="row_<?php echo esc_attr( $key ) ?>">
                    <th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( $total['label'] ); ?></th>
                    <td class="td" style="text-align:<?php echo esc_attr( $text_align_r ); ?>;"><?php echo wp_kses_post( $total['value'] ); ?></td>
                </tr>
                <?php
            }
        }
        if ( $order->get_customer_note() ) {
            ?>
            <tr>
                <th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
                <td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
            </tr>
            <?php
        }
        ?>
    </tfoot>
</table>

<table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td style="padding: 0; margin: 0; border: none;" height="30">&nbsp;</td>
    </tr>
</table>

<table class="order-info" cellspacing="0" cellpadding="6" style="width: 100%;" border="1">
    <tbody>
    <tr>
        <td>
            <?php esc_html_e( 'Order Number', 'ruby_studio' ); ?>:<br>
            <?php
            if ( $sent_to_admin ) {
                $before = '<a class="link" href="' . esc_url( $order->get_edit_order_url() ) . '">';
                $after  = '</a>';
            } else {
                $before = '';
                $after  = '';
            }
            /* translators: %s: Order ID. */
            echo '<strong>' . wp_kses_post( $before . '#' . $order->get_order_number() . $after ) . '</strong>';
            ?>
        </td>
        <td>
            <?php esc_html_e( 'Order Date', 'ruby_studio' ); ?>:<br>
            <strong>
                <time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>">
                    <?php echo esc_html( wc_format_datetime( $order->get_date_created(), 'd.m.Y' ) ); ?>
                </time>
            </strong>
        </td>
        <td>
            <?php esc_html_e( 'Payment Method', 'ruby_studio' ); ?>:<br>
            <strong><?php echo $order->get_payment_method_title(); ?></strong>
        </td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td style="padding: 0; margin: 0; border: none;" height="40">&nbsp;</td>
    </tr>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
