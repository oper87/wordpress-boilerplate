<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://woo.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 8.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg        = get_option( 'woocommerce_email_background_color' );
$body      = get_option( 'woocommerce_email_body_background_color' );
$base      = get_option( 'woocommerce_email_base_color' );
$base_text = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text      = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
$link_color = wc_hex_is_light( $base ) ? $base : $base_text;

if ( wc_hex_is_light( $body ) ) {
	$link_color = wc_hex_is_light( $base ) ? $base_text : $base;
}

$bg_darker_10    = '#E8E4D8'; //wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );
$text_lighter_40 = wc_hex_lighter( $text, 40 );
$table_border    = wc_light_or_dark( $text, '#202020', '#ffffff' );

$font_family = '"Author", Arial, sans-serif';
$font_family_serif = '"Gambarino", "Times New Roman", sans-serif';

// Dark Mode
$dark_bg   = get_option( 'woocommerce_email_background_color_dark' );
$dark_body = get_option( 'woocommerce_email_body_background_color_dark' );
$dark_base = get_option( 'woocommerce_email_base_color_dark' );
$dark_text = get_option( 'woocommerce_email_text_color_dark' );

$dark_body_darker_1 = wc_hex_darker( $dark_base, 1 );
$dark_table_border  = wc_light_or_dark( $dark_text, '#202020', '#ffffff' );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
// body{padding: 0;} ensures proper scale/positioning of the email in the iOS native email app.
?>

@font-face {
    font-family: 'Author';
    src: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/Author-Regular.woff2') format('woff2');
    font-weight: 400;
    font-display: swap;
    font-style: normal;
}

@font-face {
    font-family: 'Author';
    src: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/Author-Medium.woff2') format('woff2');
    font-weight: 500;
    font-display: swap;
    font-style: normal;
}

@font-face {
    font-family: 'Gambarino';
    src: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/Gambarino-Regular.woff2') format('woff2');
    font-weight: 400;
    font-display: swap;
    font-style: normal;
}

body {
	padding: 0;
}

strong {
    font-weight: 500;
}

#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	margin: 0;
	padding: 40px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
    font-family: <?php echo $font_family; ?>;
}

#template_container {
    background-color: <?php echo esc_attr( $body ); ?>;
    border-radius: 0 !important;
    max-width: 600px;
}

#template_header {
	background-color: <?php echo esc_attr( $base ); ?>;
	border-radius: 3px 3px 0 0 !important;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: <?php echo $font_family; ?>;
}

#template_header h1,
#template_header h1 a {
	color: <?php echo esc_attr( $base_text ); ?>;
	background-color: inherit;
}

#template_header_image  {
    padding: 30px 48px;
    display: block;
    text-align: center;
}

#template_header_image img {
	width: 250px;
	margin-left: 0;
	margin-right: 0;
}

#header_wrapper {
    padding: 24px 32px;
    display: block;
}

#template_footer td {
	padding: 0;
	border-radius: 6px;
}

#template_footer #credit {
	border: 0;
	color: <?php echo esc_attr( $text_lighter_40 ); ?>;
	font-family: <?php echo $font_family; ?>;
	font-size: 12px;
	line-height: 130%;
	text-align: center;
	padding: 24px 0;
}

#template_footer #credit p {
	margin: 0 0 24px;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content table td {
	padding: 0;
}

#body_content table #body_content_inner,
#body_content table #body_content_secondary_inner {
	padding: 32px;
}

#body_content table #body_content_secondary_inner {
    border-top: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;
}

#body_content table td td {
	padding: 8px 10px;
    vertical-align: top;
}

#body_content table td th {
	padding: 6px 10px;
    font-weight: normal;
}

#body_content table tfoot th,
#body_content table tfoot td {
    padding: 6px 10px;
}

#body_content table tfoot .order_total-tax-row th,
#body_content table tfoot .order_total-tax-row td {
    padding: 0;
}

#body_content table tfoot .payment-method-row th {
    padding: 18px 0 0 0;
}

#body_content td ul.wc-item-meta {
	font-size: 16px;
	margin: 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.3em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 24px;
}

#body_content_inner,
#body_content_secondary_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-family: <?php echo $font_family; ?>;
	font-size: 18px;
	line-height: 130%;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    font-family: <?php echo $font_family; ?> !important;
    font-size: 18px;
	vertical-align: top;
    border-color: #E8E4D8;
    border-collapse: collapse;
    border-style: solid;
}

.address {
	font-family: <?php echo $font_family; ?>;
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    font-style: normal;
    font-size: 18px;
    line-height: 130%;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: <?php echo $font_family; ?>;
}

.link {
	color: <?php echo esc_attr( $link_color ); ?>;
}

.button {
    cursor: pointer;
    display: inline-block;
    text-transform: uppercase;
    font-size: 14px;
    color: <?php echo esc_attr( $body ); ?>;
    text-align: center;
    padding: 10px 24px;
    box-sizing: border-box;
    white-space: nowrap;
    line-height: 1.4;
    height: 35px;
    overflow: hidden;
    position: relative;
    border: none;
    background-color: <?php echo esc_attr( $text ); ?>;
    text-decoration: none;
}

h1 {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: <?php echo $font_family; ?>;
	font-size: 28px;
	font-weight: 300;
	line-height: 130%;
	margin: 0;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
	text-shadow: 0 1px 0 <?php echo esc_attr( $base_lighter_20 ); ?>;
}

h2 {
	color: <?php echo esc_attr( $text ); ?>;
	display: block;
	font-family: <?php echo $font_family_serif; ?>;
    font-size: 24px;
    font-weight: 300;
	line-height: 130%;
	margin: 0 0 24px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h3 {
	color: <?php echo esc_attr( $text ); ?>;
	display: block;
	font-family: <?php echo $font_family; ?>;
	font-size: 18px;
	font-weight: 300;
	line-height: 130%;
	margin: 16px 0 8px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
	color: <?php echo esc_attr( $link_color ); ?>;
	font-weight: normal;
	text-decoration: underline;
}

img {
	border: none;
	display: inline-block;
	font-weight: bold;
	height: auto;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
	vertical-align: middle;
	max-width: 100%;
    width: 100%;
	height: auto;
}

.order-info, .order-details {
    border: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;
    border-collapse: collapse;
}

.order-info td, .order-details td {
    border: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;
}

@media only screen and (max-width: 600px){
    #wrapper {
        padding: 0 !important;
    }

    #template_container {
        border: none !important;
        box-shadow: none !important;
    }

    #body_content_inner,
    #body_content_secondary_inner,
    .td,
    .address,
    #body_content p {
        font-size: 16px !important;
    }

    h1 {
        font-size: 30px !important;
    }

    h2 {
        font-size: 22px !important;
    }

    h3 {
        font-size: 20px !important;
    }

    .button,
    #template_footer #credit {
        font-size: 14px !important;
    }

    #template_header_image  {
        padding: 10px 24px !important;
    }

    #body_content table #body_content_inner,
    #body_content table #body_content_secondary_inner {
        padding: 32px 24px 32px !important;
    }

}

@media (prefers-color-scheme: dark ){

    /* Shows Dark Mode-Only Content, Like Images */
    .dark-img { display:block !important; width: auto !important; overflow: visible !important; float: none !important; max-height:inherit !important; max-width:inherit !important; line-height: auto !important; margin-top:0px !important; visibility:inherit !important; }

    /* Hides Light Mode-Only Content, Like Images */
    .light-img { display:none; display:none !important; }

    #wrapper.darkmode { background-color: <?php echo $dark_bg ?> !important; }

    .darkmode #template_container { background-color: <?php echo $dark_body ?> !important; border-color: <?php echo $dark_body ?> !important; }

    .darkmode #template_header { background-color: <?php echo $dark_body ?> !important; }
    .darkmode #body_content { background-color: <?php echo $dark_body ?> !important; }

    .darkmode #template_header_image { border-color: <?php echo $dark_bg ?> !important; }

    .darkmode #body_content_inner {color: <?php echo $dark_text ?>  !important; }
    .darkmode #body_content_secondary_inner  {color: <?php echo $dark_text ?>  !important; }
    .darkmode p {color: <?php echo $dark_text ?>  !important; }
    .darkmode h1 {color: <?php echo $dark_text ?>  !important; }
    .darkmode h2 {color: <?php echo $dark_text ?>  !important; }
    .darkmode h3 {color: <?php echo $dark_text ?>  !important; }
    .darkmode .text {color: <?php echo $dark_text ?>  !important; }
    .darkmode a {color: <?php echo $dark_text ?>  !important; }
    .darkmode #template_footer #credit { color: <?php echo $dark_text ?> !important; }
    .darkmode .address { color: <?php echo $dark_text ?> !important; }

    .darkmode td {color: <?php echo $dark_text ?> !important; }
    .darkmode th {color: <?php echo $dark_text ?> !important; }
    .darkmode #body_content table tfoot th { border-top-color: <?php echo esc_attr( $dark_text ); ?> !important;  }
    .darkmode #body_content table tfoot td { border-top-color: <?php echo esc_attr( $dark_text ); ?> !important;  }

    .darkmode a.button { color: #000000 !important; background: <?php echo $dark_body_darker_1 ?> !important; }
}


    /* Shows Dark Mode-Only Content, Like Images */
    [data-ogsc] .dark-img { display:block !important; width: auto !important; overflow: visible !important; float: none !important; max-height:inherit !important; max-width:inherit !important; line-height: auto !important; margin-top:0px !important; visibility:inherit !important; }

    /* Hides Light Mode-Only Content, Like Images */
    [data-ogsc] .light-img { display:none; display:none !important; }

    [data-ogsc] #wrapper.darkmode { background-color: <?php echo $dark_bg ?> !important; }

    [data-ogsc] .darkmode #template_container { background-color: <?php echo $dark_body ?> !important; border-color: <?php echo $dark_body ?> !important; }

    [data-ogsc] .darkmode #template_header { background-color: <?php echo $dark_body ?> !important; }
    [data-ogsc] .darkmode #body_content { background-color: <?php echo $dark_body ?> !important; }

    [data-ogsc] .darkmode #template_header_image { border-color: <?php echo $dark_bg ?> !important; }

    [data-ogsc] .darkmode #body_content_inner {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode #body_content_secondary_inner  {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode p {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode h1 {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode h2 {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode h3 {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode .text {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode a {color: <?php echo $dark_text ?>  !important; }
    [data-ogsc] .darkmode #template_footer #credit { color: <?php echo $dark_text ?> !important; }
    [data-ogsc] .darkmode .address { color: <?php echo $dark_text ?> !important; }

    [data-ogsc] .darkmode td {color: <?php echo $dark_text ?> !important; }
    [data-ogsc] .darkmode th {color: <?php echo $dark_text ?> !important; }
    [data-ogsc] .darkmode #body_content table tfoot th { border-top-color: <?php echo esc_attr( $dark_text ); ?> !important;  }
    [data-ogsc] .darkmode #body_content table tfoot td { border-top-color: <?php echo esc_attr( $dark_text ); ?> !important;  }

    [data-ogsc] .darkmode a.button { color: #000000 !important; background: <?php echo $dark_body_darker_1 ?> !important; }
<?php
